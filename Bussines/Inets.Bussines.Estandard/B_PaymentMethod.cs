﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;
using System.Data;
using Inets.Models.Entity_Entities;
using Inets.Data.BaseDate;

namespace Inets.Bussines.Estandard
{
	public class B_PaymentMethod
	{
		public DataTable visualizar()
		{
			Q_PaymentMethodSQL q_PaymentMethodSQL = new Q_PaymentMethodSQL();
			return q_PaymentMethodSQL.Visualize();
		}

		public E_PaymentMethod search(E_PaymentMethod e_PaymentMethod)
		{
			Q_PaymentMethodSQL q_PaymentMethodSQL = new Q_PaymentMethodSQL();

			return q_PaymentMethodSQL.Search(e_PaymentMethod);
		}

		public E_PaymentMethod searchid(E_PaymentMethod e_PaymentMethod)
		{
			Q_PaymentMethodSQL q_PaymentMethodSQL = new Q_PaymentMethodSQL();

			return q_PaymentMethodSQL.Searchid(e_PaymentMethod);
		}
	}
}
