﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;
using System.Data;
using Inets.Models.Entity_Entities;
using Inets.Data.BaseDate;

namespace Inets.Bussines.Estandard
{
	public class B_CashSales
	{
		
		//ROV 11/10/18 metodo para extraer los datos 
		public E_CashSales SearchCashSales(E_CashSales e_CashSales)
        {
            Q_CashSalesSQL q_CashSalesSQL = new Q_CashSalesSQL();
            return q_CashSalesSQL.SearchCash(e_CashSales);
        }

		public E_CashSales SearchID(E_CashSales e_CashSales)
		{
			Q_CashSalesSQL q_CashSalesSQL = new Q_CashSalesSQL();
			return q_CashSalesSQL.SearchCashID(e_CashSales);
		}

		//ROV 11/10/18 metodo para Visualize los datos en datagrid
		public DataTable VisualizateCash()
        {
            Q_CashSalesSQL q_CashSalesSQL = new Q_CashSalesSQL();
            return q_CashSalesSQL.VisualizateCash();
        }
    }
}
