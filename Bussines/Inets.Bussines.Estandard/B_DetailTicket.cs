﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;

using Inets.Data.BaseDate;
using Inets.Models.Entity_Entities;
using ExternalClass;

namespace Inets.Bussines.Estandard
{
	public class B_DetailTicket
	{
		//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		//ROV 11/09/18 GENERACION DE LISTAS DINAMICAS PARA EL PUNTO DE VENTA 
		List<LineaProduct> _lineaProducts = new List<LineaProduct>();
		List<SalesTicket> salesTickets = new List<SalesTicket>();
		
		int i;

		B_MPTicket b_MPTicket = new B_MPTicket();

		
		//ROV 11/09/18 Funcion para Visualize la tabla de detalle ticket pero con detalles 
		public DataTable Visualize(E_DetailTicket e_DetailTicket)
		{
			Q_TicketDetailSQL q_TicketDetailSQL = new Q_TicketDetailSQL();
			return q_TicketDetailSQL.Visualize(e_DetailTicket);
		}

		//ROV 11/09/18 funcion para relaizar los valores 
		public double resultado(double valor1, double valor2)
		{
			double result = valor1 - valor2;
			return result;
		}

		//ROV 11/09/18 Funcion para buscar el producto del detalle 
		public E_DetailTicket SearchDetail(E_DetailTicket e_DetailTicket)
		{
			Q_TicketDetailSQL q_TicketDetailSQL = new Q_TicketDetailSQL();

			return q_TicketDetailSQL.Search(e_DetailTicket);
		}

		//ROV 11/09/18 Fucnion para calcular el total del ticket 
		public decimal totalticket(E_DetailTicket e_DetailTicket)
		{
			Q_TicketDetailSQL q_TicketDetailSQL = new Q_TicketDetailSQL();
			return q_TicketDetailSQL.Sum(e_DetailTicket);
		}


		//ROV 11/10/18  TODO ESTO SE REALIZA CON LISTAR PARA QUE AL FINAL DE GENERAR LA VENTA SI INSERTE A LA BASE DE DATOS 
		//SE GENERAN DOS LISTAS UNA DONDE VAMOS A INSERTAR Y ELIMINAR LOS PRODUCTOS LA SEGUNDA DONDE SE ALMACENARA CUANTOS PRODUCTOS SE ELIMINARON Y AUMENTARON, 
		// LA PRIMERA LISTA LLEVARA EL CONTEO 1 X 1 Y EN LA SEGUNDA LISTA IRA INCREMENTANDO LOS DATOS 

		//ROV 13/09/18 LISTA EN GRAL  
		public void InsertlistTicket(LineaProduct lineaProduct)
		{
			_lineaProducts.Add(new LineaProduct
			{
				Numberticket = lineaProduct.Numberticket,
				Quantity = lineaProduct.Quantity,
				Deletes = lineaProduct.Deletes,
				IdProduct = lineaProduct.IdProduct,
				UPC = lineaProduct.UPC,
				Product = lineaProduct.Product,
				Price = lineaProduct.Price,
				TotalAmount = lineaProduct.TotalAmount,
				idPromotion = lineaProduct.idPromotion

			});
		}

		//ROV 11/10/18 Este metodo valida que el producto ya esta registrado en la lista
		public bool validate(string product)
		{
			if (_lineaProducts.Count > 0)
			{
				for (i = 0; i < _lineaProducts.Count; i++)
				{
					if (_lineaProducts[i].Product == product)
					{
						return true;
					}
				}
				return false;
			}
			return false;
		}

		//ROV 11/10/18 con este metodo hace el incremento en la lista precio cantidad y monto  
		public void Increment(string producto, decimal cantidad, decimal monto)
		{
			foreach (var item in _lineaProducts)
			{
				if (item.Product == producto)
				{
					item.Quantity = _lineaProducts[i].Quantity + cantidad;
					item.TotalAmount = _lineaProducts[i].TotalAmount + monto;
				}

			}
		}

		//ROV 11/10/18 ira en decremento los datos 
		public void Quit(string producto, decimal quantity, decimal price)
		{
			foreach (var item in _lineaProducts)
			{
				if (item.Product == producto)
				{
					item.Quantity = item.Quantity - quantity;
					item.TotalAmount = item.TotalAmount - price;
					item.Deletes = item.Deletes + quantity;
				}
			}
		}

		//ROV 11/10/18 para Visualize la lista generada 
		public List<LineaProduct> visualizalist()
		{
			return _lineaProducts.ToList();
		}

		//ROV 11/10/18 limpiara la lista una vez terminado 
		public void clearlist()
		{
			_lineaProducts.Clear();
		}
		
		//ROV 13/09/18 LISTA EN DETALLE 
		//ROV 11/10/18 insertara la lista como lo mensionado 1x1
		public void insertlistdetail(SalesTicket salesTicket)
		{
			salesTickets.Add(new SalesTicket
			{
				Product = salesTicket.Product,
				Quantity = salesTicket.Quantity,
				Price = salesTicket.Price,
				TotalAmount = salesTicket.TotalAmount,
				UPC = salesTicket.UPC,
				TipeSales = "vendido"
			});

		}

		//ROV 11/10/18 eliminara el product de la lista en la posicion indicada 
		public void DeletePosicion(int ini)
		{
			if (salesTickets.Count <= 0)
			{

			}
			else
			{
				salesTickets.RemoveAt(ini);
			}
		}

		//ROV 11/10/18 lo visualizara en un datagrid 
		public List<SalesTicket> visualizartick()
		{
			return salesTickets.ToList();
		}

		//ROV 11/10/18 calculara cantidades  
		public decimal calcular()
		{
			decimal total = 0;

			foreach (var item in salesTickets)
			{
				total = total + item.TotalAmount;
			}
			return total;
		}

		//ROV 11/10/18 limpiara al finalizar la venta 
		public void ClearList()
		{
			salesTickets.Clear();
		}

		//ROV 11/10/18 contara cuantos productos se vendieron 
		public decimal Quantity()
		{
			decimal cantidad = 0;
			foreach (var item in salesTickets)
			{
				cantidad = cantidad + item.Quantity;
			}
			return cantidad;
		}
		
	}
}
