﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;
using System.Data;
using Inets.Models.Entity_Entities;
using Inets.Data.BaseDate;

namespace Inets.Bussines.Estandard
{
	public class B_Denomination
	{
		//ROV 11/10/18 metodo para extraer los datos  y visualizarlos en un data o cmbbox
		public DataTable visualizate(E_Denomination e_Denomination)
		{
			Q_DenominationSQL q_DenominationSQL = new Q_DenominationSQL();
			return q_DenominationSQL.Visualize(e_Denomination);
		}

		//ROV 11/10/18 metodo para buscar por dato de empresa
		public E_Denomination Search(E_Denomination e_Denomination)
		{
			Q_DenominationSQL q_DenominationSQL = new Q_DenominationSQL();
			return q_DenominationSQL.Search(e_Denomination);
		}

		//ROV 11/10/18 metodo para extraer los datos con el id
		public E_Denomination SearchId(E_Denomination e_Denomination)
		{
			Q_DenominationSQL q_DenominationSQL = new Q_DenominationSQL();
			return q_DenominationSQL.Searchid(e_Denomination);
		}

	}
}
