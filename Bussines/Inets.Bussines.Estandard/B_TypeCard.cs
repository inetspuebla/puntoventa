﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;
using System.Data;
using Inets.Models.Entity_Entities;
using Inets.Data.BaseDate;


namespace Inets.Bussines.Estandard
{
	public class B_TypeCard
	{
		public DataTable Visualize()
		{
			Q_TypeCardSQL q_TypeCardSQL = new Q_TypeCardSQL();
			return q_TypeCardSQL.Visualize();
		}

		public E_TypeCard search(E_TypeCard e_TypeCard )
		{
			Q_TypeCardSQL q_TypeCardSQL = new Q_TypeCardSQL();
			return q_TypeCardSQL.Search(e_TypeCard);
		}
	}
}
