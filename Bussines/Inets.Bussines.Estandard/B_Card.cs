﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;
using System.Data;
using Inets.Models.Entity_Entities;
using Inets.Data.BaseDate;

namespace Inets.Bussines.Estandard
{
	public class B_Card
	{
		//ROV 11/10/18 metodo de Visualize datos de la base de datos
		public DataTable visualize()
		{
			//ROV 11/10/18 se crea objetos de la clase consulta 
			Q_CardSQL q_CardSQL = new Q_CardSQL();

			//ROV 11/10/18 se returna la consulta de select
			return q_CardSQL.visualize();
		}


		//ROV 11/10/18 Metodo para una busqueda en la base de datos
		public E_Card search(E_Card e_Card)
		{
			Q_CardSQL q_CardSQL = new Q_CardSQL();
			//ROV 11/10/18 Se retorna la entidad con la consutla generada 
			return e_Card = q_CardSQL.Search(e_Card);
		}
	}
}
