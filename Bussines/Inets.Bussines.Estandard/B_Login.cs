﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;
using Inets.Data.BaseDate;
using Inets.Models.Entity_Entities;

namespace Inets.Bussines.Estandard
{
    public class B_Login
    {
        public bool StarSesion(E_Users e_Users)
        {
            Q_LoginSQL q_LoginSQL = new Q_LoginSQL();

            if (q_LoginSQL.StartSesion(e_Users) == true)
            {
                return true;
            }
            else
                return false;
        }

		public E_Users SearshUser( E_Users e_Users)
		{
			Q_LoginSQL q_LoginSQL = new Q_LoginSQL();

			e_Users = q_LoginSQL.SearchUserlogin(e_Users);
			return e_Users;
		}

		public bool Autorization(E_Users e_Users)
		{
			Q_LoginSQL q_LoginSQL = new Q_LoginSQL();

			if (q_LoginSQL.Validation(e_Users) == true)
			{
				return true;
			}
			else
				return false;
		}

    }
}
