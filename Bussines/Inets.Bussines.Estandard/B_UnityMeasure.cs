﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using Inets.Data.BaseDate;
using Inets.Models.Entity_Entities;

namespace Inets.Bussines.Estandard
{
    public class B_UnityMeasure
    {
        public DataTable selectUnityMeasure()
        {
            Q_UnityMeasureSQL q_UnityMeasure = new Q_UnityMeasureSQL();

            return q_UnityMeasure.Select();
        }

        public E_UnityMeasure SearchProduct( E_UnityMeasure e_UnityMeasure)
        {
            Q_UnityMeasureSQL q_UnityMeasure = new Q_UnityMeasureSQL();
            e_UnityMeasure = q_UnityMeasure.Search(e_UnityMeasure);

            return e_UnityMeasure;
        }

    }
}
