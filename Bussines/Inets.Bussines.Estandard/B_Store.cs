﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Inets.Data.BaseDate;
using Inets.Models.Entity_Entities;

namespace Inets.Bussines.Estandard
{
    public class B_Store
    {
        E_Store e_Store = new E_Store();
        Q_StoreSQL q_Store = new Q_StoreSQL();

        public E_Store SearchStore(E_Store e_Store)
        {
            return q_Store.Search(e_Store);
        }

		public E_Store SearchStoreName(E_Store e_Store)
		{
			return q_Store.Search_Name(e_Store);
		}
	}
}
