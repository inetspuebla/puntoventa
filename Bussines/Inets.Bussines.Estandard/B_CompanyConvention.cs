﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;
using System.Data;
using Inets.Models.Entity_Entities;
using Inets.Data.BaseDate;

namespace Inets.Bussines.Estandard
{
	public class B_CompanyConvention
	{
		//ROV 11/10/18 metodo para extraer los datos de la consutla select
		public E_CompanyConvention Search(E_CompanyConvention e_CompanyConvention)
		{
			Q_CompanyConventionSQL q_CompanyConventionSQL = new Q_CompanyConventionSQL();
			return q_CompanyConventionSQL.Search(e_CompanyConvention);

		}

		//ROV 11/10/18 metodo para visualziar los datos en datagrid
		public DataTable visualizate()
		{
			Q_CompanyConventionSQL q_CompanyConventionSQL = new Q_CompanyConventionSQL();
			return q_CompanyConventionSQL.visualize();
		}
	}
}
