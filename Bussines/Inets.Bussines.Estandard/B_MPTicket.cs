﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;
using System.Data;
using Inets.Models.Entity_Entities;
using Inets.Data.BaseDate;

namespace Inets.Bussines.Estandard
{
	public class B_MPTicket
	{
		int i;

		//ROV 11/10/18 las formas de pago se trabajan mediante listas 
		List<E_MPTicket> _MPTickets = new List<E_MPTicket>();
		
		//ROV 11/10/18 Se insertan los datos a la lista
		public void insertlistMPTickets(E_MPTicket e_MPTicket)
		{
			_MPTickets.Add(new E_MPTicket
			{
				idMPTicket = e_MPTicket.idMPTicket,
				idTicket = e_MPTicket.idTicket,
				idMPayment = e_MPTicket.idMPayment,
				idDenomination = e_MPTicket.idDenomination,
				idCard = e_MPTicket.idCard,
				idTypeCard = e_MPTicket.idTypeCard,
				dQuantity = e_MPTicket.dQuantity,
				sBank = e_MPTicket.sBank,
				sFolio = e_MPTicket.sFolio,
				dDateEmition = e_MPTicket.dDateEmition,
				iAuthorizacion = e_MPTicket.iAuthorizacion,
				iNumCard = e_MPTicket.iNumCard,
				dAmountPayable = e_MPTicket.dAmountPayable,
				Amountpay = e_MPTicket.Amountpay,
				Methodpay = e_MPTicket.Methodpay,
				bValidate= e_MPTicket.bValidate
			});
		}

		//ROV 15/10/18 Se eliminan los metodos de pago
		public void cancelmethodpay(int index,string Method)
		{
			if (_MPTickets.Count <= 0)
			{

			}
			else
			{
				foreach(var list in _MPTickets)
				{
					if(list.Methodpay == Method)
					{
						list.dCancel = list.Amountpay;
						list.Amountpay = 0;
					}
				}
			}
				
		}

		//ROV 11/10/18 Se valida que el metodo de pago ya fue utilizado para incrementar las cantidades
		public bool validate(string Methodpay)
		{
			if (_MPTickets.Count > 0)
			{
				for (int i = 0; i < _MPTickets.Count; i++)
				{
					if (_MPTickets[i].Methodpay == Methodpay)
					{
						if (_MPTickets[i].bValidate == true)
						{
							return true;
						}
					}
				}
				return false;
			}
			return false;
		}

		//ROV 11/10/18 Se aumenta la Quantity ingresada con el metodo de pago igual
		public void Incrementar(string Formpay, decimal Quantity)
		{
			foreach (var item in _MPTickets)
			{
				if (item.Methodpay == Formpay)
				{
					if (item.bValidate == true)
					{
						item.Amountpay = _MPTickets[i].Amountpay + Quantity;
					}
				}
			}
		}
		
		//ROV 11/10/18 Se visualiza las formas de pago 
		public List<E_MPTicket> Visualize()
		{
			 return _MPTickets.ToList();
		}

		//
		public void clear()
		{
			_MPTickets.Clear();	
		}

		//ROV 11/10/18 Se reta el metodo menos el total
		public string op_change(decimal total, decimal monto)
		{
			decimal cambio = total - monto;
			return cambio.ToString();
		}
		
		//Se calcula lo que el pago que se cancelo 
		public string op_cancel(decimal total, decimal monto)
		{
			decimal cambio = total + monto;
			return cambio.ToString();
		}

		//ROV 17/10/18 Metodo para la resta ya con la lista precargada
		public string op_sum_monto(decimal total)
		{
			decimal valor = 0;
			decimal resta = 0;

			foreach(var lis in _MPTickets)
			{
				valor = valor + lis.Amountpay;
			}
			resta = valor - total;

			return resta.ToString();
		}

		//ROV 11/10/18 Para multiplicar la denominaciones 
		public string op_calcdenomination(int cantidad, decimal denominacion)
		{
			decimal total = cantidad * denominacion;
			return total.ToString();
		}

		//ROV 11/10/18 inrcementar las formas de pago utilizadas 
		public string op_sumamount()
		{
			decimal cont = 0;
			foreach(var item in _MPTickets)
			{
				if(item.bValidate == true)
				{
					cont = cont + item.Amountpay;
				}
			}
			return cont.ToString();
		}	
		
	}
}
