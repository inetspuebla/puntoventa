﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;
using System.Data;
using Inets.Models.Entity_Entities;
using Inets.Data.BaseDate;

namespace Inets.Bussines.Estandard
{
    public class B_Cash
    {
		
		//ROV 11/10/18 metodo para Visualize cajas abiertas
		public DataTable Freebox()
        {
            Q_CashSQL q_CashSQL = new Q_CashSQL();

            return q_CashSQL.VisualizateCash();
        }

		//ROV 11/10/18 metodo para hacer una consulta de cajas
		public E_Cash Search(E_Cash e_Cash)
        {
            Q_CashSQL q_CashSQL = new Q_CashSQL();
            e_Cash = q_CashSQL.Search(e_Cash);
            return e_Cash;

        }

		//ROV 11/10/18 metodo para extraer la caja con el id 
		public E_Cash SearchIdCash(E_Cash e_Cash)
        {
            Q_CashSQL q_CashSQL = new Q_CashSQL();
            e_Cash = q_CashSQL.SearchId(e_Cash);
            return e_Cash;
        }
    }
}