﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;

using Inets.Data.BaseDate;
using Inets.Models.Entity_Entities;


namespace Inets.Bussines.Estandard
{
    public class B_Ticket
    {
        Int64 num = 11111;
		
        public E_Ticket SearchNT(E_Ticket e_Ticket)
        {
            Q_TicketSQL q_TicketSQL = new Q_TicketSQL();
            return q_TicketSQL.Search_NT(e_Ticket);
        }

		public E_Ticket SearchDT()
		{
			Q_TicketSQL q_TicketSQL = new Q_TicketSQL();
			E_Ticket e_Ticket = new E_Ticket();
			e_Ticket.sDateTicket = DateTime.Now.ToShortDateString() + DateTime.Now.ToShortTimeString();

			return q_TicketSQL.Seach_DT(e_Ticket);
		}

		public E_Ticket SearchID(E_Ticket e_Ticket)
		{
			Q_TicketSQL q_TicketSQL = new Q_TicketSQL();
			
			return q_TicketSQL.Search_ID(e_Ticket);
		}

		public string NewTicket(Int64 valor)
        {
			Q_TicketSQL q_TicketSQL = new Q_TicketSQL();
			E_Ticket e_Ticket = new E_Ticket();

			int dia = DateTime.Now.Day;
			int mes = DateTime.Now.Month;
			int año = DateTime.Now.Year;

			e_Ticket = q_TicketSQL.Search_UT();

			if(e_Ticket == null)
			{
				if (valor == 0)
				{
					string Ticket = dia.ToString() + mes.ToString() + año.ToString() + num.ToString();
					return Ticket;
				}
				else
				{
					num = valor + 1;
					return num.ToString();
				}

			}else
			{
				num = Convert.ToInt64(e_Ticket.NumTicket) + 1;
				return num.ToString();
			}

		}
    }
}