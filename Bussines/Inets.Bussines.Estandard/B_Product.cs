﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using Inets.Data.BaseDate;
using Inets.Models.Entity_Entities;

namespace Inets.Bussines.Estandard
{
	public class B_Product
	{
		//ROV 29/08/18  Funcion para mandar a buscar un producto
		public E_Product SearchProduct(E_Product e_Product)
		{
			Q_ProductSQL q_Product = new Q_ProductSQL();
			//ROV 29/08/18  funcion para realizar la busqueda de producto 
			e_Product = q_Product.SearchProduct(e_Product);

			return e_Product;
		}

		//ROV 29/08/18 Funcion para llenar una tabla o un combobox 
		public DataTable SelectProduct()
		{
			Q_ProductSQL q_Product = new Q_ProductSQL();
			//ROV 29/08/18  Se manda a call el metodo de select 
			return q_Product.SelectProduct();
		}

		public DataTable selectUltimProduct()
		{
			Q_ProductSQL q_Product = new Q_ProductSQL();
			return q_Product.SelectUltiProduct();
		}

		public DataTable ShowProduct(int Quantity)
		{
			Q_ProductSQL q_Product = new Q_ProductSQL();
			return q_Product.ShowProduct(Quantity);
		}

		public DataTable filtrosearch(string Clave, String valore)
		{
			Q_ProductSQL q_Product = new Q_ProductSQL();
			return q_Product.FitroSearch(Clave, valore);
		}

		public DataTable SearchClave(string word)
		{
			Q_ProductSQL q_Product = new Q_ProductSQL();
			return q_Product.SearchClave(word);
		}
    }
}
