﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Inets.Data.BaseDate;
using Inets.Models.Entity_Entities;
using ExternalClass;

namespace Inets.Bussines.Estandard
{
	public class B_formats
	{
		CrearTicket ticket = new CrearTicket();

		// generacion de ticket
		public void PrintTicket(E_Store e_Store, E_Employee e_Employee, E_Cash e_Cash, string ntick, decimal deduct, decimal compra, List<LineaProduct> lineaProducts, List<E_MPTicket> e_MPTickets, E_TCAplied e_TCApplicable)
		{
			E_MPaymentChange e_MPaymentChange = new E_MPaymentChange();
			B_MPaymentChange b_MPaymentChange = new B_MPaymentChange();
			B_Denomination b_Denomination = new B_Denomination();
			E_Denomination e_Denomination = new E_Denomination();

			e_MPaymentChange.idMPaymentChange = e_TCApplicable.idMPaymentChange;
			e_MPaymentChange = b_MPaymentChange.Search3(e_MPaymentChange);

			B_Store b_Store = new B_Store();
			e_Store = b_Store.SearchStoreName(e_Store);
			decimal products = 0;

			var item = e_MPTickets;
			var item2 = lineaProducts;

			//Ya podemos usar todos sus metodos
			//ticket.AbreCajon();//Para abrir el cajon de dinero.

			//Datos de la cabecera del Ticket.
			ticket.TextoCentro("NOMBRE DE TIENDA:" + e_Store.sNameStore);
			ticket.TextoIzquierda(" ");
			ticket.TextoIzquierda("DIRECCION:" + e_Store.sAddress);
			ticket.TextoIzquierda("TELEF:" + e_Store.sTelephon);
			ticket.TextoIzquierda("R.F.C:" + e_Store.sRFC);
			ticket.TextoIzquierda("EMAIL:" + e_Store.sEmail);
			ticket.TextoIzquierda("");
			ticket.TextoExtremos("CAJA" + e_Cash.sNameCash, "TICKET #" + ntick);
			ticket.lineasAsteriscos();

			//Sub cabecera.
			ticket.TextoIzquierda("");
			ticket.TextoIzquierda("ATENDIÓ:" + e_Employee.sNameEmployee);
			ticket.TextoIzquierda("CLIENTE: PUBLICO EN GENERAL");
			ticket.TextoIzquierda("");
			ticket.TextoExtremos("FECHA:" + DateTime.Now.ToShortDateString(), "HORA: " + DateTime.Now.ToShortTimeString());
			ticket.lineasAsteriscos();

			//Articulos a vender.
			ticket.EncabezadoVenta();//NOMBRE DEL ARTICULO, CANT, PRECIO, IMPORTE
			ticket.lineasAsteriscos();

			foreach (var fila in item2)
			{

				if (fila.Quantity > 0)
				{
					ticket.AgregaArticulo(fila.Quantity, fila.Product, fila.Price, fila.TotalAmount);
				}
			}

			ticket.lineasIgual();

			//Resumen de la venta. Sólo son ejemplos
			ticket.AgregarTotales("         SUBTOTAL......$", 0);
			ticket.AgregarTotales("         IVA...........$", 0);//La M indica que es un decimal en C#
			ticket.AgregarTotales("         TOTAL.........$", compra);
			ticket.TextoIzquierda("");
			foreach (var lista in item)
			{
				if(lista.Amountpay > 0)
				{
					if (lista.Methodpay == "Efectivo")
					{
						ticket.AgregarTotales("         " + lista.Methodpay + "......$", lista.Amountpay);
						ticket.TextoIzquierda("");
					}
					if (lista.idMPayment == 2)
					{
						ticket.AgregarTotales("          " + lista.Methodpay + "......$", lista.Amountpay);
						ticket.AgregarMedios("     No Autorizacion....", lista.iNumCard.ToString());
						ticket.TextoIzquierda("");
					}
					if (lista.idMPayment == 3)
					{
						e_Denomination.idDenomination = lista.idDenomination;
						e_Denomination = b_Denomination.SearchId(e_Denomination);

						ticket.AgregarTotales("           " + lista.Methodpay + "......$", lista.Amountpay);
						ticket.AgregarMedios("        Denominacion....", e_Denomination.dValue.ToString());
						ticket.AgregarMedios("            Quantity....", lista.dQuantity.ToString());
						ticket.TextoIzquierda("");
					}
					if (lista.Methodpay == "Cheques")
					{
						ticket.AgregarTotales("          " + lista.Methodpay + "......$", lista.Amountpay);
						ticket.AgregarMedios("            No Folio....", lista.sFolio);
						ticket.TextoIzquierda("");
					}

				}
			}

			//ticket.AgregarTotales("         EFECTIVO......$", lista.Amountpay);
			ticket.AgregarTotales("  Cambio " + e_MPaymentChange.typechange + "........$", e_TCApplicable.ValueChange);

			//Texto final del Ticket.
			ticket.TextoIzquierda("");
			foreach (var fila in item2)
			{
				products = products + fila.Quantity;
			}

			ticket.TextoIzquierda("ARTÍCULOS VENDIDOS:" + products);
			ticket.TextoIzquierda("");
			ticket.TextoCentro("¡GRACIAS POR SU COMPRA!");

			//ticket.CortaTicket();
			ticket.ImprimirTicket("Microsoft XPS Document Writer");//Nombre de la impresora ticketera

		}

		public void printcancelproduct(E_Cancellations e_Cancellations, string ntick, List<LineaProduct> lineaProducts)
		{
			E_Cash e_Cash = new E_Cash();
			B_Cash b_Cash = new B_Cash();
			E_Store e_Store = new E_Store();
			B_Store b_Store = new B_Store();
			E_Ticket e_Ticket = new E_Ticket();
			B_Ticket b_Ticket = new B_Ticket();
			E_Employee e_Employee = new E_Employee();
			B_Employee b_Employee = new B_Employee();
			E_CashSales e_CashSales = new E_CashSales();
			B_CashSales b_CashSales = new B_CashSales();

			e_Store.sNameStore = e_Cancellations.store;
			e_Store = b_Store.SearchStoreName(e_Store);

			e_Employee.iUsers = e_Cancellations.idUser;
			e_Employee = b_Employee.SearshEmployee(e_Employee);
			string autorization = e_Employee.sNameEmployee;

			e_CashSales.idUser = e_Cancellations.idUser;
			e_CashSales = b_CashSales.SearchCashSales(e_CashSales);

			e_Employee = new E_Employee();

			e_Employee.iUsers = e_Cancellations.idUser;
			e_Employee = b_Employee.SearshEmployee(e_Employee);
			e_Cash.iIdCash = e_CashSales.idCash;
			e_Cash = b_Cash.Search(e_Cash);

			var item2 = lineaProducts;

			ticket.TextoCentro("CANCELACION DE PRODUCTO");
			ticket.lineasAsteriscos();
			ticket.lineasAsteriscos();
			ticket.TextoCentro("NOMBRE DE TIENDA:" + e_Store.sNameStore);
			ticket.TextoIzquierda(" ");
			ticket.TextoIzquierda("DIRECCION:" + e_Store.sAddress);
			ticket.TextoIzquierda("TELEF:" + e_Store.sTelephon);
			ticket.TextoIzquierda("R.F.C:" + e_Store.sRFC);
			ticket.TextoIzquierda("EMAIL:" + e_Store.sEmail);
			ticket.TextoIzquierda("");
			ticket.TextoExtremos("CAJA" + e_Cash.sNameCash, "TICKET #" + ntick);
			ticket.lineasAsteriscos();

			ticket.TextoIzquierda("");
			ticket.TextoIzquierda("OPERADOR:" + e_Employee.sNameEmployee);
			ticket.TextoIzquierda("");
			ticket.TextoExtremos("FECHA:" + DateTime.Now.ToShortDateString(), "HORA: " + DateTime.Now.ToShortTimeString());
			ticket.lineasAsteriscos();

			ticket.TextoIzquierda("");
			ticket.TextoIzquierda("AUTORIZO:" + autorization);
			ticket.TextoIzquierda("");
			ticket.TextoExtremos("FECHA:" + DateTime.Now.ToShortDateString(), "HORA: " + DateTime.Now.ToShortTimeString());
			ticket.lineasAsteriscos();

			ticket.EncabezadoVenta();
			ticket.lineasAsteriscos();

			foreach (var fila in item2)
			{
				if (fila.Deletes > 0)
				{
					ticket.AgregaArticulo(fila.Deletes, fila.Product, fila.Price, fila.TotalAmount);
				}
			}

			ticket.lineasIgual();
			//ticket.CortaTicket();
			ticket.ImprimirTicket2("Microsoft XPS Document Writer");//Nombre de la impresora ticketera

		}

		public void printcancelMethodpay(E_Cancellations e_Cancellations, string ntick, List<E_MPTicket> e_MPTickets)
		{
			E_Cash e_Cash = new E_Cash();
			B_Cash b_Cash = new B_Cash();
			E_Store e_Store = new E_Store();
			B_Store b_Store = new B_Store();
			E_Ticket e_Ticket = new E_Ticket();
			B_Ticket b_Ticket = new B_Ticket();
			E_Employee e_Employee = new E_Employee();
			B_Employee b_Employee = new B_Employee();
			E_CashSales e_CashSales = new E_CashSales();
			B_CashSales b_CashSales = new B_CashSales();
			E_Denomination e_Denomination = new E_Denomination();
			B_Denomination b_Denomination = new B_Denomination();

			e_Store.sNameStore = e_Cancellations.store;
			e_Store = b_Store.SearchStoreName(e_Store);

			e_Employee.iUsers = e_Cancellations.idUser;
			e_Employee = b_Employee.SearshEmployee(e_Employee);
			string autorization = e_Employee.sNameEmployee;


			e_CashSales.idUser = e_Cancellations.idUser;
			e_CashSales = b_CashSales.SearchCashSales(e_CashSales);

			e_Employee = new E_Employee();

			e_Employee.iUsers = e_Cancellations.idUser;
			e_Employee = b_Employee.SearshEmployee(e_Employee);
			e_Cash.iIdCash = e_CashSales.idCash;
			e_Cash = b_Cash.Search(e_Cash);

			ticket.TextoCentro("CANCELACION DE METODO DE PAGO");
			ticket.lineasAsteriscos();
			ticket.lineasAsteriscos();
			ticket.TextoCentro("NOMBRE DE TIENDA:" + e_Store.sNameStore);
			ticket.TextoIzquierda(" ");
			ticket.TextoIzquierda("DIRECCION:" + e_Store.sAddress);
			ticket.TextoIzquierda("TELEF:" + e_Store.sTelephon);
			ticket.TextoIzquierda("R.F.C:" + e_Store.sRFC);
			ticket.TextoIzquierda("EMAIL:" + e_Store.sEmail);
			ticket.TextoIzquierda("");
			ticket.TextoExtremos("CAJA" + e_Cash.sNameCash, "TICKET #" + e_Ticket.NumTicket);
			ticket.lineasAsteriscos();

			ticket.TextoIzquierda("");
			ticket.TextoIzquierda("OPERADOR:" + e_Employee.sNameEmployee);
			ticket.TextoIzquierda("");
			ticket.TextoExtremos("FECHA:" + DateTime.Now.ToShortDateString(), "HORA: " + DateTime.Now.ToShortTimeString());
			ticket.lineasAsteriscos();

			ticket.TextoIzquierda("");
			ticket.TextoIzquierda("AUTORIZO:" + autorization);
			ticket.TextoIzquierda("");
			ticket.lineasAsteriscos();

			foreach(var list in e_MPTickets )
			{
				if (list.dCancel > 0)
				{
					if (list.Methodpay == "Efectivo")
					{
						ticket.AgregarTotales("         " + list.Methodpay + "......$", list.dCancel);
						ticket.TextoIzquierda("");
					}
					if (list.idMPayment == 2)
					{
						ticket.AgregarTotales("          " + list.Methodpay + "......$", list.dCancel);
						ticket.AgregarMedios("     No Autorizacion....", list.iNumCard.ToString());
						ticket.TextoIzquierda("");
					}
					if (list.idMPayment == 3)
					{
						e_Denomination.idDenomination = list.idDenomination;
						e_Denomination = b_Denomination.SearchId(e_Denomination);

						ticket.AgregarTotales("           " + list.Methodpay + "......$", list.dCancel);
						ticket.AgregarMedios("        Denominacion....", e_Denomination.dValue.ToString());
						ticket.AgregarMedios("            Quantity....", list.dQuantity.ToString());
						ticket.TextoIzquierda("");
					}
					if (list.Methodpay == "Cheques")
					{
						ticket.AgregarTotales("          " + list.Methodpay + "......$", list.dCancel);
						ticket.TextoIzquierda("");
					}
				}
			}

			ticket.lineasAsteriscos();

			ticket.lineasIgual();
			//ticket.CortaTicket();
			ticket.ImprimirTicket3("Microsoft XPS Document Writer");//Nombre de la impresora ticketera
		}

		public void printcancelTicket(E_Cancellations e_Cancellations, List<LineaProduct> lineaProducts)
		{
			E_Cash e_Cash = new E_Cash();
			B_Cash b_Cash = new B_Cash();
			E_Store e_Store = new E_Store();
			B_Store b_Store = new B_Store();
			E_Ticket e_Ticket = new E_Ticket();
			B_Ticket b_Ticket = new B_Ticket();
			E_Employee e_Employee = new E_Employee();
			B_Employee b_Employee = new B_Employee();
			E_CashSales e_CashSales = new E_CashSales();
			B_CashSales b_CashSales = new B_CashSales();

			e_Store.sNameStore = e_Cancellations.store;
			e_Store = b_Store.SearchStoreName(e_Store);

			e_Employee.iUsers = e_Cancellations.idUser;
			e_Employee = b_Employee.SearshEmployee(e_Employee);
			string autorization = e_Employee.sNameEmployee;

			e_Ticket.iIdTicket = e_Cancellations.idTicket;
			e_Ticket = b_Ticket.SearchID(e_Ticket);
			e_CashSales.iIdCashSales = e_Ticket.iIdCashSales;
			e_CashSales = b_CashSales.SearchID(e_CashSales);

			e_Employee = new E_Employee();

			e_Employee.iUsers = e_CashSales.idUser;
			e_Employee = b_Employee.SearshEmployee(e_Employee);
			e_Cash.iIdCash = e_CashSales.idCash;
			e_Cash = b_Cash.Search(e_Cash);

			var item2 = lineaProducts;

			ticket.TextoCentro("CANCELACION DE TICKET");
			ticket.lineasAsteriscos();
			ticket.lineasAsteriscos();
			ticket.TextoCentro("NOMBRE DE TIENDA:" + e_Store.sNameStore);
			ticket.TextoIzquierda(" ");
			ticket.TextoIzquierda("DIRECCION:" + e_Store.sAddress);
			ticket.TextoIzquierda("TELEF:" + e_Store.sTelephon);
			ticket.TextoIzquierda("R.F.C:" + e_Store.sRFC);
			ticket.TextoIzquierda("EMAIL:" + e_Store.sEmail);
			ticket.TextoIzquierda("");
			ticket.TextoExtremos("Caja" + e_Cash.sNameCash, "TICKET #" + e_Ticket.NumTicket);
			ticket.lineasAsteriscos();

			ticket.TextoIzquierda("");
			ticket.TextoIzquierda("OPERADOR:" + e_Employee.sNameEmployee);
			ticket.TextoIzquierda("");
			ticket.TextoExtremos("FECHA:" + DateTime.Now.ToShortDateString(), "HORA: " + DateTime.Now.ToShortTimeString());
			ticket.lineasAsteriscos();

			ticket.TextoIzquierda("");
			ticket.TextoIzquierda("AUTORIZO:" + autorization);
			ticket.TextoIzquierda("");
			ticket.TextoExtremos("FECHA:" + DateTime.Now.ToShortDateString(), "HORA: " + DateTime.Now.ToShortTimeString());
			ticket.lineasAsteriscos();

			ticket.EncabezadoVenta();
			ticket.lineasAsteriscos();

			foreach (var fila in item2)
			{
				if (fila.Quantity > 0)
				{
					ticket.AgregaArticulo(fila.Quantity, fila.Product, fila.Price, fila.TotalAmount);
				}
			}

			ticket.lineasIgual();
			//ticket.CortaTicket();
			ticket.ImprimirTicket4("Microsoft XPS Document Writer");//Nombre de la impresora ticketera

		}
		
	}
}
