﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using Inets.Data.BaseDate;
using Inets.Models.Entity_Entities;

namespace Inets.Bussines.Estandard
{
    public class B_Departament
    {
        //ROV 30/08/18 Funcion para traer un departmento a selecionar 
        public E_Departament SearchDepartament(E_Departament e_Departament)
        {
            Q_DepartmentSQL q_Departament = new Q_DepartmentSQL();

            e_Departament = q_Departament.SearchDepartament(e_Departament);
            return e_Departament;
            
        }

        //ROV 30/08/18 Funcion para extraer departamento y mostrarlo en un combobox o datagridview        
        public DataTable SelectDepartment()
        {
            Q_DepartmentSQL q_Departament = new Q_DepartmentSQL();
            return q_Departament.SelectDpto();
        }

    }
}
