﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;
using System.Data;
using Inets.Models.Entity_Entities;
using Inets.Data.BaseDate;

namespace Inets.Bussines.Estandard
{

	public class B_TCAplied
	{
		List<E_TCAplied> l_TCApplicables = new List<E_TCAplied>();
		
		public DataTable Visualize()
		{
			Q_TCApliedSQL q_TCApplicableSQL = new Q_TCApliedSQL();
			return q_TCApplicableSQL.Visualize();
		}

		public void list(E_TCAplied e_TCApplicable)
		{
			l_TCApplicables.Add(new E_TCAplied {

				idMPTicket = e_TCApplicable.idMPTicket,
				idMPaymentChange = e_TCApplicable.idMPaymentChange,
				ValueChange = e_TCApplicable.ValueChange

			});
		}
		
		public bool validation(E_TCAplied e_TCApplicable)
		{
			foreach(var lista in l_TCApplicables)
			{
				if (lista.idTCApplicable == e_TCApplicable.idTCApplicable)
				{
					return true;
				}
				else return false;
			}
			return false;
		}


	}
}
