﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;
using System.Data;
using Inets.Models.Entity_Entities;
using Inets.Data.BaseDate;

namespace Inets.Bussines.Estandard
{
	public class B_MPaymentChange
	{
		public DataTable visualizar()
		{
			Q_MPaymentChangeSQL q_MPaymentChangeSQL = new Q_MPaymentChangeSQL();
			return q_MPaymentChangeSQL.Visualize();
		}

		public DataTable visualizar2()
		{
			Q_MPaymentChangeSQL q_MPaymentChangeSQL = new Q_MPaymentChangeSQL();
			return q_MPaymentChangeSQL.Visualize2();
		}

		public E_MPaymentChange SearchNickname(E_MPaymentChange e_MPaymentChange)
		{
			Q_MPaymentChangeSQL q_MPaymentChangeSQL = new Q_MPaymentChangeSQL();

			return q_MPaymentChangeSQL.SearchNickname(e_MPaymentChange);
		}

		public E_MPaymentChange Searchlimit(E_MPaymentChange e_MPaymentChange)
		{
			Q_MPaymentChangeSQL q_MPaymentChangeSQL = new Q_MPaymentChangeSQL();

			return q_MPaymentChangeSQL.Searchlimit(e_MPaymentChange);
		}

		public DataTable Searchformethod(E_MPaymentChange e_MPaymentChange)
		{
			Q_MPaymentChangeSQL q_MPaymentChangeSQL = new Q_MPaymentChangeSQL();
			return q_MPaymentChangeSQL.Searchformethod(e_MPaymentChange);
		}

		public E_MPaymentChange Search2(E_MPaymentChange e_MPaymentChange)
		{
			Q_MPaymentChangeSQL q_MPaymentChangeSQL = new Q_MPaymentChangeSQL();
			return q_MPaymentChangeSQL.SearchIdMPC(e_MPaymentChange);
		}

		public E_MPaymentChange Search3(E_MPaymentChange e_MPaymentChange)
		{
			Q_MPaymentChangeSQL q_MPaymentChangeSQL = new Q_MPaymentChangeSQL();
			return q_MPaymentChangeSQL.Search3(e_MPaymentChange);
		}
	}
}
