﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using Inets.Data.BaseDate;
using Inets.Models.Entity_Entities;
using Json3;

namespace Inets.Bussines.Estandard
{
    public class B_CashCut
    {
		//ROV 11/10/18 metodo para calcular el valor de inicio 
		public double CalculateAmount(double valor1, double valor2)
        {
            double total = valor1 + valor2;
            return total;

        }
		
		//ROV 11/10/18 metdo para Visualize la tabla 
		public DataTable VisualizeTable(E_CashCut e_CashCut)
        {
            Q_CashCutSQL q_CashCutSQL = new Q_CashCutSQL();
            return q_CashCutSQL.VisualizeTable(e_CashCut);

        }

		
	}
}
