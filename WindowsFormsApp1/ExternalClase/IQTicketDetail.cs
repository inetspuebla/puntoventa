﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;
using Inets.Models.Entity_Entities;

namespace ExternalClase
{
	interface IQTicketDetail
	{
		bool Insert(E_DetailTicket e_DetailTicket);
		bool Update(E_DetailTicket e_DetailTicket);
		bool Delete(E_DetailTicket e_DetailTicket);
		E_DetailTicket Search(E_DetailTicket e_DetailTicket);
		Decimal Sum(E_DetailTicket e_DetailTicket);
		DataTable Visualize(E_DetailTicket e_DetailTicket);
		DataRow WievTable(SqlDataAdapter adapter);
	}
}
