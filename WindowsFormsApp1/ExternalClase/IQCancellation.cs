﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;
using Inets.Models.Entity_Entities;

namespace ExternalClase
{
	interface IQCancellation
	{
		bool Insert(E_Cancellations e_Cancellation);
		bool Update(E_Cancellations e_Cancellation);
	}
}
