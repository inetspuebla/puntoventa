﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;

using Inets.Models.Entity_Entities;


namespace ExternalClase
{
	interface IQEmployee
	{
		bool InsertEmployee(E_Employee e_Employee);
		bool UpdateEmployee(E_Employee e_Employee);
		bool DeleteEmployee(E_Employee e_Employee);
		E_Employee SearchEmployee(E_Employee e_Employee);
		DataRow WievTable(SqlDataAdapter adapter);
	}
}
