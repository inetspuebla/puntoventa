﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;
using System.Data;
using Inets.Models.Entity_Entities;

namespace ExternalClase
{
	interface IQCash
	{
		bool Insert(E_Cash e_Cash);
		bool Update(E_Cash e_Cash);
		bool Delete(E_Cash e_Cash);
		E_Cash Search(E_Cash e_Cash);
		E_Cash SearchId(E_Cash e_Cash);
		bool ValidateRegistre(E_Cash e_Cash);
		DataTable Freebox();
		DataTable VisualizateCash();
		DataRow WievTable(SqlDataAdapter adapter);
	}
}
