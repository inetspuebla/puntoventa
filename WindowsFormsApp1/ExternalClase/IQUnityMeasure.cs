﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;
using Inets.Models.Entity_Entities;


namespace ExternalClase
{
	interface IQUnityMeasure
	{
		bool Insert(E_UnityMeasure e_UnityMeasure);
		bool Delete(E_UnityMeasure e_UnityMeasure);
		E_UnityMeasure Search(E_UnityMeasure e_UnityMeasure);
		DataTable Select();
		DataRow WievTable(SqlDataAdapter adapter);

	}
}
