﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;

using Inets.Models.Entity_Entities;

namespace ExternalClase
{
	interface IQProduct
	{
		bool Insert(E_Product e_Product);
		bool Update(E_Product e_Product);
		bool Delete(E_Product e_Product);
		DataTable SelectProduct();
		DataTable SelectUltiProduct();
		DataTable ShowProduct(int Quantity);
		DataTable FiltroSearch(string clave, string Value);
		E_Product SearchProduct(E_Product e_Product);
		E_Product SearchFiltro(E_Product e_Product);
		DataTable SearchClave(string Word);
		DataRow WievTable(SqlDataAdapter adapter);
		bool ValidateRegistre(E_Product e_Product);
	}
}
