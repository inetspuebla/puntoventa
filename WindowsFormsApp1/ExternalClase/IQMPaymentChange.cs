﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;
using Inets.Models.Entity_Entities;

namespace ExternalClase
{
	interface IQMPaymentChange
	{
		bool Insert(E_MPaymentChange e_MPaymentChange);
		bool Update(E_MPaymentChange e_MPaymentChange);
		bool Delete(E_MPaymentChange e_MPaymentChange);
		DataTable Visualize();
		DataTable Visualize2();
		E_MPaymentChange Search(E_MPaymentChange e_MPaymentChange);
		E_MPaymentChange Search2(E_MPaymentChange e_MPaymentChange);
		E_MPaymentChange Search3(E_MPaymentChange e_MPaymentChange);
		DataTable Searchformethod(E_MPaymentChange e_MPaymentChange);
		E_MPaymentChange Searchlimit(E_MPaymentChange e_MPaymentChange);
		DataRow WievTable(SqlDataAdapter adapter);
	}
}
