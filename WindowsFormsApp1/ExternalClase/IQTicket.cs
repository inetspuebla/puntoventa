﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;
using Inets.Models.Entity_Entities;

namespace ExternalClase
{
	interface IQTicket
	{
		bool Insert(E_Ticket e_Ticket);
		bool Delete(E_Ticket e_Ticket);
		E_Ticket Search_NT(E_Ticket e_Ticket);
		E_Ticket Search_DT(E_Ticket e_Ticket);
		E_Ticket Search_UT(E_Ticket e_Ticket);
		DataRow WievTable(SqlDataAdapter adapter);

	}
}
