﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;
using Inets.Models.Entity_Entities;

namespace ExternalClase
{
	interface IQCard
	{
		bool Insert(E_Card e_Card);
		bool Update(E_Card e_Card);
		bool Delete(E_Card e_Card);
		DataTable visualize();
		E_Card Search(E_Card e_Card);
		DataRow WievTable(SqlDataAdapter adapter);
	}
}
