﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;
using Inets.Models.Entity_Entities;

namespace ExternalClase
{
	interface IQCompanyConvention
	{
		bool Insert(E_CompanyConvention e_CompanyConvention);
		bool Update(E_CompanyConvention e_CompanyConvention);
		bool Delete(E_CompanyConvention e_CompanyConvention);
		DataTable visualize();
		E_CompanyConvention Search(E_CompanyConvention e_CompanyConvention);
		DataRow WievTable(SqlDataAdapter adapter);

	}
}
