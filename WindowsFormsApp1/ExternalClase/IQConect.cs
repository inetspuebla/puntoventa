﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace ExternalClase
{
	interface IQConect
	{
		bool OpenDB();
		void ClosedDB();
		SqlCommand Build_Command(string Char);
		SqlDataAdapter Build_Adapter(string cadena);
		SqlDataReader RunReader();
	}
}
