﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;

using Inets.Models.Entity_Entities;

namespace ExternalClase
{
	interface IQIngressProduct
	{
		bool Insert(E_IngressProduct e_IngressProduct);
		bool Update(E_IngressProduct e_IngressProduct);
		bool Delete(E_IngressProduct e_IngressProduct);
	}
}
