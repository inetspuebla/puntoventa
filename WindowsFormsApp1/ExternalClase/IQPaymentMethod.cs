﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;

using Inets.Models.Entity_Entities;

namespace ExternalClase
{
	interface IQPaymentMethod
	{
		bool Inset(E_PaymentMethod e_PaymentMethod);
		bool Update(E_PaymentMethod e_PaymentMethod);
		bool Delete(E_PaymentMethod e_PaymentMethod);
		DataTable Visualize();
		E_PaymentMethod Search(E_PaymentMethod e_PaymentMethod);
		E_PaymentMethod SearchId(E_PaymentMethod e_PaymentMethod);
		DataRow WievTable(SqlDataAdapter adapter);
	}
}
