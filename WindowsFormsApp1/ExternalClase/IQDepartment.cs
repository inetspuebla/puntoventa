﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;

using Inets.Models.Entity_Entities;

namespace ExternalClase
{
	interface IQDepartment
	{
		bool InsertDepartament(E_Departament e_Departament);
		bool DeleteDepartament(E_Departament e_Departament);
		DataTable SelectDpto();
		E_Departament SearchDepartament(E_Departament e_Departament);
		DataRow WievTable(SqlDataAdapter adapter);
		bool ValidateRegistre(E_Departament e_Departament);
	}
}
