﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;
using Inets.Models.Entity_Entities;

namespace ExternalClase
{
	interface IQTCAplied
	{
		bool Insert(E_TCAplied e_TCAplied);
		bool Update(E_TCAplied e_TCAplied);
		bool Delete(E_TCAplied e_TCAplied);
		DataTable Visualize();
		E_TCAplied Search(E_TCAplied e_TCAplied);
		DataRow WievTable(SqlDataAdapter adapter);
	}
}
