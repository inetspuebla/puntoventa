﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;
using Inets.Models.Entity_Entities;

namespace ExternalClase
{
	interface IQMPTicket
	{
		bool InsertEfectivo(E_MPTicket e_MPTicket);
		bool InsertCheques(E_MPTicket e_MPTicket);
		bool InsertVales(E_MPTicket e_MPTicket);
		bool InsertTarjeta(E_MPTicket e_MPTicket);

		bool Update(E_MPTicket e_MPTicket);
		bool Delete(E_MPTicket e_MPTicket);

		DataTable Visualize();

		E_MPTicket Search(E_MPTicket e_MPTicket);
		E_MPTicket SearchId(E_MPTicket e_MPTicket);
		DataRow WievTable(SqlDataAdapter adapter);
	}
}
