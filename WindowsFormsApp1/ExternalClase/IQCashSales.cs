﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;
using System.Data;
using Inets.Models.Entity_Entities;

namespace ExternalClase
{
	interface IQCashSales
	{
		bool InsertCashSales(E_CashSales e_CashSales);
		bool UpdateCashSales(E_CashSales e_CashSales);
		bool DeleteCashSales(E_CashSales e_CashSales);
		E_CashSales SearchCash(E_CashSales e_CashSales);
		DataRow WievTable(SqlDataAdapter adapter);
		DataTable VisualizateCash();
	}
}
