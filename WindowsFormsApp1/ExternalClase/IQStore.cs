﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;

using Inets.Models.Entity_Entities;

namespace ExternalClase
{
	interface IQStore
	{
		bool Insert(E_Store e_Store);
		bool Update(E_Store e_Store);
		bool Delete(E_Store e_Store);
		E_Store Search(E_Store e_Store);
		E_Store Search_Name(E_Store e_Store);
		DataRow WievTable(SqlDataAdapter adapter);
	}
}
