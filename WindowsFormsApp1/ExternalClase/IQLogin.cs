﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;
using System.Data;
using Inets.Models.Entity_Entities;


namespace ExternalClase
{
	interface IQLogin
	{
		bool StartSesion(E_Users e_Users);
		bool Validation(E_Users e_Users);
		E_Users SearchUserlogin(E_Users e_Users);
		E_Users SearchUser(E_Users e_Users);
		DataRow WievTable(SqlDataAdapter adapter);
	}
}
