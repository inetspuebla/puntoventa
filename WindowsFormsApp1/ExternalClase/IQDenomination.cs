﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;
using Inets.Models.Entity_Entities;

namespace ExternalClase
{
	interface IQDenomination
	{
		bool Insert(E_Denomination e_Denomination);
		bool Update(E_Denomination e_Denomination);
		bool Delete(E_Denomination e_Denomination);
		DataTable Visualize(E_Denomination e_Denomination);
		E_Denomination Search(E_Denomination e_Denomination);
		E_Denomination Searchid(E_Denomination e_Denomination);
		DataRow WievTable(SqlDataAdapter adapter);
	}
}
