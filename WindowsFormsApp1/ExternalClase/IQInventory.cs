﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;
using Inets.Models.Entity_Entities;

namespace ExternalClase
{
	interface IQInventory
	{
		bool InsertInventory(E_Inventory e_Inventory);
		bool UpdateInventory(E_Inventory e_Inventory);
		bool DeleteInventory(E_Inventory e_Inventory);
		E_Inventory SearchInventory(E_Inventory e_Inventory);
		DataRow WievTable(SqlDataAdapter adapter);

	}
}
