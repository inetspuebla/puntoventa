﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;
using Inets.Models.Entity_Entities;

namespace ExternalClase
{
	interface IQTypeCard
	{
		bool Insert(E_TypeCard e_TypeCard);
		bool Update(E_TypeCard e_TypeCard);
		bool Delete(E_TypeCard e_TypeCard);
		DataTable Visualize();
		E_TypeCard Search(E_TypeCard e_TypeCard);
		DataRow WievTable(SqlDataAdapter adapter);

	}
}
