﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;
using Inets.Models.Entity_Entities;

namespace ExternalClase
{
	interface IQUser
	{
		bool Insert(E_Users e_Users);
		bool update(E_Users e_Users);
		E_Employee search(E_Employee e_Employee);
		DataRow WievTable(SqlDataAdapter adapter);
		DataTable VisualizeCashier();

	}
}
