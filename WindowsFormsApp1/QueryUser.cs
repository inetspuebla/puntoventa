﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using System.data.sqlclient;
using System.data;
using System.Configuration;

public class QueryUser // CLASE PARA  CONSULTAS BASICAS
{
    String Name, LastName, Address, Mail;
    int Telephone; 

    Conectbd database = new Conectbd();
    sqlcommand command;

    public CommandInsertUsuario(String Name,string LastName, String Address,string Mail, int Telephone)
    {
        if(database.conectarbd())
        {
            command = database.construyecommand("insert into Person(Name, LastName, Adrress,Telephone,Mail)" +
                "values(@Name, @LastName, @Adrress,@Telephone,@Mail)");

            command.Parameters.Add(new SqlParameter("@Names", SqlDbType.VarChar, 15));
            comando.Parameters["@Names"].Value = Name ;

            command.Parameters.Add(new SqlParameter("@LastName", SqlDbType.VarChar, 30));
            comando.Parameters["@LastName"].Value = LastName;

            command.Parameters.Add(new SqlParameter("@Address", SqlDbType.VarChar, 50));
            comando.Parameters["@Address"].Value = Address;

            command.Parameters.Add(new SqlParameter("@Telephone", SqlDbType.Int));
            comando.Parameters["@Telephone"].Value = Telephone;

            command.Parameters.Add(new SqlParameter("@Mail", SqlDbType.VarChar, 30));
            comando.Parameters["@Mail"].Value = Mail;

        }

    }

}
