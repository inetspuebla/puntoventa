﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using System.data.sqlclient;
using System.data;
using System.Configuration;

public class conectbd //Clase para conexion de BD  
{

    private SqlDataReader read;
    private Sqlconnection conectar;
    private SqlCommand cadena_sql;
    private SQLDataAdapter adaptar;

    public SqlDataReader _read
    {
        get{
            return (read);
        }
    }
    
    public bool conectarbd()  // Funcion conectar base de datos 
    {

        conectar = new SqlConnection();
        conectar.ConnectionString = @"Data Source=SQLEXPRESS;Initial Catalog=inetsPointofsale;Integrated Security=True";// cadena de conexion a base de datos 

        try// validar que la cadena de conexion sea correcta 
        {
            conectar.Open();
            return true;
        }
        catch (Exception oEx)
        {
            MessageBox.Show(oEx.Message);
            return false;
        }


    }

    public void desconect_bd()// funcion cierre de base de datos
    {
            conectar.close();
    }

    public void contruye_reader(string cadena)
    {
        cadena_sql= new sqlcommand();
        cadena_sql.Connection = conectar;
        cadena_sql.CommandText = cadena;
    }
    
    public SqlDataReader ejecutar_reader()
    {
        try
        { read = cadena_sql.EjecutaReader();
            return read;

        }
        catch (Exception oEx)
        {
            return null;
        }
    }

    public intejecutanonquery()
    {
        int afectados;
        try
        {
            afectados = cadena_sql.ExecuteNonQuery();
            return (afectados);
        }
        catch (Exeption oEx)
        {
            return (0);
        }
    }

    public SqlCommand construye_command(string cadena)
    {
        cadena_sql = new SqlCommand(cadena, conectar);
        return (cadena_sql);
    }

    public SqlDataAdapter contruye_adapter(string cadena)
    {
        adaptar = new SqlDataAdapter(cadena, conectar);
        return (adaptar);
    }

}
