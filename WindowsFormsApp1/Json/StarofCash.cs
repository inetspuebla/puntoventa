﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Web.Script.Serialization;
using System.IO;

using Inets.Models.Entity_Entities;

namespace Json3
{
    public class StarofCash
    {
		E_JStarofcash jStarofcash = new E_JStarofcash();


		public void insert(E_JStarofcash eJStarofcash)
		{
			JavaScriptSerializer ser = new JavaScriptSerializer();
			string hora = DateTime.Now.Hour.ToString();
			string minuto = DateTime.Now.Minute.ToString();
			string segundo = DateTime.Now.Second.ToString();

			string number = hora + minuto + segundo;

			string outputjson = ser.Serialize(eJStarofcash);

			File.WriteAllText(@"C:\json\" + number + "ini.json", outputjson);

		}

		public void insertTicket(E_JTicket jTicket)
		{
			string hora = DateTime.Now.Hour.ToString();
			string minuto = DateTime.Now.Minute.ToString();
			string segundo = DateTime.Now.Second.ToString();

			JavaScriptSerializer ser = new JavaScriptSerializer();

			string number = jTicket.NumberTicket + hora + minuto + segundo;
			
			string outputjson = ser.Serialize(jTicket);

			File.WriteAllText(@"C:\json\" + number+".json", outputjson);

			
		}

	}
}
