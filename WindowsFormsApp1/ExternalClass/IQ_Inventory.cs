﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;
using System.Data;
using Inets.Models.Entity_Entities;

namespace Inets.Data.BaseDate
{
    public interface IQ_Inventory
    {
        bool InsertInventory(E_Inventory e_Inventory);
        bool UpdateInventory(E_Inventory e_Inventory);
        bool DeleteInventory(E_Inventory e_Inventory);
        E_Inventory SearchcInventory(E_Inventory e_Inventory);
        DataRow WievTable(SqlDataAdapter adapter);

    }
}
