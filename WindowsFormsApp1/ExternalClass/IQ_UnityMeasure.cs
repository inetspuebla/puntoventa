﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;
using Inets.Models.Entity_Entities;

namespace Inets.Data.BaseDate
{
    public interface IQ_UnityMeasure
    {
        bool InsertUnityMeasure(E_UnityMeasure e_UnityMeasure);
        bool DeleteUnityMeasure(E_UnityMeasure e_UnityMeasure);
        E_UnityMeasure SearchUnityMeasure(E_UnityMeasure e_UnityMeasure);
        DataTable SelectUnityMeasure();
        DataRow WievTable(SqlDataAdapter adapter);
    }
}
