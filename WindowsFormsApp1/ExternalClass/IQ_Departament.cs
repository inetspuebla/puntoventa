﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;
using System.Data;
using Inets.Models.Entity_Entities;

namespace Inets.Data.BaseDate
{
    public interface IQ_Departament
    {
        bool InsertDepartament(E_Departament e_Departament);
        bool DeleteDepartament(E_Departament e_Departament);
        DataTable SelectDepto();
        E_Departament SearchDepartament(E_Departament e_Departament);
        DataRow Wievtable(SqlDataAdapter sqlDataAdapter);
        bool ValidateRegistre(E_Departament e_Departament);
    }
}
