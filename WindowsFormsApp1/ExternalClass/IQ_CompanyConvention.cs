﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;
using System.Data;
using Inets.Models.Entity_Entities;

namespace Inets.Data.BaseDate
{
	interface IQ_CompanyConvention
	{
		bool Insert(E_CompanyConvention e_CompanyConvention);

		bool Update(E_CompanyConvention e_CompanyConvention);

		bool Delete(E_CompanyConvention e_CompanyConvention);

		DataTable visualize();

		E_CompanyConvention Search();

		DataRow WievTable(SqlDataAdapter adapter);

	}
}
