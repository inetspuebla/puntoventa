﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;
using System.Data;
using Inets.Models.Entity_Entities;


namespace Inets.Data.BaseDate
{
	interface IQ_Card
	{
		bool Insert(E_Card e_Card);

		bool Update(E_Card e_Card);

		bool Delete(E_Card e_Card);

		DataTable Visualize();

		E_Card search(E_Card e_Card);

		DataRow WievTable(SqlDataAdapter adapter);
	}
}
