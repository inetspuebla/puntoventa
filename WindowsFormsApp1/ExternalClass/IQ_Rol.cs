﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;
using Inets.Models.Entity_Entities;

namespace Inets.Data.BaseDate
{
    public interface IQ_Rol
    {
        bool Insert(E_Rol e_Rol);
        bool Delete(E_Rol e_Rol);
    }
}
