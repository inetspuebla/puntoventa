﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;
using System.Data;
using Inets.Models.Entity_Entities;

namespace Inets.Data.BaseDate
{
    public interface IQ_Employee
    {
        bool InsertEmployee(E_Employee e_Employee);
        bool UpdateEmployee(E_Employee e_Employee);
        bool DeleteEmployee(E_Employee e_Employee);
        E_Employee SearchEmployee(E_Employee e_Employee);
    }
}
