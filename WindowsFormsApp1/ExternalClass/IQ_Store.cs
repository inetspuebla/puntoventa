﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;
using Inets.Models.Entity_Entities;

namespace Inets.Data.BaseDate
{
    public interface IQ_Store
    {
        bool InsertStore(E_Store e_Store);
        bool UdateStore(E_Store e_Store);
        bool DeleteStore(E_Store e_Store);
    }
}
