﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;
using System.Data;
using Inets.Models.Entity_Entities;

namespace Inets.Data.BaseDate
{
    public interface IQ_Cash
    {
        bool InsertCash(E_Cash e_Cash);
        bool UpdateCash(E_Cash e_Cash);
        bool DeleteCash(E_Cash e_Cash);
    }
}
