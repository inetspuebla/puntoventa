﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;
using Inets.Models.Entity_Entities;

namespace Inets.Data.BaseDate
{
    public interface IQ_Profile
    {
        bool InsertProfile(E_Profiles e_Profiles);
        bool DeleteProfile(E_Profiles e_Profiles);
    }
}
