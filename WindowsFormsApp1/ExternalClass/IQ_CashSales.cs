﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;
using System.Data;
using Inets.Models.Entity_Entities;

namespace Inets.Data.BaseDate
{
    public interface IQ_CashSales
    {
        bool InsertCashSales(E_CashSales e_CashSales);
        bool UpdateCashSales(E_CashSales e_CashSales);
        bool DeleteCashSales(E_CashSales e_CashSales);
    }
}
