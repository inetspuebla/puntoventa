﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;
using System.Data;
using Inets.Models.Entity_Entities;

namespace Inets.Data.BaseDate
{
    public interface IQ_IngressProduct
    {
        bool InsertIngressProduct(E_IngressProduct e_IngressProduct);
        bool UpdateIngressProduct(E_IngressProduct e_IngressProduct);
        bool DeleteIngressProduct(E_IngressProduct e_IngressProduct);
    }
}
