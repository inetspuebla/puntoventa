﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;
using System.Data;
using Inets.Models.Entity_Entities;

namespace Inets.Data.BaseDate
{
	interface IQ_Cancellation
	{
		bool insert(E_Cancellations e_Cancellations);
		bool update(E_Cancellations e_Cancellations);
	}
}
