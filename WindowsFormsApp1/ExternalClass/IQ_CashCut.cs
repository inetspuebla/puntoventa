﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;
using System.Data;
using Inets.Models.Entity_Entities;

namespace Inets.Data.BaseDate
{
    public interface IQ_CashCut
    {
        bool InsertCashCut(E_CashCut e_CashCut);
        bool UpdateCashCut(E_CashCut e_CashCut);
        bool DeleteCashCut(E_CashCut e_CashCut);
    }
}
