﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;
using System.Data;
using Inets.Models.Entity_Entities;

namespace Inets.Data.BaseDate
{
    public interface IQ_Ticket
    {
        bool InsertTicket(E_Ticket e_Ticket);
        bool UpdateTicket(E_Ticket e_Ticket);
        bool DeleteTicket(E_Ticket e_Ticket);
    }
}
