﻿using System;

namespace ExternalClass
{
	public class LineaProduct
	{
		public string Producto { get; set; }
		public Decimal Cantidad { get; set; }
		public decimal Precio { get; set; }
		public decimal Monto_Total { get; set; }
		public decimal Eliminados {get; set;}
		public int IdProduct { get; set; }
		public string UPC { get; set; }
		public string Numberticket { get; set; }
		public int idPromotion { get; set; }

		public decimal preciototal(decimal precio, decimal cantidad)
		{
			decimal total = precio * cantidad;
			return total;
		}
    }
}
