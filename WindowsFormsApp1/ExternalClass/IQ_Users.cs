﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;
using Inets.Models.Entity_Entities;

namespace Inets.Data.BaseDate
{
    public interface IQ_Users
    {
        bool InsertUser(E_Users e_Users);
        bool UpdateUser(E_Users e_Users);
        bool DeleteUser(E_Users e_Users);
        E_Users SearchUseer(E_Users e_Users);

    }
}
