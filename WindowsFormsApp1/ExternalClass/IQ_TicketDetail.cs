﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;
using System.Data;
using Inets.Models.Entity_Entities;

namespace Inets.Data.BaseDate
{
    public interface IQ_TicketDetail
    {
        bool InsertTicketDetail(E_DetailTicket e_DetailTicket);
        bool UpdateDetailTicket(E_DetailTicket e_DetailTicket);
        bool DeleteDetailTicket(E_DetailTicket e_DetailTicket);
        DataTable VisualizeCashier();
    }
}
