﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace ExternalClass
{
    public class SalesTicket
    {
        public string Producto { get; set; }
        public Decimal Cantidad { get; set; }
        public decimal Precio { get; set; }
        public decimal Monto_Total { get; set; }
        public string UPC { get; set; }
		public string TipeSales { get; set; }
	}
}
