﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using System.data.sqlclient;
using System.data;
using System.Configuration;

public class User
{

    public Users()
    {

    }

    Conectbd database = new Conectbd();

    public CommandInsertUser(String Name, string LastName, String Address, string Mail, int Telephone)// Funcion de insert persona
    {
        if (database.conectarbd())
        {
            command = database.construyecommand("insert into Person(Name, LastName, Adrress, Telephone, Mail)" +
                "values(@Name, @LastName, @Adrress, @Telephone, @Mail)");// Comando para ingresar usuarios

            command.Parameters.Add(new SqlParameter("@Name", SqlDbType.VarChar, 15));// relacionar parametros con las variables a utilizar
            comando.Parameters["@Name"].Value = Name;

            command.Parameters.Add(new SqlParameter("@LastName", SqlDbType.VarChar, 30));
            comando.Parameters["@LastName"].Value = LastName;

            command.Parameters.Add(new SqlParameter("@Address", SqlDbType.VarChar, 50));
            comando.Parameters["@Address"].Value = Address;

            command.Parameters.Add(new SqlParameter("@Telephone", SqlDbType.Int));
            comando.Parameters["@Telephone"].Value = Telephone;

            command.Parameters.Add(new SqlParameter("@Mail", SqlDbType.VarChar, 30));
            comando.Parameters["@Mail"].Value = Mail;

            command = database.construyecommand("insert into Person(User, Password)" + //Comando para insertar el usuario y la contrseña para el login 
             "values(@Name, @LastName, @Adrress,@Telephone,@Mail)");

            command.Parameters.Add(new SqlParameter("@User", SqlDbType.VarChar, 15));
            comando.Parameters["@User"].Value = Name;

            command.Parameters.Add(new SqlParameter("@Password", SqlDbType.VarChar, 30));
            comando.Parameters["@Password"].Value = LastName;


            if ((herramientas.ejecutanonquery()) != 0)
            {
                MessageBox.Show("Datos Ingresados", "Mensaje");
            }
            else
            {
                MessageBox.Show("datos incompletos");
            }
            comando.Connection.Close();
            herramientas.desconect();
        }
    }

    public CommandupdatetUser(String Name, string LastName, String Address, string Mail, int Telephone)
    {
        if (database.conectarbd())
        {
            command = database.construyecommand("update Person set Name = @Name, LastName=@LastName, Adrress = @Adrress, Telephone = @Telephone, Mail= @Mail");//comando para actualizar informacion

            command.Parameters.Add(new SqlParameter("@Names", SqlDbType.VarChar, 15));
            comando.Parameters["@Names"].Value = Name;

            command.Parameters.Add(new SqlParameter("@LastName", SqlDbType.VarChar, 30));
            comando.Parameters["@LastName"].Value = LastName;

            command.Parameters.Add(new SqlParameter("@Address", SqlDbType.VarChar, 50));
            comando.Parameters["@Address"].Value = Address;

            command.Parameters.Add(new SqlParameter("@Telephone", SqlDbType.Int));
            comando.Parameters["@Telephone"].Value = Telephone;

            command.Parameters.Add(new SqlParameter("@Mail", SqlDbType.VarChar, 30));
            comando.Parameters["@Mail"].Value = Mail;


            if ((herramientas.ejecutanonquery()) != 0)
            {
                MessageBox.Show("Datos Modificados", "Mensaje");
            }
            else
            {
                MessageBox.Show("datos incompletos");
            }
            comando.Connection.Close();
            herramientas.desconect();
        }
    }
    public CommandeletetUser(String Name, string LastName, String Address, string Mail, int Telephone)
    {
        if (herramientas.conect())
        {
            comando = herramientas.construye_command("delete from User where iduser=@iduser");// comando para eliminar

            comando.Parameters.Add(new SqlParameter("@iduser", SqlDbType.VarChar, 50));
            comando.Parameters["@iduser"].Value = combonombres2.Text;
            if ((herramientas.ejecutanonquery()) != 0)
            {
                return ("Registro eliminado", "Mensaje");

            }
            else
            {
                return ("No se elimino ningun registro", "Mensaje");
        
            }
            comando.Connection.Close();
        }
    }
}
