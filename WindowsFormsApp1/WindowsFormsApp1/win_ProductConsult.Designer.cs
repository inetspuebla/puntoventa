﻿namespace inets.UI.Logging
{
	partial class win_ProductConsult
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(win_ProductConsult));
			this.DataProduct = new System.Windows.Forms.DataGridView();
			this.txtproduct = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.cmbbuscar = new System.Windows.Forms.ComboBox();
			this.lbfiltro = new System.Windows.Forms.Label();
			this.cmbdepto = new System.Windows.Forms.ComboBox();
			this.btnBuscar = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.DataProduct)).BeginInit();
			this.SuspendLayout();
			// 
			// DataProduct
			// 
			this.DataProduct.AllowUserToAddRows = false;
			this.DataProduct.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
			dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
			dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
			this.DataProduct.DefaultCellStyle = dataGridViewCellStyle1;
			this.DataProduct.Location = new System.Drawing.Point(12, 104);
			this.DataProduct.Name = "DataProduct";
			this.DataProduct.Size = new System.Drawing.Size(462, 248);
			this.DataProduct.TabIndex = 0;
			this.DataProduct.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataProduct_CellDoubleClick);
			// 
			// txtproduct
			// 
			this.txtproduct.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtproduct.Location = new System.Drawing.Point(186, 38);
			this.txtproduct.Name = "txtproduct";
			this.txtproduct.Size = new System.Drawing.Size(121, 21);
			this.txtproduct.TabIndex = 1;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(9, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(70, 15);
			this.label1.TabIndex = 2;
			this.label1.Text = "Buscar por:";
			// 
			// cmbbuscar
			// 
			this.cmbbuscar.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cmbbuscar.FormattingEnabled = true;
			this.cmbbuscar.Location = new System.Drawing.Point(12, 36);
			this.cmbbuscar.Name = "cmbbuscar";
			this.cmbbuscar.Size = new System.Drawing.Size(121, 23);
			this.cmbbuscar.TabIndex = 3;
			this.cmbbuscar.SelectedIndexChanged += new System.EventHandler(this.cmbbuscar_SelectedIndexChanged);
			this.cmbbuscar.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbbuscar_KeyPress);
			// 
			// lbfiltro
			// 
			this.lbfiltro.AutoSize = true;
			this.lbfiltro.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbfiltro.Location = new System.Drawing.Point(183, 10);
			this.lbfiltro.Name = "lbfiltro";
			this.lbfiltro.Size = new System.Drawing.Size(103, 15);
			this.lbfiltro.TabIndex = 4;
			this.lbfiltro.Text = "Escribe producto:";
			// 
			// cmbdepto
			// 
			this.cmbdepto.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cmbdepto.FormattingEnabled = true;
			this.cmbdepto.Location = new System.Drawing.Point(186, 36);
			this.cmbdepto.Name = "cmbdepto";
			this.cmbdepto.Size = new System.Drawing.Size(121, 23);
			this.cmbdepto.TabIndex = 5;
			this.cmbdepto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbdepto_KeyPress);
			// 
			// btnBuscar
			// 
			this.btnBuscar.BackColor = System.Drawing.Color.Transparent;
			this.btnBuscar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnBuscar.BackgroundImage")));
			this.btnBuscar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.btnBuscar.FlatAppearance.BorderSize = 0;
			this.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnBuscar.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnBuscar.Location = new System.Drawing.Point(344, 36);
			this.btnBuscar.Name = "btnBuscar";
			this.btnBuscar.Size = new System.Drawing.Size(75, 23);
			this.btnBuscar.TabIndex = 6;
			this.btnBuscar.Text = "Buscar";
			this.btnBuscar.UseVisualStyleBackColor = false;
			this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
			// 
			// win_ProductConsult
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(486, 364);
			this.Controls.Add(this.btnBuscar);
			this.Controls.Add(this.lbfiltro);
			this.Controls.Add(this.cmbbuscar);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.DataProduct);
			this.Controls.Add(this.cmbdepto);
			this.Controls.Add(this.txtproduct);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "win_ProductConsult";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Consulta de Producto";
			this.Load += new System.EventHandler(this.win_ProductConsult_Load);
			((System.ComponentModel.ISupportInitialize)(this.DataProduct)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion
		private System.Windows.Forms.TextBox txtproduct;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox cmbbuscar;
		private System.Windows.Forms.Label lbfiltro;
		private System.Windows.Forms.ComboBox cmbdepto;
		private System.Windows.Forms.Button btnBuscar;
		public System.Windows.Forms.DataGridView DataProduct;
	}
}