﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Inets.Bussines.Estandard;
using Inets.Models.Entity_Entities;
using ExternalClass;

namespace inets.UI.Logging
{
	public partial class win_ProductConsult : Form
	{
		E_Departament e_Departament = new E_Departament();
		B_Departament b_Departament = new B_Departament();
		E_Product e_Product = new E_Product();
		B_Product b_Product = new B_Product();

		public Iproducts iproducts { get; set; }

		public string product;
		
		public win_ProductConsult()
		{
			InitializeComponent();
		}

		private void win_ProductConsult_Load(object sender, EventArgs e)
		{

			lbfiltro.Visible = false;
			cmbdepto.Visible = false;
			txtproduct.Visible = false;

			cmbbuscar.Items.Add("Departamento");
			cmbbuscar.Items.Add("Producto");

			cmbdepto.ValueMember = "Nombre de Departamento";
			cmbdepto.DataSource = b_Departament.SelectDepartment();
		}

		private void cmbbuscar_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbbuscar.SelectedIndex == 0)
			{
				txtproduct.Visible = false;
				lbfiltro.Visible = true;
				lbfiltro.Text = "Seleccione un departamento";
				cmbdepto.Visible = true;
			}
			else
			{
				cmbdepto.Visible = false;
				lbfiltro.Visible = true;
				lbfiltro.Text = "Escriba un producto";
				txtproduct.Visible = true;
				
			}	
		}

		private void btnBuscar_Click(object sender, EventArgs e)
		{
			string tabla, valor;

			if (cmbbuscar.SelectedIndex == 0)
			{
				if(cmbdepto.SelectedValue == null)
				{
					MessageBox.Show("Seleccione un departamento");
				}else
				{
					e_Departament.iIdDepartment = 0;
					e_Departament.sNameDepartment = cmbdepto.SelectedValue.ToString();
					e_Departament = b_Departament.SearchDepartament(e_Departament);
					e_Product.iIdDepartament = e_Departament.iIdDepartment;

					tabla = "Product.iIdDepartment";
					valor = e_Product.iIdDepartament.ToString();
					DataProduct.DataSource = b_Product.filtrosearch(tabla, valor);
					DataProduct.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
				}
			}
			else
			{
				DataProduct.DataSource = b_Product.SearchClave(txtproduct.Text);
				DataProduct.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
			}
			DataProduct.ReadOnly = true;
		}
		
		public void DataProduct_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
		{
			if(DataProduct.DataSource == null)
			{

			}else
			{
				string product = "Descripción " + DataProduct.CurrentRow.Cells[0].Value.ToString() + "\r\nPrecio $" + DataProduct.CurrentRow.Cells[2].Value.ToString();
				string upc = DataProduct.CurrentRow.Cells[1].Value.ToString();

				iproducts.ConsultProduct(product, upc);
			}
		}

		private void cmbbuscar_KeyPress(object sender, KeyPressEventArgs e)
		{
			
			e.Handled = true;
		}

		private void cmbdepto_KeyPress(object sender, KeyPressEventArgs e)
		{
			e.Handled = true;
		}
	}
}
