﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Inets.Models.Entity_Entities;
using Inets.Bussines.Estandard;
using ExternalClass;

namespace inets.UI.Logging
{
	public partial class win_MethodPay : Form, Iproducts
	{
		public win_MethodPay()
		{
			InitializeComponent();
		}
		
		public Iproducts iproducts { get; set; }

		public decimal Totality;
		public string employee, Autorization,autotization2, observation,observation2, note, note2, Tick, Store, cash, Method, Quantity;
		int index = -1;
		int iduseraut;
		bool valid = false;
		bool cancel = false;

		public List<E_MPTicket> l_MPTickets = new List<E_MPTicket>();
		public List<LineaProduct> lineaProducts;
		public E_JTicket e_jticket = new E_JTicket();
		public E_Cancellations e_Cancellation = new E_Cancellations();

		B_CompanyConvention b_CompanyConvention = new B_CompanyConvention();
		E_CompanyConvention e_CompanyConvention = new E_CompanyConvention();
		B_MPaymentChange b_MPaymentChange = new B_MPaymentChange();
		E_MPaymentChange e_MPaymentChange = new E_MPaymentChange();
		B_PaymentMethod b_PaymentMethod = new B_PaymentMethod();
		E_PaymentMethod e_PaymentMethod = new E_PaymentMethod();
		B_Cancellations b_Cancellations = new B_Cancellations();
		B_DetailTicket b_DetailTicket = new B_DetailTicket();
		E_DetailTicket e_DetailTicket = new E_DetailTicket();
		B_Denomination b_Denomination = new B_Denomination();
		E_Denomination e_Denomination = new E_Denomination();
		B_TCAplied b_TCApplicable = new B_TCAplied();
		E_TCAplied e_TCApplicable = new E_TCAplied();
		B_Employee b_Employee = new B_Employee();
		E_Employee e_Employee = new E_Employee();
		B_MPTicket b_MPTicket = new B_MPTicket();
		E_MPTicket e_MPTicket = new E_MPTicket();
		B_TypeCard b_TypeCard = new B_TypeCard();
		E_TypeCard e_TypeCard = new E_TypeCard();
		E_Ticket e_Ticket = new E_Ticket();
		B_Ticket b_Ticket = new B_Ticket();
		E_Store e_Store = new E_Store();
		B_Store b_Store = new B_Store();
		E_Users e_Users = new E_Users();
		B_Users b_Users = new B_Users();
		E_Cash e_Cash = new E_Cash();
		B_Cash b_Cash = new B_Cash();
		E_Card e_Card = new E_Card();
		B_Card b_Card = new B_Card();

		B_JStartofcash b_jstarofcash = new B_JStartofcash();
		win_Validation win_Validation = new win_Validation();

		//************************************************Funciones de las formas de pago
		//ROV 24/10/18 inicio de metodos de pago 
		private void Pago_Load(object sender, EventArgs e)
		{
			gpboxCheques.Visible = false;
			gpboxEfectivo.Visible = true;
			gpboxTarjeta.Visible = false;
			gpboxVales.Visible = false;


			lbticket.Text = Tick;
			lbtotal.Text = Totality.ToString();

			cmbformapago.ValueMember = "Nickname";
			cmbformapago.DataSource = b_MPaymentChange.visualizar2();
			
			//Efectivo
			txtmonto3.MaxLength = 11;

			//Vales
			txtNumVoucher.MaxLength = 2;

			//Cheque
			txtFolio1.MaxLength = 18;
			txtMonto1.MaxLength = 11;

			//Tarjeta
			txtAutorization.MaxLength = 10;
			txtfolio.MaxLength = 18;
			txtTarjeta.MaxLength = 4;
			txtmonto.MaxLength = 11;
			txtBank.MaxLength = 100;

			//ROV 18/10/18 en el dado caso que se genere una cancelacion parcial se valida si se hizo una forma de pago 
			if (l_MPTickets.Count == 0)
			{

			}
			else
			{
				int last = 0;
				//ROV 18/10/18 se inserta la lista y se manda a mostrar en el datagrid 
				foreach (var list in l_MPTickets)
				{
					b_MPTicket.insertlistMPTickets(list);

				}

				for (int x = 0; x < l_MPTickets.Count; x++)
				{
					last = l_MPTickets[x].idMPayment;
				}

				e_PaymentMethod.idMPayment = last;
				e_PaymentMethod = b_PaymentMethod.search(e_PaymentMethod);

				lbdeduct.Text = b_MPTicket.op_sum_monto(Totality).Replace("-", "");
				Totality = Convert.ToDecimal(lbdeduct.Text);
				lbtotalEffective.Text = Totality.ToString();
				decimal cantidad = Convert.ToDecimal(b_MPTicket.op_sumamount());

				Convert.ToDecimal(lbtotal.Text);

				datagridstyle();

				cmbformapago.Enabled = false;

				if (cantidad < Convert.ToDecimal(lbtotal.Text))
				{
					cmbformapago.Enabled = true;
					gpboxEfectivo.Visible = true;

					gpboxCheques.Visible = false;
					gpboxTarjeta.Visible = false;
					gpboxVales.Visible = false;

				}
				else if (cantidad == Convert.ToDecimal(lbtotal.Text))
				{
					gpboxCheques.Visible = false;
					gpboxEfectivo.Visible = false;
					gpboxTarjeta.Visible = false;
					gpboxVales.Visible = false;

				}
			}

		}

		//ROV 18/10/18 Seleccion de formas de pago 
		private void cmbformapago_SelectedIndexChanged(object sender, EventArgs e)
		{

			e_MPaymentChange.nickname = cmbformapago.SelectedValue.ToString();
			e_MPaymentChange = b_MPaymentChange.SearchNickname(e_MPaymentChange);

			e_PaymentMethod.idMPayment = e_MPaymentChange.idTypeChange;
			e_PaymentMethod = b_PaymentMethod.search(e_PaymentMethod);
			e_MPaymentChange.typechange = e_PaymentMethod.sNameMPayment;

			if (e_MPaymentChange.typepay == "Efectivo")
			{
				gpboxCheques.Visible = false;
				gpboxEfectivo.Visible = true;
				gpboxTarjeta.Visible = false;
				gpboxVales.Visible = false;
				label27.Text = e_MPaymentChange.typepay;

				lbtotalEffective.Text = Totality.ToString();

				if (e_MPaymentChange.activechange == false)
				{
					label24.Visible = false;
					label26.Visible = false;
					lbchange.Visible = false;
					lblimit.Visible = false;
				} else
				{
					lbchange.Text = e_PaymentMethod.sNameMPayment;
					lblimit.Text = e_MPaymentChange.dLimitvalue.ToString();
				}

			}
			else if (e_MPaymentChange.typepay == "Tarjeta")
			{
				gpboxCheques.Visible = false;
				gpboxEfectivo.Visible = false;
				gpboxTarjeta.Visible = true;
				gpboxVales.Visible = false;

				cmbcard.ValueMember = "sNameCard";
				cmbcard.DataSource = b_Card.visualize();
				cmbtypecard.ValueMember = "sNameTCard";
				cmbtypecard.DataSource = b_TypeCard.Visualize();

				lberror1.Visible = false;
				lberror2.Visible = false;
				lberror3.Visible = false;
				lberror4.Visible = lberror8.Visible = lberror9.Visible = false;
				label27.Text = e_MPaymentChange.typepay;

				if (e_MPaymentChange.activechange == false)
				{
					label23.Visible = false;
					label25.Visible = false;
					lbchange3.Visible = false; ;
					lblimit3.Visible = false; ;
				}
				else
				{
					lbchange3.Text = e_PaymentMethod.sNameMPayment;
					lblimit3.Text = e_MPaymentChange.dLimitvalue.ToString();
				}

			}
			else if (e_MPaymentChange.typepay == "Vales")
			{
				gpboxCheques.Visible = false;
				gpboxEfectivo.Visible = false;
				gpboxTarjeta.Visible = false;
				gpboxVales.Visible = true;
				label27.Text = e_MPaymentChange.typepay;

				cmbCompany.ValueMember = "NameCompany";
				txtmonto2.Enabled = false;
				cmbCompany.DataSource = b_CompanyConvention.visualizate();

				if (e_MPaymentChange.activechange == false)
				{
					label19.Visible = false;
					label31.Visible = false;
					lbchange1.Visible = false;
					lblimit1.Visible = false;
				}
				else
				{
					lbchange1.Text = e_PaymentMethod.sNameMPayment;
					lblimit1.Text = e_MPaymentChange.dLimitvalue.ToString();
				}
			}
			else if (e_MPaymentChange.typepay == "Cheques")
			{
				gpboxCheques.Visible = true;
				gpboxEfectivo.Visible = false;
				gpboxTarjeta.Visible = false;
				gpboxVales.Visible = false;
				lberror5.Visible = lberror6.Visible = lberror7.Visible = false;
				label27.Text = e_MPaymentChange.typepay;

				if (e_MPaymentChange.activechange == false)
				{
					label20.Visible = false;
					label22.Visible = false;
					lbchange2.Visible = false;
					lblimit2.Visible = false;
				}
				else
				{
					lbchange2.Text = e_PaymentMethod.sNameMPayment;
					lblimit2.Text = e_MPaymentChange.dLimitvalue.ToString();
				}

			}
		}

		//ROV 24/10/18 ROV 18/10/18 Seleccion de compañias con valores
		private void cmbCompany_SelectedIndexChanged(object sender, EventArgs e)
		{

			e_CompanyConvention.NameCompany = cmbCompany.SelectedValue.ToString();
			e_CompanyConvention = b_CompanyConvention.Search(e_CompanyConvention);

			e_Denomination.idCompany = e_CompanyConvention.idCompany;

			cmbDenominacion.ValueMember = "dValue";
			cmbDenominacion.DataSource = b_Denomination.visualizate(e_Denomination);

		}


		//************************************************Funciones de las formas de pago
		//ROV 24/10/18 Efectivo enter
		private void txtmonto3_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
			{
				e.Handled = true;
			}
			if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
			{
				e.Handled = true;
			}
			if ((int)e.KeyChar == (int)Keys.Enter)
			{

				if (txtmonto3.Text == "")
				{
					MessageBox.Show("Ingrese un valor");
				}
				else
				{
					if (txtmonto3.Text == "0" || Convert.ToDecimal(txtmonto3.Text) <= 0)
					{
						MessageBox.Show("Cantidad no valida");
					}
					else
					{
						if (Convert.ToDecimal(txtmonto3.Text) > Totality)
						{
							lbdeduct.Text = b_MPTicket.op_change(Totality, Convert.ToDecimal(txtmonto3.Text)).Replace("-", "");

							if (Convert.ToDecimal(lbdeduct.Text) <= Convert.ToDecimal(lblimit.Text) || Convert.ToDecimal(lblimit.Text) == 0)
							{
								//Se insertan los datos en su entidad

								e_PaymentMethod.sNameMPayment = e_MPaymentChange.typepay;
								e_PaymentMethod = b_PaymentMethod.searchid(e_PaymentMethod);
								
								e_MPTicket.idMPayment = e_PaymentMethod.idMPayment;
								e_MPTicket.dAmountPayable = Totality;
								e_MPTicket.Amountpay = Convert.ToDecimal(txtmonto3.Text);
								e_MPTicket.Methodpay = e_PaymentMethod.sNameMPayment;
								cancel = true;
								e_MPTicket.bValidate = cancel;

								//Se valida que ya se inserto la misma forma de pago
								if (b_MPTicket.validate(e_MPTicket.Methodpay) == true)
								{
									b_MPTicket.Incrementar(e_MPTicket.Methodpay, e_MPTicket.Amountpay);
									lbdeduct.Text = b_MPTicket.op_change(Totality, Convert.ToDecimal(txtmonto3.Text)).Replace("-", "");
								}
								else //Se inserta como un objeto nuevo 
								{
									b_MPTicket.insertlistMPTickets(e_MPTicket);
									lbdeduct.Text = b_MPTicket.op_change(Totality, Convert.ToDecimal(txtmonto3.Text)).Replace("-", "");
								}

								lbstatus.Text = "Cambio";
								txtmonto3.Enabled = false;
								cmbformapago.Enabled = false;
								gpboxEfectivo.Enabled = false;
								gpboxEfectivo.Visible = false;

								Totality = Convert.ToDecimal(lbdeduct.Text);

								e_TCApplicable.idMPTicket = e_MPTicket.idTicket;
								e_TCApplicable.idMPaymentChange = e_MPaymentChange.idMPaymentChange;
								e_TCApplicable.ValueChange = Convert.ToDecimal(lbdeduct.Text);
								

								e_Employee.sNameEmployee = employee;
								e_Store.sNameStore = Store;
								e_Cash.sNameCash = cash;

								//manda a imprimir el Tick de compra 
								printticket();
							}
							else if (Convert.ToDecimal(lbdeduct.Text) > Convert.ToDecimal(lblimit.Text))
							{
								MessageBox.Show("el pago que esta realizando es mayor al cambio que se puede compartir ");
								lbdeduct.Text = "0";
							}

						}
						else
						{
							//Se insertan los datos en su entidad
							e_PaymentMethod.sNameMPayment = e_MPaymentChange.typepay;
							e_PaymentMethod = b_PaymentMethod.searchid(e_PaymentMethod);
							
														
							e_MPTicket.idMPayment = e_PaymentMethod.idMPayment;
							e_MPTicket.dAmountPayable = Totality;
							e_MPTicket.Amountpay = Convert.ToDecimal(txtmonto3.Text);
							e_MPTicket.Methodpay = e_PaymentMethod.sNameMPayment;
							cancel = true;
							e_MPTicket.bValidate = cancel;

							//Se valida que ya se inserto la misma forma de pago
							if (b_MPTicket.validate(e_MPTicket.Methodpay) == true)
							{
								b_MPTicket.Incrementar(e_MPTicket.Methodpay, e_MPTicket.Amountpay);
								lbdeduct.Text = b_MPTicket.op_change(Totality, Convert.ToDecimal(txtmonto3.Text)).Replace("-", "");
							}
							else //Se inserta como un objeto nuevo 
							{
								b_MPTicket.insertlistMPTickets(e_MPTicket);
								lbdeduct.Text = b_MPTicket.op_change(Totality, Convert.ToDecimal(txtmonto3.Text)).Replace("-", "");
							}

							if (Totality > Convert.ToDecimal(txtmonto3.Text))
							{
								lbstatus.Text = "Resta";
								Totality = Convert.ToDecimal(lbdeduct.Text);

								datagridstyle();

							}
							else if (Totality == Convert.ToDecimal(txtmonto3.Text))
							{
								lbstatus.Text = "Cambio";
								txtmonto3.Enabled = false;
								cmbformapago.Enabled = false;
								gpboxEfectivo.Enabled = false;

								gpboxEfectivo.Visible = false;

								Totality = Convert.ToDecimal(lbdeduct.Text);
								

								// Se inserta datos 
								e_TCApplicable.idMPTicket = e_MPTicket.idMPTicket;
								e_TCApplicable.idMPaymentChange = e_MPaymentChange.idMPaymentChange;
								e_TCApplicable.ValueChange = Convert.ToDecimal(lbdeduct.Text);
								

								e_Employee.sNameEmployee = employee;
								e_Store.sNameStore = Store;
								e_Cash.sNameCash = cash;

								printticket();

							}

						}

						lbtotalEffective.Text = Totality.ToString();
						BtnCancelPrice.Visible = true;
						txtmonto3.Clear();
						index = -1;
					}
				}
			}
		}

		//ROV 24/10/18 Efectivo boton 
		private void Btnaplicar1_Click_1(object sender, EventArgs e)
		{
			if (txtmonto3.Text == "")
			{
				MessageBox.Show("Ingrese un valor");
			}
			else
			{
				if (txtmonto3.Text == "0" || Convert.ToDecimal(txtmonto3.Text) <= 0)
				{
					MessageBox.Show("Cantidad no valida");
				}
				else
				{
					if (Convert.ToDecimal(txtmonto3.Text) > Totality)
					{
						lbdeduct.Text = b_MPTicket.op_change(Totality, Convert.ToDecimal(txtmonto3.Text)).Replace("-", "");

						if (Convert.ToDecimal(lbdeduct.Text) <= Convert.ToDecimal(lblimit.Text) || Convert.ToDecimal(lblimit.Text) == 0)
						{
							//Se insertan los datos en su entidad

							e_PaymentMethod.sNameMPayment = e_MPaymentChange.typepay;
							e_PaymentMethod = b_PaymentMethod.searchid(e_PaymentMethod);
							

							e_MPTicket.idMPayment = e_PaymentMethod.idMPayment;
							e_MPTicket.dAmountPayable = Totality;
							e_MPTicket.Amountpay = Convert.ToDecimal(txtmonto3.Text);
							e_MPTicket.Methodpay = e_PaymentMethod.sNameMPayment;
							cancel = true;
							e_MPTicket.bValidate = cancel;

							//Se valida que ya se inserto la misma forma de pago
							if (b_MPTicket.validate(e_MPTicket.Methodpay) == true)
							{
								b_MPTicket.Incrementar(e_MPTicket.Methodpay, e_MPTicket.Amountpay);
								lbdeduct.Text = b_MPTicket.op_change(Totality, Convert.ToDecimal(txtmonto3.Text)).Replace("-", "");
							}
							else //Se inserta como un objeto nuevo 
							{
								b_MPTicket.insertlistMPTickets(e_MPTicket);
								lbdeduct.Text = b_MPTicket.op_change(Totality, Convert.ToDecimal(txtmonto3.Text)).Replace("-", "");
							}

							lbstatus.Text = "Cambio";
							txtmonto3.Enabled = false;
							cmbformapago.Enabled = false;
							gpboxEfectivo.Enabled = false;
							gpboxEfectivo.Visible = false;

							Totality = Convert.ToDecimal(lbdeduct.Text);
							

							e_Ticket.NumTicket = Tick;

							e_TCApplicable.idMPTicket = e_MPTicket.idTicket;
							e_TCApplicable.idMPaymentChange = e_MPaymentChange.idMPaymentChange;
							e_TCApplicable.ValueChange = Convert.ToDecimal(lbdeduct.Text);
							

							e_Employee.sNameEmployee = employee;
							e_Store.sNameStore = Store;
							e_Cash.sNameCash = cash;

							//manda a imprimir el Tick de compra 
							printticket();
						}
						else if (Convert.ToDecimal(lbdeduct.Text) > Convert.ToDecimal(lblimit.Text))
						{
							MessageBox.Show("el pago que esta realizando es mayor al cambio que se puede compartir ");
							lbdeduct.Text = "0";
						}

					}
					else
					{
						//Se insertan los datos en su entidad

						e_PaymentMethod.sNameMPayment = e_MPaymentChange.typepay;
						e_PaymentMethod = b_PaymentMethod.searchid(e_PaymentMethod);
						

						e_MPTicket.idMPayment = e_PaymentMethod.idMPayment;
						e_MPTicket.dAmountPayable = Totality;
						e_MPTicket.Amountpay = Convert.ToDecimal(txtmonto3.Text);
						e_MPTicket.Methodpay = e_PaymentMethod.sNameMPayment;
						cancel = true;
						e_MPTicket.bValidate = cancel;

						//Se valida que ya se inserto la misma forma de pago
						if (b_MPTicket.validate(e_MPTicket.Methodpay) == true)
						{
							b_MPTicket.Incrementar(e_MPTicket.Methodpay, e_MPTicket.Amountpay);
							lbdeduct.Text = b_MPTicket.op_change(Totality, Convert.ToDecimal(txtmonto3.Text)).Replace("-", "");
						}
						else //Se inserta como un objeto nuevo 
						{
							b_MPTicket.insertlistMPTickets(e_MPTicket);
							lbdeduct.Text = b_MPTicket.op_change(Totality, Convert.ToDecimal(txtmonto3.Text)).Replace("-", "");
						}

						if (Totality > Convert.ToDecimal(txtmonto3.Text))
						{
							lbstatus.Text = "Resta";
							Totality = Convert.ToDecimal(lbdeduct.Text);

							datagridstyle();

						}
						else if (Totality == Convert.ToDecimal(txtmonto3.Text))
						{
							lbstatus.Text = "Cambio";
							txtmonto3.Enabled = false;
							cmbformapago.Enabled = false;
							gpboxEfectivo.Enabled = false;

							gpboxEfectivo.Visible = false;

							Totality = Convert.ToDecimal(lbdeduct.Text);

							e_TCApplicable.idMPTicket = e_MPTicket.idMPTicket;
							e_TCApplicable.idMPaymentChange = e_MPaymentChange.idMPaymentChange;
							e_TCApplicable.ValueChange = Convert.ToDecimal(lbdeduct.Text);
							

							e_Employee.sNameEmployee = employee;
							e_Store.sNameStore = Store;
							e_Cash.sNameCash = cash;

							printticket();

						}

					}

					lbtotalEffective.Text = Totality.ToString();
					BtnCancelPrice.Visible = true;
					txtmonto3.Clear();
					index = -1;
				}
			}
		}


		//ROV 24/10/18 Vales enter
		private void txtNumVoucher_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
			{
				if ((int)e.KeyChar == (int)Keys.Enter)
				{
					if (txtNumVoucher.Text == "0")
					{
						MessageBox.Show("Cantidad no valida");
					}
					else
					{
						if (txtNumVoucher.Text == "")
						{
							MessageBox.Show("Ingrese cantidad de vales proporcionados");
						}
						else
						{
							txtmonto2.Text = b_MPTicket.op_calcdenomination(Convert.ToInt32(txtNumVoucher.Text), Convert.ToDecimal(cmbDenominacion.SelectedValue)).ToString();

							if (Convert.ToDecimal(txtmonto2.Text) > Totality)
							{
								lbdeduct.Text = b_MPTicket.op_change(Totality, Convert.ToDecimal(txtmonto2.Text)).Replace("-", "");

								if (Convert.ToDecimal(lbdeduct.Text) <= Convert.ToDecimal(lblimit1.Text) || Convert.ToDecimal(lblimit1.Text) == 0)
								{
									e_PaymentMethod.sNameMPayment = e_MPaymentChange.typepay;
									e_PaymentMethod = b_PaymentMethod.searchid(e_PaymentMethod);
									

									e_CompanyConvention.NameCompany = cmbCompany.SelectedValue.ToString();
									e_CompanyConvention = b_CompanyConvention.Search(e_CompanyConvention);

									e_Denomination.idCompany = e_CompanyConvention.idCompany;
									e_Denomination.dValue = Convert.ToDecimal(cmbDenominacion.SelectedValue);
									e_Denomination = b_Denomination.Search(e_Denomination);
									

									e_MPTicket.idMPayment = e_PaymentMethod.idMPayment;
									e_MPTicket.idDenomination = e_Denomination.idDenomination;
									e_MPTicket.dQuantity = Convert.ToInt32(txtNumVoucher.Text);
									e_MPTicket.dAmountPayable = Totality;
									e_MPTicket.Amountpay = Convert.ToDecimal(b_MPTicket.op_calcdenomination(Convert.ToInt32(txtNumVoucher.Text), Convert.ToDecimal(cmbDenominacion.SelectedValue)));
									e_MPTicket.Methodpay = e_PaymentMethod.sNameMPayment + " " + cmbDenominacion.SelectedValue.ToString();
									e_MPTicket.bValidate = true;

									b_MPTicket.insertlistMPTickets(e_MPTicket);

									txtmonto2.Text = b_MPTicket.op_calcdenomination(Convert.ToInt32(txtNumVoucher.Text), Convert.ToDecimal(cmbDenominacion.SelectedValue)).ToString();

									lbdeduct.Text = b_MPTicket.op_change(Totality, Convert.ToDecimal(txtmonto2.Text)).Replace("-", "");

									lbstatus.Text = "Cambio";
									txtmonto2.Enabled = false;
									cmbformapago.Enabled = false;
									gpboxVales.Enabled = false;
									gpboxVales.Visible = false;

									Totality = Convert.ToDecimal(lbdeduct.Text);
									

									e_Ticket.NumTicket = Tick;
									e_Ticket = b_Ticket.SearchNT(e_Ticket);

									e_TCApplicable.idMPTicket = e_MPTicket.idTicket;
									e_TCApplicable.idMPaymentChange = e_MPaymentChange.idMPaymentChange;
									e_TCApplicable.ValueChange = Convert.ToDecimal(lbdeduct.Text);
									

									e_Employee.sNameEmployee = employee;
									e_Store.sNameStore = Store;
									e_Cash.sNameCash = cash;

									//manda a imprimir el Tick de compra 
									printticket();
								}
								else if (Convert.ToDecimal(lbdeduct.Text) > Convert.ToDecimal(lblimit1.Text))
								{
									MessageBox.Show("el pago que esta realizando es mayor al cambio que se puede compartir ");
									lbdeduct.Text = "0";
								}


							}
							else
							{
								//Se insertan los datos en su entidad

								e_PaymentMethod.sNameMPayment = e_MPaymentChange.typepay;
								e_PaymentMethod = b_PaymentMethod.searchid(e_PaymentMethod);
								
								e_Ticket = b_Ticket.SearchNT(e_Ticket);

								e_CompanyConvention.NameCompany = cmbCompany.SelectedValue.ToString();
								e_CompanyConvention = b_CompanyConvention.Search(e_CompanyConvention);

								e_Denomination.idCompany = e_CompanyConvention.idCompany;
								e_Denomination.dValue = Convert.ToDecimal(cmbDenominacion.SelectedValue);
								e_Denomination = b_Denomination.Search(e_Denomination);
								
								e_MPTicket.idMPayment = e_PaymentMethod.idMPayment;
								e_MPTicket.idDenomination = e_Denomination.idDenomination;
								e_MPTicket.dQuantity = Convert.ToInt32(txtNumVoucher.Text);
								e_MPTicket.dAmountPayable = Totality;
								e_MPTicket.Amountpay = Convert.ToDecimal(b_MPTicket.op_calcdenomination(Convert.ToInt32(txtNumVoucher.Text), Convert.ToDecimal(cmbDenominacion.SelectedValue)));
								e_MPTicket.Methodpay = e_PaymentMethod.sNameMPayment + " " + cmbDenominacion.SelectedValue.ToString();
								e_MPTicket.bValidate = true;

								b_MPTicket.insertlistMPTickets(e_MPTicket);

								txtmonto2.Text = b_MPTicket.op_calcdenomination(Convert.ToInt32(txtNumVoucher.Text), Convert.ToDecimal(cmbDenominacion.SelectedValue)).ToString();

								lbdeduct.Text = b_MPTicket.op_change(Totality, Convert.ToDecimal(txtmonto2.Text)).Replace("-", "");


								if (Totality > Convert.ToDecimal(txtmonto2.Text))
								{
									lbstatus.Text = "Resta";
									Totality = Convert.ToDecimal(lbdeduct.Text);
								}
								else if (Totality == Convert.ToDecimal(txtmonto2.Text))
								{
									lbstatus.Text = "Cambio";
									txtNumVoucher.Enabled = false;
									txtmonto2.Enabled = false;
									cmbformapago.Enabled = false;
									gpboxVales.Enabled = false;
									gpboxVales.Visible = false;
									

									e_Ticket.NumTicket = Tick;

									e_TCApplicable.idMPTicket = e_MPTicket.idTicket;
									e_TCApplicable.idMPaymentChange = e_MPaymentChange.idMPaymentChange;
									e_TCApplicable.ValueChange = Convert.ToDecimal(lbdeduct.Text);
									
									printticket();
								}

							}

							BtnCancelPrice.Visible = true;
							txtNumVoucher.Clear();
							datagridstyle();
							index = -1;
						}
					}
				}
				e.Handled = true;
				return;
			}
		}

		//ROV 24/10/18 Vales boton
		private void btnaplicar2_Click(object sender, EventArgs e)
		{
			if (txtNumVoucher.Text == "0")
			{
				MessageBox.Show("Cantidad no valida");
			}
			else
			{
				if (txtNumVoucher.Text == "")
				{
					MessageBox.Show("Ingrese cantidad de vales proporcionados");
				}
				else
				{
					txtmonto2.Text = b_MPTicket.op_calcdenomination(Convert.ToInt32(txtNumVoucher.Text), Convert.ToDecimal(cmbDenominacion.SelectedValue)).ToString();

					if (Convert.ToDecimal(txtmonto2.Text) > Totality)
					{
						lbdeduct.Text = b_MPTicket.op_change(Totality, Convert.ToDecimal(txtmonto2.Text)).Replace("-", "");

						if (Convert.ToDecimal(lbdeduct.Text) <= Convert.ToDecimal(lblimit1.Text) || Convert.ToDecimal(lblimit1.Text) == 0)
						{
							e_PaymentMethod.sNameMPayment = e_MPaymentChange.typepay;
							e_PaymentMethod = b_PaymentMethod.searchid(e_PaymentMethod);
							

							e_CompanyConvention.NameCompany = cmbCompany.SelectedValue.ToString();
							e_CompanyConvention = b_CompanyConvention.Search(e_CompanyConvention);

							e_Denomination.idCompany = e_CompanyConvention.idCompany;
							e_Denomination.dValue = Convert.ToDecimal(cmbDenominacion.SelectedValue);
							e_Denomination = b_Denomination.Search(e_Denomination);
							
							e_MPTicket.idMPayment = e_PaymentMethod.idMPayment;
							e_MPTicket.idDenomination = e_Denomination.idDenomination;
							e_MPTicket.dQuantity = Convert.ToInt32(txtNumVoucher.Text);
							e_MPTicket.dAmountPayable = Totality;
							e_MPTicket.Amountpay = Convert.ToDecimal(b_MPTicket.op_calcdenomination(Convert.ToInt32(txtNumVoucher.Text), Convert.ToDecimal(cmbDenominacion.SelectedValue)));
							e_MPTicket.Methodpay = e_PaymentMethod.sNameMPayment + " " + cmbDenominacion.SelectedValue.ToString();
							e_MPTicket.bValidate = true;

							b_MPTicket.insertlistMPTickets(e_MPTicket);

							txtmonto2.Text = b_MPTicket.op_calcdenomination(Convert.ToInt32(txtNumVoucher.Text), Convert.ToDecimal(cmbDenominacion.SelectedValue)).ToString();

							lbdeduct.Text = b_MPTicket.op_change(Totality, Convert.ToDecimal(txtmonto2.Text)).Replace("-", "");

							lbstatus.Text = "Cambio";
							txtmonto2.Enabled = false;
							cmbformapago.Enabled = false;
							gpboxVales.Enabled = false;
							gpboxVales.Visible = false;

							Totality = Convert.ToDecimal(lbdeduct.Text);
							

							e_Ticket.NumTicket = Tick;

							e_TCApplicable.idMPTicket = e_MPTicket.idTicket;
							e_TCApplicable.idMPaymentChange = e_MPaymentChange.idMPaymentChange;
							e_TCApplicable.ValueChange = Convert.ToDecimal(lbdeduct.Text);
							

							e_Employee.sNameEmployee = employee;
							e_Store.sNameStore = Store;
							e_Cash.sNameCash = cash;

							//manda a imprimir el Tick de compra 
							printticket();
						}
						else if (Convert.ToDecimal(lbdeduct.Text) > Convert.ToDecimal(lblimit1.Text))
						{
							MessageBox.Show("el pago que esta realizando es mayor al cambio que se puede compartir ");
							lbdeduct.Text = "0";
						}


					}
					else
					{
						//Se insertan los datos en su entidad

						e_PaymentMethod.sNameMPayment = e_MPaymentChange.typepay;
						e_PaymentMethod = b_PaymentMethod.searchid(e_PaymentMethod);
						
						e_CompanyConvention.NameCompany = cmbCompany.SelectedValue.ToString();
						e_CompanyConvention = b_CompanyConvention.Search(e_CompanyConvention);

						e_Denomination.idCompany = e_CompanyConvention.idCompany;
						e_Denomination.dValue = Convert.ToDecimal(cmbDenominacion.SelectedValue);
						e_Denomination = b_Denomination.Search(e_Denomination);
						
						e_MPTicket.idMPayment = e_PaymentMethod.idMPayment;
						e_MPTicket.idDenomination = e_Denomination.idDenomination;
						e_MPTicket.dQuantity = Convert.ToInt32(txtNumVoucher.Text);
						e_MPTicket.dAmountPayable = Totality;
						e_MPTicket.Amountpay = Convert.ToDecimal(b_MPTicket.op_calcdenomination(Convert.ToInt32(txtNumVoucher.Text), Convert.ToDecimal(cmbDenominacion.SelectedValue)));
						e_MPTicket.Methodpay = e_PaymentMethod.sNameMPayment + " " + cmbDenominacion.SelectedValue.ToString();
						e_MPTicket.bValidate = true;

						b_MPTicket.insertlistMPTickets(e_MPTicket);

						txtmonto2.Text = b_MPTicket.op_calcdenomination(Convert.ToInt32(txtNumVoucher.Text), Convert.ToDecimal(cmbDenominacion.SelectedValue)).ToString();

						lbdeduct.Text = b_MPTicket.op_change(Totality, Convert.ToDecimal(txtmonto2.Text)).Replace("-", "");


						if (Totality > Convert.ToDecimal(txtmonto2.Text))
						{
							lbstatus.Text = "Resta";
							Totality = Convert.ToDecimal(lbdeduct.Text);
						}
						else if (Totality == Convert.ToDecimal(txtmonto2.Text))
						{
							lbstatus.Text = "Cambio";
							txtNumVoucher.Enabled = false;
							txtmonto2.Enabled = false;
							cmbformapago.Enabled = false;
							gpboxVales.Enabled = false;
							gpboxVales.Visible = false;
							

							e_Ticket.NumTicket = Tick;

							e_TCApplicable.idMPTicket = e_MPTicket.idTicket;
							e_TCApplicable.idMPaymentChange = e_MPaymentChange.idMPaymentChange;
							e_TCApplicable.ValueChange = Convert.ToDecimal(lbdeduct.Text);
							
							printticket();
						}

					}

					BtnCancelPrice.Visible = true;
					txtNumVoucher.Clear();
					datagridstyle();
					index = -1;
				}
			}
		}


		//ROV 24/10/18 Cheques enter
		private void txtMonto1_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
			{
				e.Handled = true;
			}
			if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
			{
				e.Handled = true;
			}
			if ((int)e.KeyChar == (int)Keys.Enter)
			{
				lberror5.Visible = lberror6.Visible = lberror7.Visible = false;

				if (txtMonto1.Text == "")
				{
					MessageBox.Show("Cantidad no valida");
				}
				else
				{
					if (txtMonto1.Text == "0" || Convert.ToDecimal(txtMonto1.Text) <= 0) //ROV 18/10/18 SE VALIDA QUE EL MONTO NO SEA MENOR O IGUAL A 0
					{
						MessageBox.Show("Cantidad no valida");
					}
					else
					{
						if (txtBank.Text == "" || txtFolio1.Text == "" || txtMonto1.Text == "") //ROV 18/10/18 Se valida que no esten vacio los campos 
						{
							MessageBox.Show("Validar que los datos esten completos");

							if (txtBank.Text == "")
							{
								lberror5.Visible = true;
							}
							if (txtFolio1.Text == "")
							{
								lberror6.Visible = true;
							}
							if (txtMonto1.Text == "")
							{
								lberror7.Visible = true;
							}

						}
						else
						{
							if (Convert.ToDecimal(txtMonto1.Text) > Totality)
							{
								lbdeduct.Text = b_MPTicket.op_change(Totality, Convert.ToDecimal(txtMonto1.Text)).Replace("-", "");

								if (Convert.ToDecimal(lbdeduct.Text) <= Convert.ToDecimal(lblimit2.Text) || Convert.ToDecimal(lblimit2.Text) == 0)
								{
									//ROV 18/10/18 Se buscan datos y se empiezazan a generar el metodo de pago de la compra 

									e_PaymentMethod.sNameMPayment = e_MPaymentChange.typepay;
									e_PaymentMethod = b_PaymentMethod.searchid(e_PaymentMethod);
									
									e_MPTicket.idMPayment = e_PaymentMethod.idMPayment;
									e_MPTicket.sBank = txtBank.Text;
									e_MPTicket.sFolio = txtFolio1.Text;
									e_MPTicket.dDateEmition = dtimemision.Text;
									e_MPTicket.dAmountPayable = Totality;
									e_MPTicket.Amountpay = Convert.ToDecimal(txtMonto1.Text);
									e_MPTicket.Methodpay = e_PaymentMethod.sNameMPayment;

									e_MPTicket.bValidate = true;

									b_MPTicket.insertlistMPTickets(e_MPTicket);//ROV 18/10/18 Se inserta a la lista cada metodo generado 

									lbdeduct.Text = b_MPTicket.op_change(Totality, Convert.ToDecimal(txtMonto1.Text)).Replace("-", "");

									lbstatus.Text = "Cambio";
									txtMonto1.Enabled = false;
									cmbformapago.Enabled = false;
									gpboxCheques.Enabled = false;
									gpboxCheques.Visible = false;
									

									e_Ticket.NumTicket = Tick;

									e_TCApplicable.idMPTicket = e_MPTicket.idTicket;
									e_TCApplicable.idMPaymentChange = e_MPaymentChange.idMPaymentChange;
									e_TCApplicable.ValueChange = Convert.ToDecimal(lbdeduct.Text);
									
									printticket();
								}
								else if (Convert.ToDecimal(lbdeduct.Text) > Convert.ToDecimal(lblimit2.Text))
								{
									MessageBox.Show("el pago que esta realizando es mayor al cambio que se puede compartir ");
									lbdeduct.Text = "0";
								}


							}
							else
							{
								//ROV 18/10/18 Se buscan datos y se empiezazan a generar el metodo de pago de la compra 

								e_PaymentMethod.sNameMPayment = e_MPaymentChange.typepay;
								e_PaymentMethod = b_PaymentMethod.searchid(e_PaymentMethod);
								

								e_MPTicket.idMPayment = e_PaymentMethod.idMPayment;
								e_MPTicket.sBank = txtBank.Text;
								e_MPTicket.sFolio = txtFolio1.Text;
								e_MPTicket.dDateEmition = dtimemision.Text;
								e_MPTicket.dAmountPayable = Totality;
								e_MPTicket.Amountpay = Convert.ToDecimal(txtMonto1.Text);
								e_MPTicket.Methodpay = e_PaymentMethod.sNameMPayment;

								e_MPTicket.bValidate = true;

								b_MPTicket.insertlistMPTickets(e_MPTicket);//ROV 18/10/18 Se inserta a la lista cada metodo generado 

								lbdeduct.Text = b_MPTicket.op_change(Totality, Convert.ToDecimal(txtMonto1.Text)).Replace("-", "");

								if (Totality > Convert.ToDecimal(txtMonto1.Text))//ROV 18/10/18 valida que sea menor que el Totality
								{
									lbstatus.Text = "Resta";
									Totality = Convert.ToDecimal(lbdeduct.Text);
								}
								else if (Totality == Convert.ToDecimal(txtMonto1.Text)) //ROV 18/10/18 valida que pueda ser igual 
								{
									lbstatus.Text = "Cambio";
									txtMonto1.Enabled = false;
									cmbformapago.Enabled = false;
									gpboxCheques.Enabled = false;
									gpboxCheques.Visible = false;
									

									e_Ticket.NumTicket = Tick;

									e_TCApplicable.idMPTicket = e_MPTicket.idTicket;
									e_TCApplicable.idMPaymentChange = e_MPaymentChange.idMPaymentChange;
									e_TCApplicable.ValueChange = Convert.ToDecimal(lbdeduct.Text);

									printticket();

								}

							}
							BtnCancelPrice.Visible = true;
							txtBank.Text = txtFolio1.Text = txtMonto1.Text = "";
							lberror5.Visible = lberror6.Visible = lberror7.Visible = false;
							datagridstyle();
							index = -1;
						}
					}
				}
			}
		}

		//ROV 24/10/18 Cheques boton 
		private void btnaplicar3_Click(object sender, EventArgs e)
		{
			lberror5.Visible = lberror6.Visible = lberror7.Visible = false;

			if (txtBank.Text == "" || txtFolio1.Text == "" || txtMonto1.Text == "") //ROV 18/10/18 Se valida que no esten vacio los campos 
			{
				MessageBox.Show("Validar que los datos esten completos");

				if (txtBank.Text == "")
				{
					lberror5.Visible = true;
				}
				if (txtFolio1.Text == "")
				{
					lberror6.Visible = true;
				}
				if (txtMonto1.Text == "")
				{
					lberror7.Visible = true;
				}

			}
			else
			{
				if (txtMonto1.Text == "")
				{
					MessageBox.Show("Cantidad no valida");
				}
				else
				{
					if (txtMonto1.Text == "0" || Convert.ToDecimal(txtMonto1.Text) <= 0) //ROV 18/10/18 SE VALIDA QUE EL MONTO NO SEA MENOR O IGUAL A 0
					{
						MessageBox.Show("Cantidad no valida");
					}
					else
					{
						{//ROV 18/10/18 Se buscan datos y se empiezazan a generar el metodo de pago de la compra 
							if (Convert.ToDecimal(txtMonto1.Text) > Totality)
							{
								lbdeduct.Text = b_MPTicket.op_change(Totality, Convert.ToDecimal(txtMonto1.Text)).Replace("-", "");

								if (Convert.ToDecimal(lbdeduct.Text) <= Convert.ToDecimal(lblimit2.Text) || Convert.ToDecimal(lblimit2.Text) == 0)
								{
									//ROV 18/10/18 Se buscan datos y se empiezazan a generar el metodo de pago de la compra 

									e_PaymentMethod.sNameMPayment = e_MPaymentChange.typepay;
									e_PaymentMethod = b_PaymentMethod.searchid(e_PaymentMethod);
									
								
									e_MPTicket.idMPayment = e_PaymentMethod.idMPayment;
									e_MPTicket.sBank = txtBank.Text;
									e_MPTicket.sFolio = txtFolio1.Text;
									e_MPTicket.dDateEmition = dtimemision.Text;
									e_MPTicket.dAmountPayable = Totality;
									e_MPTicket.Amountpay = Convert.ToDecimal(txtMonto1.Text);
									e_MPTicket.Methodpay = e_PaymentMethod.sNameMPayment;

									e_MPTicket.bValidate = true;

									b_MPTicket.insertlistMPTickets(e_MPTicket);//ROV 18/10/18 Se inserta a la lista cada metodo generado 

									lbdeduct.Text = b_MPTicket.op_change(Totality, Convert.ToDecimal(txtMonto1.Text)).Replace("-", "");

									lbstatus.Text = "Cambio";
									txtMonto1.Enabled = false;
									cmbformapago.Enabled = false;
									gpboxCheques.Enabled = false;
									gpboxCheques.Visible = false;
									
									e_Ticket.NumTicket = Tick;

									e_TCApplicable.idMPTicket = e_MPTicket.idTicket;
									e_TCApplicable.idMPaymentChange = e_MPaymentChange.idMPaymentChange;
									e_TCApplicable.ValueChange = Convert.ToDecimal(lbdeduct.Text);
									
									printticket();
								}
								else if (Convert.ToDecimal(lbdeduct.Text) > Convert.ToDecimal(lblimit2.Text))
								{
									MessageBox.Show("el pago que esta realizando es mayor al cambio que se puede compartir ");
									lbdeduct.Text = "0";
								}


							}
							else
							{
								//ROV 18/10/18 Se buscan datos y se empiezazan a generar el metodo de pago de la compra 

								e_PaymentMethod.sNameMPayment = e_MPaymentChange.typepay;
								e_PaymentMethod = b_PaymentMethod.searchid(e_PaymentMethod);
								

								e_MPTicket.idMPayment = e_PaymentMethod.idMPayment;
								e_MPTicket.sBank = txtBank.Text;
								e_MPTicket.sFolio = txtFolio1.Text;
								e_MPTicket.dDateEmition = dtimemision.Text;
								e_MPTicket.dAmountPayable = Totality;
								e_MPTicket.Amountpay = Convert.ToDecimal(txtMonto1.Text);
								e_MPTicket.Methodpay = e_PaymentMethod.sNameMPayment;

								e_MPTicket.bValidate = true;

								b_MPTicket.insertlistMPTickets(e_MPTicket);//ROV 18/10/18 Se inserta a la lista cada metodo generado 

								lbdeduct.Text = b_MPTicket.op_change(Totality, Convert.ToDecimal(txtMonto1.Text)).Replace("-", "");

								if (Totality > Convert.ToDecimal(txtMonto1.Text))//ROV 18/10/18 valida que sea menor que el Totality
								{
									lbstatus.Text = "Resta";
									Totality = Convert.ToDecimal(lbdeduct.Text);
								}
								else if (Totality == Convert.ToDecimal(txtMonto1.Text)) //ROV 18/10/18 valida que pueda ser igual 
								{
									lbstatus.Text = "Cambio";
									txtMonto1.Enabled = false;
									cmbformapago.Enabled = false;
									gpboxCheques.Enabled = false;
									gpboxCheques.Visible = false;
									

									e_Ticket.NumTicket = Tick;
								

									e_TCApplicable.idMPTicket = e_MPTicket.idTicket;
									e_TCApplicable.idMPaymentChange = e_MPaymentChange.idMPaymentChange;
									e_TCApplicable.ValueChange = Convert.ToDecimal(lbdeduct.Text);
									
									printticket();

								}

							}
							BtnCancelPrice.Visible = true;
							txtBank.Text = txtFolio1.Text = txtMonto1.Text = "";
							lberror5.Visible = lberror6.Visible = lberror7.Visible = false;
							datagridstyle();
							index = -1;
						}
					}
				}
			}
		}


		//ROV 24/10/18 Tarjeta enter
		private void txtmonto_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
			{
				e.Handled = true;
			}
			if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
			{
				e.Handled = true;
			}
			if ((int)e.KeyChar == (int)Keys.Enter)
			{
				lberror1.Visible = lberror2.Visible = lberror3.Visible = lberror4.Visible = lberror8.Visible = lberror9.Visible = false;

				if (txtAutorization.Text == "" || txtfolio.Text == "" || txtTarjeta.Text == "") //ROV 18/10/18 Se valida que no existan campos vacios para mostrar los puntos importantes o faltantes a llenar 
				{
					MessageBox.Show("Validar que los campos esten completos");

					if (txtAutorization.Text == "")
					{
						lberror1.Visible = true;
					}
					if (txtfolio.Text == "")
					{
						lberror2.Visible = true;
					}
					if (txtTarjeta.Text == "")
					{
						lberror3.Visible = true;
					}
					if (txtmonto.Text == "")
					{
						lberror4.Visible = true;
					}
				}
				else
				{
					if (txtmonto.Text == "")
					{
						MessageBox.Show("Cantidad no valida");
					}
					else
					{
						if (txtmonto.Text == "0" || Convert.ToDecimal(txtmonto.Text) <= 0) //ROV 18/10/18Se valida que no este vacio esos campos 
						{
							MessageBox.Show("Cantidad no valida");
						}
						else
						{
							if (Totality < Convert.ToDecimal(txtmonto.Text))//ROV 18/10/18 Se valida que no la cantidad no sea mayor que el Totality 
							{
								MessageBox.Show("No se puede generar transaccion favor de ingresar un valor igual o menos");
							}
							else
							{
								//ROV 18/10/18 Se realiza busqueda y validacion dedatos para metodo de pago
								e_PaymentMethod.sNameMPayment = e_MPaymentChange.typepay;
								e_PaymentMethod = b_PaymentMethod.searchid(e_PaymentMethod);
								

								e_Card.sNameCard = cmbcard.SelectedValue.ToString();
								e_Card = b_Card.search(e_Card);

								e_TypeCard.sNameTCard = cmbtypecard.SelectedValue.ToString();
								e_TypeCard = b_TypeCard.search(e_TypeCard);
								
								e_MPTicket.idMPayment = e_PaymentMethod.idMPayment;
								e_MPTicket.idCard = e_Card.idCard;
								e_MPTicket.idTypeCard = e_TypeCard.idTCard;
								e_MPTicket.iAuthorizacion = txtAutorization.Text;
								e_MPTicket.iNumCard = Convert.ToInt32(txtTarjeta.Text);
								e_MPTicket.dAmountPayable = Totality;
								e_MPTicket.Amountpay = Convert.ToDecimal(txtmonto.Text);
								e_MPTicket.Methodpay = e_PaymentMethod.sNameMPayment + " " + cmbtypecard.SelectedValue.ToString();
								e_MPTicket.bValidate = true;

								//ROV 18/10/18 Se inserta a la lista los metodos de pago 
								b_MPTicket.insertlistMPTickets(e_MPTicket);


								lbdeduct.Text = b_MPTicket.op_change(Totality, Convert.ToDecimal(txtmonto.Text)).Replace("-", "");

								//ROV 18/10/18 validacion que el menor no sea menor
								if (Totality > Convert.ToDecimal(txtmonto.Text))
								{
									lbstatus.Text = "Resta";
									Totality = Convert.ToDecimal(lbdeduct.Text);
								}
								//ROV 18/10/18 Se valida que sean de la misma cantidad 
								else if (Totality == Convert.ToDecimal(txtmonto.Text))
								{

									lbstatus.Text = "Cambio";
									txtmonto.Enabled = false;
									cmbformapago.Enabled = false;
									gpboxTarjeta.Enabled = false;
									

									e_Ticket.NumTicket = Tick;

									e_TCApplicable.idMPTicket = e_MPTicket.idTicket;
									e_TCApplicable.idMPaymentChange = e_MPaymentChange.idMPaymentChange;
									e_TCApplicable.ValueChange = Convert.ToDecimal(lbdeduct.Text);
									
									printticket();

								}
								BtnCancelPrice.Visible = true;
								txtAutorization.Text = txtfolio.Text = txtTarjeta.Text = txtmonto.Text = "";
								lberror1.Visible = lberror2.Visible = lberror3.Visible = lberror4.Visible = false;
								datagridstyle();

							}
						}
					}
				}
				index = -1;
			}
		}

		//ROV 24/10/18 Tarjeta boton
		private void btnaplicar4_Click(object sender, EventArgs e)
		{
			lberror1.Visible = lberror2.Visible = lberror3.Visible = lberror4.Visible = lberror8.Visible = lberror9.Visible = false;

			if (txtAutorization.Text == "" || txtfolio.Text == "" || txtTarjeta.Text == "") //ROV 18/10/18 Se valida que no existan campos vacios para mostrar los puntos importantes o faltantes a llenar 
			{
				MessageBox.Show("Validar que los campos esten completos");

				if (txtAutorization.Text == "")
				{
					lberror1.Visible = true;
				}
				if (txtfolio.Text == "")
				{
					lberror2.Visible = true;
				}
				if (txtTarjeta.Text == "")
				{
					lberror3.Visible = true;
				}
				if (txtmonto.Text == "")
				{
					lberror4.Visible = true;
				}
			}
			else
			{
				if (txtmonto.Text == "")
				{
					MessageBox.Show("Cantidad no valida");
				}
				else
				{
					if (txtmonto.Text == "0" || Convert.ToDecimal(txtmonto.Text) <= 0) //ROV 18/10/18Se valida que no este vacio esos campos 
					{
						MessageBox.Show("Cantidad no valida");
					}
					else
					{
						if (Totality < Convert.ToDecimal(txtmonto.Text))//ROV 18/10/18 Se valida que no la cantidad no sea mayor que el Totality 
						{
							MessageBox.Show("No se puede generar transaccion favor de ingresar un valor igual o menos");
						}
						else
						{
							if (Totality < Convert.ToDecimal(txtmonto.Text))//ROV 18/10/18 Se valida que no la cantidad no sea mayor que el Totality 
							{
								MessageBox.Show("No se puede generar transaccion favor de ingresar un valor igual o menos");
							}
							else
							{
								//ROV 18/10/18 Se realiza busqueda y validacion dedatos para metodo de pago
								e_PaymentMethod.sNameMPayment = e_MPaymentChange.typepay;
								e_PaymentMethod = b_PaymentMethod.searchid(e_PaymentMethod);
								

								e_Card.sNameCard = cmbcard.SelectedValue.ToString();
								e_Card = b_Card.search(e_Card);

								e_TypeCard.sNameTCard = cmbtypecard.SelectedValue.ToString();
								e_TypeCard = b_TypeCard.search(e_TypeCard);
								
								e_MPTicket.idMPayment = e_PaymentMethod.idMPayment;
								e_MPTicket.idCard = e_Card.idCard;
								e_MPTicket.idTypeCard = e_TypeCard.idTCard;
								e_MPTicket.iAuthorizacion = txtAutorization.Text;
								e_MPTicket.iNumCard = Convert.ToInt32(txtTarjeta.Text);
								e_MPTicket.dAmountPayable = Totality;
								e_MPTicket.Amountpay = Convert.ToDecimal(txtmonto.Text);
								e_MPTicket.Methodpay = e_PaymentMethod.sNameMPayment + " " + cmbtypecard.SelectedValue.ToString();
								e_MPTicket.bValidate = true;

								//ROV 18/10/18 Se inserta a la lista los metodos de pago 
								b_MPTicket.insertlistMPTickets(e_MPTicket);


								lbdeduct.Text = b_MPTicket.op_change(Totality, Convert.ToDecimal(txtmonto.Text)).Replace("-", "");

								//ROV 18/10/18 validacion que el menor no sea menor
								if (Totality > Convert.ToDecimal(txtmonto.Text))
								{
									lbstatus.Text = "Resta";
									Totality = Convert.ToDecimal(lbdeduct.Text);
								}
								//ROV 18/10/18 Se valida que sean de la misma cantidad 
								else if (Totality == Convert.ToDecimal(txtmonto.Text))
								{

									lbstatus.Text = "Cambio";
									txtmonto.Enabled = false;
									cmbformapago.Enabled = false;
									gpboxTarjeta.Enabled = false;
									

									e_Ticket.NumTicket = Tick;

									e_TCApplicable.idMPTicket = e_MPTicket.idTicket;
									e_TCApplicable.idMPaymentChange = e_MPaymentChange.idMPaymentChange;
									e_TCApplicable.ValueChange = Convert.ToDecimal(lbdeduct.Text);
									
									printticket();

								}
								BtnCancelPrice.Visible = true;
								txtAutorization.Text = txtfolio.Text = txtTarjeta.Text = txtmonto.Text = "";
								lberror1.Visible = lberror2.Visible = lberror3.Visible = lberror4.Visible = false;
								datagridstyle();
								index = -1;
							}
						}
					}
				}
			}
		}
		
		//************************************************Funciones de las formas de pago
		
		//ROV 24/10/18 Boton para cerrar la ventana 
		private void btnnewsale_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		//ROV 18/10/18 btn para cancelar forma de pago
		private void BtnCancelPrice_Click(object sender, EventArgs e)
		{
			E_MPTicket emptick = new E_MPTicket();
			win_Validation = new win_Validation();

			if (datapagos.DataSource == null)
			{
				MessageBox.Show("Ingrese una forma de pago ");
			}
			else
			{
				if (index == -1)
				{
					MessageBox.Show("Seleccione una forma de pago ");
				}
				else
				{
					if (index == datapagos.CurrentRow.Index)
					{
						DialogResult result = MessageBox.Show("¿Quiere cancelar la forma de pago? ", "", MessageBoxButtons.OKCancel);

						if (result == DialogResult.OK)
						{
							b_MPTicket.cancelmethodpay(index, Method);
							
							datagridstyle();

							cmbformapago.ValueMember = "Nickname";
							cmbformapago.DataSource = b_MPaymentChange.visualizar2();

							lbdeduct.Text = b_MPTicket.op_cancel(Totality, Convert.ToDecimal(Quantity));
							Totality = Convert.ToDecimal(b_MPTicket.op_cancel(Totality, Convert.ToDecimal(Quantity)));
							lbtotalEffective.Text = Totality.ToString();

							lbstatus.Text = "Resta";

							//ROV 18/10/18 manipulacion de controles 
							cmbformapago.Enabled = true;

							txtmonto.Enabled = true;
							txtMonto1.Enabled = true;
							txtmonto3.Enabled = true;
							txtNumVoucher.Enabled = true;

							gpboxEfectivo.Visible = true;
							gpboxCheques.Visible = false;
							gpboxTarjeta.Visible = false;
							gpboxVales.Visible = false;

							gpboxCheques.Enabled = true;
							gpboxEfectivo.Enabled = true;
							gpboxTarjeta.Enabled = true;
							gpboxVales.Enabled = true;

							valid = true;

						}
						else
						{

						}
					}
				}
			}
			index = -1;

		}

		//ROV 18/10/18 al seleccionar una opcion del combobox para activar formas de cancelar 
		private void btnCticket_Click(object sender, EventArgs e)
		{
			l_MPTickets = b_MPTicket.Visualize();

			if (l_MPTickets.Count > 0)//ROV 18/10/18 se valida que no se tenga formas de pago generados 
			{
				MessageBox.Show("Cancelar la forma de pago antes de cancelar Tick de compra");
			}
			else
			{
				e_Ticket.NumTicket = Tick;
				
				e_Cancellation.ticket = Tick;
				e_Cancellation.total = Convert.ToDecimal(Totality);
				e_Cancellation.store = Store;

				win_Validation.l_MPTickets = b_MPTicket.Visualize();

				win_Validation.lineaProducts = b_DetailTicket.visualizalist();

				//ROV 18/10/18 recibe datos y las envia a la ventana de validacion 
				win_Validation.l_MPTickets = b_MPTicket.Visualize();
				win_Validation.lineaProducts = lineaProducts;
				win_Validation.number = 3;
				win_Validation.iproducts = this;
				win_Validation.ShowDialog();

			}
		}

		//ROV 24/10/18 Boton para regresar la Tick
		private void btnback_Click(object sender, EventArgs e)
		{
			l_MPTickets = b_MPTicket.Visualize();
			iproducts.ConsultMpticket(l_MPTickets);

			this.Close();
		}

		//ROV 18/10/18 Te genera el indice del valor que esta seleccionando en el datagrid 
		private void datapagos_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
		{

			index = datapagos.CurrentRow.Index;

			Method = datapagos.CurrentRow.Cells[0].Value.ToString();
			Quantity = datapagos.CurrentRow.Cells[1].Value.ToString();

			datapagos.Rows[index].Selected = true;
		}

		//Validacion del DATETIME
		private void dtimemision_ValueChanged(object sender, EventArgs e)
		{
			DateTime fecha = DateTime.Now.Date;
			DateTime limit = DateTime.Now.AddDays(-16);

			if (dtimemision.Value > fecha)
			{
				dtimemision.Value = DateTime.Now.Date;
				MessageBox.Show("Fecha no permitida");
			} else if (dtimemision.Value < limit)
			{
				dtimemision.Value = DateTime.Now.Date;
				MessageBox.Show("Fecha no permitida");
			}
		}

		//ROV 18/10/18 Visualizar el datagridview
		public void datagridstyle()
		{
			DataGridViewCheckBoxColumn check = new DataGridViewCheckBoxColumn();

			var item = from x in b_MPTicket.Visualize()
					   where x.Amountpay > 0
					   select x;
			
			if (item == null)
			{
				datapagos.DataSource = null;
			}
			else
			{
				datapagos.DataSource = item.Select(x => new
				{	
					x.Methodpay,
					x.Amountpay

				}).ToList();

				datapagos.Columns["Methodpay"].HeaderText = "Método de pago";
				datapagos.Columns["Amountpay"].HeaderText = "Monto pagado";

				datapagos.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
				datapagos.CellBorderStyle = DataGridViewCellBorderStyle.None;
				index = -1;
			}
		}
		

		//************************************************Funciones de ticket Y JSON
		//ROV 24/10/18 FUNCION PARA IMPRIMIR TICKET
		public void printticket()
		{
			win_Validation = new win_Validation();

			e_Employee = new E_Employee();
			B_formats b_Tickets = new B_formats();
			gpboxCheques.Visible = false;
			gpboxEfectivo.Visible = true;
			gpboxTarjeta.Visible = false;
			gpboxVales.Visible = false;

			//prepara datos para enviar 
			e_Employee.sNameEmployee = employee;
			e_Store.sNameStore = Store;
			e_Cash.sNameCash = cash;

			if(valid== true)
			{
				win_Validation.number = 2;
				win_Validation.l_MPTickets = b_MPTicket.Visualize();
				win_Validation.ticket = lbticket.Text;
				win_Validation.e_Cancellation = e_Cancellation;

				win_Validation.iproducts = this;

				win_Validation.ShowDialog();
				
			}else
			{
				//manda a imprimir el Tick de compra 
				b_Tickets.PrintTicket(e_Store, e_Employee, e_Cash, Tick, Convert.ToDecimal(lbdeduct.Text), Convert.ToDecimal(lbtotal.Text), lineaProducts, b_MPTicket.Visualize(), e_TCApplicable);

				iproducts.Cancelticket();

				iproducts.Change(Convert.ToDecimal(lbdeduct.Text), Tick);

				insertjson();

				b_MPTicket.clear();

				this.Close();

			}

		}

		//ROV 29/10/18 FUNCION PARA JSON
		public void insertjson()
		{
			BJTicket bJTicket = new BJTicket();
			B_Employee b_Employee = new B_Employee();
			E_Employee e_Employee = new E_Employee();

			e_Store.sNameStore = Store;
			e_Store = b_Store.SearchStoreName(e_Store);

			e_jticket.NumberTicket = Tick;
			e_jticket.Datemonday = DateTime.Now.ToShortDateString();
			e_jticket.hour = DateTime.Now.ToShortTimeString();
			e_jticket.TotalPay = Convert.ToDecimal(lbtotal.Text);
			e_jticket.Change = Convert.ToDecimal(lbdeduct.Text);

			e_jticket.jstore = new jStore()
			{
				idStore = e_Store.iIdStore,
				namestore = e_Store.sNameStore,
				address = e_Store.sAddress,
				phone = e_Store.sTelephon,
				RFC = e_Store.sRFC,
				email = e_Store.sEmail
			};


			e_jticket.jproduct = new List<jProducts>();

			foreach (var line in lineaProducts)
			{
				e_jticket.jproduct.Add(new jProducts
				{
					idproduct = line.IdProduct,
					Quantity = line.Quantity,
					Nameproduct = line.Product,
					PriceUnitary = line.Price,
					cancel = line.Deletes
				});
			}

			e_jticket.jautorizacionp = new jAutorizacionproduct();

			e_Employee = new E_Employee();

			e_Employee.sNameEmployee = Autorization;
			e_Employee = b_Employee.Searsh2(e_Employee);


			if (e_Employee != null)
			{
				e_jticket.jautorizacionp.jemployee = new jEmployee()
				{
					idemployee = e_Employee.iUsers,
					nameemployee = e_Employee.sNameEmployee
				};

				e_jticket.jautorizacionp.TypeAutorization = "Cancelacion de producto";
				e_jticket.jautorizacionp.Observation = observation;
				e_jticket.jautorizacionp.Note = note;
			}

			e_jticket.jautorizacionp.jproductcancel = new List<jProductscancel>();

			foreach (var line in lineaProducts)
			{
				if (line.Deletes > 0)
				{
					e_jticket.jautorizacionp.jproductcancel.Add(new jProductscancel
					{
						idproduct = line.IdProduct,
						Quantity = line.Deletes,
						Nameproduct = line.Product,
						PriceUnitary = line.Price
					});
				}
			}

			e_jticket.jmethodpay = new List<jMethodPay>();

			foreach (var mpaymenth in b_MPTicket.Visualize())
			{
				if(mpaymenth.Amountpay>0)
				{
					e_jticket.jmethodpay.Add(new jMethodPay
					{
						idMethodPay = mpaymenth.idMPTicket,
						NameMethodPay = mpaymenth.Methodpay,
						AmountPay = mpaymenth.Amountpay,
					});
				}
			}

			e_Employee = new E_Employee();

			e_Employee.sNameEmployee = autotization2;
			e_Employee = b_Employee.Searsh2(e_Employee);

			e_jticket.jAutorizationmp = new jAutorizationmp();
			e_jticket.jAutorizationmp.jemployee = new jEmployee();

			if(e_Employee != null)
			{

				e_jticket.jAutorizationmp.jemployee.idemployee = e_Employee.iUsers;
				e_jticket.jAutorizationmp.jemployee.nameemployee = e_Employee.sNameEmployee;

				e_jticket.jAutorizationmp.TypeAutorization = "Cancelacion de Metodo de Pago";
				e_jticket.jAutorizationmp.Observation = observation;
				e_jticket.jAutorizationmp.Note = note;

			}

			e_jticket.jAutorizationmp.jMethodPaycancel = new List<jMethodPaycancel>();

			foreach (var list in b_MPTicket.Visualize())
			{
				if (list.dCancel>0)
				{
					e_jticket.jAutorizationmp.jMethodPaycancel.Add(new jMethodPaycancel
					{
						idMethodPay = list.idMPayment,
						NameMethodPay = list.Methodpay,
						AmountPay = list.dCancel
					});
				}
			}

			bJTicket.insert(e_jticket);
		}

		//************************************************
		

		//************************************************Funciones Interface
		public void ConsultProduct(string Date1, string Date2)
		{
			observation = Date1;
			note = Date2;
		}

		public void Cancelticket()
		{
			iproducts.Cancelticket();
		}

		public void ConsultMpticket(List<E_MPTicket> _MPTicket)
		{

		}

		public void Change(decimal value, string ticket)
		{

		}

		public void call4()
		{
			valid = false;
			printticket();

		}

		public void cancelation(int value,int iduser)
		{
			if (value == 2)
			{
				iduseraut = iduser;

			} else if (value == 3)
			{

				this.Close();
			}
		}

		public void validation(string value)
		{
			autotization2 = value;
		}

		//************************************************funciones interface
		

		//************************************************ROV 17/10/18 TXT limitados a numeros y bloqueo de escritura 
		private void txtFolio1_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
			{
				if ((int)e.KeyChar == (int)Keys.Enter)
				{

				}

				e.Handled = true;
				return;
			}
		}

		private void txtAutorization_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
			{
				if ((int)e.KeyChar == (int)Keys.Enter)
				{

				}
				e.Handled = true;
				return;
			}
		}

		private void txtfolio_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
			{
				if ((int)e.KeyChar == (int)Keys.Enter)
				{

				}
				e.Handled = true;
				return;
			}
		}

		private void txtTarjeta_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
			{
				if ((int)e.KeyChar == (int)Keys.Enter)
				{

				}
				e.Handled = true;
				return;
			}
		}

		private void txtmonto2_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
			{
				if ((int)e.KeyChar == (int)Keys.Enter)
				{

				}
				e.Handled = true;
				return;
			}
		}

		private void cmbtypecard_SelectedIndexChanged(object sender, EventArgs e)
		{
			txtAutorization.Clear();
			txtfolio.Clear();
			txtTarjeta.Clear();
			txtmonto.Clear();
		}

		private void txtBank_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (e.KeyChar == ' ')
			{
				e.Handled = false;
			}
			else if (e.KeyChar == '.')
			{
				e.Handled = false;
			}
			else if (e.KeyChar == '-')
			{
				e.Handled = false;
			}
			else if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
			{
				e.Handled = true;
				return;
			}
		}

		private void cmbformapago_KeyPress(object sender, KeyPressEventArgs e)
		{
			e.Handled = true;
		}

		private void cmbCompany_KeyPress(object sender, KeyPressEventArgs e)
		{
			e.Handled = true;
		}

		private void cmbDenominacion_KeyPress(object sender, KeyPressEventArgs e)
		{
			e.Handled = true;
		}

		private void cmbtypecard_KeyPress(object sender, KeyPressEventArgs e)
		{
			e.Handled = true;
		}

		private void cmbcard_KeyPress(object sender, KeyPressEventArgs e)
		{
			e.Handled = true;
		}

		private void cmbfuntion_KeyPress(object sender, KeyPressEventArgs e)
		{
			e.Handled = true;
		}

		private void txtquantychange_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
			{
				e.Handled = true;
			}
			if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
			{
				e.Handled = true;
			}
			if ((int)e.KeyChar == (int)Keys.Enter)
			{
				e.Handled = true;
			}
		}

		private void cmbTypeChange_KeyPress(object sender, KeyPressEventArgs e)
		{
			e.Handled = true;
		}

		//************************************************ROV 17/10/18 TXT limitados a numeros y bloqueo de escritura 
	}
}

