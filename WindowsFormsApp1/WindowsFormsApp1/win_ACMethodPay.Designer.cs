﻿namespace inets.UI.Logging
{
	partial class win_ACMethodPay
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(win_ACMethodPay));
			this.label3 = new System.Windows.Forms.Label();
			this.cmbreason = new System.Windows.Forms.ComboBox();
			this.btnCancellation = new System.Windows.Forms.Button();
			this.txtobservation = new System.Windows.Forms.TextBox();
			this.label10 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.datacancel = new System.Windows.Forms.DataGridView();
			((System.ComponentModel.ISupportInitialize)(this.datacancel)).BeginInit();
			this.SuspendLayout();
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(18, 208);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(86, 30);
			this.label3.TabIndex = 2;
			this.label3.Text = "Motivo de \r\nla cancelacion";
			// 
			// cmbreason
			// 
			this.cmbreason.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cmbreason.FormattingEnabled = true;
			this.cmbreason.Location = new System.Drawing.Point(119, 215);
			this.cmbreason.Name = "cmbreason";
			this.cmbreason.Size = new System.Drawing.Size(121, 23);
			this.cmbreason.TabIndex = 1;
			this.cmbreason.SelectedIndexChanged += new System.EventHandler(this.cmbreason_SelectedIndexChanged);
			this.cmbreason.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbreason_KeyPress);
			// 
			// btnCancellation
			// 
			this.btnCancellation.BackColor = System.Drawing.Color.Transparent;
			this.btnCancellation.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCancellation.BackgroundImage")));
			this.btnCancellation.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.btnCancellation.FlatAppearance.BorderSize = 0;
			this.btnCancellation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnCancellation.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnCancellation.Location = new System.Drawing.Point(165, 288);
			this.btnCancellation.Name = "btnCancellation";
			this.btnCancellation.Size = new System.Drawing.Size(75, 27);
			this.btnCancellation.TabIndex = 3;
			this.btnCancellation.Text = "Aceptar";
			this.btnCancellation.UseVisualStyleBackColor = false;
			this.btnCancellation.Click += new System.EventHandler(this.btnCancellation_Click);
			// 
			// txtobservation
			// 
			this.txtobservation.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtobservation.Location = new System.Drawing.Point(119, 252);
			this.txtobservation.Name = "txtobservation";
			this.txtobservation.Size = new System.Drawing.Size(121, 21);
			this.txtobservation.TabIndex = 2;
			this.txtobservation.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtobservation_KeyPress);
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label10.Location = new System.Drawing.Point(18, 258);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(76, 15);
			this.label10.TabIndex = 13;
			this.label10.Text = "Observación";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(18, 21);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(223, 18);
			this.label4.TabIndex = 16;
			this.label4.Text = "Cancelación de forma de pago";
			// 
			// datacancel
			// 
			this.datacancel.BackgroundColor = System.Drawing.SystemColors.ControlLight;
			this.datacancel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.datacancel.Location = new System.Drawing.Point(21, 52);
			this.datacancel.Name = "datacancel";
			this.datacancel.Size = new System.Drawing.Size(227, 130);
			this.datacancel.TabIndex = 17;
			// 
			// win_ACMethodPay
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(271, 323);
			this.Controls.Add(this.datacancel);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.txtobservation);
			this.Controls.Add(this.label10);
			this.Controls.Add(this.btnCancellation);
			this.Controls.Add(this.cmbreason);
			this.Controls.Add(this.label3);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "win_ACMethodPay";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Load += new System.EventHandler(this.win_ACMethodPay_Load);
			((System.ComponentModel.ISupportInitialize)(this.datacancel)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.ComboBox cmbreason;
		private System.Windows.Forms.Button btnCancellation;
		private System.Windows.Forms.TextBox txtobservation;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.DataGridView datacancel;
	}
}