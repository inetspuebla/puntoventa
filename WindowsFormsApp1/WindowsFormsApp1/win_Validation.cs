﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Inets.Bussines.Estandard;
using Inets.Models.Entity_Entities;
using inets.UI.Logging;
using ExternalClass;

namespace inets.UI.Logging
{
	public partial class win_Validation : Form, Iproducts
	{
		public Iproducts iproducts { get; set; }
		
		public win_Validation()
		{
			InitializeComponent();
		}

		public string observation, note, ticket;
		public int number;
		public decimal change, total;

		public List<LineaProduct> lineaProducts = new List<LineaProduct>();
		public List<E_MPTicket> l_MPTickets = new List<E_MPTicket>();
		public E_Cancellations e_Cancellation = new E_Cancellations();

		B_Cancellations b_Cancellations = new B_Cancellations();
		E_Cancellations cancellation = new E_Cancellations();
		B_MPTicket b_MPTicket = new B_MPTicket();
		E_Employee e_Employee = new E_Employee();
		B_Employee b_Employee = new B_Employee();
		E_Store e_Store = new E_Store();
		B_Store b_Store = new B_Store();
		E_Users e_Users = new E_Users();
		B_Users b_Users = new B_Users();
		B_Login b_Login = new B_Login();


		private void win_Validation_Load(object sender, EventArgs e)
		{
			txtpasword.PasswordChar = '*';
		}

		private void btnAcept_Click(object sender, EventArgs e)
		{
			E_Users e_Users = new E_Users();
			B_Users b_Users = new B_Users();

			e_Users.sNickname = txtSuper.Text;
			e_Users.sPassword = txtpasword.Text;

			if (txtSuper.Text == "" || txtSuper.Text == " ")
			{
				MessageBox.Show("Favor de escribir el usuario");
			}
			else
			{
				if (txtpasword.Text == "" || txtpasword.Text == " ")
				{
					MessageBox.Show("Favor de  escribir una contraseña");
				}
				else
				{
					if (b_Login.Autorization(e_Users) == true)
					{

						txtSuper.Text = txtpasword.Text = "";
						
						e_Users = b_Users.SearchIdUser(e_Users);
						e_Employee.iUsers = e_Users.iIdUsers;
						e_Employee = b_Employee.SearshEmployee(e_Employee);
						iproducts.validation(e_Employee.sNameEmployee);

						callvalidation();

						this.Close();

					}
					else
					{
						MessageBox.Show("El usuario o contraseña son incorrectos");
					}
				}
			}
		}
		
		public void callvalidation()
		{
			win_ACMethodPay win_ACMethodPay = new win_ACMethodPay();
			win_ACProduct win_ACproduct = new win_ACProduct();
			win_ACTicket win_ACticket = new win_ACTicket();
	
			e_Users.sNickname = txtSuper.Text;
			e_Users.sPassword = txtpasword.Text;
			
			int validation = number;

			switch (validation)
			{
				case 1:

					win_ACproduct.e_Cancellation = e_Cancellation;
					win_ACproduct.user = e_Employee.sNameEmployee;
					win_ACproduct.ticket = ticket;
					win_ACproduct.l_MPTickets = l_MPTickets;
					win_ACproduct.lineaProducts = lineaProducts;

					this.Hide();
					this.Close();
					win_ACproduct.iproducts = this;
					win_ACproduct.ShowDialog();

					break;

				case 2:
					win_ACMethodPay.e_Cancellation = e_Cancellation;
					win_ACMethodPay.number = number;
					win_ACMethodPay.l_MPTickets = l_MPTickets;
					
					this.Hide();
					this.Close();

					iproducts.ConsultProduct(observation, note);
					iproducts.call4();
					win_ACMethodPay.iproducts = this;
					
					win_ACMethodPay.ShowDialog();
					break;

				case 3:
					cancellation.idTicket = e_Cancellation.idTicket;
					cancellation.idUser = e_Users.iIdUsers;
					cancellation.ticket = e_Cancellation.ticket;
					cancellation.total = e_Cancellation.total;
					cancellation.store = e_Cancellation.store;
					
					win_ACticket.number = number;
					win_ACticket.lineaProducts = lineaProducts;
					win_ACticket.e_MPTickets = l_MPTickets;
					win_ACticket.e_Store = e_Store;
					win_ACticket.iproducts = this;

					this.Hide();
					this.Close();
					iproducts.ConsultMpticket(l_MPTickets);
					win_ACticket.ShowDialog();
					win_ACticket.Focus();

					break;
			}
		}

		
		//*metodos interface
		public void ConsultProduct(string Date1, string Date2)
		{
			observation = Date1;
			note = Date2;
		}

		public void Cancelticket()
		{
			iproducts.Cancelticket();
		}

		public void ConsultMpticket(List<E_MPTicket> _MPTicket)
		{
			l_MPTickets = _MPTicket;
			iproducts.ConsultMpticket(l_MPTickets);
		}

		public void Change(decimal value, string ticket)
		{
			change = value;
			iproducts.Change(value,ticket);
		}

		public void call4()
		{
			iproducts.call4();
		}

		public void cancelation(int value, int iduser)
		{
			
		}

		public void validation(string value)
		{

		}
	}
}
