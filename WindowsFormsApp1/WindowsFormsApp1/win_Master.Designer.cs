﻿namespace inets.UI.Logging
{
	partial class win_Master
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(win_Master));
			this.Panelforms = new System.Windows.Forms.Panel();
			this.panel4 = new System.Windows.Forms.Panel();
			this.label19 = new System.Windows.Forms.Label();
			this.lbefetivo = new System.Windows.Forms.Label();
			this.pictuproduc = new System.Windows.Forms.PictureBox();
			this.txtDescription = new System.Windows.Forms.TextBox();
			this.txtUpc = new System.Windows.Forms.TextBox();
			this.gridTicket = new System.Windows.Forms.DataGridView();
			this.label23 = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this.label20 = new System.Windows.Forms.Label();
			this.lbTotal = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.lbTicket = new System.Windows.Forms.Label();
			this.lbhora = new System.Windows.Forms.Label();
			this.lbcaja = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.lbfecha = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.lbtienda = new System.Windows.Forms.Label();
			this.lbUsuario = new System.Windows.Forms.Label();
			this.label17 = new System.Windows.Forms.Label();
			this.btnpayment = new System.Windows.Forms.Button();
			this.lbCantidad = new System.Windows.Forms.Label();
			this.label18 = new System.Windows.Forms.Label();
			this.btnQuit = new System.Windows.Forms.Button();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.label22 = new System.Windows.Forms.Label();
			this.btnEnter = new System.Windows.Forms.Button();
			this.btn0 = new System.Windows.Forms.Button();
			this.btn3 = new System.Windows.Forms.Button();
			this.btn2 = new System.Windows.Forms.Button();
			this.btn1 = new System.Windows.Forms.Button();
			this.btn6 = new System.Windows.Forms.Button();
			this.btn5 = new System.Windows.Forms.Button();
			this.btn4 = new System.Windows.Forms.Button();
			this.btn9 = new System.Windows.Forms.Button();
			this.btn8 = new System.Windows.Forms.Button();
			this.btn7 = new System.Windows.Forms.Button();
			this.txtCalculate = new System.Windows.Forms.TextBox();
			this.lbtimer = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.button2 = new System.Windows.Forms.Button();
			this.btnConsultProduct = new System.Windows.Forms.Button();
			this.button5 = new System.Windows.Forms.Button();
			this.btnCancelTick = new System.Windows.Forms.Button();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.lbHra = new System.Windows.Forms.Label();
			this.panelLogin = new System.Windows.Forms.Panel();
			this.panel2 = new System.Windows.Forms.Panel();
			this.pictureBox5 = new System.Windows.Forms.PictureBox();
			this.label1 = new System.Windows.Forms.Label();
			this.pictureBox4 = new System.Windows.Forms.PictureBox();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.btnIngres = new System.Windows.Forms.Button();
			this.TxtUser = new System.Windows.Forms.TextBox();
			this.TxtPassword = new System.Windows.Forms.TextBox();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.btnCaja = new System.Windows.Forms.Button();
			this.btnbloquear = new System.Windows.Forms.Button();
			this.panelcash = new System.Windows.Forms.Panel();
			this.panel3 = new System.Windows.Forms.Panel();
			this.btncalculate = new System.Windows.Forms.Button();
			this.label25 = new System.Windows.Forms.Label();
			this.lbdate = new System.Windows.Forms.Label();
			this.label26 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.txttotal = new System.Windows.Forms.TextBox();
			this.txtmoney = new System.Windows.Forms.TextBox();
			this.label24 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.lbcashusuario = new System.Windows.Forms.Label();
			this.txt20 = new System.Windows.Forms.TextBox();
			this.lbcashcaja = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.btninsertcash = new System.Windows.Forms.Button();
			this.txt50 = new System.Windows.Forms.TextBox();
			this.label16 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.txt500 = new System.Windows.Forms.TextBox();
			this.txt100 = new System.Windows.Forms.TextBox();
			this.label15 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.txt200 = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.txtdesc = new System.Windows.Forms.TextBox();
			this.Label28 = new System.Windows.Forms.Label();
			this.txtprice = new System.Windows.Forms.TextBox();
			this.label27 = new System.Windows.Forms.Label();
			this.button1 = new System.Windows.Forms.Button();
			this.Panelforms.SuspendLayout();
			this.panel4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictuproduc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridTicket)).BeginInit();
			this.panel1.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.panelLogin.SuspendLayout();
			this.panel2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.panelcash.SuspendLayout();
			this.panel3.SuspendLayout();
			this.SuspendLayout();
			// 
			// Panelforms
			// 
			this.Panelforms.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.Panelforms.BackColor = System.Drawing.Color.Silver;
			this.Panelforms.Controls.Add(this.txtDescription);
			this.Panelforms.Controls.Add(this.txtprice);
			this.Panelforms.Controls.Add(this.label27);
			this.Panelforms.Controls.Add(this.txtdesc);
			this.Panelforms.Controls.Add(this.Label28);
			this.Panelforms.Controls.Add(this.panel4);
			this.Panelforms.Controls.Add(this.pictuproduc);
			this.Panelforms.Controls.Add(this.txtUpc);
			this.Panelforms.Controls.Add(this.gridTicket);
			this.Panelforms.Controls.Add(this.label23);
			this.Panelforms.Controls.Add(this.panel1);
			this.Panelforms.Controls.Add(this.groupBox1);
			this.Panelforms.Controls.Add(this.btnpayment);
			this.Panelforms.Controls.Add(this.lbCantidad);
			this.Panelforms.Controls.Add(this.label18);
			this.Panelforms.Controls.Add(this.btnQuit);
			this.Panelforms.Controls.Add(this.groupBox2);
			this.Panelforms.Location = new System.Drawing.Point(9, 12);
			this.Panelforms.Name = "Panelforms";
			this.Panelforms.Size = new System.Drawing.Size(1102, 558);
			this.Panelforms.TabIndex = 0;
			// 
			// panel4
			// 
			this.panel4.BackColor = System.Drawing.Color.LightGray;
			this.panel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.panel4.Controls.Add(this.label19);
			this.panel4.Controls.Add(this.lbefetivo);
			this.panel4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.panel4.Location = new System.Drawing.Point(398, 511);
			this.panel4.Name = "panel4";
			this.panel4.Size = new System.Drawing.Size(219, 37);
			this.panel4.TabIndex = 21;
			// 
			// label19
			// 
			this.label19.AutoSize = true;
			this.label19.BackColor = System.Drawing.Color.Transparent;
			this.label19.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label19.Location = new System.Drawing.Point(3, 9);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(128, 18);
			this.label19.TabIndex = 5;
			this.label19.Text = "Monto Pagado: $";
			// 
			// lbefetivo
			// 
			this.lbefetivo.AutoSize = true;
			this.lbefetivo.BackColor = System.Drawing.Color.Transparent;
			this.lbefetivo.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbefetivo.Location = new System.Drawing.Point(146, 9);
			this.lbefetivo.Name = "lbefetivo";
			this.lbefetivo.Size = new System.Drawing.Size(17, 18);
			this.lbefetivo.TabIndex = 6;
			this.lbefetivo.Text = "0";
			// 
			// pictuproduc
			// 
			this.pictuproduc.Location = new System.Drawing.Point(949, 73);
			this.pictuproduc.Name = "pictuproduc";
			this.pictuproduc.Size = new System.Drawing.Size(139, 146);
			this.pictuproduc.TabIndex = 20;
			this.pictuproduc.TabStop = false;
			// 
			// txtDescription
			// 
			this.txtDescription.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.txtDescription.Enabled = false;
			this.txtDescription.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtDescription.Location = new System.Drawing.Point(623, 73);
			this.txtDescription.Multiline = true;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Size = new System.Drawing.Size(296, 64);
			this.txtDescription.TabIndex = 18;
			// 
			// txtUpc
			// 
			this.txtUpc.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.txtUpc.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtUpc.Location = new System.Drawing.Point(749, 201);
			this.txtUpc.Name = "txtUpc";
			this.txtUpc.Size = new System.Drawing.Size(187, 18);
			this.txtUpc.TabIndex = 9;
			this.txtUpc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtUpc_KeyPress_1);
			// 
			// gridTicket
			// 
			this.gridTicket.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
			dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.gridTicket.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.gridTicket.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
			dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
			dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
			this.gridTicket.DefaultCellStyle = dataGridViewCellStyle2;
			this.gridTicket.Location = new System.Drawing.Point(15, 119);
			this.gridTicket.Name = "gridTicket";
			this.gridTicket.Size = new System.Drawing.Size(477, 379);
			this.gridTicket.TabIndex = 19;
			this.gridTicket.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.gridTicket_RowHeaderMouseClick);
			// 
			// label23
			// 
			this.label23.AutoSize = true;
			this.label23.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label23.Location = new System.Drawing.Point(620, 202);
			this.label23.Name = "label23";
			this.label23.Size = new System.Drawing.Size(120, 17);
			this.label23.TabIndex = 7;
			this.label23.Text = "Código de barras";
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.LightGray;
			this.panel1.Controls.Add(this.label20);
			this.panel1.Controls.Add(this.lbTotal);
			this.panel1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.panel1.Location = new System.Drawing.Point(193, 511);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(139, 37);
			this.panel1.TabIndex = 16;
			// 
			// label20
			// 
			this.label20.AutoSize = true;
			this.label20.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label20.Location = new System.Drawing.Point(3, 9);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(57, 18);
			this.label20.TabIndex = 5;
			this.label20.Text = "Total: $";
			// 
			// lbTotal
			// 
			this.lbTotal.AutoSize = true;
			this.lbTotal.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbTotal.Location = new System.Drawing.Point(66, 9);
			this.lbTotal.Name = "lbTotal";
			this.lbTotal.Size = new System.Drawing.Size(17, 18);
			this.lbTotal.TabIndex = 6;
			this.lbTotal.Text = "0";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.lbTicket);
			this.groupBox1.Controls.Add(this.lbhora);
			this.groupBox1.Controls.Add(this.lbcaja);
			this.groupBox1.Controls.Add(this.label10);
			this.groupBox1.Controls.Add(this.label11);
			this.groupBox1.Controls.Add(this.label12);
			this.groupBox1.Controls.Add(this.lbfecha);
			this.groupBox1.Controls.Add(this.label14);
			this.groupBox1.Controls.Add(this.lbtienda);
			this.groupBox1.Controls.Add(this.lbUsuario);
			this.groupBox1.Controls.Add(this.label17);
			this.groupBox1.Location = new System.Drawing.Point(15, 12);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(477, 97);
			this.groupBox1.TabIndex = 17;
			this.groupBox1.TabStop = false;
			// 
			// lbTicket
			// 
			this.lbTicket.AutoSize = true;
			this.lbTicket.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbTicket.Location = new System.Drawing.Point(126, 45);
			this.lbTicket.Name = "lbTicket";
			this.lbTicket.Size = new System.Drawing.Size(48, 15);
			this.lbTicket.TabIndex = 10;
			this.lbTicket.Text = "label16";
			// 
			// lbhora
			// 
			this.lbhora.AutoSize = true;
			this.lbhora.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbhora.Location = new System.Drawing.Point(175, 76);
			this.lbhora.Name = "lbhora";
			this.lbhora.Size = new System.Drawing.Size(48, 15);
			this.lbhora.TabIndex = 9;
			this.lbhora.Text = "label15";
			// 
			// lbcaja
			// 
			this.lbcaja.AutoSize = true;
			this.lbcaja.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbcaja.Location = new System.Drawing.Point(328, 76);
			this.lbcaja.Name = "lbcaja";
			this.lbcaja.Size = new System.Drawing.Size(48, 15);
			this.lbcaja.TabIndex = 8;
			this.lbcaja.Text = "label14";
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label10.Location = new System.Drawing.Point(286, 76);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(36, 15);
			this.label10.TabIndex = 7;
			this.label10.Text = "Caja:";
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label11.Location = new System.Drawing.Point(6, 45);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(110, 15);
			this.label11.TabIndex = 6;
			this.label11.Text = "Número de ticket #";
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label12.Location = new System.Drawing.Point(6, 76);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(47, 15);
			this.label12.TabIndex = 5;
			this.label12.Text = "Fecha :";
			// 
			// lbfecha
			// 
			this.lbfecha.AutoSize = true;
			this.lbfecha.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbfecha.Location = new System.Drawing.Point(73, 76);
			this.lbfecha.Name = "lbfecha";
			this.lbfecha.Size = new System.Drawing.Size(48, 15);
			this.lbfecha.TabIndex = 4;
			this.lbfecha.Text = "label10";
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label14.Location = new System.Drawing.Point(223, 16);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(57, 15);
			this.label14.TabIndex = 3;
			this.label14.Text = "Usuario :";
			// 
			// lbtienda
			// 
			this.lbtienda.AutoSize = true;
			this.lbtienda.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbtienda.Location = new System.Drawing.Point(73, 16);
			this.lbtienda.Name = "lbtienda";
			this.lbtienda.Size = new System.Drawing.Size(41, 15);
			this.lbtienda.TabIndex = 2;
			this.lbtienda.Text = "label7";
			// 
			// lbUsuario
			// 
			this.lbUsuario.AutoSize = true;
			this.lbUsuario.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbUsuario.Location = new System.Drawing.Point(286, 16);
			this.lbUsuario.Name = "lbUsuario";
			this.lbUsuario.Size = new System.Drawing.Size(41, 15);
			this.lbUsuario.TabIndex = 1;
			this.lbUsuario.Text = "label5";
			// 
			// label17
			// 
			this.label17.AutoSize = true;
			this.label17.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label17.Location = new System.Drawing.Point(6, 16);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(61, 15);
			this.label17.TabIndex = 0;
			this.label17.Text = "Tienda # :";
			// 
			// btnpayment
			// 
			this.btnpayment.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnpayment.BackgroundImage")));
			this.btnpayment.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.btnpayment.FlatAppearance.BorderSize = 0;
			this.btnpayment.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnpayment.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnpayment.Image = ((System.Drawing.Image)(resources.GetObject("btnpayment.Image")));
			this.btnpayment.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnpayment.Location = new System.Drawing.Point(498, 172);
			this.btnpayment.Name = "btnpayment";
			this.btnpayment.Size = new System.Drawing.Size(81, 33);
			this.btnpayment.TabIndex = 13;
			this.btnpayment.Text = "Pago";
			this.btnpayment.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.btnpayment.UseVisualStyleBackColor = true;
			this.btnpayment.Click += new System.EventHandler(this.btnPago_Click);
			// 
			// lbCantidad
			// 
			this.lbCantidad.AutoSize = true;
			this.lbCantidad.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbCantidad.Location = new System.Drawing.Point(146, 520);
			this.lbCantidad.Name = "lbCantidad";
			this.lbCantidad.Size = new System.Drawing.Size(17, 18);
			this.lbCantidad.TabIndex = 4;
			this.lbCantidad.Text = "0";
			// 
			// label18
			// 
			this.label18.AutoSize = true;
			this.label18.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label18.Location = new System.Drawing.Point(17, 520);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(121, 18);
			this.label18.TabIndex = 3;
			this.label18.Text = "N° de artículos #";
			// 
			// btnQuit
			// 
			this.btnQuit.AutoSize = true;
			this.btnQuit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnQuit.BackgroundImage")));
			this.btnQuit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.btnQuit.FlatAppearance.BorderSize = 0;
			this.btnQuit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnQuit.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnQuit.Image = ((System.Drawing.Image)(resources.GetObject("btnQuit.Image")));
			this.btnQuit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnQuit.Location = new System.Drawing.Point(498, 119);
			this.btnQuit.Name = "btnQuit";
			this.btnQuit.Size = new System.Drawing.Size(81, 36);
			this.btnQuit.TabIndex = 12;
			this.btnQuit.Text = "Baja";
			this.btnQuit.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.btnQuit.UseVisualStyleBackColor = true;
			this.btnQuit.Click += new System.EventHandler(this.btnBaja_Click);
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.label22);
			this.groupBox2.Controls.Add(this.btnEnter);
			this.groupBox2.Controls.Add(this.btn0);
			this.groupBox2.Controls.Add(this.btn3);
			this.groupBox2.Controls.Add(this.btn2);
			this.groupBox2.Controls.Add(this.btn1);
			this.groupBox2.Controls.Add(this.btn6);
			this.groupBox2.Controls.Add(this.btn5);
			this.groupBox2.Controls.Add(this.btn4);
			this.groupBox2.Controls.Add(this.btn9);
			this.groupBox2.Controls.Add(this.btn8);
			this.groupBox2.Controls.Add(this.btn7);
			this.groupBox2.Controls.Add(this.txtCalculate);
			this.groupBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.groupBox2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.groupBox2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.groupBox2.Location = new System.Drawing.Point(623, 241);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(380, 257);
			this.groupBox2.TabIndex = 11;
			this.groupBox2.TabStop = false;
			// 
			// label22
			// 
			this.label22.AutoSize = true;
			this.label22.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label22.Location = new System.Drawing.Point(19, 25);
			this.label22.Name = "label22";
			this.label22.Size = new System.Drawing.Size(66, 17);
			this.label22.TabIndex = 14;
			this.label22.Text = "Cantidad";
			// 
			// btnEnter
			// 
			this.btnEnter.BackColor = System.Drawing.Color.Transparent;
			this.btnEnter.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnEnter.BackgroundImage")));
			this.btnEnter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.btnEnter.FlatAppearance.BorderSize = 0;
			this.btnEnter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnEnter.Image = ((System.Drawing.Image)(resources.GetObject("btnEnter.Image")));
			this.btnEnter.Location = new System.Drawing.Point(296, 163);
			this.btnEnter.Name = "btnEnter";
			this.btnEnter.Size = new System.Drawing.Size(63, 87);
			this.btnEnter.TabIndex = 12;
			this.btnEnter.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
			this.btnEnter.UseVisualStyleBackColor = false;
			this.btnEnter.Click += new System.EventHandler(this.btnEnter_Click);
			// 
			// btn0
			// 
			this.btn0.BackColor = System.Drawing.Color.Transparent;
			this.btn0.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn0.BackgroundImage")));
			this.btn0.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.btn0.FlatAppearance.BorderSize = 0;
			this.btn0.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn0.Font = new System.Drawing.Font("Arial Black", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn0.Location = new System.Drawing.Point(207, 209);
			this.btn0.Name = "btn0";
			this.btn0.Size = new System.Drawing.Size(83, 41);
			this.btn0.TabIndex = 11;
			this.btn0.Text = "0";
			this.btn0.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
			this.btn0.UseVisualStyleBackColor = false;
			this.btn0.Click += new System.EventHandler(this.btn0_Click);
			// 
			// btn3
			// 
			this.btn3.BackColor = System.Drawing.Color.Transparent;
			this.btn3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn3.BackgroundImage")));
			this.btn3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.btn3.FlatAppearance.BorderSize = 0;
			this.btn3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn3.Font = new System.Drawing.Font("Arial Black", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn3.Location = new System.Drawing.Point(207, 162);
			this.btn3.Name = "btn3";
			this.btn3.Size = new System.Drawing.Size(83, 41);
			this.btn3.TabIndex = 9;
			this.btn3.Text = "3";
			this.btn3.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
			this.btn3.UseVisualStyleBackColor = false;
			this.btn3.Click += new System.EventHandler(this.btn3_Click_1);
			// 
			// btn2
			// 
			this.btn2.BackColor = System.Drawing.Color.Transparent;
			this.btn2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn2.BackgroundImage")));
			this.btn2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.btn2.FlatAppearance.BorderSize = 0;
			this.btn2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn2.Font = new System.Drawing.Font("Arial Black", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn2.Location = new System.Drawing.Point(118, 162);
			this.btn2.Name = "btn2";
			this.btn2.Size = new System.Drawing.Size(83, 41);
			this.btn2.TabIndex = 8;
			this.btn2.Text = "2";
			this.btn2.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
			this.btn2.UseVisualStyleBackColor = false;
			this.btn2.Click += new System.EventHandler(this.btn2_Click_1);
			// 
			// btn1
			// 
			this.btn1.BackColor = System.Drawing.Color.Transparent;
			this.btn1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn1.BackgroundImage")));
			this.btn1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.btn1.FlatAppearance.BorderSize = 0;
			this.btn1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn1.Font = new System.Drawing.Font("Arial Black", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn1.Location = new System.Drawing.Point(29, 162);
			this.btn1.Name = "btn1";
			this.btn1.Size = new System.Drawing.Size(83, 41);
			this.btn1.TabIndex = 7;
			this.btn1.Text = "1";
			this.btn1.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
			this.btn1.UseVisualStyleBackColor = false;
			this.btn1.Click += new System.EventHandler(this.btn1_Click_1);
			// 
			// btn6
			// 
			this.btn6.BackColor = System.Drawing.Color.Transparent;
			this.btn6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn6.BackgroundImage")));
			this.btn6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.btn6.FlatAppearance.BorderSize = 0;
			this.btn6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn6.Font = new System.Drawing.Font("Arial Black", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn6.Location = new System.Drawing.Point(207, 115);
			this.btn6.Name = "btn6";
			this.btn6.Size = new System.Drawing.Size(83, 41);
			this.btn6.TabIndex = 6;
			this.btn6.Text = "6";
			this.btn6.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
			this.btn6.UseVisualStyleBackColor = false;
			this.btn6.Click += new System.EventHandler(this.btn6_Click_1);
			// 
			// btn5
			// 
			this.btn5.BackColor = System.Drawing.Color.Transparent;
			this.btn5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn5.BackgroundImage")));
			this.btn5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.btn5.FlatAppearance.BorderSize = 0;
			this.btn5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn5.Font = new System.Drawing.Font("Arial Black", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn5.Location = new System.Drawing.Point(119, 113);
			this.btn5.Name = "btn5";
			this.btn5.Size = new System.Drawing.Size(83, 41);
			this.btn5.TabIndex = 5;
			this.btn5.Text = "5";
			this.btn5.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
			this.btn5.UseVisualStyleBackColor = false;
			this.btn5.Click += new System.EventHandler(this.btn5_Click_1);
			// 
			// btn4
			// 
			this.btn4.BackColor = System.Drawing.Color.Transparent;
			this.btn4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn4.BackgroundImage")));
			this.btn4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.btn4.FlatAppearance.BorderSize = 0;
			this.btn4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn4.Font = new System.Drawing.Font("Arial Black", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn4.Location = new System.Drawing.Point(29, 115);
			this.btn4.Name = "btn4";
			this.btn4.Size = new System.Drawing.Size(83, 41);
			this.btn4.TabIndex = 4;
			this.btn4.Text = "4";
			this.btn4.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
			this.btn4.UseVisualStyleBackColor = false;
			this.btn4.Click += new System.EventHandler(this.btn4_Click_1);
			// 
			// btn9
			// 
			this.btn9.BackColor = System.Drawing.Color.Transparent;
			this.btn9.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn9.BackgroundImage")));
			this.btn9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.btn9.FlatAppearance.BorderSize = 0;
			this.btn9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn9.Font = new System.Drawing.Font("Arial Black", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn9.Location = new System.Drawing.Point(207, 68);
			this.btn9.Name = "btn9";
			this.btn9.Size = new System.Drawing.Size(83, 41);
			this.btn9.TabIndex = 3;
			this.btn9.Text = "9";
			this.btn9.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
			this.btn9.UseVisualStyleBackColor = false;
			this.btn9.Click += new System.EventHandler(this.btn9_Click_1);
			// 
			// btn8
			// 
			this.btn8.BackColor = System.Drawing.Color.Transparent;
			this.btn8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn8.BackgroundImage")));
			this.btn8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.btn8.FlatAppearance.BorderSize = 0;
			this.btn8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn8.Font = new System.Drawing.Font("Arial Black", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn8.Location = new System.Drawing.Point(118, 68);
			this.btn8.Name = "btn8";
			this.btn8.Size = new System.Drawing.Size(83, 41);
			this.btn8.TabIndex = 2;
			this.btn8.Text = "8";
			this.btn8.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
			this.btn8.UseVisualStyleBackColor = false;
			this.btn8.Click += new System.EventHandler(this.btn8_Click_1);
			// 
			// btn7
			// 
			this.btn7.BackColor = System.Drawing.Color.Transparent;
			this.btn7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn7.BackgroundImage")));
			this.btn7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.btn7.FlatAppearance.BorderSize = 0;
			this.btn7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn7.Font = new System.Drawing.Font("Arial Black", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn7.Location = new System.Drawing.Point(29, 68);
			this.btn7.Name = "btn7";
			this.btn7.Size = new System.Drawing.Size(83, 41);
			this.btn7.TabIndex = 1;
			this.btn7.Text = "7";
			this.btn7.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
			this.btn7.UseVisualStyleBackColor = false;
			this.btn7.Click += new System.EventHandler(this.btn7_Click_1);
			// 
			// txtCalculate
			// 
			this.txtCalculate.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.txtCalculate.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtCalculate.Location = new System.Drawing.Point(94, 24);
			this.txtCalculate.Name = "txtCalculate";
			this.txtCalculate.Size = new System.Drawing.Size(187, 18);
			this.txtCalculate.TabIndex = 0;
			this.txtCalculate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCalculate_KeyPress_1);
			// 
			// lbtimer
			// 
			this.lbtimer.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.lbtimer.AutoSize = true;
			this.lbtimer.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbtimer.Location = new System.Drawing.Point(1118, 133);
			this.lbtimer.Name = "lbtimer";
			this.lbtimer.Size = new System.Drawing.Size(50, 18);
			this.lbtimer.TabIndex = 2;
			this.lbtimer.Text = "label1";
			// 
			// label2
			// 
			this.label2.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(1118, 103);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(52, 18);
			this.label2.TabIndex = 3;
			this.label2.Text = "Fecha";
			// 
			// button2
			// 
			this.button2.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.button2.BackColor = System.Drawing.Color.Transparent;
			this.button2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button2.BackgroundImage")));
			this.button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.button2.FlatAppearance.BorderSize = 0;
			this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button2.Location = new System.Drawing.Point(977, 576);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(135, 37);
			this.button2.TabIndex = 11;
			this.button2.Text = "Liquidación";
			this.button2.UseVisualStyleBackColor = false;
			// 
			// btnConsultProduct
			// 
			this.btnConsultProduct.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnConsultProduct.BackColor = System.Drawing.Color.Transparent;
			this.btnConsultProduct.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnConsultProduct.BackgroundImage")));
			this.btnConsultProduct.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.btnConsultProduct.FlatAppearance.BorderSize = 0;
			this.btnConsultProduct.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnConsultProduct.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnConsultProduct.Location = new System.Drawing.Point(1118, 475);
			this.btnConsultProduct.Name = "btnConsultProduct";
			this.btnConsultProduct.Size = new System.Drawing.Size(135, 48);
			this.btnConsultProduct.TabIndex = 13;
			this.btnConsultProduct.Text = "Consulta de producto";
			this.btnConsultProduct.UseVisualStyleBackColor = false;
			this.btnConsultProduct.Click += new System.EventHandler(this.btnConsultProduct_Click);
			// 
			// button5
			// 
			this.button5.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.button5.BackColor = System.Drawing.Color.Transparent;
			this.button5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button5.BackgroundImage")));
			this.button5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.button5.FlatAppearance.BorderSize = 0;
			this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button5.Location = new System.Drawing.Point(1118, 529);
			this.button5.Name = "button5";
			this.button5.Size = new System.Drawing.Size(135, 39);
			this.button5.TabIndex = 14;
			this.button5.Text = "Configuración";
			this.button5.UseVisualStyleBackColor = false;
			// 
			// btnCancelTick
			// 
			this.btnCancelTick.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnCancelTick.BackColor = System.Drawing.Color.Transparent;
			this.btnCancelTick.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCancelTick.BackgroundImage")));
			this.btnCancelTick.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.btnCancelTick.FlatAppearance.BorderSize = 0;
			this.btnCancelTick.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnCancelTick.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnCancelTick.Location = new System.Drawing.Point(1118, 432);
			this.btnCancelTick.Name = "btnCancelTick";
			this.btnCancelTick.Size = new System.Drawing.Size(135, 37);
			this.btnCancelTick.TabIndex = 17;
			this.btnCancelTick.Text = "Cancelar ticket";
			this.btnCancelTick.UseVisualStyleBackColor = false;
			this.btnCancelTick.Click += new System.EventHandler(this.btnCancelTick_Click);
			// 
			// timer1
			// 
			this.timer1.Enabled = true;
			this.timer1.Interval = 1000;
			this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
			// 
			// lbHra
			// 
			this.lbHra.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.lbHra.AutoSize = true;
			this.lbHra.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbHra.Location = new System.Drawing.Point(1117, 163);
			this.lbHra.Name = "lbHra";
			this.lbHra.Size = new System.Drawing.Size(50, 18);
			this.lbHra.TabIndex = 18;
			this.lbHra.Text = "label1";
			// 
			// panelLogin
			// 
			this.panelLogin.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.panelLogin.BackColor = System.Drawing.Color.Silver;
			this.panelLogin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.panelLogin.Controls.Add(this.panel2);
			this.panelLogin.Location = new System.Drawing.Point(9, 12);
			this.panelLogin.Name = "panelLogin";
			this.panelLogin.Size = new System.Drawing.Size(1103, 558);
			this.panelLogin.TabIndex = 20;
			// 
			// panel2
			// 
			this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.panel2.BackColor = System.Drawing.Color.WhiteSmoke;
			this.panel2.Controls.Add(this.pictureBox5);
			this.panel2.Controls.Add(this.label1);
			this.panel2.Controls.Add(this.pictureBox4);
			this.panel2.Controls.Add(this.label4);
			this.panel2.Controls.Add(this.label3);
			this.panel2.Controls.Add(this.btnIngres);
			this.panel2.Controls.Add(this.TxtUser);
			this.panel2.Controls.Add(this.TxtPassword);
			this.panel2.Location = new System.Drawing.Point(398, 88);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(305, 378);
			this.panel2.TabIndex = 1;
			// 
			// pictureBox5
			// 
			this.pictureBox5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox5.BackgroundImage")));
			this.pictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
			this.pictureBox5.Location = new System.Drawing.Point(23, 157);
			this.pictureBox5.Name = "pictureBox5";
			this.pictureBox5.Size = new System.Drawing.Size(45, 37);
			this.pictureBox5.TabIndex = 7;
			this.pictureBox5.TabStop = false;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(118, 60);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(116, 18);
			this.label1.TabIndex = 5;
			this.label1.Text = "Inicio de sesión";
			// 
			// pictureBox4
			// 
			this.pictureBox4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox4.BackgroundImage")));
			this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
			this.pictureBox4.Location = new System.Drawing.Point(23, 105);
			this.pictureBox4.Name = "pictureBox4";
			this.pictureBox4.Size = new System.Drawing.Size(45, 37);
			this.pictureBox4.TabIndex = 6;
			this.pictureBox4.TabStop = false;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(74, 124);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(62, 18);
			this.label4.TabIndex = 0;
			this.label4.Text = "Usuario";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(74, 176);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(89, 18);
			this.label3.TabIndex = 1;
			this.label3.Text = "Contraseña";
			// 
			// btnIngres
			// 
			this.btnIngres.BackColor = System.Drawing.Color.Transparent;
			this.btnIngres.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnIngres.BackgroundImage")));
			this.btnIngres.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.btnIngres.FlatAppearance.BorderSize = 0;
			this.btnIngres.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnIngres.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnIngres.Image = ((System.Drawing.Image)(resources.GetObject("btnIngres.Image")));
			this.btnIngres.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.btnIngres.Location = new System.Drawing.Point(138, 290);
			this.btnIngres.Name = "btnIngres";
			this.btnIngres.Size = new System.Drawing.Size(99, 43);
			this.btnIngres.TabIndex = 4;
			this.btnIngres.Text = "Ingresar";
			this.btnIngres.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnIngres.UseVisualStyleBackColor = false;
			this.btnIngres.Click += new System.EventHandler(this.btnIngres_Click);
			// 
			// TxtUser
			// 
			this.TxtUser.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.TxtUser.Location = new System.Drawing.Point(173, 116);
			this.TxtUser.Name = "TxtUser";
			this.TxtUser.Size = new System.Drawing.Size(100, 26);
			this.TxtUser.TabIndex = 2;
			// 
			// TxtPassword
			// 
			this.TxtPassword.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.TxtPassword.Location = new System.Drawing.Point(173, 169);
			this.TxtPassword.Name = "TxtPassword";
			this.TxtPassword.Size = new System.Drawing.Size(100, 26);
			this.TxtPassword.TabIndex = 3;
			// 
			// pictureBox1
			// 
			this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
			this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.pictureBox1.Location = new System.Drawing.Point(1118, 12);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(141, 88);
			this.pictureBox1.TabIndex = 1;
			this.pictureBox1.TabStop = false;
			// 
			// btnCaja
			// 
			this.btnCaja.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnCaja.BackColor = System.Drawing.Color.Transparent;
			this.btnCaja.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCaja.BackgroundImage")));
			this.btnCaja.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.btnCaja.FlatAppearance.BorderSize = 0;
			this.btnCaja.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnCaja.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnCaja.Location = new System.Drawing.Point(1118, 574);
			this.btnCaja.Name = "btnCaja";
			this.btnCaja.Size = new System.Drawing.Size(135, 37);
			this.btnCaja.TabIndex = 21;
			this.btnCaja.Text = "Inicio de caja";
			this.btnCaja.UseVisualStyleBackColor = false;
			this.btnCaja.Click += new System.EventHandler(this.btnCaja_Click);
			// 
			// btnbloquear
			// 
			this.btnbloquear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnbloquear.BackColor = System.Drawing.Color.Transparent;
			this.btnbloquear.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnbloquear.BackgroundImage")));
			this.btnbloquear.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.btnbloquear.FlatAppearance.BorderSize = 0;
			this.btnbloquear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnbloquear.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnbloquear.Location = new System.Drawing.Point(1118, 338);
			this.btnbloquear.Name = "btnbloquear";
			this.btnbloquear.Size = new System.Drawing.Size(135, 37);
			this.btnbloquear.TabIndex = 22;
			this.btnbloquear.Text = "Bloquear la caja";
			this.btnbloquear.UseVisualStyleBackColor = false;
			this.btnbloquear.Click += new System.EventHandler(this.btnbloquear_Click);
			// 
			// panelcash
			// 
			this.panelcash.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.panelcash.BackColor = System.Drawing.Color.Silver;
			this.panelcash.Controls.Add(this.panel3);
			this.panelcash.Location = new System.Drawing.Point(9, 12);
			this.panelcash.Name = "panelcash";
			this.panelcash.Size = new System.Drawing.Size(1102, 558);
			this.panelcash.TabIndex = 20;
			// 
			// panel3
			// 
			this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.panel3.BackColor = System.Drawing.Color.WhiteSmoke;
			this.panel3.Controls.Add(this.btncalculate);
			this.panel3.Controls.Add(this.label25);
			this.panel3.Controls.Add(this.lbdate);
			this.panel3.Controls.Add(this.label26);
			this.panel3.Controls.Add(this.label6);
			this.panel3.Controls.Add(this.txttotal);
			this.panel3.Controls.Add(this.txtmoney);
			this.panel3.Controls.Add(this.label24);
			this.panel3.Controls.Add(this.label7);
			this.panel3.Controls.Add(this.lbcashusuario);
			this.panel3.Controls.Add(this.txt20);
			this.panel3.Controls.Add(this.lbcashcaja);
			this.panel3.Controls.Add(this.label8);
			this.panel3.Controls.Add(this.btninsertcash);
			this.panel3.Controls.Add(this.txt50);
			this.panel3.Controls.Add(this.label16);
			this.panel3.Controls.Add(this.label9);
			this.panel3.Controls.Add(this.txt500);
			this.panel3.Controls.Add(this.txt100);
			this.panel3.Controls.Add(this.label15);
			this.panel3.Controls.Add(this.label13);
			this.panel3.Controls.Add(this.txt200);
			this.panel3.Location = new System.Drawing.Point(342, 114);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(460, 337);
			this.panel3.TabIndex = 1;
			// 
			// btncalculate
			// 
			this.btncalculate.BackColor = System.Drawing.Color.Transparent;
			this.btncalculate.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btncalculate.BackgroundImage")));
			this.btncalculate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.btncalculate.FlatAppearance.BorderSize = 0;
			this.btncalculate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btncalculate.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btncalculate.Location = new System.Drawing.Point(308, 227);
			this.btncalculate.Name = "btncalculate";
			this.btncalculate.Size = new System.Drawing.Size(111, 33);
			this.btncalculate.TabIndex = 21;
			this.btncalculate.Text = "Calcular";
			this.btncalculate.UseVisualStyleBackColor = false;
			this.btncalculate.Click += new System.EventHandler(this.btncalculate_Click);
			// 
			// label25
			// 
			this.label25.AutoSize = true;
			this.label25.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label25.Location = new System.Drawing.Point(23, 25);
			this.label25.Name = "label25";
			this.label25.Size = new System.Drawing.Size(47, 15);
			this.label25.TabIndex = 2;
			this.label25.Text = "Cajero:";
			// 
			// lbdate
			// 
			this.lbdate.AutoSize = true;
			this.lbdate.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbdate.Location = new System.Drawing.Point(359, 25);
			this.lbdate.Name = "lbdate";
			this.lbdate.Size = new System.Drawing.Size(31, 15);
			this.lbdate.TabIndex = 20;
			this.lbdate.Text = "date";
			// 
			// label26
			// 
			this.label26.AutoSize = true;
			this.label26.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label26.Location = new System.Drawing.Point(304, 59);
			this.label26.Name = "label26";
			this.label26.Size = new System.Drawing.Size(115, 22);
			this.label26.TabIndex = 0;
			this.label26.Text = "Saldo inicial:";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.Location = new System.Drawing.Point(305, 25);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(44, 15);
			this.label6.TabIndex = 19;
			this.label6.Text = "Fecha:";
			// 
			// txttotal
			// 
			this.txttotal.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txttotal.Location = new System.Drawing.Point(308, 97);
			this.txttotal.Name = "txttotal";
			this.txttotal.Size = new System.Drawing.Size(111, 29);
			this.txttotal.TabIndex = 1;
			this.txttotal.Text = "0";
			// 
			// txtmoney
			// 
			this.txtmoney.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtmoney.Location = new System.Drawing.Point(193, 284);
			this.txtmoney.Name = "txtmoney";
			this.txtmoney.Size = new System.Drawing.Size(100, 21);
			this.txtmoney.TabIndex = 18;
			this.txtmoney.Text = "0";
			this.txtmoney.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtmorralla_KeyPress);
			// 
			// label24
			// 
			this.label24.AutoSize = true;
			this.label24.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label24.Location = new System.Drawing.Point(201, 25);
			this.label24.Name = "label24";
			this.label24.Size = new System.Drawing.Size(36, 15);
			this.label24.TabIndex = 3;
			this.label24.Text = "Caja:";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label7.Location = new System.Drawing.Point(61, 290);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(126, 15);
			this.label7.TabIndex = 17;
			this.label7.Text = "Cantidad de morralla:";
			// 
			// lbcashusuario
			// 
			this.lbcashusuario.AutoSize = true;
			this.lbcashusuario.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbcashusuario.Location = new System.Drawing.Point(78, 24);
			this.lbcashusuario.Name = "lbcashusuario";
			this.lbcashusuario.Size = new System.Drawing.Size(39, 15);
			this.lbcashusuario.TabIndex = 4;
			this.lbcashusuario.Text = "name";
			// 
			// txt20
			// 
			this.txt20.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt20.Location = new System.Drawing.Point(193, 240);
			this.txt20.Name = "txt20";
			this.txt20.Size = new System.Drawing.Size(100, 21);
			this.txt20.TabIndex = 16;
			this.txt20.Text = "0";
			this.txt20.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt20_KeyPress);
			// 
			// lbcashcaja
			// 
			this.lbcashcaja.AutoSize = true;
			this.lbcashcaja.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbcashcaja.Location = new System.Drawing.Point(245, 25);
			this.lbcashcaja.Name = "lbcashcaja";
			this.lbcashcaja.Size = new System.Drawing.Size(30, 15);
			this.lbcashcaja.TabIndex = 5;
			this.lbcashcaja.Text = "caja";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label8.Location = new System.Drawing.Point(30, 245);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(154, 15);
			this.label8.TabIndex = 15;
			this.label8.Text = "Cantidad de billete de $20:";
			// 
			// btninsertcash
			// 
			this.btninsertcash.BackColor = System.Drawing.Color.Transparent;
			this.btninsertcash.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btninsertcash.BackgroundImage")));
			this.btninsertcash.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.btninsertcash.FlatAppearance.BorderSize = 0;
			this.btninsertcash.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btninsertcash.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btninsertcash.Location = new System.Drawing.Point(308, 272);
			this.btninsertcash.Name = "btninsertcash";
			this.btninsertcash.Size = new System.Drawing.Size(111, 33);
			this.btninsertcash.TabIndex = 6;
			this.btninsertcash.Text = "Insertar a caja";
			this.btninsertcash.UseVisualStyleBackColor = false;
			this.btninsertcash.Click += new System.EventHandler(this.btninsertcash_Click);
			// 
			// txt50
			// 
			this.txt50.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt50.Location = new System.Drawing.Point(193, 195);
			this.txt50.Name = "txt50";
			this.txt50.Size = new System.Drawing.Size(100, 21);
			this.txt50.TabIndex = 14;
			this.txt50.Text = "0";
			this.txt50.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt50_KeyPress);
			// 
			// label16
			// 
			this.label16.AutoSize = true;
			this.label16.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label16.Location = new System.Drawing.Point(23, 65);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(161, 15);
			this.label16.TabIndex = 7;
			this.label16.Text = "Cantidad de billete de $500:";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label9.Location = new System.Drawing.Point(30, 198);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(157, 15);
			this.label9.TabIndex = 13;
			this.label9.Text = "Cantidad  de billete de $50:";
			// 
			// txt500
			// 
			this.txt500.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt500.Location = new System.Drawing.Point(193, 60);
			this.txt500.Name = "txt500";
			this.txt500.Size = new System.Drawing.Size(100, 21);
			this.txt500.TabIndex = 8;
			this.txt500.Text = "0";
			this.txt500.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt500_KeyPress);
			// 
			// txt100
			// 
			this.txt100.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt100.Location = new System.Drawing.Point(193, 150);
			this.txt100.Name = "txt100";
			this.txt100.Size = new System.Drawing.Size(100, 21);
			this.txt100.TabIndex = 12;
			this.txt100.Text = "0";
			this.txt100.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt100_KeyPress);
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label15.Location = new System.Drawing.Point(23, 110);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(161, 15);
			this.label15.TabIndex = 9;
			this.label15.Text = "Cantidad de billete de $200:";
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label13.Location = new System.Drawing.Point(23, 155);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(164, 15);
			this.label13.TabIndex = 11;
			this.label13.Text = "Cantidad  de billete de $100:";
			// 
			// txt200
			// 
			this.txt200.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt200.Location = new System.Drawing.Point(193, 105);
			this.txt200.Name = "txt200";
			this.txt200.Size = new System.Drawing.Size(100, 21);
			this.txt200.TabIndex = 10;
			this.txt200.Text = "0";
			this.txt200.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt200_KeyPress);
			// 
			// label5
			// 
			this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(12, 598);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(89, 13);
			this.label5.TabIndex = 23;
			this.label5.Text = "version  01.24.01";
			// 
			// txtdesc
			// 
			this.txtdesc.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.txtdesc.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtdesc.Location = new System.Drawing.Point(720, 86);
			this.txtdesc.Name = "txtdesc";
			this.txtdesc.Size = new System.Drawing.Size(187, 18);
			this.txtdesc.TabIndex = 23;
			// 
			// Label28
			// 
			this.Label28.AutoSize = true;
			this.Label28.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Label28.Location = new System.Drawing.Point(628, 86);
			this.Label28.Name = "Label28";
			this.Label28.Size = new System.Drawing.Size(86, 17);
			this.Label28.TabIndex = 22;
			this.Label28.Text = "Descripcion";
			// 
			// txtprice
			// 
			this.txtprice.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.txtprice.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtprice.Location = new System.Drawing.Point(720, 113);
			this.txtprice.Name = "txtprice";
			this.txtprice.Size = new System.Drawing.Size(187, 18);
			this.txtprice.TabIndex = 25;
			// 
			// label27
			// 
			this.label27.AutoSize = true;
			this.label27.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label27.Location = new System.Drawing.Point(628, 113);
			this.label27.Name = "label27";
			this.label27.Size = new System.Drawing.Size(50, 17);
			this.label27.TabIndex = 24;
			this.label27.Text = "Precio";
			// 
			// button1
			// 
			this.button1.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.button1.BackColor = System.Drawing.Color.Transparent;
			this.button1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button1.BackgroundImage")));
			this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.button1.FlatAppearance.BorderSize = 0;
			this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button1.Location = new System.Drawing.Point(1118, 381);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(135, 45);
			this.button1.TabIndex = 24;
			this.button1.Text = "Cambio de Precio";
			this.button1.UseVisualStyleBackColor = false;
			// 
			// win_Master
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1264, 620);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.btnbloquear);
			this.Controls.Add(this.btnCaja);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.lbHra);
			this.Controls.Add(this.btnCancelTick);
			this.Controls.Add(this.button5);
			this.Controls.Add(this.btnConsultProduct);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.lbtimer);
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.Panelforms);
			this.Controls.Add(this.panelLogin);
			this.Controls.Add(this.panelcash);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "win_Master";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Load += new System.EventHandler(this.Master_Load);
			this.Panelforms.ResumeLayout(false);
			this.Panelforms.PerformLayout();
			this.panel4.ResumeLayout(false);
			this.panel4.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictuproduc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridTicket)).EndInit();
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.panelLogin.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.panelcash.ResumeLayout(false);
			this.panel3.ResumeLayout(false);
			this.panel3.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Panel Panelforms;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.Label lbtimer;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button btnConsultProduct;
		private System.Windows.Forms.Button button5;
		private System.Windows.Forms.Button btnCancelTick;
		private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.Label lbHra;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label lbTicket;
		private System.Windows.Forms.Label lbhora;
		private System.Windows.Forms.Label lbcaja;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label lbfecha;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label lbtienda;
		private System.Windows.Forms.Label lbUsuario;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.DataGridView gridTicket;
		private System.Windows.Forms.Button btnpayment;
		private System.Windows.Forms.Button btnQuit;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.Label lbTotal;
		private System.Windows.Forms.Label lbCantidad;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.Button btnEnter;
		private System.Windows.Forms.Button btn0;
		private System.Windows.Forms.Button btn3;
		private System.Windows.Forms.Button btn2;
		private System.Windows.Forms.Button btn1;
		private System.Windows.Forms.Button btn6;
		private System.Windows.Forms.Button btn5;
		private System.Windows.Forms.Button btn4;
		private System.Windows.Forms.Button btn9;
		private System.Windows.Forms.Button btn8;
		private System.Windows.Forms.Button btn7;
		private System.Windows.Forms.TextBox txtCalculate;
		private System.Windows.Forms.Panel panelLogin;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button btnIngres;
		private System.Windows.Forms.TextBox TxtPassword;
		private System.Windows.Forms.TextBox TxtUser;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.PictureBox pictureBox5;
		private System.Windows.Forms.PictureBox pictureBox4;
		private System.Windows.Forms.Button btnCaja;
		private System.Windows.Forms.Button btnbloquear;
		private System.Windows.Forms.Panel panelcash;
		private System.Windows.Forms.Button btncalculate;
		private System.Windows.Forms.Label lbdate;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox txtmoney;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox txt20;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox txt50;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox txt100;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.TextBox txt200;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.TextBox txt500;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Button btninsertcash;
		private System.Windows.Forms.Label lbcashcaja;
		private System.Windows.Forms.Label lbcashusuario;
		private System.Windows.Forms.Label label24;
		private System.Windows.Forms.Label label25;
		private System.Windows.Forms.TextBox txttotal;
		private System.Windows.Forms.Label label26;
		private System.Windows.Forms.Label label5;
		public System.Windows.Forms.TextBox txtDescription;
		public System.Windows.Forms.TextBox txtUpc;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.PictureBox pictuproduc;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.Label lbefetivo;
		public System.Windows.Forms.TextBox txtprice;
		private System.Windows.Forms.Label label27;
		public System.Windows.Forms.TextBox txtdesc;
		private System.Windows.Forms.Label Label28;
		private System.Windows.Forms.Button button1;
	}
}