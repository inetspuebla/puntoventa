﻿namespace inets.UI.Logging
{
	partial class win_MethodPay
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(win_MethodPay));
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
			this.label1 = new System.Windows.Forms.Label();
			this.cmbformapago = new System.Windows.Forms.ComboBox();
			this.label3 = new System.Windows.Forms.Label();
			this.txtmonto = new System.Windows.Forms.TextBox();
			this.cmbtypecard = new System.Windows.Forms.ComboBox();
			this.label7 = new System.Windows.Forms.Label();
			this.txtfolio = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.txtTarjeta = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.gpboxTarjeta = new System.Windows.Forms.GroupBox();
			this.label23 = new System.Windows.Forms.Label();
			this.label25 = new System.Windows.Forms.Label();
			this.lblimit3 = new System.Windows.Forms.Label();
			this.lbchange3 = new System.Windows.Forms.Label();
			this.btnaplicar4 = new System.Windows.Forms.Button();
			this.lberror9 = new System.Windows.Forms.Label();
			this.lberror8 = new System.Windows.Forms.Label();
			this.lberror4 = new System.Windows.Forms.Label();
			this.lberror3 = new System.Windows.Forms.Label();
			this.lberror2 = new System.Windows.Forms.Label();
			this.lberror1 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.cmbcard = new System.Windows.Forms.ComboBox();
			this.txtAutorization = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.gpboxEfectivo = new System.Windows.Forms.GroupBox();
			this.label24 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.label26 = new System.Windows.Forms.Label();
			this.lbtotalEffective = new System.Windows.Forms.Label();
			this.lbchange = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.lblimit = new System.Windows.Forms.Label();
			this.txtmonto3 = new System.Windows.Forms.TextBox();
			this.Btnaplicar1 = new System.Windows.Forms.Button();
			this.gpboxVales = new System.Windows.Forms.GroupBox();
			this.label19 = new System.Windows.Forms.Label();
			this.label31 = new System.Windows.Forms.Label();
			this.lblimit1 = new System.Windows.Forms.Label();
			this.btnaplicar2 = new System.Windows.Forms.Button();
			this.label6 = new System.Windows.Forms.Label();
			this.lbchange1 = new System.Windows.Forms.Label();
			this.txtmonto2 = new System.Windows.Forms.TextBox();
			this.cmbDenominacion = new System.Windows.Forms.ComboBox();
			this.label4 = new System.Windows.Forms.Label();
			this.txtNumVoucher = new System.Windows.Forms.TextBox();
			this.label14 = new System.Windows.Forms.Label();
			this.cmbCompany = new System.Windows.Forms.ComboBox();
			this.label17 = new System.Windows.Forms.Label();
			this.lbstatus = new System.Windows.Forms.Label();
			this.lbdeduct = new System.Windows.Forms.Label();
			this.gpboxCheques = new System.Windows.Forms.GroupBox();
			this.label20 = new System.Windows.Forms.Label();
			this.label22 = new System.Windows.Forms.Label();
			this.lblimit2 = new System.Windows.Forms.Label();
			this.lbchange2 = new System.Windows.Forms.Label();
			this.btnaplicar3 = new System.Windows.Forms.Button();
			this.lberror7 = new System.Windows.Forms.Label();
			this.lberror6 = new System.Windows.Forms.Label();
			this.lberror5 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.dtimemision = new System.Windows.Forms.DateTimePicker();
			this.txtBank = new System.Windows.Forms.TextBox();
			this.label15 = new System.Windows.Forms.Label();
			this.txtMonto1 = new System.Windows.Forms.TextBox();
			this.label16 = new System.Windows.Forms.Label();
			this.txtFolio1 = new System.Windows.Forms.TextBox();
			this.label18 = new System.Windows.Forms.Label();
			this.lbtotal = new System.Windows.Forms.Label();
			this.label21 = new System.Windows.Forms.Label();
			this.datapagos = new System.Windows.Forms.DataGridView();
			this.label11 = new System.Windows.Forms.Label();
			this.lbticket = new System.Windows.Forms.Label();
			this.BtnCancelPrice = new System.Windows.Forms.Button();
			this.btnCticket = new System.Windows.Forms.Button();
			this.btnback = new System.Windows.Forms.Button();
			this.label27 = new System.Windows.Forms.Label();
			this.gpboxTarjeta.SuspendLayout();
			this.gpboxEfectivo.SuspendLayout();
			this.gpboxVales.SuspendLayout();
			this.gpboxCheques.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.datapagos)).BeginInit();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(277, 30);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(91, 15);
			this.label1.TabIndex = 0;
			this.label1.Text = "Forma de pago";
			// 
			// cmbformapago
			// 
			this.cmbformapago.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cmbformapago.FormattingEnabled = true;
			this.cmbformapago.Location = new System.Drawing.Point(396, 22);
			this.cmbformapago.Name = "cmbformapago";
			this.cmbformapago.Size = new System.Drawing.Size(121, 23);
			this.cmbformapago.TabIndex = 19;
			this.cmbformapago.SelectedIndexChanged += new System.EventHandler(this.cmbformapago_SelectedIndexChanged);
			this.cmbformapago.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbformapago_KeyPress);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(78, 208);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(44, 16);
			this.label3.TabIndex = 3;
			this.label3.Text = "Monto";
			// 
			// txtmonto
			// 
			this.txtmonto.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtmonto.Location = new System.Drawing.Point(129, 202);
			this.txtmonto.Name = "txtmonto";
			this.txtmonto.Size = new System.Drawing.Size(121, 22);
			this.txtmonto.TabIndex = 6;
			this.txtmonto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtmonto_KeyPress);
			// 
			// cmbtypecard
			// 
			this.cmbtypecard.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cmbtypecard.FormattingEnabled = true;
			this.cmbtypecard.Location = new System.Drawing.Point(129, 22);
			this.cmbtypecard.Name = "cmbtypecard";
			this.cmbtypecard.Size = new System.Drawing.Size(121, 24);
			this.cmbtypecard.TabIndex = 1;
			this.cmbtypecard.SelectedIndexChanged += new System.EventHandler(this.cmbtypecard_SelectedIndexChanged);
			this.cmbtypecard.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbtypecard_KeyPress);
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label7.Location = new System.Drawing.Point(30, 30);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(90, 16);
			this.label7.TabIndex = 11;
			this.label7.Text = "Tipo de tarjeta";
			// 
			// txtfolio
			// 
			this.txtfolio.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtfolio.Location = new System.Drawing.Point(129, 129);
			this.txtfolio.Name = "txtfolio";
			this.txtfolio.Size = new System.Drawing.Size(121, 22);
			this.txtfolio.TabIndex = 4;
			this.txtfolio.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtfolio_KeyPress);
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label8.Location = new System.Drawing.Point(86, 135);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(36, 16);
			this.label8.TabIndex = 12;
			this.label8.Text = "Folio";
			// 
			// txtTarjeta
			// 
			this.txtTarjeta.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtTarjeta.Location = new System.Drawing.Point(129, 164);
			this.txtTarjeta.Name = "txtTarjeta";
			this.txtTarjeta.Size = new System.Drawing.Size(121, 22);
			this.txtTarjeta.TabIndex = 5;
			this.txtTarjeta.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTarjeta_KeyPress);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.Location = new System.Drawing.Point(26, 164);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(94, 32);
			this.label5.TabIndex = 15;
			this.label5.Text = "Ultimos dígitos\r\nde tarjeta";
			this.label5.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// gpboxTarjeta
			// 
			this.gpboxTarjeta.Controls.Add(this.label23);
			this.gpboxTarjeta.Controls.Add(this.label25);
			this.gpboxTarjeta.Controls.Add(this.lblimit3);
			this.gpboxTarjeta.Controls.Add(this.lbchange3);
			this.gpboxTarjeta.Controls.Add(this.btnaplicar4);
			this.gpboxTarjeta.Controls.Add(this.lberror9);
			this.gpboxTarjeta.Controls.Add(this.lberror8);
			this.gpboxTarjeta.Controls.Add(this.lberror4);
			this.gpboxTarjeta.Controls.Add(this.lberror3);
			this.gpboxTarjeta.Controls.Add(this.lberror2);
			this.gpboxTarjeta.Controls.Add(this.lberror1);
			this.gpboxTarjeta.Controls.Add(this.label9);
			this.gpboxTarjeta.Controls.Add(this.cmbcard);
			this.gpboxTarjeta.Controls.Add(this.txtAutorization);
			this.gpboxTarjeta.Controls.Add(this.label7);
			this.gpboxTarjeta.Controls.Add(this.txtTarjeta);
			this.gpboxTarjeta.Controls.Add(this.label3);
			this.gpboxTarjeta.Controls.Add(this.cmbtypecard);
			this.gpboxTarjeta.Controls.Add(this.label2);
			this.gpboxTarjeta.Controls.Add(this.label8);
			this.gpboxTarjeta.Controls.Add(this.txtmonto);
			this.gpboxTarjeta.Controls.Add(this.txtfolio);
			this.gpboxTarjeta.Controls.Add(this.label5);
			this.gpboxTarjeta.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.gpboxTarjeta.Location = new System.Drawing.Point(290, 106);
			this.gpboxTarjeta.Name = "gpboxTarjeta";
			this.gpboxTarjeta.Size = new System.Drawing.Size(276, 348);
			this.gpboxTarjeta.TabIndex = 17;
			this.gpboxTarjeta.TabStop = false;
			// 
			// label23
			// 
			this.label23.AutoSize = true;
			this.label23.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label23.Location = new System.Drawing.Point(13, 276);
			this.label23.Name = "label23";
			this.label23.Size = new System.Drawing.Size(104, 15);
			this.label23.TabIndex = 37;
			this.label23.Text = "Forma de cambio";
			// 
			// label25
			// 
			this.label25.AutoSize = true;
			this.label25.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label25.Location = new System.Drawing.Point(36, 307);
			this.label25.Name = "label25";
			this.label25.Size = new System.Drawing.Size(87, 15);
			this.label25.TabIndex = 38;
			this.label25.Text = "Monto máximo";
			// 
			// lblimit3
			// 
			this.lblimit3.AutoSize = true;
			this.lblimit3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblimit3.Location = new System.Drawing.Point(128, 307);
			this.lblimit3.Name = "lblimit3";
			this.lblimit3.Size = new System.Drawing.Size(14, 15);
			this.lblimit3.TabIndex = 39;
			this.lblimit3.Text = "0";
			// 
			// lbchange3
			// 
			this.lbchange3.AutoSize = true;
			this.lbchange3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbchange3.Location = new System.Drawing.Point(127, 276);
			this.lbchange3.Name = "lbchange3";
			this.lbchange3.Size = new System.Drawing.Size(14, 15);
			this.lbchange3.TabIndex = 40;
			this.lbchange3.Text = "0";
			// 
			// btnaplicar4
			// 
			this.btnaplicar4.BackColor = System.Drawing.Color.Transparent;
			this.btnaplicar4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnaplicar4.BackgroundImage")));
			this.btnaplicar4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.btnaplicar4.FlatAppearance.BorderSize = 0;
			this.btnaplicar4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnaplicar4.Location = new System.Drawing.Point(129, 236);
			this.btnaplicar4.Name = "btnaplicar4";
			this.btnaplicar4.Size = new System.Drawing.Size(75, 23);
			this.btnaplicar4.TabIndex = 32;
			this.btnaplicar4.Text = "Aplicar";
			this.btnaplicar4.UseVisualStyleBackColor = false;
			this.btnaplicar4.Click += new System.EventHandler(this.btnaplicar4_Click);
			// 
			// lberror9
			// 
			this.lberror9.AutoSize = true;
			this.lberror9.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lberror9.ForeColor = System.Drawing.Color.Red;
			this.lberror9.Location = new System.Drawing.Point(255, 64);
			this.lberror9.Name = "lberror9";
			this.lberror9.Size = new System.Drawing.Size(14, 18);
			this.lberror9.TabIndex = 30;
			this.lberror9.Text = "*";
			// 
			// lberror8
			// 
			this.lberror8.AutoSize = true;
			this.lberror8.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lberror8.ForeColor = System.Drawing.Color.Red;
			this.lberror8.Location = new System.Drawing.Point(255, 27);
			this.lberror8.Name = "lberror8";
			this.lberror8.Size = new System.Drawing.Size(14, 18);
			this.lberror8.TabIndex = 31;
			this.lberror8.Text = "*";
			// 
			// lberror4
			// 
			this.lberror4.AutoSize = true;
			this.lberror4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lberror4.ForeColor = System.Drawing.Color.Red;
			this.lberror4.Location = new System.Drawing.Point(256, 202);
			this.lberror4.Name = "lberror4";
			this.lberror4.Size = new System.Drawing.Size(14, 18);
			this.lberror4.TabIndex = 24;
			this.lberror4.Text = "*";
			// 
			// lberror3
			// 
			this.lberror3.AutoSize = true;
			this.lberror3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lberror3.ForeColor = System.Drawing.Color.Red;
			this.lberror3.Location = new System.Drawing.Point(256, 164);
			this.lberror3.Name = "lberror3";
			this.lberror3.Size = new System.Drawing.Size(14, 18);
			this.lberror3.TabIndex = 23;
			this.lberror3.Text = "*";
			// 
			// lberror2
			// 
			this.lberror2.AutoSize = true;
			this.lberror2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lberror2.ForeColor = System.Drawing.Color.Red;
			this.lberror2.Location = new System.Drawing.Point(256, 131);
			this.lberror2.Name = "lberror2";
			this.lberror2.Size = new System.Drawing.Size(14, 18);
			this.lberror2.TabIndex = 22;
			this.lberror2.Text = "*";
			// 
			// lberror1
			// 
			this.lberror1.AutoSize = true;
			this.lberror1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lberror1.ForeColor = System.Drawing.Color.Red;
			this.lberror1.Location = new System.Drawing.Point(256, 94);
			this.lberror1.Name = "lberror1";
			this.lberror1.Size = new System.Drawing.Size(14, 18);
			this.lberror1.TabIndex = 21;
			this.lberror1.Text = "*";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label9.Location = new System.Drawing.Point(76, 65);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(46, 16);
			this.label9.TabIndex = 20;
			this.label9.Text = "Tarjeta";
			// 
			// cmbcard
			// 
			this.cmbcard.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cmbcard.FormattingEnabled = true;
			this.cmbcard.Location = new System.Drawing.Point(129, 57);
			this.cmbcard.Name = "cmbcard";
			this.cmbcard.Size = new System.Drawing.Size(121, 24);
			this.cmbcard.TabIndex = 2;
			this.cmbcard.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbcard_KeyPress);
			// 
			// txtAutorization
			// 
			this.txtAutorization.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtAutorization.Location = new System.Drawing.Point(129, 94);
			this.txtAutorization.Name = "txtAutorization";
			this.txtAutorization.Size = new System.Drawing.Size(121, 22);
			this.txtAutorization.TabIndex = 3;
			this.txtAutorization.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAutorization_KeyPress);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(23, 99);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(98, 16);
			this.label2.TabIndex = 17;
			this.label2.Text = "No autorización";
			// 
			// gpboxEfectivo
			// 
			this.gpboxEfectivo.Controls.Add(this.label24);
			this.gpboxEfectivo.Controls.Add(this.label13);
			this.gpboxEfectivo.Controls.Add(this.label26);
			this.gpboxEfectivo.Controls.Add(this.lbtotalEffective);
			this.gpboxEfectivo.Controls.Add(this.lbchange);
			this.gpboxEfectivo.Controls.Add(this.label10);
			this.gpboxEfectivo.Controls.Add(this.lblimit);
			this.gpboxEfectivo.Controls.Add(this.txtmonto3);
			this.gpboxEfectivo.Controls.Add(this.Btnaplicar1);
			this.gpboxEfectivo.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.gpboxEfectivo.Location = new System.Drawing.Point(290, 103);
			this.gpboxEfectivo.Name = "gpboxEfectivo";
			this.gpboxEfectivo.Size = new System.Drawing.Size(276, 190);
			this.gpboxEfectivo.TabIndex = 19;
			this.gpboxEfectivo.TabStop = false;
			// 
			// label24
			// 
			this.label24.AutoSize = true;
			this.label24.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label24.Location = new System.Drawing.Point(13, 124);
			this.label24.Name = "label24";
			this.label24.Size = new System.Drawing.Size(104, 15);
			this.label24.TabIndex = 7;
			this.label24.Text = "Forma de cambio";
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label13.Location = new System.Drawing.Point(84, 29);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(35, 16);
			this.label13.TabIndex = 11;
			this.label13.Text = "Total";
			// 
			// label26
			// 
			this.label26.AutoSize = true;
			this.label26.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label26.Location = new System.Drawing.Point(35, 154);
			this.label26.Name = "label26";
			this.label26.Size = new System.Drawing.Size(87, 15);
			this.label26.TabIndex = 10;
			this.label26.Text = "Monto máximo";
			// 
			// lbtotalEffective
			// 
			this.lbtotalEffective.AutoSize = true;
			this.lbtotalEffective.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbtotalEffective.Location = new System.Drawing.Point(125, 29);
			this.lbtotalEffective.Name = "lbtotalEffective";
			this.lbtotalEffective.Size = new System.Drawing.Size(15, 16);
			this.lbtotalEffective.TabIndex = 12;
			this.lbtotalEffective.Text = "0";
			// 
			// lbchange
			// 
			this.lbchange.AutoSize = true;
			this.lbchange.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbchange.Location = new System.Drawing.Point(126, 124);
			this.lbchange.Name = "lbchange";
			this.lbchange.Size = new System.Drawing.Size(14, 15);
			this.lbchange.TabIndex = 20;
			this.lbchange.Text = "0";
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label10.Location = new System.Drawing.Point(75, 60);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(44, 16);
			this.label10.TabIndex = 3;
			this.label10.Text = "Monto";
			// 
			// lblimit
			// 
			this.lblimit.AutoSize = true;
			this.lblimit.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblimit.Location = new System.Drawing.Point(126, 154);
			this.lblimit.Name = "lblimit";
			this.lblimit.Size = new System.Drawing.Size(14, 15);
			this.lblimit.TabIndex = 11;
			this.lblimit.Text = "0";
			// 
			// txtmonto3
			// 
			this.txtmonto3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtmonto3.Location = new System.Drawing.Point(128, 54);
			this.txtmonto3.Name = "txtmonto3";
			this.txtmonto3.Size = new System.Drawing.Size(121, 22);
			this.txtmonto3.TabIndex = 18;
			this.txtmonto3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtmonto3_KeyPress);
			// 
			// Btnaplicar1
			// 
			this.Btnaplicar1.BackColor = System.Drawing.Color.Transparent;
			this.Btnaplicar1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Btnaplicar1.BackgroundImage")));
			this.Btnaplicar1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.Btnaplicar1.FlatAppearance.BorderSize = 0;
			this.Btnaplicar1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.Btnaplicar1.Location = new System.Drawing.Point(130, 82);
			this.Btnaplicar1.Name = "Btnaplicar1";
			this.Btnaplicar1.Size = new System.Drawing.Size(75, 23);
			this.Btnaplicar1.TabIndex = 19;
			this.Btnaplicar1.Text = "Aplicar";
			this.Btnaplicar1.UseVisualStyleBackColor = false;
			this.Btnaplicar1.Click += new System.EventHandler(this.Btnaplicar1_Click_1);
			// 
			// gpboxVales
			// 
			this.gpboxVales.Controls.Add(this.label19);
			this.gpboxVales.Controls.Add(this.label31);
			this.gpboxVales.Controls.Add(this.lblimit1);
			this.gpboxVales.Controls.Add(this.btnaplicar2);
			this.gpboxVales.Controls.Add(this.label6);
			this.gpboxVales.Controls.Add(this.lbchange1);
			this.gpboxVales.Controls.Add(this.txtmonto2);
			this.gpboxVales.Controls.Add(this.cmbDenominacion);
			this.gpboxVales.Controls.Add(this.label4);
			this.gpboxVales.Controls.Add(this.txtNumVoucher);
			this.gpboxVales.Controls.Add(this.label14);
			this.gpboxVales.Controls.Add(this.cmbCompany);
			this.gpboxVales.Controls.Add(this.label17);
			this.gpboxVales.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.gpboxVales.Location = new System.Drawing.Point(293, 111);
			this.gpboxVales.Name = "gpboxVales";
			this.gpboxVales.Size = new System.Drawing.Size(276, 264);
			this.gpboxVales.TabIndex = 20;
			this.gpboxVales.TabStop = false;
			// 
			// label19
			// 
			this.label19.AutoSize = true;
			this.label19.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label19.Location = new System.Drawing.Point(12, 205);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(104, 15);
			this.label19.TabIndex = 7;
			this.label19.Text = "Forma de cambio";
			// 
			// label31
			// 
			this.label31.AutoSize = true;
			this.label31.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label31.Location = new System.Drawing.Point(35, 236);
			this.label31.Name = "label31";
			this.label31.Size = new System.Drawing.Size(87, 15);
			this.label31.TabIndex = 10;
			this.label31.Text = "Monto máximo";
			// 
			// lblimit1
			// 
			this.lblimit1.AutoSize = true;
			this.lblimit1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblimit1.Location = new System.Drawing.Point(126, 236);
			this.lblimit1.Name = "lblimit1";
			this.lblimit1.Size = new System.Drawing.Size(14, 15);
			this.lblimit1.TabIndex = 11;
			this.lblimit1.Text = "0";
			// 
			// btnaplicar2
			// 
			this.btnaplicar2.BackColor = System.Drawing.Color.Transparent;
			this.btnaplicar2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnaplicar2.BackgroundImage")));
			this.btnaplicar2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.btnaplicar2.FlatAppearance.BorderSize = 0;
			this.btnaplicar2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnaplicar2.Location = new System.Drawing.Point(130, 164);
			this.btnaplicar2.Name = "btnaplicar2";
			this.btnaplicar2.Size = new System.Drawing.Size(75, 23);
			this.btnaplicar2.TabIndex = 27;
			this.btnaplicar2.Text = "Aplicar";
			this.btnaplicar2.UseVisualStyleBackColor = false;
			this.btnaplicar2.Click += new System.EventHandler(this.btnaplicar2_Click);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.Location = new System.Drawing.Point(75, 135);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(44, 16);
			this.label6.TabIndex = 26;
			this.label6.Text = "Monto";
			// 
			// lbchange1
			// 
			this.lbchange1.AutoSize = true;
			this.lbchange1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbchange1.Location = new System.Drawing.Point(125, 205);
			this.lbchange1.Name = "lbchange1";
			this.lbchange1.Size = new System.Drawing.Size(14, 15);
			this.lbchange1.TabIndex = 20;
			this.lbchange1.Text = "0";
			// 
			// txtmonto2
			// 
			this.txtmonto2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtmonto2.Location = new System.Drawing.Point(130, 132);
			this.txtmonto2.Name = "txtmonto2";
			this.txtmonto2.Size = new System.Drawing.Size(121, 22);
			this.txtmonto2.TabIndex = 10;
			this.txtmonto2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtmonto2_KeyPress);
			// 
			// cmbDenominacion
			// 
			this.cmbDenominacion.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cmbDenominacion.FormattingEnabled = true;
			this.cmbDenominacion.Location = new System.Drawing.Point(130, 58);
			this.cmbDenominacion.Name = "cmbDenominacion";
			this.cmbDenominacion.Size = new System.Drawing.Size(121, 24);
			this.cmbDenominacion.TabIndex = 8;
			this.cmbDenominacion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbDenominacion_KeyPress);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(16, 100);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(104, 16);
			this.label4.TabIndex = 23;
			this.label4.Text = "Número de vales";
			// 
			// txtNumVoucher
			// 
			this.txtNumVoucher.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtNumVoucher.Location = new System.Drawing.Point(130, 95);
			this.txtNumVoucher.Name = "txtNumVoucher";
			this.txtNumVoucher.Size = new System.Drawing.Size(121, 22);
			this.txtNumVoucher.TabIndex = 9;
			this.txtNumVoucher.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNumVoucher_KeyPress);
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label14.Location = new System.Drawing.Point(63, 30);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(60, 16);
			this.label14.TabIndex = 21;
			this.label14.Text = "Empresa";
			// 
			// cmbCompany
			// 
			this.cmbCompany.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cmbCompany.FormattingEnabled = true;
			this.cmbCompany.Location = new System.Drawing.Point(130, 22);
			this.cmbCompany.Name = "cmbCompany";
			this.cmbCompany.Size = new System.Drawing.Size(121, 24);
			this.cmbCompany.TabIndex = 7;
			this.cmbCompany.SelectedIndexChanged += new System.EventHandler(this.cmbCompany_SelectedIndexChanged);
			this.cmbCompany.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbCompany_KeyPress);
			// 
			// label17
			// 
			this.label17.AutoSize = true;
			this.label17.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label17.Location = new System.Drawing.Point(34, 66);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(90, 16);
			this.label17.TabIndex = 3;
			this.label17.Text = "Denominación";
			// 
			// lbstatus
			// 
			this.lbstatus.AutoSize = true;
			this.lbstatus.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbstatus.Location = new System.Drawing.Point(15, 90);
			this.lbstatus.Name = "lbstatus";
			this.lbstatus.Size = new System.Drawing.Size(49, 18);
			this.lbstatus.TabIndex = 19;
			this.lbstatus.Text = "Resta";
			// 
			// lbdeduct
			// 
			this.lbdeduct.AutoSize = true;
			this.lbdeduct.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbdeduct.Location = new System.Drawing.Point(80, 90);
			this.lbdeduct.Name = "lbdeduct";
			this.lbdeduct.Size = new System.Drawing.Size(16, 18);
			this.lbdeduct.TabIndex = 20;
			this.lbdeduct.Text = "0";
			// 
			// gpboxCheques
			// 
			this.gpboxCheques.Controls.Add(this.label20);
			this.gpboxCheques.Controls.Add(this.label22);
			this.gpboxCheques.Controls.Add(this.lblimit2);
			this.gpboxCheques.Controls.Add(this.lbchange2);
			this.gpboxCheques.Controls.Add(this.btnaplicar3);
			this.gpboxCheques.Controls.Add(this.lberror7);
			this.gpboxCheques.Controls.Add(this.lberror6);
			this.gpboxCheques.Controls.Add(this.lberror5);
			this.gpboxCheques.Controls.Add(this.label12);
			this.gpboxCheques.Controls.Add(this.dtimemision);
			this.gpboxCheques.Controls.Add(this.txtBank);
			this.gpboxCheques.Controls.Add(this.label15);
			this.gpboxCheques.Controls.Add(this.txtMonto1);
			this.gpboxCheques.Controls.Add(this.label16);
			this.gpboxCheques.Controls.Add(this.txtFolio1);
			this.gpboxCheques.Controls.Add(this.label18);
			this.gpboxCheques.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.gpboxCheques.Location = new System.Drawing.Point(291, 110);
			this.gpboxCheques.Name = "gpboxCheques";
			this.gpboxCheques.Size = new System.Drawing.Size(276, 271);
			this.gpboxCheques.TabIndex = 23;
			this.gpboxCheques.TabStop = false;
			// 
			// label20
			// 
			this.label20.AutoSize = true;
			this.label20.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label20.Location = new System.Drawing.Point(13, 209);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(104, 15);
			this.label20.TabIndex = 33;
			this.label20.Text = "Forma de cambio";
			// 
			// label22
			// 
			this.label22.AutoSize = true;
			this.label22.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label22.Location = new System.Drawing.Point(36, 240);
			this.label22.Name = "label22";
			this.label22.Size = new System.Drawing.Size(87, 15);
			this.label22.TabIndex = 34;
			this.label22.Text = "Monto máximo";
			// 
			// lblimit2
			// 
			this.lblimit2.AutoSize = true;
			this.lblimit2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblimit2.Location = new System.Drawing.Point(128, 240);
			this.lblimit2.Name = "lblimit2";
			this.lblimit2.Size = new System.Drawing.Size(14, 15);
			this.lblimit2.TabIndex = 35;
			this.lblimit2.Text = "0";
			// 
			// lbchange2
			// 
			this.lbchange2.AutoSize = true;
			this.lbchange2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbchange2.Location = new System.Drawing.Point(127, 209);
			this.lbchange2.Name = "lbchange2";
			this.lbchange2.Size = new System.Drawing.Size(14, 15);
			this.lbchange2.TabIndex = 36;
			this.lbchange2.Text = "0";
			// 
			// btnaplicar3
			// 
			this.btnaplicar3.BackColor = System.Drawing.Color.Transparent;
			this.btnaplicar3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnaplicar3.BackgroundImage")));
			this.btnaplicar3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.btnaplicar3.FlatAppearance.BorderSize = 0;
			this.btnaplicar3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnaplicar3.Location = new System.Drawing.Point(130, 173);
			this.btnaplicar3.Name = "btnaplicar3";
			this.btnaplicar3.Size = new System.Drawing.Size(75, 23);
			this.btnaplicar3.TabIndex = 32;
			this.btnaplicar3.Text = "Aplicar";
			this.btnaplicar3.UseVisualStyleBackColor = false;
			this.btnaplicar3.Click += new System.EventHandler(this.btnaplicar3_Click);
			// 
			// lberror7
			// 
			this.lberror7.AutoSize = true;
			this.lberror7.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lberror7.ForeColor = System.Drawing.Color.Red;
			this.lberror7.Location = new System.Drawing.Point(257, 128);
			this.lberror7.Name = "lberror7";
			this.lberror7.Size = new System.Drawing.Size(14, 18);
			this.lberror7.TabIndex = 31;
			this.lberror7.Text = "*";
			// 
			// lberror6
			// 
			this.lberror6.AutoSize = true;
			this.lberror6.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lberror6.ForeColor = System.Drawing.Color.Red;
			this.lberror6.Location = new System.Drawing.Point(257, 93);
			this.lberror6.Name = "lberror6";
			this.lberror6.Size = new System.Drawing.Size(14, 18);
			this.lberror6.TabIndex = 30;
			this.lberror6.Text = "*";
			// 
			// lberror5
			// 
			this.lberror5.AutoSize = true;
			this.lberror5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lberror5.ForeColor = System.Drawing.Color.Red;
			this.lberror5.Location = new System.Drawing.Point(257, 22);
			this.lberror5.Name = "lberror5";
			this.lberror5.Size = new System.Drawing.Size(14, 18);
			this.lberror5.TabIndex = 29;
			this.lberror5.Text = "*";
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label12.Location = new System.Drawing.Point(10, 65);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(111, 16);
			this.label12.TabIndex = 25;
			this.label12.Text = "Fecha de emisión";
			// 
			// dtimemision
			// 
			this.dtimemision.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dtimemision.Location = new System.Drawing.Point(130, 60);
			this.dtimemision.Name = "dtimemision";
			this.dtimemision.Size = new System.Drawing.Size(121, 21);
			this.dtimemision.TabIndex = 12;
			this.dtimemision.ValueChanged += new System.EventHandler(this.dtimemision_ValueChanged);
			// 
			// txtBank
			// 
			this.txtBank.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtBank.Location = new System.Drawing.Point(130, 23);
			this.txtBank.Name = "txtBank";
			this.txtBank.Size = new System.Drawing.Size(121, 22);
			this.txtBank.TabIndex = 11;
			this.txtBank.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBank_KeyPress);
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label15.Location = new System.Drawing.Point(80, 134);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(44, 16);
			this.label15.TabIndex = 15;
			this.label15.Text = "Monto";
			// 
			// txtMonto1
			// 
			this.txtMonto1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtMonto1.Location = new System.Drawing.Point(130, 128);
			this.txtMonto1.Name = "txtMonto1";
			this.txtMonto1.Size = new System.Drawing.Size(121, 22);
			this.txtMonto1.TabIndex = 14;
			this.txtMonto1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMonto1_KeyPress);
			// 
			// label16
			// 
			this.label16.AutoSize = true;
			this.label16.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label16.Location = new System.Drawing.Point(79, 32);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(45, 16);
			this.label16.TabIndex = 21;
			this.label16.Text = "Banco";
			// 
			// txtFolio1
			// 
			this.txtFolio1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtFolio1.Location = new System.Drawing.Point(130, 93);
			this.txtFolio1.Name = "txtFolio1";
			this.txtFolio1.Size = new System.Drawing.Size(121, 22);
			this.txtFolio1.TabIndex = 13;
			this.txtFolio1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFolio1_KeyPress);
			// 
			// label18
			// 
			this.label18.AutoSize = true;
			this.label18.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label18.Location = new System.Drawing.Point(88, 100);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(36, 16);
			this.label18.TabIndex = 3;
			this.label18.Text = "Folio";
			// 
			// lbtotal
			// 
			this.lbtotal.AutoSize = true;
			this.lbtotal.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbtotal.Location = new System.Drawing.Point(80, 60);
			this.lbtotal.Name = "lbtotal";
			this.lbtotal.Size = new System.Drawing.Size(16, 18);
			this.lbtotal.TabIndex = 25;
			this.lbtotal.Text = "0";
			// 
			// label21
			// 
			this.label21.AutoSize = true;
			this.label21.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label21.Location = new System.Drawing.Point(12, 60);
			this.label21.Name = "label21";
			this.label21.Size = new System.Drawing.Size(55, 18);
			this.label21.TabIndex = 24;
			this.label21.Text = "Total $";
			// 
			// datapagos
			// 
			this.datapagos.AllowUserToAddRows = false;
			dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.datapagos.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.datapagos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
			dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
			dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
			this.datapagos.DefaultCellStyle = dataGridViewCellStyle2;
			this.datapagos.Location = new System.Drawing.Point(12, 111);
			this.datapagos.Name = "datapagos";
			this.datapagos.Size = new System.Drawing.Size(248, 196);
			this.datapagos.TabIndex = 26;
			this.datapagos.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.datapagos_RowHeaderMouseClick);
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label11.Location = new System.Drawing.Point(12, 30);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(52, 18);
			this.label11.TabIndex = 27;
			this.label11.Text = "Ticket";
			// 
			// lbticket
			// 
			this.lbticket.AutoSize = true;
			this.lbticket.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbticket.Location = new System.Drawing.Point(80, 30);
			this.lbticket.Name = "lbticket";
			this.lbticket.Size = new System.Drawing.Size(16, 18);
			this.lbticket.TabIndex = 28;
			this.lbticket.Text = "0";
			// 
			// BtnCancelPrice
			// 
			this.BtnCancelPrice.BackColor = System.Drawing.Color.Transparent;
			this.BtnCancelPrice.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtnCancelPrice.BackgroundImage")));
			this.BtnCancelPrice.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.BtnCancelPrice.FlatAppearance.BorderSize = 0;
			this.BtnCancelPrice.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.BtnCancelPrice.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.BtnCancelPrice.Location = new System.Drawing.Point(12, 367);
			this.BtnCancelPrice.Name = "BtnCancelPrice";
			this.BtnCancelPrice.Size = new System.Drawing.Size(132, 35);
			this.BtnCancelPrice.TabIndex = 21;
			this.BtnCancelPrice.Text = "Cancelar pago";
			this.BtnCancelPrice.UseVisualStyleBackColor = false;
			this.BtnCancelPrice.Click += new System.EventHandler(this.BtnCancelPrice_Click);
			// 
			// btnCticket
			// 
			this.btnCticket.BackColor = System.Drawing.Color.Transparent;
			this.btnCticket.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCticket.BackgroundImage")));
			this.btnCticket.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.btnCticket.FlatAppearance.BorderSize = 0;
			this.btnCticket.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnCticket.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnCticket.Location = new System.Drawing.Point(12, 415);
			this.btnCticket.Name = "btnCticket";
			this.btnCticket.Size = new System.Drawing.Size(132, 35);
			this.btnCticket.TabIndex = 33;
			this.btnCticket.Text = "Cancelar ticket";
			this.btnCticket.UseVisualStyleBackColor = false;
			this.btnCticket.Click += new System.EventHandler(this.btnCticket_Click);
			// 
			// btnback
			// 
			this.btnback.BackColor = System.Drawing.Color.Transparent;
			this.btnback.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnback.BackgroundImage")));
			this.btnback.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.btnback.FlatAppearance.BorderSize = 0;
			this.btnback.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnback.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnback.Location = new System.Drawing.Point(12, 316);
			this.btnback.Name = "btnback";
			this.btnback.Size = new System.Drawing.Size(132, 35);
			this.btnback.TabIndex = 34;
			this.btnback.Text = "Regresar a ticket";
			this.btnback.UseVisualStyleBackColor = false;
			this.btnback.Click += new System.EventHandler(this.btnback_Click);
			// 
			// label27
			// 
			this.label27.AutoSize = true;
			this.label27.Location = new System.Drawing.Point(277, 65);
			this.label27.Name = "label27";
			this.label27.Size = new System.Drawing.Size(41, 13);
			this.label27.TabIndex = 35;
			this.label27.Text = "label27";
			// 
			// win_MethodPay
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(588, 466);
			this.ControlBox = false;
			this.Controls.Add(this.label27);
			this.Controls.Add(this.btnCticket);
			this.Controls.Add(this.BtnCancelPrice);
			this.Controls.Add(this.lbticket);
			this.Controls.Add(this.label11);
			this.Controls.Add(this.datapagos);
			this.Controls.Add(this.lbtotal);
			this.Controls.Add(this.label21);
			this.Controls.Add(this.lbdeduct);
			this.Controls.Add(this.lbstatus);
			this.Controls.Add(this.cmbformapago);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.btnback);
			this.Controls.Add(this.gpboxEfectivo);
			this.Controls.Add(this.gpboxCheques);
			this.Controls.Add(this.gpboxVales);
			this.Controls.Add(this.gpboxTarjeta);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
			this.Name = "win_MethodPay";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Forma de pago";
			this.Load += new System.EventHandler(this.Pago_Load);
			this.gpboxTarjeta.ResumeLayout(false);
			this.gpboxTarjeta.PerformLayout();
			this.gpboxEfectivo.ResumeLayout(false);
			this.gpboxEfectivo.PerformLayout();
			this.gpboxVales.ResumeLayout(false);
			this.gpboxVales.PerformLayout();
			this.gpboxCheques.ResumeLayout(false);
			this.gpboxCheques.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.datapagos)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox cmbformapago;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox txtmonto;
		private System.Windows.Forms.ComboBox cmbtypecard;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox txtfolio;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox txtTarjeta;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.GroupBox gpboxTarjeta;
		private System.Windows.Forms.TextBox txtAutorization;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label lbstatus;
		private System.Windows.Forms.Label lbdeduct;
		private System.Windows.Forms.GroupBox gpboxEfectivo;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.TextBox txtmonto3;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label lbtotalEffective;
		private System.Windows.Forms.GroupBox gpboxVales;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.ComboBox cmbCompany;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.GroupBox gpboxCheques;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.TextBox txtMonto1;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.TextBox txtFolio1;
		private System.Windows.Forms.Label lbtotal;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.DataGridView datapagos;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.ComboBox cmbcard;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox txtNumVoucher;
		private System.Windows.Forms.TextBox txtBank;
		private System.Windows.Forms.ComboBox cmbDenominacion;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label lbticket;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox txtmonto2;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.DateTimePicker dtimemision;
		private System.Windows.Forms.Label lberror4;
		private System.Windows.Forms.Label lberror3;
		private System.Windows.Forms.Label lberror2;
		private System.Windows.Forms.Label lberror1;
		private System.Windows.Forms.Label lberror7;
		private System.Windows.Forms.Label lberror6;
		private System.Windows.Forms.Label lberror5;
		private System.Windows.Forms.Button BtnCancelPrice;
		private System.Windows.Forms.Label lberror9;
		private System.Windows.Forms.Label lberror8;
		private System.Windows.Forms.Label lblimit;
		private System.Windows.Forms.Label label26;
		private System.Windows.Forms.Label label24;
		private System.Windows.Forms.Button btnCticket;
		private System.Windows.Forms.Button btnback;
		private System.Windows.Forms.Button Btnaplicar1;
		private System.Windows.Forms.Button btnaplicar2;
		private System.Windows.Forms.Button btnaplicar3;
		private System.Windows.Forms.Button btnaplicar4;
		private System.Windows.Forms.Label lbchange;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.Label label31;
		private System.Windows.Forms.Label lbchange1;
		private System.Windows.Forms.Label lblimit1;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.Label lblimit2;
		private System.Windows.Forms.Label lbchange2;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.Label label25;
		private System.Windows.Forms.Label lblimit3;
		private System.Windows.Forms.Label lbchange3;
		private System.Windows.Forms.Label label27;
	}
}