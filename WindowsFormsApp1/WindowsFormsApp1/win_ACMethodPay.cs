﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Inets.Bussines.Estandard;
using Inets.Models.Entity_Entities;
using inets.UI.Logging;
using ExternalClass;

namespace inets.UI.Logging
{
	public partial class win_ACMethodPay : Form
	{
		public Iproducts iproducts { get; set; }
		
		//ROV 16/10/18 SE DECLARAN LAS VARIABLES QUE RECIBIRAN DATOS DE LAS DEMAS FORMULARIOS
		public string user, ticket;
		public int number;
		public int iduser;
		public int index;


		//ROV 16/10/18 SE GENERAN LOS OBJETOS A UTILIZAR DURANTE LA CLASE
		public  E_MPTicket e_MPTicket = new E_MPTicket();
		public List<E_MPTicket> l_MPTickets = new List<E_MPTicket>();
		public E_Cancellations e_Cancellation = new E_Cancellations();

		B_MPTicket b_MPTicket = new B_MPTicket();
		B_formats b_Formats = new B_formats();
		E_Cancellations cancellation = new E_Cancellations();
		B_Cancellations b_Cancellations = new B_Cancellations();
		
		public win_ACMethodPay()
		{
			InitializeComponent();
		}
		
		//ROV 16/10/18 CARGA DE FORMULARIO 
		private void win_ACMethodPay_Load(object sender, EventArgs e)
		{
			label10.Visible = false;
			txtobservation.Visible = false;

			var item = from x in l_MPTickets.ToList()
					   where x.dCancel > 0
					   select x;

			datacancel.DataSource = item.Select(x => new {x.Methodpay,x.dCancel }).ToList();

			cmbreason.Items.Add("Otro");
		}

		//ROV 16/10/18 Bloqueo de escritura en en el combobox 
		private void cmbreason_KeyPress(object sender, KeyPressEventArgs e)
		{
			e.Handled = true;
		}

		private void txtobservation_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (e.KeyChar == ')')
			{
				e.Handled = false;
			}
			if (e.KeyChar == '(')
			{
				e.Handled = false;
			}
			if (e.KeyChar == ',')
			{
				e.Handled = false;
			}
			if (e.KeyChar == ' ')
			{
				e.Handled = false;
			}
			else if (e.KeyChar == '.')
			{
				e.Handled = false;
			}
			else if (e.KeyChar == '-')
			{
				e.Handled = false;
			}
			else if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)Keys.Back) && (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.')))
			{
				e.Handled = true;
				return;
			}
		}

		//ROV 16/10/18  Funcion del datagrid al momento de seleccionar un item 
		private void cmbreason_SelectedIndexChanged(object sender, EventArgs e)
		{
			if(cmbreason.SelectedItem.ToString()== "Otro")
			{
				label10.Visible = true;
				txtobservation.Visible = true;
			}
			else
			{
				label10.Visible = false;
				txtobservation.Visible = false;
			}
		}

		//ROV 16/10/18 Funcion del boton cancelar al momento de recibir y validar los datos del metodo de pago 
		private void btnCancellation_Click(object sender, EventArgs e)
		{
			if(cmbreason.SelectedItem== null)
			{
				MessageBox.Show("Seleccione un motivo de cancelación");
			}else
			{
				if(txtobservation.Visible== true)
				{
					if (txtobservation.Text == "" || txtobservation.Text == " ")
					{
						MessageBox.Show("Favor de escribir una observacion de cancelacion");

					}
					else
					{
						e_Cancellation.idTicket = e_Cancellation.idTicket;
						e_Cancellation.idUser = e_Cancellation.idUser;
						e_Cancellation.sObservation = cmbreason.SelectedItem.ToString();
						e_Cancellation.Comentary = txtobservation.Text;
						e_Cancellation.dDatetime = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
						
						b_Formats.printcancelMethodpay(e_Cancellation, ticket, l_MPTickets);
						iproducts.ConsultProduct(cmbreason.Text, txtobservation.Text);

						this.Close();

					}
				}
				else
				{
					//ROV 16/10/18 Se insertan los datos en la entidad de cancelacion
					e_Cancellation.idUser = e_Cancellation.idUser;
					e_Cancellation.sObservation = cmbreason.SelectedItem.ToString();
					e_Cancellation.Comentary = txtobservation.Text;
					e_Cancellation.dDatetime = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
					
					b_Formats.printcancelMethodpay(e_Cancellation, ticket, l_MPTickets);

					iproducts.ConsultProduct(cmbreason.Text, txtobservation.Text);

					this.Close();
				}
			}
		}

	}
}
