﻿namespace inets.UI.Logging
{
	partial class win_Validation
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(win_Validation));
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.txtSuper = new System.Windows.Forms.TextBox();
			this.txtpasword = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.btnAcept = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(64, 62);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(51, 15);
			this.label1.TabIndex = 1;
			this.label1.Text = "Usuario";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(43, 111);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(72, 15);
			this.label2.TabIndex = 2;
			this.label2.Text = "Contraseña";
			// 
			// txtSuper
			// 
			this.txtSuper.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtSuper.Location = new System.Drawing.Point(121, 56);
			this.txtSuper.Name = "txtSuper";
			this.txtSuper.Size = new System.Drawing.Size(130, 21);
			this.txtSuper.TabIndex = 1;
			// 
			// txtpasword
			// 
			this.txtpasword.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtpasword.Location = new System.Drawing.Point(121, 105);
			this.txtpasword.Name = "txtpasword";
			this.txtpasword.Size = new System.Drawing.Size(130, 21);
			this.txtpasword.TabIndex = 2;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(43, 24);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(192, 18);
			this.label3.TabIndex = 5;
			this.label3.Text = "Autorización de supervisor";
			// 
			// btnAcept
			// 
			this.btnAcept.BackColor = System.Drawing.Color.Transparent;
			this.btnAcept.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAcept.BackgroundImage")));
			this.btnAcept.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.btnAcept.FlatAppearance.BorderSize = 0;
			this.btnAcept.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnAcept.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnAcept.Image = ((System.Drawing.Image)(resources.GetObject("btnAcept.Image")));
			this.btnAcept.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.btnAcept.Location = new System.Drawing.Point(159, 156);
			this.btnAcept.Name = "btnAcept";
			this.btnAcept.Size = new System.Drawing.Size(92, 26);
			this.btnAcept.TabIndex = 3;
			this.btnAcept.Text = "Ingresar";
			this.btnAcept.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnAcept.UseVisualStyleBackColor = false;
			this.btnAcept.Click += new System.EventHandler(this.btnAcept_Click);
			// 
			// win_Validation
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(303, 202);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.txtpasword);
			this.Controls.Add(this.txtSuper);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.btnAcept);
			this.Name = "win_Validation";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Load += new System.EventHandler(this.win_Validation_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btnAcept;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox txtSuper;
		private System.Windows.Forms.TextBox txtpasword;
		private System.Windows.Forms.Label label3;
	}
}