﻿namespace inets.UI.Logging
{
	partial class win_ACProduct
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(win_ACProduct));
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			this.label1 = new System.Windows.Forms.Label();
			this.cmbreason = new System.Windows.Forms.ComboBox();
			this.label2 = new System.Windows.Forms.Label();
			this.lbcant = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.txtObservation = new System.Windows.Forms.TextBox();
			this.btnConfirmation = new System.Windows.Forms.Button();
			this.btnlist = new System.Windows.Forms.Button();
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(14, 115);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(74, 30);
			this.label1.TabIndex = 0;
			this.label1.Text = "Tipo de \r\nobservación";
			// 
			// cmbreason
			// 
			this.cmbreason.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cmbreason.FormattingEnabled = true;
			this.cmbreason.Location = new System.Drawing.Point(96, 122);
			this.cmbreason.Name = "cmbreason";
			this.cmbreason.Size = new System.Drawing.Size(159, 23);
			this.cmbreason.TabIndex = 2;
			this.cmbreason.SelectedIndexChanged += new System.EventHandler(this.cmbreason_SelectedIndexChanged);
			this.cmbreason.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbreason_KeyPress);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(14, 53);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(73, 30);
			this.label2.TabIndex = 2;
			this.label2.Text = "#Productos \r\neliminados";
			// 
			// lbcant
			// 
			this.lbcant.AutoSize = true;
			this.lbcant.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbcant.Location = new System.Drawing.Point(93, 68);
			this.lbcant.Name = "lbcant";
			this.lbcant.Size = new System.Drawing.Size(41, 15);
			this.lbcant.TabIndex = 3;
			this.lbcant.Text = "label3";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(30, 9);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(248, 18);
			this.label4.TabIndex = 4;
			this.label4.Text = "Cancelación de productos de línea";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.Location = new System.Drawing.Point(14, 179);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(76, 15);
			this.label5.TabIndex = 6;
			this.label5.Text = "Observación";
			// 
			// txtObservation
			// 
			this.txtObservation.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtObservation.Location = new System.Drawing.Point(96, 173);
			this.txtObservation.Name = "txtObservation";
			this.txtObservation.Size = new System.Drawing.Size(159, 21);
			this.txtObservation.TabIndex = 3;
			this.txtObservation.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtObservation_KeyPress);
			// 
			// btnConfirmation
			// 
			this.btnConfirmation.BackColor = System.Drawing.Color.Transparent;
			this.btnConfirmation.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnConfirmation.BackgroundImage")));
			this.btnConfirmation.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.btnConfirmation.FlatAppearance.BorderSize = 0;
			this.btnConfirmation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnConfirmation.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnConfirmation.Location = new System.Drawing.Point(166, 221);
			this.btnConfirmation.Name = "btnConfirmation";
			this.btnConfirmation.Size = new System.Drawing.Size(89, 31);
			this.btnConfirmation.TabIndex = 4;
			this.btnConfirmation.Text = "Aceptar";
			this.btnConfirmation.UseVisualStyleBackColor = false;
			this.btnConfirmation.Click += new System.EventHandler(this.btnConfirmation_Click);
			// 
			// btnlist
			// 
			this.btnlist.BackColor = System.Drawing.Color.Transparent;
			this.btnlist.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnlist.BackgroundImage")));
			this.btnlist.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.btnlist.FlatAppearance.BorderSize = 0;
			this.btnlist.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnlist.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnlist.Location = new System.Drawing.Point(151, 53);
			this.btnlist.Name = "btnlist";
			this.btnlist.Size = new System.Drawing.Size(104, 30);
			this.btnlist.TabIndex = 1;
			this.btnlist.Text = "Ver productos";
			this.btnlist.UseVisualStyleBackColor = false;
			this.btnlist.Click += new System.EventHandler(this.btnlist_Click);
			// 
			// dataGridView1
			// 
			dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView1.Location = new System.Drawing.Point(306, 18);
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.Size = new System.Drawing.Size(361, 254);
			this.dataGridView1.TabIndex = 9;
			// 
			// win_ACProduct
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(297, 281);
			this.Controls.Add(this.dataGridView1);
			this.Controls.Add(this.btnConfirmation);
			this.Controls.Add(this.txtObservation);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.btnlist);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.lbcant);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.cmbreason);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
			this.Name = "win_ACProduct";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Load += new System.EventHandler(this.win_Confirmation_Load);
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox cmbreason;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label lbcant;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox txtObservation;
		private System.Windows.Forms.Button btnConfirmation;
		private System.Windows.Forms.Button btnlist;
		private System.Windows.Forms.DataGridView dataGridView1;
	}
}