﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Inets.Bussines.Estandard;
using Inets.Models.Entity_Entities;
using inets.UI.Logging;
using ExternalClass;

namespace inets.UI.Logging
{
	public partial class win_ACProduct : Form ,Iproducts
	{
		public Iproducts iproducts { get; set; }

		public string Store, user, ticket;
		decimal change;


		//ROV 16/10/18 Se declaran los objetos y variables a trabajar recibir informacion
		public List<LineaProduct> lineaProducts = new List<LineaProduct>();
		public List<E_MPTicket> l_MPTickets = new List<E_MPTicket>();
		public E_Cancellations e_Cancellation = new E_Cancellations();

		E_Store e_Store = new E_Store();
		B_Store b_Store = new B_Store();
		E_DetailTicket e_DetailTicket = new E_DetailTicket();
		B_DetailTicket b_DetailTicket = new B_DetailTicket();
		E_Cancellations e_cancellation = new E_Cancellations();
		B_Cancellations b_Cancellation = new B_Cancellations();
		

		public win_ACProduct()
		{
			InitializeComponent();
		}

		//ROV 16/10/18 carga del windows form 
		private void win_Confirmation_Load(object sender, EventArgs e)
		{
			decimal prod = 0;
			cmbreason.Items.Add("No le alcanzo");
			cmbreason.Items.Add("Otro");

			txtObservation.Visible = false;

			label5.Visible = false;

			//ROV 16/10/18 marca el tamaño del formulario de que dimensiones se tiene que abrir al inicio 
			this.Size = new Size(321, 323);

			//ROV 16/10/18 muestra la cantidad de productos cancelados 
			foreach (var item in lineaProducts)
			{
				prod = prod + item.Deletes;
			}
			lbcant.Text = prod.ToString();

			foreach (var item in lineaProducts)
			{
				if (item.Deletes > 0)
				{
					b_DetailTicket.InsertlistTicket(item);
				}
			}

			//ROV 16/10/18 Carga el datagridview con los datos que se tienen que Visualize
			dataGridView1.DataSource = b_DetailTicket.visualizalist().Select(x => new
			{
				x.Deletes,
				x.UPC,
				x.Product,
				x.Price
			}).ToList();

			dataGridView1.Columns["Deletes"].HeaderText = "Eliminados";
			dataGridView1.Columns["Product"].HeaderText = "Producto";
			dataGridView1.Columns["Price"].HeaderText = "Precio";

			/*dataGridView1.DataSource = lineaProducts.Select( x => new
			{
				x.Deletes,
				x.UPC,
				x.Product,
				x.Price
			}).ToList();*/

			//ROV 16/10/18 Ajusta las columnas que esten del tamaño del datagrid y evitar el scroll
			dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

		}

		//ROV 16/10/18 Funcion del boton de la carga de la ventana 
		private void btnlist_Click(object sender, EventArgs e)
		{
			//ROV 16/10/18 cambia el tamaño de la pantalla 
			if (this.Size.Width == 695 && this.Size.Height == 323)
			{
				this.Size = new Size(321, 323);
			}
			else
			{
				this.Size = new Size(695, 323);
			}

		}

		//ROV 16/10/18 funcion del combobox al momento de seleccionar un item 
		private void cmbreason_SelectedIndexChanged(object sender, EventArgs e)
		{
			if(cmbreason.SelectedItem.ToString() == "Otro")
			{
				txtObservation.Visible = true;
				label5.Visible = true;
			}
			else
			{
				txtObservation.Visible = false;
				label5.Visible = false;
			}
		}

		//ROV 16/10/18 funcion del boton de autorizacion de cancelacion de productos 
		private void btnConfirmation_Click(object sender, EventArgs e)
		{
			win_MethodPay win_MethodPay = new win_MethodPay();

			//ROV 16/10/18 validacion de que se selecciono un motivo
			if (cmbreason.SelectedItem == null)
			{
				MessageBox.Show("Seleccione un motivo de cancelación");
			}
			else
			{
				if (txtObservation.Visible == true)
				{
					if (txtObservation.Text == "" || txtObservation.Text == " ")
					{
						MessageBox.Show("Favor de escribir una observacion de cancelacion");

					}
					else
					{
						//ROV 16/10/18 Se almacena los datos en su entidad
						e_cancellation.idTicket = e_Cancellation.idTicket;
						e_cancellation.idUser = e_Cancellation.idUser;
						e_cancellation.sObservation = cmbreason.SelectedItem.ToString();
						e_cancellation.Comentary = txtObservation.Text;
						e_cancellation.dDatetime = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
						
						//ROV 16/10/18 Se mandan datos  a la ventana de formas de pago 
						win_MethodPay.Store = e_Cancellation.store;
						win_MethodPay.Totality = e_Cancellation.total;
						win_MethodPay.Tick = ticket;
						win_MethodPay.lineaProducts = lineaProducts;
						win_MethodPay.iproducts = this;
						win_MethodPay.Autorization = user;
						win_MethodPay.observation2 = cmbreason.Text;
						win_MethodPay.note2 = txtObservation.Text;
						win_MethodPay.e_Cancellation = e_Cancellation;
						iproducts.call4();
						printfolio();

						//ROV 16/10/18 Se cierra 
						this.Hide();
						this.Close();
						
						
						//ROV 16/10/18 Se manda a llamar la ventana
						win_MethodPay.ShowDialog();
						win_MethodPay.l_MPTickets = l_MPTickets;
						win_MethodPay.Focus();
						
					}
				}
				else
				{
					//ROV 16/10/18 Se almacena los datos en su entidad
					e_cancellation.idTicket = e_Cancellation.idTicket;
					e_cancellation.idUser = e_Cancellation.idUser;
					e_cancellation.sObservation = cmbreason.SelectedItem.ToString();
					e_cancellation.Comentary = txtObservation.Text;
					e_cancellation.dDatetime = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
					
					//ROV 16/10/18 Se mandan datos  a la ventana de formas de pago 
					win_MethodPay.Store = e_Cancellation.store;
					win_MethodPay.Totality = e_Cancellation.total;
					win_MethodPay.Tick = ticket;
					win_MethodPay.lineaProducts = lineaProducts;
					win_MethodPay.iproducts = this;
					win_MethodPay.Autorization = user;
					win_MethodPay.l_MPTickets = l_MPTickets;
					win_MethodPay.observation2 = cmbreason.Text;
					win_MethodPay.note2 = txtObservation.Text;
					win_MethodPay.e_Cancellation = e_Cancellation;

					iproducts.call4();

					printfolio();
					
					//ROV 16/10/18 Se cierra 
					this.Hide();
					this.Close();
					
					//ROV 16/10/18 Se manda a llamar la ventana
					win_MethodPay.ShowDialog();
					iproducts.ConsultMpticket(l_MPTickets);
					iproducts.Change(change,ticket);
					win_MethodPay.Focus();

				}
			}
		}

		//metodos para hacer el llamado de las funciones que realiza la pantalla master
		
		private void cmbreason_KeyPress(object sender, KeyPressEventArgs e)
		{
			e.Handled = true;
		}

		private void txtObservation_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (e.KeyChar == ')')
			{
				e.Handled = false;
			}
			if (e.KeyChar == '(')
			{
				e.Handled = false;
			}
			if (e.KeyChar == ',')
			{
				e.Handled = false;
			}
			if (e.KeyChar == ' ')
			{
				e.Handled = false;
			}
			else if (e.KeyChar == '.')
			{
				e.Handled = false;
			}
			else if (e.KeyChar == '-')
			{
				e.Handled = false;
			}
			else if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)Keys.Back) && (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.')))
			{
				e.Handled = true;
				return;
			}
		}

		//Imprimir la cancelacion de productos 
		public void printfolio()
		{
			B_formats b_Formats = new B_formats();
			b_Formats.printcancelproduct(e_Cancellation, ticket, lineaProducts);
		}
		
		public void ConsultProduct(string product, string upc)
		{

		}
		
		public void Cancelticket()
		{
			iproducts.Cancelticket();
		}

		public void ConsultMpticket(List<E_MPTicket> _MPTicket)
		{
			l_MPTickets = _MPTicket;
			iproducts.ConsultMpticket(_MPTicket);
		}

		public void Change(decimal value, string ticket)
		{
			change = value;
		}

		public void call4()
		{

		}
		
		public void cancelation(int value, int id)
		{

		}

		public void validation(string value)
		{

		}

	}
}
