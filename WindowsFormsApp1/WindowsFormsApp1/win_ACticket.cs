﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Inets.Bussines.Estandard;
using Inets.Models.Entity_Entities;
using inets.UI.Logging;
using ExternalClass;

namespace inets.UI.Logging
{
	public partial class win_ACTicket : Form
	{
		public Iproducts iproducts { get; set; }

		//ROV 16/10/18 Se declaran los objetos y variables a trabajar y recibir informacion 
		public int number;
		decimal product = 0;
		
		public List<E_MPTicket> e_MPTickets = new List<E_MPTicket>();
		public List<LineaProduct> lineaProducts = new List<LineaProduct>();
		public E_Cancellations e_Cancellation = new E_Cancellations();
		public E_Store e_Store = new E_Store();

		B_Store b_Store = new B_Store();
		B_formats b_Formats = new B_formats();
		E_JTicket E_JTicket = new E_JTicket();
		B_MPTicket b_MPTicket = new B_MPTicket();
		E_Cancellations cancellation = new E_Cancellations();
		B_Cancellations b_Cancellations = new B_Cancellations();


		public win_ACTicket()
		{
			InitializeComponent();
		}

		//ROV 16/10/18 Actividades a inicializar la ventana 
		private void win_ACticket_Load(object sender, EventArgs e)
		{
			var item = e_MPTickets;
			

			//ROV 16/10/18 Se visualiza los productos agregados 
			foreach (var lista in lineaProducts)
			{
				product = product + lista.Quantity;
			}
			
			lbnumprod.Text = product.ToString();
			lbtotal.Text = product.ToString();
			lbticket.Text = e_Cancellation.ticket;
			lbtotal.Text = e_Cancellation.total.ToString();

			//ROV 16/10/18 Se visualiza si se genero una forma de pago 
			if (e_MPTickets.Count > 0)
			{
				label3.Visible = true;

				dataGridView1.DataSource = item.Select(x => new
				{
					x.Methodpay,
					x.Amountpay
				}).ToList();

				dataGridView1.Columns["Methodpay"].HeaderText = "Método de Pago";
				dataGridView1.Columns["Amountpay"].HeaderText = "Monto Pagado";
				dataGridView1.ReadOnly = true;

			} else
			{
				label3.Visible = false;
				dataGridView1.Visible = false;
			}

			label10.Visible = false;
			txtobservartion.Visible = false;

			cmbreason.Items.Add("Otro");

			//ROV 16/10/18 Se ajusta el datagrid respecto a las columnas 
			dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

		}

		//ROV 16/10/18 funcion del boton de cancelar Tick 
		private void btnCancelTick_Click(object sender, EventArgs e)
		{
			//ROV 16/10/18 Se valida que seleccione un motivo de cancelacion
			if (cmbreason.SelectedItem == null)
			{
				MessageBox.Show("Seleccionar un motivo de cancelación");
			}
			else
			{
				if (txtobservartion.Visible == true)
				{
					if (txtobservartion.Text == "" || txtobservartion.Text == " ")
					{
						MessageBox.Show("Favor de escribir una observacion de cancelacion");
					}
					else
					{
						//ROV 16/10/18 Se insertan los datos a la entidad de cancelacion
						cancellation.idTicket = e_Cancellation.idTicket;
						cancellation.idUser = e_Cancellation.idUser;
						cancellation.sObservation = cmbreason.SelectedItem.ToString();
						cancellation.Comentary = txtobservartion.Text;
						cancellation.dDatetime = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
						
						iproducts.Cancelticket();
						iproducts.call4();
						iproducts.cancelation(number,e_Cancellation.idUser);
						this.Close();
						b_Formats.printcancelTicket(e_Cancellation, lineaProducts);

					}
				}
				else
				{
					//ROV 16/10/18 Se insertan los datos a la entidad de cancelacion
					cancellation.idTicket = e_Cancellation.idTicket;
					cancellation.idUser = e_Cancellation.idUser;
					cancellation.sObservation = cmbreason.SelectedItem.ToString();
					cancellation.Comentary = txtobservartion.Text;
					cancellation.dDatetime = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
					
					iproducts.Cancelticket();
					b_MPTicket.clear();
					iproducts.call4();
					iproducts.cancelation(number,e_Cancellation.idUser);
					this.Close();
				}
			}
		}

		private void cmbreason_SelectedIndexChanged(object sender, EventArgs e)
		{
			if(cmbreason.SelectedItem.ToString() == "Otro")
			{
				label10.Visible = true;
				txtobservartion.Visible = true;
			}
		}

		private void cmbreason_KeyPress(object sender, KeyPressEventArgs e)
		{
			e.Handled = true;
		}

		private void txtobservartion_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (e.KeyChar == ')')
			{
				e.Handled = false;
			}
			if (e.KeyChar == '(')
			{
				e.Handled = false;
			}
			if (e.KeyChar == ',')
			{
				e.Handled = false;
			}
			if (e.KeyChar == ' ')
			{
				e.Handled = false;
			}
			else if (e.KeyChar == '.')
			{
				e.Handled = false;
			}
			else if (e.KeyChar == '-')
			{
				e.Handled = false;
			}
			else if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)Keys.Back) && (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.')))
			{
				e.Handled = true;
				return;
			}
		}
		
	}
}
