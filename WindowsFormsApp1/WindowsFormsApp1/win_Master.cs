﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;


using Inets.Bussines.Estandard;
using Inets.Models.Entity_Entities;
using inets.UI.Logging;
using ExternalClass;


namespace inets.UI.Logging
{
	public partial class win_Master : Form, Iproducts
	{

		//************************************************ VARIABLES A TRABAJAR Y OBJETOS A TRABAJAR
		int index = -1;
		int number;
		int cantidad;
		decimal total;
		public bool bandera = false;

		//Constructores
		SalesTicket salesTicket = new SalesTicket();
		LineaProduct lineaProduct = new LineaProduct();
		
		public List<E_MPTicket> l_MPTickets = new List<E_MPTicket>();

		E_Cancellations e_cancellations = new E_Cancellations();
		B_Cancellations b_Cancellations = new B_Cancellations();
		E_DetailTicket e_DetailTicket = new E_DetailTicket();
		B_DetailTicket b_DetailTicket = new B_DetailTicket();
		E_CashSales e_CashSales = new E_CashSales();
		B_CashSales b_CashSales = new B_CashSales();
		E_Employee e_Employee = new E_Employee();
		B_Employee b_Employee = new B_Employee();
		E_Product e_Product = new E_Product();
		B_Product b_Product = new B_Product();
		E_Ticket e_Ticket = new E_Ticket();
		B_Ticket b_Ticket = new B_Ticket();
		E_Store e_Store = new E_Store();
		B_Store b_Store = new B_Store();
		E_Users e_Users = new E_Users();
		B_Users b_Users = new B_Users();
		E_Cash e_Cash = new E_Cash();
		B_Cash b_Cash = new B_Cash();

		B_JStartofcash b_JStartofcash = new B_JStartofcash();
		E_JStarofcash e_JStarofcash = new E_JStarofcash();


		win_ProductConsult win_ProductConsult = new win_ProductConsult();
		win_MethodPay win_MethodPay = new win_MethodPay();
		E_MPTicket e_MPTicket = new E_MPTicket();
		//************************************************ VARIABLES 

		public win_Master()
		{
			InitializeComponent();
		}

		private void Master_Load(object sender, EventArgs e)
		{
			panel4.Visible = false;

			btnbloquear.Visible = false;

			panelLogin.Visible = true;
			Panelforms.Visible = false;
			panelcash.Visible = false;

			lbTicket.Text = "0";
			timer1.Enabled = true;

			TxtPassword.PasswordChar = '°';

			btninsertcash.Visible = false;
			txttotal.Enabled = false;

			e_Employee.iUsers = b_Users.Validation();
			e_CashSales.idUser = b_Users.Validation();

			txt500.Text = "0";
			txt200.Text = "0";
			txt100.Text = "0";
			txt50.Text = "0";
			txt20.Text = "0";
			txtmoney.Text = "0";

			txtUpc.MaxLength = 13;
			txtCalculate.MaxLength = 3;

			txt500.MaxLength = 2;
			txt200.MaxLength = 2;
			txt100.MaxLength = 2;
			txt50.MaxLength = 2;
			txt20.MaxLength = 2;
			txtmoney.MaxLength = 11;

		}

		private void timer1_Tick(object sender, EventArgs e)
		{
			lbfecha.Text = DateTime.Now.ToShortDateString();
			lbhora.Text = DateTime.Now.ToShortTimeString();

			lbtimer.Text = DateTime.Now.ToShortDateString();
			lbHra.Text = DateTime.Now.ToShortTimeString();

			lbdate.Text = DateTime.Now.ToShortDateString();
		}

		//************************************************ROV 17/10/18 TPV

		//Funcion para buscar producto al dar Enter
		private void txtUpc_KeyPress_1(object sender, KeyPressEventArgs e)
		{
			if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
			{
				//24/sept/18 ROV funcion para indicar que accion realizar al hacer enter 
				if ((int)e.KeyChar == (int)Keys.Enter)
				{
					if (txtUpc.Text == "")
					{
						MessageBox.Show("Escriba el codigo de barras");
					}
					else
					{
						e_Product = new E_Product();
						e_Product.sUPC = txtUpc.Text;
						e_Product.sUPCStore = "";
						e_Product = b_Product.SearchProduct(e_Product);

						//ROV 06/09/18 se valida que se encuentre el producto o marcar un error 
						if (e_Product == null)
						{
							MessageBox.Show("Producto no encontrado", "Mensajes de error ");
						}
						else
						{
							//ROV 06/09/18 se Llena los txtbx para funcionamiento
							txtDescription.Text = "Descripción " + e_Product.Description + "\r\nPrecio $" + e_Product.dSalesPrice;

							if (e_Product.imageproduct != null)
							{
								MemoryStream memoryStream = new MemoryStream(e_Product.imageproduct);
								pictuproduc.Image = System.Drawing.Bitmap.FromStream(memoryStream);
								pictuproduc.SizeMode = PictureBoxSizeMode.CenterImage;
								pictuproduc.SizeMode = PictureBoxSizeMode.StretchImage;
							}
							else
								pictuproduc.Image = null;

							if (txtCalculate.Text == "")
							{
								txtCalculate.Text = "1";
								txtCalculate.Focus();
							}
							else
								txtCalculate.Focus();
						}
					}

				}
				e.Handled = true;
				return;
			}
		}

		//Funcion para calcular la cantidad e ingresarlos en linea 
		private void txtCalculate_KeyPress_1(object sender, KeyPressEventArgs e)
		{
			e_Product = new E_Product();
			if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
			{
				if ((int)e.KeyChar == (int)Keys.Enter)
				{

					if (txtDescription.Text == "")
					{
						MessageBox.Show("Inserte codigo de barras a buscar");
					}
					else
					{
						if (txtUpc.Text == "")
						{
							MessageBox.Show("Inserte codigo de barras a buscar");
						}
						else
						{
							if (txtCalculate.Text == "")
							{
								txtCalculate.Text = "1";
							}
							else
							{
								if (txtCalculate.Text == "0")
								{
									MessageBox.Show("Insertar un valor mayor a 0");
								}
								else
								{
									int valor = Convert.ToInt16(txtCalculate.Text);
									e_Product.sUPC = txtUpc.Text;
									e_Product.sUPCStore = "";
									e_Product = b_Product.SearchProduct(e_Product);

									if (e_Product == null)
									{
										MessageBox.Show("Validar Codigo de Barras");
									}
									else
									{
										lineaProduct.Deletes = 0;

										lineaProduct.Numberticket = lbTicket.Text;
										lineaProduct.Quantity = Convert.ToDecimal(txtCalculate.Text);
										salesTicket.Quantity = 1;
										lineaProduct.IdProduct = e_Product.iIdProduct;
										lineaProduct.Product = e_Product.Description;
										salesTicket.Product = e_Product.Description;
										lineaProduct.Price = e_Product.dSalesPrice;
										salesTicket.Price = e_Product.dSalesPrice;

										lineaProduct.TotalAmount = lineaProduct.preciototal(e_Product.dSalesPrice, Convert.ToDecimal(txtCalculate.Text));
										salesTicket.TotalAmount = e_Product.dSalesPrice;
										lineaProduct.idPromotion = 0;
										lineaProduct.UPC = txtUpc.Text;

										salesTicket.UPC = txtUpc.Text;

										if (b_DetailTicket.validate(e_Product.Description) == true)
										{
											b_DetailTicket.Increment(e_Product.Description, Convert.ToDecimal(txtCalculate.Text), lineaProduct.preciototal(e_Product.dSalesPrice, Convert.ToDecimal(txtCalculate.Text)));
											for (int x = 0; x < valor; x++)
											{
												b_DetailTicket.insertlistdetail(salesTicket);
											}
										}
										else
										{
											b_DetailTicket.InsertlistTicket(lineaProduct);
											for (int x = 0; x < valor; x++)
											{
												b_DetailTicket.insertlistdetail(salesTicket);
											}
										}

										lbTotal.Text = b_DetailTicket.calcular().ToString();

										datastyle();

										txtCalculate.Text = txtDescription.Text = txtUpc.Text = "";
										lbCantidad.Text = b_DetailTicket.Quantity().ToString();

										pictuproduc.Image = null;

									}
								}
							}
						}
					}
				}
				e.Handled = true;
				index = -1;
				return;
			}
			index = -1;
			//24/sept/18 ROV funcion para indicar que accion realizar al hacer enter 
		}

		//Funcion de boton para eliminar 
		private void btnBaja_Click(object sender, EventArgs e)
		{
			if (gridTicket.DataSource == null)
			{
				MessageBox.Show("Ingresar un producto");
			}
			else
			{
				if (index == gridTicket.CurrentRow.Index)
				{
					DialogResult result = MessageBox.Show("¿Quiere quitar este producto? ", "", MessageBoxButtons.OKCancel);

					if (result == DialogResult.OK)
					{
						b_DetailTicket.DeletePosicion(index);
						b_DetailTicket.Quit(lineaProduct.Product, lineaProduct.Quantity, lineaProduct.Price);

						lbTotal.Text = b_DetailTicket.calcular().ToString();
						lbCantidad.Text = b_DetailTicket.Quantity().ToString();
						gridTicket.DataSource = null;
						bandera = true;
						datastyle();
					}
					else
					{

					}
				}
				else
				{
					MessageBox.Show("Seleccione producto a quitar");
				}

				index = -1;
			}
		}

		private void gridTicket_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
		{
			index = gridTicket.CurrentRow.Index;

			lineaProduct.Quantity = Convert.ToDecimal(gridTicket.CurrentRow.Cells[0].Value.ToString());
			lineaProduct.Product = gridTicket.CurrentRow.Cells[1].Value.ToString();
			lineaProduct.Price = Convert.ToDecimal(gridTicket.CurrentRow.Cells[2].Value.ToString());

			gridTicket.Rows[index].Selected = true;
		}

		//************************************************ 24/sept/18 ROV Botones de calculadora 
		private void btn9_Click_1(object sender, EventArgs e)
		{
			number = 9;
			txtCalculate.Text = txtCalculate.Text + number;
			txtCalculate.Focus();
		}

		private void btn8_Click_1(object sender, EventArgs e)
		{
			number = 8;
			txtCalculate.Text = txtCalculate.Text + number;
			txtCalculate.Focus();
		}

		private void btn7_Click_1(object sender, EventArgs e)
		{
			number = 7;
			txtCalculate.Text = txtCalculate.Text + number;
			txtCalculate.Focus();
		}

		private void btn6_Click_1(object sender, EventArgs e)
		{
			number = 6;
			txtCalculate.Text = txtCalculate.Text + number;
			txtCalculate.Focus();
		}

		private void btn5_Click_1(object sender, EventArgs e)
		{
			number = 5;
			txtCalculate.Text = txtCalculate.Text + number;
			txtCalculate.Focus();
		}

		private void btn4_Click_1(object sender, EventArgs e)
		{
			number = 4;
			txtCalculate.Text = txtCalculate.Text + number;
			txtCalculate.Focus();
		}

		private void btn3_Click_1(object sender, EventArgs e)
		{
			number = 3;
			txtCalculate.Text = txtCalculate.Text + number;
			txtCalculate.Focus();
		}

		private void btn2_Click_1(object sender, EventArgs e)
		{
			number = 2;
			txtCalculate.Text = txtCalculate.Text + number;
			txtCalculate.Focus();
		}

		private void btn1_Click_1(object sender, EventArgs e)
		{
			number = 1;
			txtCalculate.Text = txtCalculate.Text + number;
			txtCalculate.Focus();
		}

		private void btn0_Click(object sender, EventArgs e)
		{
			if (txtCalculate.Text == "" || txtCalculate.Text == "0")
			{
				MessageBox.Show("Favor de poner un numero mayor");
			}
			else
			{
				number = 0;
				txtCalculate.Text = txtCalculate.Text + number;
				txtCalculate.Focus();
			}
		}

		private void btnEnter_Click(object sender, EventArgs e)
		{
			e_Product = new E_Product();
			if (txtCalculate.Text == "0")
			{
				MessageBox.Show("Favor de insertar un valor mayor a 0");
			}
			else
			{
				if (txtUpc.Text == "")
				{
					MessageBox.Show("Favor de buscar un producto");
				}
				else
				{
					if (txtDescription.Text == "")
					{
						MessageBox.Show("Favor de  realizar la busqueda");
					}
					else
					{

						if (txtCalculate.Text == "")
						{
							txtCalculate.Text = "1";
						}
						else
						{
							int valor = Convert.ToInt16(txtCalculate.Text);
							e_Product.sUPC = txtUpc.Text;
							e_Product.sUPCStore = "";
							e_Product = b_Product.SearchProduct(e_Product);

							if (e_Product == null)
							{
								MessageBox.Show("Validar codigo de barras");
							}
							else
							{
								lineaProduct.Deletes = 0;

								lineaProduct.Numberticket = lbTicket.Text;
								lineaProduct.Quantity = Convert.ToDecimal(txtCalculate.Text);
								salesTicket.Quantity = 1;
								lineaProduct.IdProduct = e_Product.iIdProduct;
								lineaProduct.Product = e_Product.Description;
								salesTicket.Product = e_Product.Description;
								lineaProduct.Price = e_Product.dSalesPrice;
								salesTicket.Price = e_Product.dSalesPrice;

								lineaProduct.TotalAmount = lineaProduct.preciototal(e_Product.dSalesPrice, Convert.ToDecimal(txtCalculate.Text));
								salesTicket.TotalAmount = e_Product.dSalesPrice;
								lineaProduct.idPromotion = 0;
								lineaProduct.UPC = txtUpc.Text;

								salesTicket.UPC = txtUpc.Text;

								if (b_DetailTicket.validate(e_Product.Description) == true)
								{
									b_DetailTicket.Increment(e_Product.Description, Convert.ToDecimal(txtCalculate.Text), lineaProduct.preciototal(e_Product.dSalesPrice, Convert.ToDecimal(txtCalculate.Text)));
									for (int x = 0; x < valor; x++)
									{
										b_DetailTicket.insertlistdetail(salesTicket);
									}
								}
								else
								{
									b_DetailTicket.InsertlistTicket(lineaProduct);
									for (int x = 0; x < valor; x++)
									{
										b_DetailTicket.insertlistdetail(salesTicket);
									}
								}

								lbTotal.Text = b_DetailTicket.calcular().ToString();

								datastyle();

								txtCalculate.Text = txtDescription.Text = txtUpc.Text = "";
								lbCantidad.Text = b_DetailTicket.Quantity().ToString();
								pictuproduc.Image = null;
							}

						}
					}
				}
			}
		}
		
		//************************************************

		//Visualizacion del datagrid
		public void datastyle()
		{
			var itemm = b_DetailTicket.visualizartick();

			gridTicket.DataSource = itemm.Select(x => new
			{
				x.Quantity,
				x.Product,
				x.Price,
				x.TotalAmount
			}).ToList();

			gridTicket.Columns["TotalAmount"].HeaderText = "Monto Total";

			//gridTicket.DataSource = b_DetailTicket.visualizalist();
			gridTicket.CellBorderStyle = DataGridViewCellBorderStyle.None;
			gridTicket.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
			gridTicket.ReadOnly = true;

			gridTicket.Columns["Quantity"].HeaderText = "Cantidad";
			gridTicket.Columns["Product"].HeaderText = "Producto";
			gridTicket.Columns["Price"].HeaderText = "Precio";
			gridTicket.Columns["TotalAmount"].HeaderText = "Monto total";
		}

		//Funcion de caja e crear numero de Tick
		public void iniciocaja()
		{
			E_Users e_Users = new E_Users();
			B_Users b_Users = new B_Users();
			E_Cash e_Cash = new E_Cash();
			B_Cash b_Cash = new B_Cash();
			E_Ticket e_Ticket = new E_Ticket();
			B_Ticket b_Ticket = new B_Ticket();

			e_Cash.sNameCash = lbcaja.Text;
			e_Cash = b_Cash.SearchIdCash(e_Cash);
			e_CashSales.idUser = b_Users.Validation();
			e_CashSales = b_CashSales.SearchCashSales(e_CashSales);

			lbTicket.Text = b_Ticket.NewTicket(Convert.ToInt64(lbTicket.Text)).ToString();

			e_Ticket.NumTicket = lbTicket.Text;
			e_Ticket.iIdCashSales = e_CashSales.iIdCashSales;
			
			e_Ticket = b_Ticket.SearchNT(e_Ticket);

			gridTicket.DataSource = null;
			bandera = false;

			lbTotal.Text = "0";
			lbCantidad.Text = "0";
			b_DetailTicket.ClearList();
			b_DetailTicket.clearlist();
			l_MPTickets.Clear();

			panel4.Visible = false;
		}

		//************************************************ ROV 17/10/18 Panle LOGIN 

		private void btnIngres_Click(object sender, EventArgs e)
		{
			E_Employee e_Employee = new E_Employee();
			B_Employee b_Employee = new B_Employee();
			E_Users e_Users = new E_Users();
			B_Users b_Users = new B_Users();
			B_Login b_Login = new B_Login();
			e_CashSales = new E_CashSales();
			

			e_Users.sNickname = TxtUser.Text;
			e_Users.sPassword = TxtPassword.Text;

			if (TxtUser.Text == "" || TxtUser.Text == " ")
			{
				MessageBox.Show("Favor de escribir el usuario");
			}
			else
			{
				if (TxtPassword.Text == "" || TxtPassword.Text == " ")
				{
					MessageBox.Show("Favor de  escribir una contraseña");
				}
				else
				{
					if (b_Login.StarSesion(e_Users) == true)
					{
						e_Users = b_Login.SearshUser(e_Users);

						b_Users.getuser(e_Users.iIdUsers);

						//LLenar los campos de arriba del Tick 
						e_Employee.iUsers = b_Users.Validation();
						e_CashSales.idUser = b_Users.Validation();

						e_Employee = b_Employee.SearshEmployee(e_Employee);
						e_CashSales = b_CashSales.SearchCashSales(e_CashSales);

						if (e_Employee == null || e_CashSales == null)
						{
							MessageBox.Show("Usuario no asignado a la caja");
						}
						else
						{
							e_Cash.iIdCash = e_CashSales.idCash;
							e_Cash = b_Cash.Search(e_Cash);

							e_CashSales.iIdCashSales = e_CashSales.iIdCashSales;
							e_CashSales.idUser = e_Employee.iUsers;
							e_CashSales.idCash = e_Cash.iIdCash;
							e_CashSales.dLogin = Convert.ToDateTime(lbtimer.Text + " " + lbHra.Text);
							e_CashSales.sStatus = "Activo";
							

							lbcashusuario.Text = e_Employee.sNameEmployee + " " + e_Employee.sLastName;
							lbcashcaja.Text = e_Cash.sNameCash;

							e_Store.iIdStore = e_Cash.iIdStore;
							e_Store = b_Store.SearchStore(e_Store);
							lbtienda.Text = e_Store.sNameStore;

							panelLogin.Visible = false;
							Panelforms.Visible = false;
							panelcash.Visible = true;


							TxtUser.Text = TxtPassword.Text = "";
							btnbloquear.Visible = true;
							
							//Inicio de Tick 
							iniciocaja();
							lbcashusuario.Text = e_Employee.sNameEmployee + " " + e_Employee.sLastName;

							e_JStarofcash.juser = new User()
							{
								id_user = e_Employee.iUsers,
								nameemploye = e_Employee.sNameEmployee
							};
							e_JStarofcash.jstore = new Store()
							{
								idstore = e_Store.iIdStore,
								nameStore = e_Store.sNameStore,
								address = e_Store.sAddress,
								phone = e_Store.sTelephon,
								RFC = e_Store.sRFC,
								email = e_Store.sEmail
							};
							e_JStarofcash.jchas = new Cash()
							{
								idcash = e_Cash.iIdCash
							};

						}
					}
					else
					{
						MessageBox.Show("El usuario o contraseña son incorrectos");
					}
				}
			}
		}


		//************************************************ ROV 17/10/18  Panel de inicio de caja

		private void btncalculate_Click(object sender, EventArgs e)
		{
			double sum = 0;

			sum = (txt500.Text == "" ? 0d : (Convert.ToDouble(txt500.Text) * 500)) +
				(txt200.Text == "" ? 0d : (Convert.ToDouble(txt200.Text) * 200)) +
				(txt100.Text == "" ? 0d : (Convert.ToDouble(txt100.Text) * 100)) +
				(txt50.Text == "" ? 0d : (Convert.ToDouble(txt50.Text) * 50)) +
				(txt20.Text == "" ? 0d : (Convert.ToDouble(txt20.Text) * 20)) +
				(txtmoney.Text == "" ? 0d : Convert.ToDouble(txtmoney.Text));

			txttotal.Text = sum.ToString();

			btninsertcash.Visible = true;
			btninsertcash.Focus();

		}

		private void btninsertcash_Click(object sender, EventArgs e)
		{
			E_Cash e_Cash = new E_Cash();
			B_Cash b_Cash = new B_Cash();
			E_Users e_Users = new E_Users();
			B_Users b_Users = new B_Users();
			E_CashCut e_CashCut = new E_CashCut();
			B_CashCut b_CashCut = new B_CashCut();
			E_CashSales e_CashSales = new E_CashSales();
			B_CashSales b_CashSales = new B_CashSales();

			e_Employee.iUsers = b_Users.Validation();
			e_CashSales.idUser = b_Users.Validation();

			e_Users = b_Users.SearchUser(e_Users);
			e_CashSales = b_CashSales.SearchCashSales(e_CashSales);

			e_CashCut.iIdCashSales = e_CashSales.iIdCashSales;
			e_CashCut.dBalanceInicial = Convert.ToDecimal(txttotal.Text);
			e_CashCut.dBalanceFinal = 0;
			e_CashCut.sDatetime = lbdate.Text;
			

			lbcaja.Text = lbcashcaja.Text;
			lbUsuario.Text = lbcashusuario.Text;

			txtmoney.Text = txt500.Text = txt200.Text = txt100.Text = txt50.Text = txt20.Text = txttotal.Text = "0";
			btninsertcash.Visible = false;

			panelcash.Visible = false;
			Panelforms.Visible = true;

			e_JStarofcash.date = DateTime.Now.Date;
			e_JStarofcash.hour = DateTime.Now.TimeOfDay.ToString();
			e_JStarofcash.Inicitalamount =Convert.ToDecimal(txttotal.Text);

			b_JStartofcash.insert(e_JStarofcash);
		}

		private void txt500_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
			{
				if ((int)e.KeyChar == (int)Keys.Enter)
				{
					if (txt500.Text == "")
					{
						txt500.Text = "0";
					}
					else
					{
						B_CashCut b_CashCut = new B_CashCut();
						double valor = 500 * Convert.ToDouble(txt500.Text);
						txt200.Focus();
						txttotal.Text = b_CashCut.CalculateAmount(valor, Convert.ToDouble(txttotal.Text)).ToString();
					}

				}
				e.Handled = true;
				return;
			}
		}

		private void txt200_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
			{
				if ((int)e.KeyChar == (int)Keys.Enter)
				{
					if (txt200.Text == "")
					{
						txt200.Text = "0";
					}
					else
					{
						B_CashCut b_CashCut = new B_CashCut();
						double valor = 200 * Convert.ToDouble(txt200.Text);

						txt100.Focus();

						txttotal.Text = b_CashCut.CalculateAmount(valor, Convert.ToDouble(txttotal.Text)).ToString();
					}
				}
				e.Handled = true;
				return;
			}
		}

		private void txt100_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
			{
				if ((int)e.KeyChar == (int)Keys.Enter)
				{
					if (txt100.Text == "")
					{
						txt100.Text = "0";
					}
					else
					{
						B_CashCut b_CashCut = new B_CashCut();
						double valor = 100 * Convert.ToDouble(txt100.Text);
						txt50.Focus();
						txttotal.Text = b_CashCut.CalculateAmount(valor, Convert.ToDouble(txttotal.Text)).ToString();
					}
				}
				e.Handled = true;
				return;
			}
		}

		private void txt50_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
			{
				if ((int)e.KeyChar == (int)Keys.Enter)
				{
					if (txt50.Text == "")
					{
						txt50.Text = "0";
					}
					else
					{
						B_CashCut b_CashCut = new B_CashCut();
						double valor = 50 * Convert.ToDouble(txt50.Text);
						txt20.Focus();
						txttotal.Text = b_CashCut.CalculateAmount(valor, Convert.ToDouble(txttotal.Text)).ToString();
					}
				}
				e.Handled = true;
				return;
			}
		}

		private void txt20_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
			{
				if ((int)e.KeyChar == (int)Keys.Enter)
				{
					if (txt20.Text == "")
					{
						txt20.Text = "0";
					}
					else
					{
						B_CashCut b_CashCut = new B_CashCut();
						double valor = 20 * Convert.ToDouble(txt20.Text);
						txtmoney.Focus();
						txttotal.Text = b_CashCut.CalculateAmount(valor, Convert.ToDouble(txttotal.Text)).ToString();
					}
				}
				e.Handled = true;
				return;
			}
		}

		private void txtmorralla_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
			{
				e.Handled = true;
			}
			if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
			{
				e.Handled = true;
			}
			if ((int)e.KeyChar == (int)Keys.Enter)
			{
				if (txtmoney.Text == "")
				{
					txtmoney.Text = "0";
				}
				else
				{
					B_CashCut b_CashCut = new B_CashCut();
					double valor = Convert.ToDouble(txtmoney.Text);
					btncalculate.Focus();
					txttotal.Text = b_CashCut.CalculateAmount(valor, Convert.ToDouble(txttotal.Text)).ToString();
				}
			}
		}

		//************************************************ 8/11/18 ROV BOTONERA

		//ROV 17/10/18 btn de consulta de productos 

		private void btnConsultProduct_Click(object sender, EventArgs e)
		{
			if (panelcash.Visible == true)
			{
				MessageBox.Show("Insertar valores a caja");
			}
			else
			{
				if (panelLogin.Visible == true)
				{
					MessageBox.Show("Favor de iniciar sesion");
				}
				else
				{
					if (win_ProductConsult.IsDisposed)
					{
						win_ProductConsult = new win_ProductConsult();
						win_ProductConsult.Show();
						win_ProductConsult.iproducts = this;
					}
					else
					{
						win_ProductConsult.Show();
						win_ProductConsult.iproducts = this;
						win_ProductConsult.Focus();
					}
				}
			}
		}

		//ROV 17/10/18 btn de bloqueo de sesion
		private void btnbloquear_Click(object sender, EventArgs e)
		{
			panelLogin.Visible = true;
			panelLogin.BringToFront();
			Panelforms.Visible = true;
			panelcash.Visible = false;
			btnbloquear.Visible = false;
		}

		//ROV 17/10/18 btn de caja inicial
		private void btnCaja_Click(object sender, EventArgs e)
		{
			if (panelLogin.Visible == true)
			{
				MessageBox.Show("Favor de iniciar sesion");
			}
			else
			{
				panelcash.Visible = true;
				Panelforms.Visible = false;
			}
		}

		//ROV 17/10/18 inicio de forma de pagos
		private void btnPago_Click(object sender, EventArgs e)
		{
			win_Validation win_Validation = new win_Validation();
			win_MethodPay win_Pago = new win_MethodPay();

			E_Employee _Employee = new E_Employee();

			total = Convert.ToDecimal(lbTotal.Text);
			cantidad = Convert.ToInt32(lbCantidad.Text);

			_Employee.sNameEmployee = lbUsuario.Text;
			_Employee = b_Employee.Searsh2(e_Employee);

			e_cancellations.idUser = e_Employee.iUsers;
			e_cancellations.ticket = lbTicket.Text;
			e_cancellations.total = Convert.ToDecimal(lbTotal.Text);
			e_cancellations.store = lbtienda.Text;

			
			if (gridTicket.Rows.Count == 0)
			{
				MessageBox.Show("No hay productos en linea para realizar una forma de pago");
			}
			else
			{
				if (bandera == true)
				{
					if (l_MPTickets.Count == 0)
					{

					}
					else
					{
						win_Validation.l_MPTickets = l_MPTickets;
					}
					
					win_Validation.lineaProducts = b_DetailTicket.visualizalist();
					win_Validation.e_Cancellation = e_cancellations;

					win_Validation.ticket = lbTicket.Text;
					win_Validation.number = 1;
					win_Validation.iproducts = this;
					win_Validation.ShowDialog();
				}
				else
				{
					if (lbTotal.Text == "0")
					{
						MessageBox.Show("Genera una compra");
					}
					else
					{
						if (l_MPTickets.Count == 0)
						{

						}
						else
						{
							win_Pago.l_MPTickets = l_MPTickets;
						}

						win_Pago.lineaProducts = b_DetailTicket.visualizalist();
						win_Pago.Tick = lbTicket.Text;
						win_Pago.Totality = Convert.ToDecimal(lbTotal.Text);
						win_Pago.employee = lbUsuario.Text;
						win_Pago.Store = lbtienda.Text;
						win_Pago.cash = lbcaja.Text;
						win_Pago.e_Cancellation = e_cancellations;

						win_Pago.iproducts = this;
						win_Pago.ShowDialog();
					}
				}
			}
		}

		//ROV 17/10/18 btn para cancelacion de Tick
		private void btnCancelTick_Click(object sender, EventArgs e)
		{
			win_MethodPay win_Pago = new win_MethodPay();
			win_Validation win_Validation = new win_Validation();

			var item = b_DetailTicket.visualizalist();

			if (item.Count == 0)
			{
				MessageBox.Show("No se ha realizado venta");
			}
			else
			{
				if (l_MPTickets.Count == 0)
				{
					e_Ticket.NumTicket = lbTicket.Text;
					e_Ticket = b_Ticket.SearchNT(e_Ticket);

					e_cancellations.idTicket = e_Ticket.iIdTicket;
					e_cancellations.ticket = lbTicket.Text;
					e_cancellations.total = Convert.ToDecimal(lbTotal.Text);
					e_cancellations.store = lbtienda.Text;

					win_Validation.iproducts = this;
					win_Validation.l_MPTickets = l_MPTickets;
					win_Validation.lineaProducts = b_DetailTicket.visualizalist();

					win_Validation.number = 3;
					win_Validation.ShowDialog();

				}
				else
				{
					MessageBox.Show("Usted ya eligio metodos de pago debe cancelar los metodos");

					win_Pago.lineaProducts = b_DetailTicket.visualizalist();
					win_Pago.Tick = lbTicket.Text;
					win_Pago.Totality = Convert.ToDecimal(lbTotal.Text);
					win_Pago.employee = lbUsuario.Text;
					win_Pago.Store = lbtienda.Text;
					win_Pago.cash = lbcaja.Text;
					win_Pago.l_MPTickets = l_MPTickets;

					win_Pago.iproducts = this;
					win_Pago.ShowDialog();
				}
			}
		}


		//************************************************ 8/11/18 ROV Funciones interfaz

		public void ConsultProduct(string Date1, string Date2)
		{
			txtDescription.Text = Date1;
			txtUpc.Text = Date2;

			e_Product = new E_Product();
			e_Product.sUPC = txtUpc.Text;
			e_Product.sUPCStore = "";
			e_Product = b_Product.SearchProduct(e_Product);

			//ROV 06/09/18 se valida que se encuentre el producto o marcar un error 
			if (e_Product == null)
			{
				MessageBox.Show("Product no encontrado", "Mensajes de error ");
			}
			else
			{
				//ROV 06/09/18 se Llena los txtbx para funcionamiento
				txtDescription.Text = "Descripción " + e_Product.Description + "\r\nPrecio $" + e_Product.dSalesPrice;

				if (e_Product.imageproduct != null)
				{
					MemoryStream memoryStream = new MemoryStream(e_Product.imageproduct);
					pictuproduc.Image = System.Drawing.Bitmap.FromStream(memoryStream);
					pictuproduc.SizeMode = PictureBoxSizeMode.CenterImage;
					pictuproduc.SizeMode = PictureBoxSizeMode.StretchImage;
				}
				else
					pictuproduc.Image = null;

				if (txtCalculate.Text == "")
				{
					txtCalculate.Text = "1";
					txtCalculate.Focus();
				}
				else
					txtCalculate.Focus();
			}
			
		}

		public void Cancelticket()
		{
			iniciocaja();
		}

		public void ConsultMpticket(List<E_MPTicket> _MPTicket)
		{
			decimal Total = 0;
			l_MPTickets = _MPTicket;

			if (l_MPTickets.Count == 0)
			{

			}
			else
			{
				foreach (var list in l_MPTickets)
				{
					Total = Total + list.Amountpay;
				}

				panel4.Visible = true;
				lbefetivo.Text = Total.ToString();
			}
		}

		public void Change(decimal value, string ticket)
		{
			txtDescription.Text = "Ultima Transaccion \r\nTicket " + ticket +
								  " \r\nNo.# " + cantidad.ToString() + 
								  "    Total $ " + total.ToString() + "    Cambio $ " + value.ToString();
			
		}

		public void call4()
		{
			bandera = false;
		}

		public void cancelation(int value, int id)
		{

		}

		public void validation(string value)
		{

		}
		//************************************************ Funciones interfaz
	}
}
