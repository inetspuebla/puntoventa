﻿namespace inets.UI.Logging
{
	partial class win_ACTicket
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(win_ACTicket));
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.lbticket = new System.Windows.Forms.Label();
			this.lbnumprod = new System.Windows.Forms.Label();
			this.lbtotal = new System.Windows.Forms.Label();
			this.cmbreason = new System.Windows.Forms.ComboBox();
			this.btnCancelTick = new System.Windows.Forms.Button();
			this.label10 = new System.Windows.Forms.Label();
			this.txtobservartion = new System.Windows.Forms.TextBox();
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			this.label6 = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(17, 50);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(82, 15);
			this.label1.TabIndex = 0;
			this.label1.Text = "Folio de ticket";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(23, 80);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(76, 15);
			this.label2.TabIndex = 1;
			this.label2.Text = "No. artículos";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(8, 110);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(91, 15);
			this.label3.TabIndex = 2;
			this.label3.Text = "Forma de pago";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(21, 190);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(75, 15);
			this.label4.TabIndex = 3;
			this.label4.Text = "Importe total";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.Location = new System.Drawing.Point(9, 221);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(90, 15);
			this.label5.TabIndex = 4;
			this.label5.Text = "Observaciones";
			// 
			// lbticket
			// 
			this.lbticket.AutoSize = true;
			this.lbticket.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbticket.Location = new System.Drawing.Point(121, 50);
			this.lbticket.Name = "lbticket";
			this.lbticket.Size = new System.Drawing.Size(41, 15);
			this.lbticket.TabIndex = 5;
			this.lbticket.Text = "label6";
			// 
			// lbnumprod
			// 
			this.lbnumprod.AutoSize = true;
			this.lbnumprod.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbnumprod.Location = new System.Drawing.Point(120, 80);
			this.lbnumprod.Name = "lbnumprod";
			this.lbnumprod.Size = new System.Drawing.Size(41, 15);
			this.lbnumprod.TabIndex = 6;
			this.lbnumprod.Text = "label7";
			// 
			// lbtotal
			// 
			this.lbtotal.AutoSize = true;
			this.lbtotal.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbtotal.Location = new System.Drawing.Point(121, 190);
			this.lbtotal.Name = "lbtotal";
			this.lbtotal.Size = new System.Drawing.Size(41, 15);
			this.lbtotal.TabIndex = 8;
			this.lbtotal.Text = "label9";
			// 
			// cmbreason
			// 
			this.cmbreason.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cmbreason.FormattingEnabled = true;
			this.cmbreason.Location = new System.Drawing.Point(124, 213);
			this.cmbreason.Name = "cmbreason";
			this.cmbreason.Size = new System.Drawing.Size(121, 23);
			this.cmbreason.TabIndex = 1;
			this.cmbreason.SelectedIndexChanged += new System.EventHandler(this.cmbreason_SelectedIndexChanged);
			this.cmbreason.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbreason_KeyPress);
			// 
			// btnCancelTick
			// 
			this.btnCancelTick.BackColor = System.Drawing.Color.Transparent;
			this.btnCancelTick.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCancelTick.BackgroundImage")));
			this.btnCancelTick.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.btnCancelTick.FlatAppearance.BorderSize = 0;
			this.btnCancelTick.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnCancelTick.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnCancelTick.Location = new System.Drawing.Point(156, 290);
			this.btnCancelTick.Name = "btnCancelTick";
			this.btnCancelTick.Size = new System.Drawing.Size(89, 31);
			this.btnCancelTick.TabIndex = 3;
			this.btnCancelTick.Text = "Aceptar";
			this.btnCancelTick.UseVisualStyleBackColor = false;
			this.btnCancelTick.Click += new System.EventHandler(this.btnCancelTick_Click);
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label10.Location = new System.Drawing.Point(23, 251);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(76, 15);
			this.label10.TabIndex = 11;
			this.label10.Text = "Observación";
			// 
			// txtobservartion
			// 
			this.txtobservartion.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtobservartion.Location = new System.Drawing.Point(124, 245);
			this.txtobservartion.Name = "txtobservartion";
			this.txtobservartion.Size = new System.Drawing.Size(121, 21);
			this.txtobservartion.TabIndex = 2;
			this.txtobservartion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtobservartion_KeyPress);
			// 
			// dataGridView1
			// 
			this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
			this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView1.Location = new System.Drawing.Point(124, 110);
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.RowHeadersVisible = false;
			this.dataGridView1.Size = new System.Drawing.Size(168, 67);
			this.dataGridView1.TabIndex = 13;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.Location = new System.Drawing.Point(77, 9);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(158, 18);
			this.label6.TabIndex = 17;
			this.label6.Text = "Cancelación de ticket";
			// 
			// win_ACTicket
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(305, 344);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.dataGridView1);
			this.Controls.Add(this.txtobservartion);
			this.Controls.Add(this.label10);
			this.Controls.Add(this.btnCancelTick);
			this.Controls.Add(this.cmbreason);
			this.Controls.Add(this.lbtotal);
			this.Controls.Add(this.lbnumprod);
			this.Controls.Add(this.lbticket);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
			this.Name = "win_ACTicket";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Load += new System.EventHandler(this.win_ACticket_Load);
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label lbticket;
		private System.Windows.Forms.Label lbnumprod;
		private System.Windows.Forms.Label lbtotal;
		private System.Windows.Forms.ComboBox cmbreason;
		private System.Windows.Forms.Button btnCancelTick;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.TextBox txtobservartion;
		private System.Windows.Forms.DataGridView dataGridView1;
		private System.Windows.Forms.Label label6;
	}
}