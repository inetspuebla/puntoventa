﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inets.Models.Entity_Entities
{
    public class E_Departament
    {
        public int iIdDepartment { get; set; }
        public string sNameDepartment { get; set; }
        public int iNumberDepartament { get; set; }
    }
}
