﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inets.Models.Entity_Entities
{
    public class E_Cash
    {
        public int iIdCash { get; set; }
        public int iIdStore { get; set; }
        public String sNameCash { get; set; }
        public bool bStatus { get; set; }
    }
}