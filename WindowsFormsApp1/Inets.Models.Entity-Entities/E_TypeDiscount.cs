﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inets.Models.Entity_Entities
{
    public class E_TypeDiscount
    {
        public int iIdTypeDiscount { get; set; }
        public string sDescription { get; set; }
    }
}
