﻿using System;

namespace ExternalClass
{
	public class SalesTicket
    {
        public string Product { get; set; }
        public Decimal Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal TotalAmount { get; set; }
        public string UPC { get; set; }
		public string TipeSales { get; set; }
	}
}
