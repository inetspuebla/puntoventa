﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inets.Models.Entity_Entities
{
	public class E_Cancellations
	{
		public int idCancellation { get; set; }
		public int idUser { get; set; }
		public int idTicket { get; set; }
		public string sObservation {get; set;}
		public string Comentary { get; set; }
		public string dDatetime { get; set; }
		public decimal total { get; set; }
		public string ticket { get; set; }
		public string store { get; set; }

	}
}
