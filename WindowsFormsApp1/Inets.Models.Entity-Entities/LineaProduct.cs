﻿using System;

namespace ExternalClass
{
	public class LineaProduct
	{
		public string Product { get; set; }
		public Decimal Quantity { get; set; }
		public decimal Price { get; set; }
		public decimal TotalAmount { get; set; }
		public decimal Deletes {get; set;}
		public int IdProduct { get; set; }
		public string UPC { get; set; }
		public string Numberticket { get; set; }
		public int idPromotion { get; set; }

		public decimal preciototal(decimal precio, decimal cantidad)
		{
			decimal total = precio * cantidad;
			return total;
		}
    }
}
