﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inets.Models.Entity_Entities
{
    public class E_UnityMeasure
    {
        public int iIdUnitiMeasure { get; set; }
        public string sDesciption { get; set; }
    }
}
