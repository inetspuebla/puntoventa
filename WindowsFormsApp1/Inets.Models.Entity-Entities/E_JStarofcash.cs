﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inets.Models.Entity_Entities
{
	public class E_JStarofcash
	{
		public E_JStarofcash jStarofcash { get; set; }

		public DateTime date { get; set; }
		public string hour { get; set; }

		public User juser { get; set; }
		public Store jstore { get; set; }
		public Cash jchas { get; set; }
		
		public decimal Inicitalamount { get; set; }
	}
	public class User
	{
		public int id_user { get; set; }
		public string nameemploye { get; set; }
	}
	public class Store
	{
		public int idstore { get; set; }
		public string nameStore { get; set; }
		public string address { get; set; }
		public string phone { get; set; }
		public string RFC { get; set; }
		public string email { get; set; }
	}
	public class Cash
	{
		public int idcash { get; set; }
	}
}
