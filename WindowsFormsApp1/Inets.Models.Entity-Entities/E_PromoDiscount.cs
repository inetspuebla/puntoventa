﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inets.Models.Entity_Entities
{
    public class E_PromoDiscount
    {
        public int iIdPromotion { get; set; }
        public int iIdTypeDiscount { get; set; }
        public decimal dValueofDiscount { get; set; }
        public int iIndexDiscount { get; set; }

    }
}
