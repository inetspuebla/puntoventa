﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inets.Models.Entity_Entities
{
    public class E_Promotion
    {
        public int iIdPromotion { get; set; }
        public int iIdProduct { get; set; }
        public System.DateTime dDateInitial { get; set; }
        public System.DateTime dDateFinal { get; set; }
        public Nullable<decimal> dQuantityProductinSale { get; set; }
    }
}
