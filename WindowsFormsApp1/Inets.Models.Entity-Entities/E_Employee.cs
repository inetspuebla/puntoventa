﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inets.Models.Entity_Entities
{
    public class E_Employee//ROV 9/08/18 Creacion de Entidades para poder revisar datos de forma mas acortada a la BD 
    {
        public int iUsers
        {
            get; set;
        }

        public string sNameEmployee
        {
            get; set;
        }

        public string sLastName
        {
            get; set;
        }

        public string sGener
        {
            get; set;
        }

        public string sBirthday
        {
            get; set;
        }

        public string sAddress
        {
            get; set;
        }
        public int iZipcode
        {
            get; set;
        }

        public string sEmail
        {
            get; set;
        }

        public string Telephone
        {
            get; set;
        }

        public string sRFC
        {
            get; set; 
        }
    }
}
