﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inets.Models.Entity_Entities
{
    public class E_Inventory
    {
        public int iIdInventary { get; set; }
        public Nullable<int> iIdProduct { get; set; }
        public Nullable<decimal> dStock { get; set; }
        public Nullable<int> iIdUnityMeasure { get; set; }
        public Nullable<int> iUnity { get; set; }
    }
}
