﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inets.Models.Entity_Entities
{
	public class E_MPaymentChange
	{
		public int idMPaymentChange { get; set; }
		public int idMPayment { get; set; }
		public int idTypeChange { get; set; }
		public decimal dLimitvalue { get; set; }
		public string nickname { get; set; }
		public string typepay { get; set; }
		public string typechange { get; set; }
		public bool activechange { get; set; }
	}
}
