﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inets.Models.Entity_Entities
{
	public class E_Denomination
	{
		public int idDenomination { get; set; }
		public decimal dValue { get; set; }
		public int idCompany { get; set; }
	}
}
