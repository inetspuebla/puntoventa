﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inets.Models.Entity_Entities
{
	public class E_JTicket
	{
		public string NumberTicket { get; set; }
		public string  Datemonday { get; set; }
		public string hour { get; set; }
		public decimal TotalPay { get; set; }
		public decimal Change { get; set; }
		public jStore jstore { get; set; }
		public jEmployee jemployee { get; set; }
		public List<jProducts> jproduct { get; set; }
		public jAutorizacionproduct jautorizacionp { get; set; }
		public List <jMethodPay> jmethodpay { get; set; }
		public jAutorizationmp jAutorizationmp { get; set; }
		public jCancelticket jcancelticket { get; set; }

	}

	public class jStore
	{
		public int idStore { get; set; }
		public string namestore { get; set; }
		public string address { get; set; }
		public string phone { get; set; }
		public string RFC { get; set; }
		public string email { get; set; }
	}

	public class jEmployee
	{
		public int idemployee { get; set; }
		public string nameemployee { get; set; }
	}

	public class jProducts
	{
		public int idproduct { get; set; }
		public decimal Quantity { get; set; }
		public decimal cancel { get; set; }
		public string Nameproduct { get; set; }
		public decimal PriceUnitary { get; set; }
		public jImpots jImpost { get; set; }

	}

	public class jImpots
	{
		public int idTax { get; set; }
		public string NameTax { get; set; }
		public decimal ValueFactory { get; set; }
	}

	public class jAutorizacionproduct
	{
		public jEmployee jemployee { get; set; }
		public string TypeAutorization { get; set; }
		public string Observation { get; set; }
		public string Note { get; set;  }
		public List<jProductscancel> jproductcancel { get; set; }
	}

	public class jProductscancel
	{
		public int idproduct { get; set; }
		public decimal Quantity { get; set; }
		public string Nameproduct { get; set; }
		public decimal PriceUnitary { get; set; }
	}

	public class jMethodPay
	{
		public int idMethodPay { get; set; }
		public string NameMethodPay { get; set; }
		public decimal AmountPay { get; set; }
	}

	public class jAutorizationmp
	{
		public jEmployee jemployee { get; set; }
		public string TypeAutorization { get; set; }
		public string Observation { get; set; }
		public string Note { get; set; }
		public List<jMethodPaycancel> jMethodPaycancel { get; set; }	
	}

	public class jMethodPaycancel
	{
		public int idMethodPay { get; set; }
		public string NameMethodPay { get; set; }
		public decimal AmountPay { get; set; }
	}
	
	public class jCancelticket
	{
		public jEmployee jemployee { get; set; }
		public string TypeAutorization { get; set; }
		public string Observation { get; set; }
		public string Note { get; set; }
	}

}
