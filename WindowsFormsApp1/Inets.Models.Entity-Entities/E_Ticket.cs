﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inets.Models.Entity_Entities
{
    public class E_Ticket
    {
        public int iIdTicket { get; set; }
        public int iIdCashSales { get; set; }
        public string NumTicket{ get; set; }
        public string sDateTicket { get; set; }
    }
}
