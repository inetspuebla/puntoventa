﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inets.Models.Entity_Entities
{
    public class E_CashSales
    {
        public int iIdCashSales { get; set; }
        public int idCash { get; set; }
        public int idUser { get; set; }
		public DateTime dLogin { get; set; }
		public string sStatus { get; set; }
    }
}
