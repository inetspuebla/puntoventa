﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inets.Models.Entity_Entities
{
	public class E_MPTicket
	{
		public int idMPTicket { get; set; }
		public int idTicket { get; set; }
		public int idMPayment { get; set; }
		public int idDenomination{ get; set; }
		public int idCard { get; set; }
		public int idTypeCard { get; set; }
		public int dQuantity { get; set; }
		public string sBank { get; set; }
		public string sFolio { get; set; }
		public string dDateEmition { get; set; }
		public string iAuthorizacion { get; set; }
		public int iNumCard { get; set; }
		public decimal dAmountPayable { get; set; }
		public bool bValidate { get; set; }
		public decimal Amountpay { get; set; }
		public string Methodpay { get; set; }
		public decimal dCancel { get; set; }

	}
}
