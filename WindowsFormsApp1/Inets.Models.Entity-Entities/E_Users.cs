﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inets.Models.Entity_Entities
{
    public class E_Users
    {
        public int iIdUsers { get; set; }
        public string sNickname { get; set; }
        public string sPassword { get; set; }
        public int iIdProfile { get; set; }
    }
}
