﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inets.Models.Entity_Entities
{
	public class E_TCAplied
	{
		public int idTCApplicable { get; set; }
		public int idMPTicket { get; set; }
		public int idMPaymentChange { get; set; }
		public decimal ValueChange { get; set; }
	}
}
