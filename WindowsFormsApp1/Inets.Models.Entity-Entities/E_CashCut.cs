﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inets.Models.Entity_Entities
{
    public class E_CashCut
    {
        public int iIdCashCut { get; set; }
        public int iIdCashSales { get; set; }
        public string sDatetime { get; set; }
        public decimal dBalanceInicial { get; set; }
        public decimal dBalanceFinal { get; set; }

    }
}
