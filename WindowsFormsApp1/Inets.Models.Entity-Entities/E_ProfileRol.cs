﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inets.Models.Entity_Entities
{
    public class E_ProfileRol
    {
        public int IdProfile { get; set; }
        public int IdRol { get; set; }
    }
}
