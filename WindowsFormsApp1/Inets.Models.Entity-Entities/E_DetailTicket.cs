﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inets.Models.Entity_Entities
{
    public class E_DetailTicket
    {
        public int iIdTicket { get; set; }
        public int iIdProduct { get; set; }
        public Nullable<int> iIdPromotion { get; set; }
        public decimal dUnitValue { get; set; }
        public decimal dQuantityProduct { get; set; }
		public decimal dQuantityDiscount { get; set; }
		
	}
}
