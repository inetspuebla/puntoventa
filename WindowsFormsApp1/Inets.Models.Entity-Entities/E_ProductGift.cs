﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inets.Models.Entity_Entities
{
    public class E_ProductGift
    {
        public int iIdProductGift { get; set; }
        public int iIdPromogift { get; set; }
        public int iIdProduct { get; set; }
    }
}
