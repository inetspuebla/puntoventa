﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inets.Models.Entity_Entities
{
    public class E_Product
    {
		public int iIdProduct;
        public string Description { get; set; }
        public int iIdDepartament { get; set; }
        public string sUPC { get; set; }
        public string sUPCStore { get; set; }
        public decimal dPurchasePrice { get; set; }
        public decimal dSalesPrice { get; set; }
		public decimal dStock;
		public byte[] imageproduct { get; set; }
	}
}
