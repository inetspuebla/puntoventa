﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inets.Models.Entity_Entities
{
    public class E_Store
    {
        public int iIdStore { get; set; }
        public string sNameStore { get; set; }
        public string sRFC { get; set; }
        public string sAddress { get; set;}
        public int iZipCode { get; set; }
        public string sTelephon { get; set; }
        public string sEmail { get; set; }

    }
}
