﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inets.Models.Entity_Entities
{
    public class E_PromoGift
    {
        public int iIdPromogift { get; set; }
        public int iIdPromotion { get; set; }
        public decimal dNumberProductGift { get; set; }
    }
}
