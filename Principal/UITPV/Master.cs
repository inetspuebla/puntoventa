﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UITPV
{
	public partial class Master : Form
	{
		public Master()
		{
			InitializeComponent();
		}

		private void PanelwhithForm(Object formnuevo) //ROV 6/08/18: Abri formulario dentro del Index
		{
			if (this.Panelforms.Controls.Count > 0) //ROV 6/08/18: validar que no hay otros formulario abiertos
			{
				this.Panelforms.Controls.RemoveAt(0); //ROV 6/08/18: Cierra formularios para abrir el nuevo 
			}
			Form fn = formnuevo as Form;
			fn.TopLevel = false;
			this.Panelforms.Controls.Add(fn);
			this.Panelforms.Tag = fn;
			fn.Show();
		}
	}
}
