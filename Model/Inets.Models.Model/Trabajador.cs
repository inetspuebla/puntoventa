﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;

namespace Inets.Models.Model
{
    public class Trabajador
    {
        public int idempleado
        {
            get;
            set;
        }

        public String NombreEmpleado
        {
            get;
            set;
        }

        public String Apellido
        {
            get;
            set;
        }
        public String  Genero
        {
            get;
            set;
        }
        public int Edad
        {
            get;
            set;
        }
        public DateTime FNacimiento
        {
            get;
            set;
        }
        public String Direccion
        {
            get;
            set;
        }
        public int CPostal
        {
            get;
            set;
        }
        public string Email
        {
            get;
            set;
        }
        public int Telefono
        {
            get;
            set;
        }
        public DateTime Fingreso

        {
            get;
            set;
        }
        public String Usuario
        {
            get;
            set;
        }
        public String Puesto
        {
            get;
            set;
        }
        public String Contraseña 
        {
            get;
            set;
        }
    }

}
