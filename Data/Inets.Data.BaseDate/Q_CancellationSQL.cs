﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;
using Inets.Models.Entity_Entities;
namespace Inets.Data.BaseDate
{
	public class Q_CancellationSQL
	{
		public bool Insert(E_Cancellations e_Cancellation)
		{
			Q_Conect q_Conect = new Q_Conect();
			SqlCommand comand;

			if (q_Conect.OpenDB())//28/08/18 se abre la conexion de base de datos 
			{
				comand = q_Conect.Build_Command("Insert Into Cancellations(idUser,idTicket,sObservation,Comentary,dDatetime)" +
					" values (@idUser, @idTicket, @sObservation, @Comentary, @dDatetime)");

				//28/08/18 se parametrisa los datos y valores de de la base de datos
				comand.Parameters.Add(new SqlParameter("@idUser ", SqlDbType.Int));
				comand.Parameters["@idUser "].Value = e_Cancellation.idUser;

				comand.Parameters.Add(new SqlParameter("@idTicket ", SqlDbType.Int));
				comand.Parameters["@idTicket "].Value = e_Cancellation.idTicket;

				comand.Parameters.Add(new SqlParameter("@sObservation ", SqlDbType.VarChar));
				comand.Parameters["@sObservation "].Value = e_Cancellation.sObservation;

				comand.Parameters.Add(new SqlParameter("@Comentary ", SqlDbType.VarChar));
				comand.Parameters["@Comentary "].Value = e_Cancellation.Comentary;

				comand.Parameters.Add(new SqlParameter("@dDatetime ", SqlDbType.DateTime));
				comand.Parameters["@dDatetime "].Value = e_Cancellation.dDatetime;

				int filas = comand.ExecuteNonQuery();//28/08/18 Se cuentan que exista el registro a la base de datos 

				q_Conect.ClosedDB();
				if (filas > 0) //28/08/18 Se valida cambio o ingreso de datos a la base de datos 
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			return false;
		}

		public bool Update(E_Cancellations e_Cancellation)
		{
			Q_Conect q_Conect = new Q_Conect();

			SqlCommand comand;

			if (q_Conect.OpenDB())//28/08/18 Se abre la conexion a la base de datos 
			{
				comand = q_Conect.Build_Command("Update Cancellations where  idUser = @idUser, idTicket=@idTicket, sObservation=@sObservation, Comentary=@Comentary, dDatetime = @dDatetime, " +
					"where idCancellations = @idCancellations; ");

				comand.Parameters.Add(new SqlParameter("@idCancellations ", SqlDbType.Int));
				comand.Parameters["@idCancellations "].Value = e_Cancellation.idCancellation;

				comand.Parameters.Add(new SqlParameter("@idUser ", SqlDbType.Int));
				comand.Parameters["@idUser "].Value = e_Cancellation.idUser;

				comand.Parameters.Add(new SqlParameter("@idTicket ", SqlDbType.Int));
				comand.Parameters["@idTicket "].Value = e_Cancellation.idTicket;

				comand.Parameters.Add(new SqlParameter("@sObservation ", SqlDbType.VarChar));
				comand.Parameters["@sObservation "].Value = e_Cancellation.sObservation;

				comand.Parameters.Add(new SqlParameter("@Comentary ", SqlDbType.VarChar));
				comand.Parameters["@Comentary "].Value = e_Cancellation.sObservation;

				comand.Parameters.Add(new SqlParameter("@dDatetime ", SqlDbType.DateTime));
				comand.Parameters["@dDatetime "].Value = e_Cancellation.dDatetime;
				int filas = comand.ExecuteNonQuery();//28/08/18 Se cuentan que exista el registro a la base de datos 

				q_Conect.ClosedDB();
				if (filas > 0) //28/08/18 Se valida cambio o ingreso de datos a la base de datos 
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			return false;
		}

	}
}
