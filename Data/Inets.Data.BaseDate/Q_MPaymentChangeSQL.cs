﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;
using Inets.Models.Entity_Entities;

namespace Inets.Data.BaseDate
{
	public class Q_MPaymentChangeSQL
	{
		public DataTable Visualize()
		{
			Q_Conect q_Conect = new Q_Conect();
			SqlCommand comand;
			DataTable dt = new DataTable();

			//ROV 29/08/18 Se realiza conexion a la base de datos 
			if (q_Conect.OpenDB())
			{
				//ROV 29/08/18 Se realiza la cnosulta de la base de datos 
				comand = q_Conect.Build_Command("Select * from MPaymentChange");
				SqlDataAdapter da = new SqlDataAdapter(comand);

				//ROV 29/08/18 Se realiza el llenado del datatable con la lista 
				da.Fill(dt);
				return dt;
			}
			q_Conect.ClosedDB();
			return null;
		}

		public DataTable Visualize2()
		{
			Q_Conect q_Conect = new Q_Conect();
			SqlCommand comand;
			DataTable dt = new DataTable();

			//ROV 29/08/18 Se realiza conexion a la base de datos 
			if (q_Conect.OpenDB())
			{
				//ROV 29/08/18 Se realiza la cnosulta de la base de datos 
				comand = q_Conect.Build_Command("Select * from MPaymentChange, PaymentMethod" +
					" where PaymentMethod.idMPayment = MPaymentChange.idMPayment ");
					
				SqlDataAdapter da = new SqlDataAdapter(comand);

				//ROV 29/08/18 Se realiza el llenado del datatable con la lista 
				da.Fill(dt);
				return dt;
			}
			q_Conect.ClosedDB();
			return null;
		}

		public E_MPaymentChange SearchIdMPC(E_MPaymentChange e_MPaymentChange)
		{
			Q_Conect q_Conect = new Q_Conect();
			DataRow fila;
			SqlDataAdapter da = new SqlDataAdapter();

			//ROV 29/08/18 Se relaiza conexion a la base de datos 
			if (q_Conect.OpenDB())
			{
				//ROV 29/08/18 Se realiza la consulta de buscar la forma correcta 
				da = q_Conect.Build_Adapter("Select * from MPaymentChange where idMPaymentChange = @idMPaymentChange ;");

				//ROV 29/08/18 Se realiza la validacion de parametros
				da.SelectCommand.Parameters.Add(new SqlParameter("@idMPaymentChange", SqlDbType.VarChar));
				da.SelectCommand.Parameters["@idMPaymentChange"].Value = e_MPaymentChange.idMPaymentChange;

				fila = WievTable(da);

				//ROV 29/08/18 Se valida que hay datos en la tabla 
				if (fila != null)
				{
					//ROV 29/08/18 Se inserta los datos en cada entidad generada 
					e_MPaymentChange.idMPaymentChange = Convert.ToInt16(fila["idMPaymentChange"]);
					e_MPaymentChange.idMPayment = Convert.ToInt16(fila["idMPayment"]);
					e_MPaymentChange.idTypeChange = Convert.ToInt16(fila["idTypeChange"]);

					q_Conect.ClosedDB();
					return e_MPaymentChange;
				}
				else

					q_Conect.ClosedDB();
				return null;
			}
			return null;
		}

		public E_MPaymentChange SearchNickname(E_MPaymentChange e_MPaymentChange)
		{
			Q_Conect q_Conect = new Q_Conect();
			DataRow fila;
			SqlDataAdapter da = new SqlDataAdapter();
			
			//ROV 29/08/18 Se relaiza conexion a la base de datos 
			if (q_Conect.OpenDB())
			{
				//ROV 29/08/18 Se realiza la consulta de buscar la forma correcta 
				da = q_Conect.Build_Adapter("Select * from MPaymentChange, PaymentMethod "+
					" where PaymentMethod.idMPayment = MPaymentChange.idMPayment "+
					" and Nickname = @Nickname; ");

				//ROV 29/08/18 Se realiza la validacion de parametros
				da.SelectCommand.Parameters.Add(new SqlParameter("@Nickname", SqlDbType.VarChar));
				da.SelectCommand.Parameters["@Nickname"].Value = e_MPaymentChange.nickname;

				fila = WievTable(da);


				//ROV 29/08/18 Se valida que hay datos en la tabla 
				if (fila != null)
				{
					//ROV 29/08/18 Se inserta los datos en cada entidad generada 
					e_MPaymentChange.idMPaymentChange = Convert.ToInt16(fila["idMPaymentChange"]);

					e_MPaymentChange.idMPayment = Convert.ToInt16(fila["idMPayment"]);
					e_MPaymentChange.idTypeChange = Convert.ToInt16(fila["idTypeChange"]);

					e_MPaymentChange.typepay = fila["sNameMPayment"].ToString();
					

					e_MPaymentChange.dLimitvalue = Convert.ToDecimal(fila["dLimitvalue"]);
					e_MPaymentChange.activechange = Convert.ToBoolean(fila["ActiveChange"]);

					
					q_Conect.ClosedDB();
					return e_MPaymentChange;
				}
				else

					q_Conect.ClosedDB();
				return null;
			}
			return null;
		}

		public E_MPaymentChange Search3(E_MPaymentChange e_MPaymentChange)
		{
			Q_Conect q_Conect = new Q_Conect();
			DataRow fila;
			SqlDataAdapter da = new SqlDataAdapter();

			//ROV 29/08/18 Se relaiza conexion a la base de datos 
			if (q_Conect.OpenDB())
			{
				//ROV 29/08/18 Se realiza la consulta de buscar la forma correcta 
				da = q_Conect.Build_Adapter("Select * from MPaymentChange, PaymentMethod " +
					" where PaymentMethod.idMPayment = MPaymentChange.idMPayment " +
					"and idMPaymentChange = @idMPaymentChange; ");

				//ROV 29/08/18 Se realiza la validacion de parametros
				da.SelectCommand.Parameters.Add(new SqlParameter("@idMPaymentChange", SqlDbType.VarChar));
				da.SelectCommand.Parameters["@idMPaymentChange"].Value = e_MPaymentChange.idMPaymentChange;

				fila = WievTable(da);

				//ROV 29/08/18 Se valida que hay datos en la tabla 
				if (fila != null)
				{
					//ROV 29/08/18 Se inserta los datos en cada entidad generada 
					e_MPaymentChange.idMPaymentChange = Convert.ToInt16(fila["idMPaymentChange"]);

					e_MPaymentChange.idMPayment = Convert.ToInt16(fila["idMPayment"]);
					e_MPaymentChange.idTypeChange = Convert.ToInt16(fila["idTypeChange"]);

					e_MPaymentChange.typepay = fila["sNameMPayment"].ToString();

					e_MPaymentChange.dLimitvalue = Convert.ToDecimal(fila["dLimitvalue"]);


					q_Conect.ClosedDB();
					return e_MPaymentChange;
				}
				else

					q_Conect.ClosedDB();
				return null;
			}
			return null;
		}

		public DataTable Searchformethod(E_MPaymentChange e_MPaymentChange)
		{
			Q_Conect q_Conect = new Q_Conect();
			SqlCommand comand;
			DataTable ddt = new DataTable();

			//ROV 29/08/18 Se realiza conexion a la base de datos 
			if (q_Conect.OpenDB())
			{
				//ROV 29/08/18 Se realiza la cnosulta de la base de datos 
				comand = q_Conect.Build_Command("Select sNameTChange as 'Tipo de Cambio' from TChange,MPaymentChange,PaymentMethod"+
					" where  PaymentMethod.idMPayment = MPaymentChange.idTypeChange" +
					" and PaymentMethod.idMPayment = MPaymentChange.idMPayment"+
					" and MPaymentChange.idMPayment = @idMPayment");
				SqlDataAdapter da = new SqlDataAdapter(comand);

				da.SelectCommand.Parameters.Add(new SqlParameter("@idMPayment", SqlDbType.VarChar));
				da.SelectCommand.Parameters["@idMPayment"].Value = e_MPaymentChange.idMPayment;

				//ROV 29/08/18 Se realiza el llenado del datatable con la lista 
				da.Fill(ddt);
				return ddt;
			}
			q_Conect.ClosedDB();
			return null;
		}

		public E_MPaymentChange Searchlimit(E_MPaymentChange e_MPaymentChange)
		{
			Q_Conect q_Conect = new Q_Conect();
			DataRow fila;
			SqlDataAdapter da = new SqlDataAdapter();

			//ROV 29/08/18 Se relaiza conexion a la base de datos 
			if (q_Conect.OpenDB())
			{
				//ROV 29/08/18 Se realiza la consulta de buscar la forma correcta 
				da = q_Conect.Build_Adapter("Select * from MPaymentChange , PaymentMethod " +
					" where  PaymentMethod.idMPayment = MPaymentChange.idTypeChange " +
					" and PaymentMethod.idMPayment = MPaymentChange.idMPayment "+
					" and MPaymentChange.idTypeChange = @idTypeChange" +
					" and MPaymentChange.idMPayment = @idMPayment ");

				//ROV 29/08/18 Se realiza la validacion de parametros
				da.SelectCommand.Parameters.Add(new SqlParameter("@idTypeChange", SqlDbType.VarChar));
				da.SelectCommand.Parameters["@idTypeChange"].Value = e_MPaymentChange.idTypeChange;

				da.SelectCommand.Parameters.Add(new SqlParameter("@idMPayment", SqlDbType.VarChar));
				da.SelectCommand.Parameters["@idMPayment"].Value = e_MPaymentChange.idMPayment;

				fila = WievTable(da);

				//ROV 29/08/18 Se valida que hay datos en la tabla 
				if (fila != null)
				{
					//ROV 29/08/18 Se inserta los datos en cada entidad generada 
					e_MPaymentChange.idMPaymentChange = Convert.ToInt16(fila["idMPaymentChange"]);
					e_MPaymentChange.idTypeChange = Convert.ToInt16(fila["idTypeChange"]);
					e_MPaymentChange.idMPayment= Convert.ToInt16(fila["idMPayment"]);
					e_MPaymentChange.dLimitvalue = Convert.ToDecimal(fila["dLimitvalue"]);

					q_Conect.ClosedDB();
					return e_MPaymentChange;
				}
				else

					q_Conect.ClosedDB();
				return null;
			}
			return null;
		}

		public DataRow WievTable(SqlDataAdapter adapter)
		{
			DataSet ds;
			DataRow fila;

			ds = new DataSet();

			try
			{
				adapter.Fill(ds, "MPaymentChange");
				//Se crea un objeto DataTable para uso de su coleccíon 
				//DataRow para extraer el registro a actualizar
				DataTable miTabla = ds.Tables["MPaymentChange"];

				//Se almacena el registro en un objeto DataRow
				fila = miTabla.Rows[0];
				return (fila);
			}

			catch //(Exception oEx)
			{

				return (null);
			}
		}


	}
}
