﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;
using Inets.Models.Entity_Entities;

namespace Inets.Data.BaseDate
{
	public class Q_MPTicketSQL
	{
		//public bool InsertEfectivo(E_MPTicket e_MPTicket)
		//{
		//	Q_Conect q_Conect = new Q_Conect();
		//	SqlCommand comand;

		//	if (q_Conect.OpenDB())//28/08/18 se abre la conexion de base de datos 
		//	{
		//		comand = q_Conect.Build_Command("Insert Into MPTicket(idTicket, idMPayment, dAmountPayable, dPaidAmount, bValidate)" +
		//			" values( @idTicket, @idMPayment, @dAmountPayable, @dPaidAmount, @bValidate)");

		//		//28/08/18 se parametrisa los datos y valores de de la base de datos
		//		comand.Parameters.Add(new SqlParameter("@idTicket", SqlDbType.Int));
		//		comand.Parameters["@idTicket"].Value = e_MPTicket.idTicket;

		//		comand.Parameters.Add(new SqlParameter("@idMPayment", SqlDbType.Int));
		//		comand.Parameters["@idMPayment"].Value = e_MPTicket.idMPayment;
				
		//		comand.Parameters.Add(new SqlParameter("@dAmountPayable ", SqlDbType.Decimal));
		//		comand.Parameters["@dAmountPayable "].Value = e_MPTicket.dAmountPayable;

		//		comand.Parameters.Add(new SqlParameter("@dPaidAmount ", SqlDbType.Decimal));
		//		comand.Parameters["@dPaidAmount "].Value = e_MPTicket.Amountpay;

		//		comand.Parameters.Add(new SqlParameter("@bValidate ", SqlDbType.Bit));
		//		comand.Parameters["@bValidate "].Value = e_MPTicket.bValidate;

		//		int filas = comand.ExecuteNonQuery();//28/08/18 Se cuentan que exista el registro a la base de datos 

		//		q_Conect.ClosedDB();
		//		if (filas > 0) //28/08/18 Se valida cambio o ingreso de datos a la base de datos 
		//		{
		//			return true;
		//		}
		//		else
		//		{
		//			return false;
		//		}
		//	}
		//	return false;
		//}

		//public bool InsertCheques(E_MPTicket e_MPTicket)
		//{
		//	Q_Conect q_Conect = new Q_Conect();
		//	SqlCommand comand;

		//	if (q_Conect.OpenDB())//28/08/18 se abre la conexion de base de datos 
		//	{
		//		comand = q_Conect.Build_Command("Insert Into MPTicket(idTicket, idMPayment, sBank, sFolio, dDateEmition, dAmountPayable, dPaidAmount, bValidate)" +
		//			" values( @idTicket, @idMPayment, @sBank, @sFolio, @dDateEmition, @dAmountPayable, @dPaidAmount, @bValidate)");

		//		//28/08/18 se parametrisa los datos y valores de de la base de datos
		//		comand.Parameters.Add(new SqlParameter("@idTicket", SqlDbType.Int));
		//		comand.Parameters["@idTicket"].Value = e_MPTicket.idTicket;

		//		comand.Parameters.Add(new SqlParameter("@idMPayment", SqlDbType.Int));
		//		comand.Parameters["@idMPayment"].Value = e_MPTicket.idMPayment;

		//		comand.Parameters.Add(new SqlParameter("@sBank ", SqlDbType.VarChar));
		//		comand.Parameters["@sBank "].Value = e_MPTicket.sBank;

		//		comand.Parameters.Add(new SqlParameter("@sFolio ", SqlDbType.VarChar));
		//		comand.Parameters["@sFolio "].Value = e_MPTicket.sFolio;

		//		comand.Parameters.Add(new SqlParameter("@dDateEmition ", SqlDbType.DateTime));
		//		comand.Parameters["@dDateEmition "].Value =Convert.ToDateTime(e_MPTicket.dDateEmition);

		//		comand.Parameters.Add(new SqlParameter("@dAmountPayable ", SqlDbType.Decimal));
		//		comand.Parameters["@dAmountPayable "].Value = e_MPTicket.dAmountPayable;

		//		comand.Parameters.Add(new SqlParameter("@dPaidAmount ", SqlDbType.Decimal));
		//		comand.Parameters["@dPaidAmount "].Value = e_MPTicket.Amountpay;

		//		comand.Parameters.Add(new SqlParameter("@bValidate ", SqlDbType.Bit));
		//		comand.Parameters["@bValidate "].Value = e_MPTicket.bValidate;

		//		int filas = comand.ExecuteNonQuery();//28/08/18 Se cuentan que exista el registro a la base de datos 

		//		q_Conect.ClosedDB();
		//		if (filas > 0) //28/08/18 Se valida cambio o ingreso de datos a la base de datos 
		//		{
		//			return true;
		//		}
		//		else
		//		{
		//			return false;
		//		}
		//	}
		//	return false;
		//}

		//public bool InsertVales(E_MPTicket e_MPTicket)
		//{
		//	Q_Conect q_Conect = new Q_Conect();
		//	SqlCommand comand;

		//	if (q_Conect.OpenDB())//28/08/18 se abre la conexion de base de datos 
		//	{
		//		comand = q_Conect.Build_Command("Insert Into MPTicket(idTicket, idMPayment, idDenomination, dQuantity, dAmountPayable, dPaidAmount, bValidate)" +
		//			" values( @idTicket, @idMPayment, @idDenomination, @dQuantity, @dAmountPayable, @dPaidAmount, @bValidate)");

		//		//28/08/18 se parametrisa los datos y valores de de la base de datos
		//		comand.Parameters.Add(new SqlParameter("@idTicket", SqlDbType.Int));
		//		comand.Parameters["@idTicket"].Value = e_MPTicket.idTicket;

		//		comand.Parameters.Add(new SqlParameter("@idMPayment", SqlDbType.Int));
		//		comand.Parameters["@idMPayment"].Value = e_MPTicket.idMPayment;

		//		comand.Parameters.Add(new SqlParameter("@idDenomination", SqlDbType.Int));
		//		comand.Parameters["@idDenomination"].Value = e_MPTicket.idDenomination;

		//		comand.Parameters.Add(new SqlParameter("@dQuantity", SqlDbType.Decimal));
		//		comand.Parameters["@dQuantity"].Value = e_MPTicket.dQuantity;

		//		comand.Parameters.Add(new SqlParameter("@iNumCard ", SqlDbType.Int));
		//		comand.Parameters["@iNumCard "].Value = e_MPTicket.iNumCard;

		//		comand.Parameters.Add(new SqlParameter("@dAmountPayable ", SqlDbType.Decimal));
		//		comand.Parameters["@dAmountPayable "].Value = e_MPTicket.dAmountPayable;

		//		comand.Parameters.Add(new SqlParameter("@dPaidAmount ", SqlDbType.Decimal));
		//		comand.Parameters["@dPaidAmount "].Value = e_MPTicket.Amountpay;

		//		comand.Parameters.Add(new SqlParameter("@bValidate ", SqlDbType.Bit));
		//		comand.Parameters["@bValidate "].Value = e_MPTicket.bValidate;


		//		int filas = comand.ExecuteNonQuery();//28/08/18 Se cuentan que exista el registro a la base de datos 

		//		q_Conect.ClosedDB();
		//		if (filas > 0) //28/08/18 Se valida cambio o ingreso de datos a la base de datos 
		//		{
		//			return true;
		//		}
		//		else
		//		{
		//			return false;
		//		}
		//	}
		//	return false;
		//}

		//public bool InsertTarjeta(E_MPTicket e_MPTicket)
		//{
		//	Q_Conect q_Conect = new Q_Conect();
		//	SqlCommand comand;

		//	if (q_Conect.OpenDB())//28/08/18 se abre la conexion de base de datos 
		//	{
		//		comand = q_Conect.Build_Command("Insert Into MPTicket(idTicket, idMPayment, idCard, idTypeCard, iAuthorizacion, iNumCard, dAmountPayable, dPaidAmount,bValidate)" +
		//			" values( @idTicket, @idMPayment, @idCard, @idTypeCard, @iAuthorizacion, @iNumCard, @dAmountPayable, @dPaidAmount, @bValidate)");

		//		//28/08/18 se parametrisa los datos y valores de de la base de datos
		//		comand.Parameters.Add(new SqlParameter("@idTicket", SqlDbType.Int));
		//		comand.Parameters["@idTicket"].Value = e_MPTicket.idTicket;

		//		comand.Parameters.Add(new SqlParameter("@idMPayment", SqlDbType.Int));
		//		comand.Parameters["@idMPayment"].Value = e_MPTicket.idMPayment;

		//		comand.Parameters.Add(new SqlParameter("@idCard", SqlDbType.Int));
		//		comand.Parameters["@idCard"].Value = e_MPTicket.idCard;

		//		comand.Parameters.Add(new SqlParameter("@idTypeCard", SqlDbType.Int));
		//		comand.Parameters["@idTypeCard"].Value = e_MPTicket.idTypeCard;

		//		comand.Parameters.Add(new SqlParameter("@iAuthorizacion ", SqlDbType.VarChar));
		//		comand.Parameters["@iAuthorizacion "].Value = e_MPTicket.iAuthorizacion;

		//		comand.Parameters.Add(new SqlParameter("@iNumCard ", SqlDbType.Int));
		//		comand.Parameters["@iNumCard "].Value = e_MPTicket.iNumCard;

		//		comand.Parameters.Add(new SqlParameter("@dAmountPayable ", SqlDbType.Decimal));
		//		comand.Parameters["@dAmountPayable "].Value = e_MPTicket.dAmountPayable;

		//		comand.Parameters.Add(new SqlParameter("@dPaidAmount ", SqlDbType.Decimal));
		//		comand.Parameters["@dPaidAmount "].Value = e_MPTicket.Amountpay;

		//		comand.Parameters.Add(new SqlParameter("@bValidate ", SqlDbType.Decimal));
		//		comand.Parameters["@bValidate "].Value = e_MPTicket.Amountpay;

		//		int filas = comand.ExecuteNonQuery();//28/08/18 Se cuentan que exista el registro a la base de datos 

		//		q_Conect.ClosedDB();
		//		if (filas > 0) //28/08/18 Se valida cambio o ingreso de datos a la base de datos 
		//		{
		//			return true;
		//		}
		//		else
		//		{
		//			return false;
		//		}
		//	}
		//	return false;
		//}
		
		//public bool Update(E_MPTicket e_MPTicket)
		//{
		//	Q_Conect q_Conect = new Q_Conect();

		//	SqlCommand comand;

		//	if (q_Conect.OpenDB())//28/08/18 Se abre la conexion a la base de datos 
		//	{
		//		comand = q_Conect.Build_Command("Update TChange where idTicket = @idTicket, idMPayment = @idMPayment, idDenomination = @idDenomination, idCard = @idCard, idTypeCard = @idTypeCard, dQuantity = @dQuantity, sBank = @sBank, sFolio = @sFolio, dDateEmition = @dDateEmition, iAuthorizacion = @iAuthorizacion, iNumCard = @iNumCard, dAmountPayable = @dAmountPayable, dPaidAmount = dPaidAmount" +
		//			"where idMPTicket = @idMPTicket; ");

		//		comand.Parameters.Add(new SqlParameter("@idMPTicket ", SqlDbType.Int));
		//		comand.Parameters["@idMPTicket "].Value = e_MPTicket.idMPTicket;

		//		comand.Parameters.Add(new SqlParameter("@idTicket ", SqlDbType.Int));
		//		comand.Parameters["@idTicket "].Value = e_MPTicket.idTicket;

		//		comand.Parameters.Add(new SqlParameter("@idMPayment ", SqlDbType.Int));
		//		comand.Parameters["@idMPayment "].Value = e_MPTicket.idMPayment;

		//		comand.Parameters.Add(new SqlParameter("@idDenomination ", SqlDbType.VarChar));
		//		comand.Parameters["@idDenomination "].Value = e_MPTicket.idDenomination;

		//		comand.Parameters.Add(new SqlParameter("@idCard ", SqlDbType.VarChar));
		//		comand.Parameters["@idCard "].Value = e_MPTicket.idCard;

		//		comand.Parameters.Add(new SqlParameter("@idTypeCard ", SqlDbType.Int));
		//		comand.Parameters["@idTypeCard "].Value = e_MPTicket.idTypeCard;

		//		comand.Parameters.Add(new SqlParameter("@dQuantity ", SqlDbType.Decimal));
		//		comand.Parameters["@dQuantity "].Value = e_MPTicket.dQuantity;

		//		comand.Parameters.Add(new SqlParameter("@sBank ", SqlDbType.VarChar));
		//		comand.Parameters["@sBank "].Value = e_MPTicket.sBank;

		//		comand.Parameters.Add(new SqlParameter("@sFolio ", SqlDbType.VarChar));
		//		comand.Parameters["@sFolio "].Value = e_MPTicket.sFolio;

		//		comand.Parameters.Add(new SqlParameter("@dDateEmition ", SqlDbType.DateTime));
		//		comand.Parameters["@dDateEmition "].Value = e_MPTicket.dDateEmition;

		//		comand.Parameters.Add(new SqlParameter("@iAuthorizacion ", SqlDbType.Int));
		//		comand.Parameters["@iAuthorizacion "].Value = e_MPTicket.iAuthorizacion;

		//		comand.Parameters.Add(new SqlParameter("@iNumCard ", SqlDbType.Int));
		//		comand.Parameters["@iNumCard "].Value = e_MPTicket.iNumCard;

		//		comand.Parameters.Add(new SqlParameter("@dAmountPayable ", SqlDbType.Decimal));
		//		comand.Parameters["@dAmountPayable "].Value = e_MPTicket.dAmountPayable;

		//		comand.Parameters.Add(new SqlParameter("@dPaidAmount ", SqlDbType.Decimal));
		//		comand.Parameters["@dPaidAmount "].Value = e_MPTicket.Amountpay;
		//		int filas = comand.ExecuteNonQuery();//28/08/18 Se cuentan que exista el registro a la base de datos 

		//		q_Conect.ClosedDB();
		//		if (filas > 0) //28/08/18 Se valida cambio o ingreso de datos a la base de datos 
		//		{
		//			return true;
		//		}
		//		else
		//		{
		//			return false;
		//		}
		//	}
		//	return false;
		//}

		//public bool Delete(E_MPTicket e_MPTicket)
		//{
		//	Q_Conect q_Conect = new Q_Conect();

		//	SqlCommand comand;

		//	if (q_Conect.OpenDB())//28/08/18 Se abre la conexion a la base de datos 
		//	{
		//		comand = q_Conect.Build_Command("Delete MPTicket where idMPTicket = @idMPTicket; ");
		//		//28/08/18 Comando para eliminar datos de la tabla de Product en la Base de datos 

		//		comand.Parameters.Add(new SqlParameter("@idMPTicket", SqlDbType.VarChar));
		//		comand.Parameters["@idMPTicket"].Value = e_MPTicket.idMPTicket;
		//		//28/08/18 se parametrisa los datos y valores de de la base de datos 

		//		int filas = comand.ExecuteNonQuery();//28/08/18 Se cuentan que exista el registro a la base de datos 

		//		q_Conect.ClosedDB();
		//		if (filas > 0) //28/08/18 Se valida cambio o ingreso de datos a la base de datos 
		//		{
		//			return true;
		//		}
		//		else
		//		{
		//			return false;
		//		}
		//	}
		//	return false;
		//}

		public DataTable Visualize()
		{
			Q_Conect q_Conect = new Q_Conect();
			SqlCommand comand;
			DataTable dt = new DataTable();

			//ROV 29/08/18 Se realiza conexion a la base de datos 
			if (q_Conect.OpenDB())
			{
				//ROV 29/08/18 Se realiza la cnosulta de la base de datos 
				comand = q_Conect.Build_Command("Select * from MPTicket");
				SqlDataAdapter da = new SqlDataAdapter(comand);

				//ROV 29/08/18 Se realiza el llenado del datatable con la lista 
				da.Fill(dt);
				return dt;
			}
			q_Conect.ClosedDB();
			return null;
		}

		public E_MPTicket Search(E_MPTicket e_MPTicket)
		{
			Q_Conect q_Conect = new Q_Conect();
			DataRow fila;
			SqlDataAdapter da = new SqlDataAdapter();

			//ROV 29/08/18 Se relaiza conexion a la base de datos 
			if (q_Conect.OpenDB())
			{
				//ROV 29/08/18 Se realiza la consulta de buscar la forma correcta 
				da = q_Conect.Build_Adapter("Select * from MPTicket where idMPTicket = @idMPTicket ;");

				//ROV 29/08/18 Se realiza la validacion de parametros
				da.SelectCommand.Parameters.Add(new SqlParameter("@idMPTicket", SqlDbType.VarChar));
				da.SelectCommand.Parameters["@idMPTicket"].Value = e_MPTicket.idMPTicket;

				fila = WievTable(da);

				//ROV 29/08/18 Se valida que hay datos en la tabla 
				if (fila != null)
				{
					//ROV 29/08/18 Se inserta los datos en cada entidad generada 
					e_MPTicket.idMPTicket = Convert.ToInt32(fila["idMPTicket"]);
					e_MPTicket.idTicket = Convert.ToInt32(fila["idTicket"]);
					e_MPTicket.idDenomination = Convert.ToInt32(fila["idDenomination"]);
					e_MPTicket.idCard = Convert.ToInt32(fila["idCard"]);
					e_MPTicket.idTypeCard = Convert.ToInt32(fila["idTypeCard"]);
					e_MPTicket.dQuantity = Convert.ToInt32(fila["dQuantity"]);
					e_MPTicket.sBank = fila["sBank"].ToString();
					e_MPTicket.sFolio = fila["sFolio"].ToString();
					e_MPTicket.dDateEmition = fila["dDateEmition"].ToString();
					e_MPTicket.iAuthorizacion = fila["iAuthorizacion"].ToString();
					e_MPTicket.iNumCard = Convert.ToInt32(fila["iNumCard"]);
					e_MPTicket.dAmountPayable = Convert.ToDecimal(fila["dAmountPayable"]);
					e_MPTicket.Amountpay = Convert.ToDecimal(fila["dPaidAmount"]);
					
					q_Conect.ClosedDB();
					return e_MPTicket;
				}
				else

					q_Conect.ClosedDB();
				return null;
			}
			return null;
		}

		public E_MPTicket SearchId(E_MPTicket e_MPTicket)
		{
			Q_Conect q_Conect = new Q_Conect();
			DataRow fila;
			SqlDataAdapter da = new SqlDataAdapter();

			//ROV 29/08/18 Se relaiza conexion a la base de datos 
			if (q_Conect.OpenDB())
			{
				//ROV 29/08/18 Se realiza la consulta de buscar la forma correcta 
				da = q_Conect.Build_Adapter("Select * from MPTicket where idTicket = @idTicket ;");

				//ROV 29/08/18 Se realiza la validacion de parametros
				da.SelectCommand.Parameters.Add(new SqlParameter("@idTicket", SqlDbType.VarChar));
				da.SelectCommand.Parameters["@idTicket"].Value = e_MPTicket.idTicket;

				fila = WievTable(da);

				//ROV 29/08/18 Se valida que hay datos en la tabla 
				if (fila != null)
				{
					//ROV 29/08/18 Se inserta los datos en cada entidad generada 
					e_MPTicket.idMPTicket = Convert.ToInt32(fila["idMPTicket"]);
					e_MPTicket.idTicket = Convert.ToInt32(fila["idTicket"]);
					

					q_Conect.ClosedDB();
					return e_MPTicket;
				}
				else

					q_Conect.ClosedDB();
				return null;
			}
			return null;
		}
		
		public DataRow WievTable(SqlDataAdapter adapter)
		{
			DataSet ds;
			DataRow fila;

			ds = new DataSet();

			try
			{
				adapter.Fill(ds, "MPTicket");
				//Se crea un objeto DataTable para uso de su coleccíon 
				//DataRow para extraer el registro a actualizar
				DataTable miTabla = ds.Tables["MPTicket"];

				//Se almacena el registro en un objeto DataRow
				fila = miTabla.Rows[0];
				return (fila);
			}

			catch //(Exception oEx)
			{

				return (null);
			}
		}
	}
}
