﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;
using System.Data;
using Inets.Models.Entity_Entities;

namespace Inets.Data.BaseDate
{
    public class Q_CashSalesSQL
    {
        
        public E_CashSales SearchCash(E_CashSales e_CashSales)
        {
            Q_Conect q_Conect = new Q_Conect();
            DataRow fila;
            SqlDataAdapter da = new SqlDataAdapter();

            //ROV 29/08/18 Se relaiza conexion a la base de datos 
            if (q_Conect.OpenDB())
            {
                //ROV 29/08/18 Se realiza la consulta de buscar la forma correcta 
                da = q_Conect.Build_Adapter("select * from CashSales where iIdUsers = @iIdUsers");

                //ROV 29/08/18 Se realiza la validacion de parametros
                da.SelectCommand.Parameters.Add(new SqlParameter("@iIdUsers", SqlDbType.VarChar));
                da.SelectCommand.Parameters["@iIdUsers"].Value = e_CashSales.idUser;

                fila = WievTable(da);

                //ROV 29/08/18 Se valida que hay datos en la tabla 
                if (fila != null)
                {
                    e_CashSales.iIdCashSales = Convert.ToInt32(fila["iIdCashSales"]);
                    e_CashSales.idCash = Convert.ToInt32( fila["iIdCash"]);
                    e_CashSales.idUser = Convert.ToInt32(fila["iIdUsers"]);

                    //ROV 29/08/18 Se inserta los datos en cada entidad generada 
                    q_Conect.ClosedDB();
                    return e_CashSales;
                }
                else
                {
                    q_Conect.ClosedDB();
                    return null;
                }

            }
            return null;
        }

		public E_CashSales SearchCashID(E_CashSales e_CashSales)
		{
			Q_Conect q_Conect = new Q_Conect();
			DataRow fila;
			SqlDataAdapter da = new SqlDataAdapter();

			//ROV 29/08/18 Se relaiza conexion a la base de datos 
			if (q_Conect.OpenDB())
			{
				//ROV 29/08/18 Se realiza la consulta de buscar la forma correcta 
				da = q_Conect.Build_Adapter("select * from CashSales where iIdCashSales = @iIdCashSales");

				//ROV 29/08/18 Se realiza la validacion de parametros
				da.SelectCommand.Parameters.Add(new SqlParameter("@iIdCashSales", SqlDbType.VarChar));
				da.SelectCommand.Parameters["@iIdCashSales"].Value = e_CashSales.iIdCashSales;

				fila = WievTable(da);

				//ROV 29/08/18 Se valida que hay datos en la tabla 
				if (fila != null)
				{
					e_CashSales.iIdCashSales = Convert.ToInt32(fila["iIdCashSales"]);
					e_CashSales.idCash = Convert.ToInt32(fila["iIdCash"]);
					e_CashSales.idUser = Convert.ToInt32(fila["iIdUsers"]);

					//ROV 29/08/18 Se inserta los datos en cada entidad generada 
					q_Conect.ClosedDB();
					return e_CashSales;
				}
				else
				{
					q_Conect.ClosedDB();
					return null;
				}

			}
			return null;
		}

		public DataRow WievTable(SqlDataAdapter adapter)
        {
            DataSet ds;
            DataRow fila;

            ds = new DataSet();

            try
            {
                adapter.Fill(ds, "CashSales");
                //Se crea un objeto DataTable para uso de su coleccíon 
                //DataRow para extraer el registro a actualizar
                DataTable miTabla = ds.Tables["CashSales"];

                //Se almacena el registro en un objeto DataRow
                fila = miTabla.Rows[0];
                return (fila);
            }

            catch //(Exception oEx)
            {

                return (null);
            }
        }

        public DataTable VisualizateCash()
        {
            Q_Conect q_Conect = new Q_Conect();
            SqlCommand comand;
            DataTable dt = new DataTable();

            //ROV 29/08/18 Se realiza conexion a la base de datos 
            if (q_Conect.OpenDB())
            {
                //ROV 29/08/18 Se realiza la cnosulta de la base de datos 
                comand = q_Conect.Build_Command("Select sNameCash as Caja, sNickname as Usuario from CashSales, Cash, Users where CashSales.iIdCash =Cash.iIdCash and CashSales.iIdUsers= Users.iIdUsers");
                SqlDataAdapter da = new SqlDataAdapter(comand);

                //ROV 29/08/18 Se realiza el llenado del datatable con la lista 
                da.Fill(dt);
                return dt;
            }
            q_Conect.ClosedDB();
            return null;
        }
    }
}
