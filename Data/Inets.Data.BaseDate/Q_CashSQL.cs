﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;
using System.Data;
using Inets.Models.Entity_Entities;

namespace Inets.Data.BaseDate
{
    public class Q_CashSQL
    {
        public E_Cash Search(E_Cash e_Cash)
        {
            Q_Conect q_Conect = new Q_Conect();
            DataRow fila;
            SqlDataAdapter da = new SqlDataAdapter();

            //ROV 29/08/18 Se relaiza conexion a la base de datos 
            if (q_Conect.OpenDB())
            {
                //ROV 29/08/18 Se realiza la consulta de buscar la forma correcta 
                da = q_Conect.Build_Adapter("Select * from Cash where iIdCash = @iIdCash");

                //ROV 29/08/18 Se realiza la validacion de parametros
                da.SelectCommand.Parameters.Add(new SqlParameter("@iIdCash", SqlDbType.VarChar));
                da.SelectCommand.Parameters["@iIdCash"].Value = e_Cash.iIdCash;

                fila = WievTable(da);

                //ROV 29/08/18 Se valida que hay datos en la tabla 
                if (fila != null)
                {
                    //ROV 29/08/18 Se inserta los datos en cada entidad generada 
                    e_Cash.iIdCash = Convert.ToInt32(fila["iIdCash"]);
                    e_Cash.iIdStore = Convert.ToInt32(fila["iIdStore"]);
                    e_Cash.sNameCash = fila["sNameCash"].ToString();

                    q_Conect.ClosedDB();
                    return e_Cash;
                }
                else
                {
                    q_Conect.ClosedDB();
                    return null;
                }

            }
            return null;
        }

        public E_Cash SearchId(E_Cash e_Cash)
        {
            Q_Conect q_Conect = new Q_Conect();
            DataRow fila;
            SqlDataAdapter da = new SqlDataAdapter();

            //ROV 29/08/18 Se relaiza conexion a la base de datos 
            if (q_Conect.OpenDB())
            {
                //ROV 29/08/18 Se realiza la consulta de buscar la forma correcta 
                da = q_Conect.Build_Adapter("Select * from Cash where sNameCash = @sNameCash");

                //ROV 29/08/18 Se realiza la validacion de parametros
                da.SelectCommand.Parameters.Add(new SqlParameter("@sNameCash", SqlDbType.VarChar));
                da.SelectCommand.Parameters["@sNameCash"].Value = e_Cash.sNameCash;

                fila = WievTable(da);

                //ROV 29/08/18 Se valida que hay datos en la tabla 
                if (fila != null)
                {
                    //ROV 29/08/18 Se inserta los datos en cada entidad generada 
                    e_Cash.iIdCash = Convert.ToInt32(fila["iIdCash"]);
                    e_Cash.iIdStore = Convert.ToInt32(fila["iIdStore"]);
                    e_Cash.sNameCash = fila["sNameCash"].ToString();

                    q_Conect.ClosedDB();
                    return e_Cash;
                }
                else
                {
                    q_Conect.ClosedDB();
                    return null;
                }
            }
            return null;
        }

        public bool ValidateRegistre(E_Cash e_Cash)
        {
            Q_Conect q_Conect = new Q_Conect();
            SqlDataReader dr;
            SqlCommand comand = new SqlCommand();

            if (q_Conect.OpenDB())
            {
                //ROV 29/08/18 se ejecuta el comando donde hace el conteo de las veces que se inserto un producto 
                comand = q_Conect.Build_Command("select count (*) from Cash where iIdStore=@iIdStore, sNameCash=@sNameCash ");

                //ROV 29/08/18 Se valida los parametros 
                comand.Parameters.Add(new SqlParameter("@iIdStore", SqlDbType.VarChar));
                comand.Parameters["@iIdStore"].Value = e_Cash.iIdStore;

                comand.Parameters.Add(new SqlParameter("@sNameCash", SqlDbType.VarChar));
                comand.Parameters["@sNameCash"].Value = e_Cash.sNameCash;

                //ROV 29/08/18 Se ejecuta la lectura del comando 
                dr = comand.ExecuteReader();

                //ROV 29/08/18 Se realiza la lectura
                dr.Read();

                //ROV 29/08/18 Se valida que hay registro 
                if (Convert.ToInt32(dr[0]) == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;
        }

        public DataTable Freebox()
        {
            Q_Conect q_Conect = new Q_Conect();
            SqlCommand comand;
            DataTable dt = new DataTable();

            //ROV 29/08/18 Se realiza conexion a la base de datos 
            if (q_Conect.OpenDB())
            {
                //ROV 29/08/18 Se realiza la cnosulta de la base de datos 
                comand = q_Conect.Build_Command("Select * from Cash where bStatus = 0");
                SqlDataAdapter da = new SqlDataAdapter(comand);

                //ROV 29/08/18 Se realiza el llenado del datatable con la lista 
                da.Fill(dt);
                return dt;
            }
            q_Conect.ClosedDB();
            return null;
        }

        public DataTable VisualizateCash()
        {
            Q_Conect q_Conect = new Q_Conect();
            SqlCommand comand;
            DataTable dt = new DataTable();

            //ROV 29/08/18 Se realiza conexion a la base de datos 
            if (q_Conect.OpenDB())
            {
                //ROV 29/08/18 Se realiza la cnosulta de la base de datos 
                comand = q_Conect.Build_Command("Select * from Cash");
                SqlDataAdapter da = new SqlDataAdapter(comand);

                //ROV 29/08/18 Se realiza el llenado del datatable con la lista 
                da.Fill(dt);
                return dt;
            }
            q_Conect.ClosedDB();
            return null;
        }

        public DataRow WievTable(SqlDataAdapter adapter)
        {
            DataSet ds;
            DataRow fila;

            ds = new DataSet();

            try
            {
                adapter.Fill(ds, "Cash");
                //Se crea un objeto DataTable para uso de su coleccíon 
                //DataRow para extraer el registro a actualizar
                DataTable miTabla = ds.Tables["Cash"];

                //Se almacena el registro en un objeto DataRow
                fila = miTabla.Rows[0];
                return (fila);
            }

            catch// (Exception oEx)
            {

                return (null);
            }
        }
		
    }
}
