﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inets.Data.BaseDate
{
    public class Q_Conect
    {
        private SqlConnection Connectdb;
        private SqlCommand CommandSql;
        private SqlDataReader Read;
        private SqlDataAdapter Adjust;

        public SqlDataReader _read
        {
            get
            {
                return (_read);
            }
        }

        public bool OpenDB()  // Funcion conectar base de datos 
        {

            Connectdb = new SqlConnection();
            Connectdb.ConnectionString = @"Data Source=DESKTOP-OAHA93R\SQLEXPRESS;Initial Catalog=DBPOSDev; Integrated Security=True";// Char de conexion a base de datos 
            //Connectdb.ConnectionString = @"Server=tcp:inetssql.database.windows.net,1433;Initial Catalog=InetTPV;Persist Security Info=False;User ID=adminets;Password=Proyectos2018;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30";
            try// validar que la Char de conexion sea correcta 
            {
                Connectdb.Open();
                return true;
            }
            catch //(Exception oEx)
            {
  
                return false;
            }
			
        }

        public void ClosedDB()// funcion cierre de base de datos
        {
            Connectdb.Close();
        }

        public SqlCommand Build_Command(string Char)//Funcion para contruir comandos 
        {
            CommandSql = new SqlCommand(Char, Connectdb);
            return (CommandSql);
        }

        public SqlDataAdapter Build_Adapter(string Char)
        {
            Adjust = new SqlDataAdapter(Char, Connectdb);//inicializa la instancia de la consulta con sus parametros 
            return (Adjust);

        }//constructor para adaptar las consultas de c# a sql

        public SqlDataReader RunReader()
        {
            try
            {
                Read = CommandSql.ExecuteReader();//si no existe ningun error en la consulta la ejecura
                return Read;
            }
            catch //(Exception oEx)
            {
                return null;
            }
        }//ejecuta consultas 

    }
}

