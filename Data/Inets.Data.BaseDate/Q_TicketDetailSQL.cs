﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;
using Inets.Models.Entity_Entities;

namespace Inets.Data.BaseDate
{
    public class Q_TicketDetailSQL
    {

        //ROV 11/09/18 Funcion para realizar el query de buscar 
        public E_DetailTicket Search(E_DetailTicket e_DetailTicket)
        {
            Q_Conect q_Conect = new Q_Conect();
            DataRow fila;
            SqlDataAdapter da = new SqlDataAdapter();

            //ROV 29/08/18 Se relaiza conexion a la base de datos 
            if (q_Conect.OpenDB())
            {
                //ROV 29/08/18 Se realiza la consulta de buscar la forma correcta 
                da = q_Conect.Build_Adapter("Select * from DetailTicket where iIdProduct = @iIdProduct;");

                //ROV 29/08/18 Se realiza la validacion de parametros
                da.SelectCommand.Parameters.Add(new SqlParameter("@iIdProduct", SqlDbType.VarChar));
                da.SelectCommand.Parameters["@iIdProduct"].Value = e_DetailTicket.iIdProduct;

                fila = WievTable(da);

                //ROV 29/08/18 Se valida que hay datos en la tabla 
                if (fila != null)
                {
                    //ROV 29/08/18 Se inserta los datos en cada entidad generada 

                    e_DetailTicket.iIdTicket = Convert.ToInt32(fila["iIdTicket"]);
                    e_DetailTicket.iIdProduct = Convert.ToInt32(fila["iIdProduct"]);
                    e_DetailTicket.dQuantityProduct = Convert.ToDecimal(fila["dQuantityProduct"]);
                    e_DetailTicket.dUnitValue = Convert.ToDecimal(fila["dUnitValue"]);

                    q_Conect.ClosedDB();
                    return e_DetailTicket;
                }
                else
                {
                    q_Conect.ClosedDB();
                    return null;
                }

            }
            return null;
        }

		//ROV 11/09/18 Funcion para usar un query de  SUM
		public Decimal Sum(E_DetailTicket e_DetailTicket)
		{
			Q_Conect q_Conect = new Q_Conect();
			DataRow fila;
			SqlDataAdapter da = new SqlDataAdapter();
			Decimal total;

			//ROV 29/08/18 Se relaiza conexion a la base de datos 
			if (q_Conect.OpenDB())
			{
				//ROV 29/08/18 Se realiza la consulta de buscar la forma correcta 
				da = q_Conect.Build_Adapter("Select Sum (dUnitValue) as total from DetailTicket where iIdTicket = @iIdTicket;");

				//ROV 29/08/18 Se realiza la validacion de parametros
				da.SelectCommand.Parameters.Add(new SqlParameter("@iIdTicket", SqlDbType.VarChar));
				da.SelectCommand.Parameters["@iIdTicket"].Value = e_DetailTicket.iIdTicket;

				fila = WievTable(da);

				//ROV 29/08/18 Se valida que hay datos en la tabla 
				q_Conect.ClosedDB();
				if (fila != null)
				{
					//ROV 29/08/18 Se inserta los datos en cada entidad generada 

					total = Convert.ToDecimal(fila["total"]);

					return total;
				}
				else
				{
					//    q_Conect.ClosedDB();
					return 0;
				}

			}
			return 0;
		}

		//ROV 11/09/18 Funcion para el query de Visualize tabla
		public DataTable Visualize(E_DetailTicket e_DetailTicket)
        {
            Q_Conect q_Conect = new Q_Conect();
            SqlCommand comand;
            DataTable dt = new DataTable();

            //ROV 29/08/18 Se realiza conexion a la base de datos 
            if (q_Conect.OpenDB())
            {
                //ROV 29/08/18 Se realiza la cnosulta de la base de datos 
                comand = q_Conect.Build_Command("Select dQuantityProduct as Cantidad,sNameProduct as Producto, dSalesPrice as Precio, dUnitValue as 'Monto total',sUPC as Codigo from DetailTicket, Ticket, Product " +
                    "where Product.IdProduct = DetailTicket.iIdProduct and DetailTicket.iIdTicket = Ticket.iIdTicket and Ticket.iIdTicket = @IdTicket; ");

                comand.Parameters.Add(new SqlParameter("@IdTicket ", SqlDbType.VarChar));
                comand.Parameters["@IdTicket "].Value = e_DetailTicket.iIdTicket;

                SqlDataAdapter da = new SqlDataAdapter(comand);

                //ROV 29/08/18 Se realiza el llenado del datatable con la lista 
                da.Fill(dt);
                return dt;
            }
            q_Conect.ClosedDB();
            return null;
        }

        public DataRow WievTable(SqlDataAdapter adapter)
        {
            DataSet ds;
            DataRow fila;

            ds = new DataSet();

            try
            {
                adapter.Fill(ds, "DetailTicket");
                //Se crea un objeto DataTable para uso de su coleccíon 
                //DataRow para extraer el registro a actualizar
                DataTable miTabla = ds.Tables["DetailTicket"];

                //Se almacena el registro en un objeto DataRow
                fila = miTabla.Rows[0];
                return (fila);
            }

            catch //(Exception oEx)
            {

                return (null);
            }
        }

        
    }
}
