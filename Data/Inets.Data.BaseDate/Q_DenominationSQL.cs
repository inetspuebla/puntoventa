﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;
using Inets.Models.Entity_Entities;

namespace Inets.Data.BaseDate
{
	public class Q_DenominationSQL
	{
		
		public DataTable Visualize(E_Denomination e_Denomination)
		{
			Q_Conect q_Conect = new Q_Conect();
			SqlCommand comand;
			DataTable dt = new DataTable();

			//ROV 29/08/18 Se realiza conexion a la base de datos 
			if (q_Conect.OpenDB())
			{
				//ROV 29/08/18 Se realiza la cnosulta de la base de datos 
				comand = q_Conect.Build_Command("Select dValue from Denomination where idCompany = @idCompany ");
				SqlDataAdapter da = new SqlDataAdapter(comand);

				comand.Parameters.Add(new SqlParameter("@idCompany", SqlDbType.Decimal));
				comand.Parameters["@idCompany"].Value = e_Denomination.idCompany;

				//ROV 29/08/18 Se realiza el llenado del datatable con la lista 
				da.Fill(dt);
				return dt;
			}
			q_Conect.ClosedDB();
			return null;
		}

		public E_Denomination Search(E_Denomination e_Denomination)
		{
			Q_Conect q_Conect = new Q_Conect();
			DataRow fila;
			SqlDataAdapter da = new SqlDataAdapter();

			//ROV 29/08/18 Se relaiza conexion a la base de datos 
			if (q_Conect.OpenDB())
			{
				//ROV 29/08/18 Se realiza la consulta de buscar la forma correcta 
				da = q_Conect.Build_Adapter("Select * from Denomination where dValue = @dValue and idCompany=@idCompany ;");

				//ROV 29/08/18 Se realiza la validacion de parametros
				da.SelectCommand.Parameters.Add(new SqlParameter("@dValue", SqlDbType.VarChar));
				da.SelectCommand.Parameters["@dValue"].Value = e_Denomination.dValue;

				da.SelectCommand.Parameters.Add(new SqlParameter("@idCompany", SqlDbType.VarChar));
				da.SelectCommand.Parameters["@idCompany"].Value = e_Denomination.idCompany;

				fila = WievTable(da);

				//ROV 29/08/18 Se valida que hay datos en la tabla 
				if (fila != null)
				{
					//ROV 29/08/18 Se inserta los datos en cada entidad generada 

					e_Denomination.idDenomination = Convert.ToInt32(fila["idDenomination"]);
					e_Denomination.dValue = Convert.ToDecimal(fila["dValue"]);
					e_Denomination.idCompany = Convert.ToInt32(fila["idCompany"]);

					q_Conect.ClosedDB();
					return e_Denomination;
				}
				else

					q_Conect.ClosedDB();
				return null;
			}
			return null;
		}

		public E_Denomination Searchid(E_Denomination e_Denomination)
		{
			Q_Conect q_Conect = new Q_Conect();
			DataRow fila;
			SqlDataAdapter da = new SqlDataAdapter();

			//ROV 29/08/18 Se relaiza conexion a la base de datos 
			if (q_Conect.OpenDB())
			{
				//ROV 29/08/18 Se realiza la consulta de buscar la forma correcta 
				da = q_Conect.Build_Adapter("Select * from Denomination where idDenomination = @idDenomination ;");

				//ROV 29/08/18 Se realiza la validacion de parametros
				da.SelectCommand.Parameters.Add(new SqlParameter("@idDenomination", SqlDbType.Int));
				da.SelectCommand.Parameters["@idDenomination"].Value = e_Denomination.idDenomination;
				
				fila = WievTable(da);

				//ROV 29/08/18 Se valida que hay datos en la tabla 
				if (fila != null)
				{
					//ROV 29/08/18 Se inserta los datos en cada entidad generada 

					e_Denomination.idDenomination = Convert.ToInt32(fila["idDenomination"]);
					e_Denomination.dValue = Convert.ToDecimal(fila["dValue"]);
					e_Denomination.idCompany = Convert.ToInt32(fila["idCompany"]);

					q_Conect.ClosedDB();
					return e_Denomination;
				}
				else

					q_Conect.ClosedDB();
				return null;
			}
			return null;
		}
		
		public DataRow WievTable(SqlDataAdapter adapter)
		{
			DataSet ds;
			DataRow fila;

			ds = new DataSet();

			try
			{
				adapter.Fill(ds, "Denomination");
				//Se crea un objeto DataTable para uso de su coleccíon 
				//DataRow para extraer el registro a actualizar
				DataTable miTabla = ds.Tables["Denomination"];

				//Se almacena el registro en un objeto DataRow
				fila = miTabla.Rows[0];
				return (fila);
			}

			catch //(Exception oEx)
			{

				return (null);
			}
		}
	}
}
