﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;
using Inets.Models.Entity_Entities;

namespace Inets.Data.BaseDate
{
	public class Q_UsersSQL
	{
		public E_Employee Search(E_Employee e_Employee)
		{
			Q_Conect q_Conect = new Q_Conect();
			DataRow fila;
			SqlDataAdapter da = new SqlDataAdapter();

			//ROV 29/08/18 Se relaiza conexion a la base de datos 
			if (q_Conect.OpenDB())
			{
				//ROV 29/08/18 Se realiza la consulta de buscar la forma correcta 
				da = q_Conect.Build_Adapter("Select * from Employees where iIUsers = @iIUsers ;");

				//ROV 29/08/18 Se realiza la validacion de parametros
				da.SelectCommand.Parameters.Add(new SqlParameter("@iIUsers", SqlDbType.VarChar));
				da.SelectCommand.Parameters["@iIUsers"].Value = e_Employee.iUsers;

				fila = WievTable(da);

				//ROV 29/08/18 Se valida que hay datos en la tabla 
				if (fila != null)
				{
					//ROV 29/08/18 Se inserta los datos en cada entidad generada 
					e_Employee.iUsers= Convert.ToInt32(fila["iIUsers"]);
					e_Employee.sNameEmployee = fila["sNameEmployee"].ToString();
					e_Employee.sLastName = fila["sLastName"].ToString();
					e_Employee.sGener = fila["sGener"].ToString();
					e_Employee.sBirthday = fila["sBirthday"].ToString();
					e_Employee.sAddress = fila["sAddress"].ToString();
					e_Employee.iZipcode = Convert.ToInt32(fila["iZipcode"]);
					e_Employee.Telephone = fila["Telephone"].ToString();
					e_Employee.sRFC = fila["sRFC"].ToString();

					q_Conect.ClosedDB();
					return e_Employee;
				}
				else
				
					q_Conect.ClosedDB();
					return null;
			}
			return null;
		}
	
		public DataRow WievTable(SqlDataAdapter adapter)
		{
			DataSet ds;
			DataRow fila;

			ds = new DataSet();

			try
			{
				adapter.Fill(ds, "Users");
				//Se crea un objeto DataTable para uso de su coleccíon 
				//DataRow para extraer el registro a actualizar
				DataTable miTabla = ds.Tables["Users"];

				//Se almacena el registro en un objeto DataRow
				fila = miTabla.Rows[0];
				return (fila);
			}

			catch //(Exception oEx)
			{

				return (null);
			}
		}

		public DataTable VisualizeCashier()
		{
			Q_Conect q_Conect = new Q_Conect();
			SqlCommand comand;
			DataTable dt = new DataTable();

			//ROV 29/08/18 Se realiza conexion a la base de datos 
			if (q_Conect.OpenDB())
			{
				//ROV 29/08/18 Se realiza la cnosulta de la base de datos 
				comand = q_Conect.Build_Command("Select * from Users where iIdProfile = 5");
				SqlDataAdapter da = new SqlDataAdapter(comand);

				//ROV 29/08/18 Se realiza el llenado del datatable con la lista 
				da.Fill(dt);
				return dt;
			}
			q_Conect.ClosedDB();
			return null;
		}
}

}
