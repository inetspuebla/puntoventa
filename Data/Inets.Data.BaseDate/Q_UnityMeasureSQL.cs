﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;
using Inets.Models.Entity_Entities;

namespace Inets.Data.BaseDate
{
    public class Q_UnityMeasureSQL
    {
        
        public E_UnityMeasure Search (E_UnityMeasure e_UnityMeasure)
        {
            Q_Conect q_Conect = new Q_Conect();
            DataRow fila;
            SqlDataAdapter da = new SqlDataAdapter();

            if(q_Conect.OpenDB())
            {
                da = q_Conect.Build_Adapter("Select * from UnityMeasure where sDescription = @sDescription");

                da.SelectCommand.Parameters.Add("@sDescription", SqlDbType.VarChar);
                da.SelectCommand.Parameters["@sDescription"].Value = e_UnityMeasure.sDesciption;

                fila = WievTable(da);

                if(fila != null)
                {
                    e_UnityMeasure.iIdUnitiMeasure = Convert.ToInt32(fila["iIdUnityMeasure"]);
                    e_UnityMeasure.sDesciption = fila["sDescription"].ToString();

                    q_Conect.ClosedDB();
                    return e_UnityMeasure;
                }
                else
                {
                    q_Conect.ClosedDB();
                    return null;
                }
            }
            return null;
        }

        public DataTable Select()
        {
            Q_Conect q_Conect = new Q_Conect();
            SqlCommand comand;
            DataTable dt = new DataTable();

            if (q_Conect.OpenDB())
            {
                comand = q_Conect.Build_Command("Select * from UnityMeasure");
                SqlDataAdapter da = new SqlDataAdapter(comand);

                da.Fill(dt);
                return dt;
            }
            q_Conect.ClosedDB();
            return null;

        }

        public DataRow WievTable(SqlDataAdapter adapter)
        {
            DataSet ds;
            DataRow fila;

            ds = new DataSet();

            try
            {
                adapter.Fill(ds, "UnityMeasure");
                //Se crea un objeto DataTable para uso de su coleccíon 
                //DataRow para extraer el registro a actualizar
                DataTable miTabla = ds.Tables["UnityMeasure"];

                //Se almacena el registro en un objeto DataRow
                fila = miTabla.Rows[0];
                return (fila);
            }

            catch //(Exception oEx)
            {

                return (null);
            }
        }

    }
}
