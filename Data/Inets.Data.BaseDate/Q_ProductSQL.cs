﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;

using Inets.Models.Entity_Entities;

namespace Inets.Data.BaseDate
{
    public class Q_ProductSQL
    {
        //ROV 29/08/18 Funcion para mandar a traer la tabla de producto
        public DataTable SelectProduct()
        {
            Q_Conect q_Conect = new Q_Conect();
            SqlCommand comand;
            DataTable dt = new DataTable();

            //ROV 29/08/18 Se realiza conexion a la base de datos 
            if (q_Conect.OpenDB())
            {
                //ROV 29/08/18 Se realiza la cnosulta de la base de datos 
                comand = q_Conect.Build_Command("Select sUPC as 'Codigo de barras', sUPCstore as 'Codigo de Tienda', sNameProduct as 'Nombre de Producto', sNameDepartment as 'Departamento', iNumberDepartment as '#Departamento',dPurchasePrice as 'Precio de Compra', dSalesPrice as 'Precio de Venta', dStock as 'dStock' from Product,Department"+
                    " where Product.iIdDepartment = Department.iIdDepartment;");
                SqlDataAdapter da = new SqlDataAdapter(comand);

                //ROV 29/08/18 Se realiza el llenado del datatable con la lista 
                da.Fill(dt);
                return dt;
            }
            q_Conect.ClosedDB();
            return null;
        }

        public DataTable SelectUltiProduct()
        {
            Q_Conect q_Conect = new Q_Conect();
            SqlCommand comand;
            DataTable dt = new DataTable();

            //ROV 29/08/18 Se realiza conexion a la base de datos 
            if (q_Conect.OpenDB())
            {
                //ROV 29/08/18 Se realiza la cnosulta de la base de datos 
                comand = q_Conect.Build_Command("Select Top 10 sUPC as 'Codigo de barras', sUPCstore as 'Codigo de tienda', sNameProduct as 'Nombre de producto', sNameDepartment as 'Departamento', iNumberDepartment as '#Departamento',dPurchasePrice as 'Precio de compra', dSalesPrice as 'Precio de venta', dStock as 'dStock' from Product,Department"+
                    " where Product.iIdDepartment = Department.iIdDepartment order by IdProduct Desc;");
                SqlDataAdapter da = new SqlDataAdapter(comand);

                //ROV 29/08/18 Se realiza el llenado del datatable con la lista 
                da.Fill(dt);
                return dt;
            }
            q_Conect.ClosedDB();
            return null;
        }

        public DataTable ShowProduct(int Quantity)
        {
            Q_Conect q_Conect = new Q_Conect();
            SqlCommand comand;
            DataTable dt = new DataTable();

            //ROV 29/08/18 Se realiza conexion a la base de datos 
            if (q_Conect.OpenDB())
            {
                //ROV 29/08/18 Se realiza la cnosulta de la base de datos 
                comand = q_Conect.Build_Command("Declare @dato int;" + 
                    " set @dato = @num;"+
                    " Select Top(@dato)  sUPC as 'Código de barras', sUPCstore as 'Código de tienda', sNameProduct as 'Nombre de producto', dPurchasePrice as 'Precio de compra', dSalesPrice as 'Precio de venta', dStock as 'Inventario' from Product" + 
                    " order by IdProduct Desc; ");

                comand.Parameters.Add(new SqlParameter("@num", SqlDbType.Int));
                comand.Parameters["@num"].Value = Quantity;

                SqlDataAdapter da = new SqlDataAdapter(comand);

                //ROV 29/08/18 Se realiza el llenado del datatable con la lista 
                da.Fill(dt);
                return dt;
            }
            q_Conect.ClosedDB();
            return null;
        }

        public DataTable FitroSearch(string clave , string Value)
        {
            Q_Conect q_Conect = new Q_Conect();
            SqlCommand comand;
            DataTable dt = new DataTable();

            //ROV 29/08/18 Se realiza conexion a la base de datos 
            if (q_Conect.OpenDB())
            {
                string query = string.Format("Select sNameProduct as 'Nombre de producto', sUPC as 'Código de barras', dSalesPrice as 'Precio de venta' from Product,Department" +
                    " where Product.iIdDepartment = Department.iIdDepartment " +
                    " and {0} = @Valor;", clave);
                //ROV 29/08/18 Se realiza la cnosulta de la base de datos 
                comand = q_Conect.Build_Command(query);

                //comand.Parameters.Add(new SqlParameter("@Clave", SqlDbType.VarChar));
                //comand.Parameters["@Clave"].Value = clave;

                comand.Parameters.Add(new SqlParameter("@Valor", SqlDbType.VarChar));
                comand.Parameters["@Valor"].Value = Value;

                SqlDataAdapter da = new SqlDataAdapter(comand);


                //ROV 29/08/18 Se realiza el llenado del datatable con la lista 
                da.Fill(dt);
                return dt;
            }
            q_Conect.ClosedDB();
            return null;
        }

        //ROV 29/08/18 funcion para mandar a buscar un producto 
        public E_Product SearchProduct(E_Product e_Product)
        {
            Q_Conect q_Conect = new Q_Conect();
            DataRow fila;
            SqlDataAdapter da = new SqlDataAdapter();

            //ROV 29/08/18 Se relaiza conexion a la base de datos 
            if (q_Conect.OpenDB())
            {
                //ROV 29/08/18 Se realiza la consulta de buscar la forma correcta 
                da = q_Conect.Build_Adapter("Select * from Product where sUPC = @sUPC OR  sUPCStore = @sUPCStore");

                //ROV 29/08/18 Se realiza la validacion de parametros
                da.SelectCommand.Parameters.Add(new SqlParameter("@sUPC", SqlDbType.VarChar));
                da.SelectCommand.Parameters["@sUPC"].Value = e_Product.sUPC;

                da.SelectCommand.Parameters.Add(new SqlParameter("@sUPCStore", SqlDbType.VarChar));
                da.SelectCommand.Parameters["@sUPCStore"].Value = e_Product.sUPCStore;

                fila = WievTable(da);

                //ROV 29/08/18 Se valida que hay datos en la tabla 
                if (fila != null)
                {
                    //ROV 29/08/18 Se inserta los datos en cada entidad generada 
                    e_Product.iIdProduct = Convert.ToInt32(fila["IdProduct"]);
                    e_Product.Description = fila["sNameProduct"].ToString();
                    e_Product.iIdDepartament = Convert.ToInt32(fila["iIdDepartment"]);
                    e_Product.sUPC = fila["sUPC"].ToString();
                    e_Product.sUPCStore = fila["sUPCStore"].ToString();
                    e_Product.dPurchasePrice = Convert.ToDecimal(fila["dPurchasePrice"]);
                    e_Product.dSalesPrice = Convert.ToDecimal(fila["dSalesPrice"]);
                    e_Product.dStock = Convert.ToDecimal(fila["dStock"]);
					string nulo = fila["imageprod"].ToString();
					if(nulo != "")
					{
						e_Product.imageproduct = (Byte[])fila["imageprod"];
					}


                    q_Conect.ClosedDB();
                    return e_Product;
                }
                else
                {
                    q_Conect.ClosedDB();
                    return null;
                }

            }
            return null;
        }

		public E_Product SearchFiltro(E_Product e_Product)
		{
			Q_Conect q_Conect = new Q_Conect();
			DataRow fila;
			SqlDataAdapter da = new SqlDataAdapter();

			//ROV 29/08/18 Se relaiza conexion a la base de datos 
			if (q_Conect.OpenDB())
			{
				//ROV 29/08/18 Se realiza la consulta de buscar la forma correcta 
				da = q_Conect.Build_Adapter("Select * from Product where sNameProduct like %@sNameProduct%");

				//ROV 29/08/18 Se realiza la validacion de parametros
				da.SelectCommand.Parameters.Add(new SqlParameter("@sNameProduct", SqlDbType.VarChar));
				da.SelectCommand.Parameters["@sNameProduct"].Value = e_Product.sUPC;


				fila = WievTable(da);

				//ROV 29/08/18 Se valida que hay datos en la tabla 
				if (fila != null)
				{
					//ROV 29/08/18 Se inserta los datos en cada entidad generada 
					e_Product.iIdProduct = Convert.ToInt32(fila["IdProduct"]);
					e_Product.Description = fila["sNameProduct"].ToString();
					e_Product.iIdDepartament = Convert.ToInt32(fila["iIdDepartment"]);
					e_Product.sUPC = fila["sUPC"].ToString();
					e_Product.sUPCStore = fila["sUPCStore"].ToString();
					e_Product.dPurchasePrice = Convert.ToDecimal(fila["dPurchasePrice"]);
					e_Product.dSalesPrice= Convert.ToDecimal(fila["dSalesPrice"]);
					e_Product.dStock = Convert.ToDecimal(fila["dStock"]);

					q_Conect.ClosedDB();
					return e_Product;
				}
				else
				{
					q_Conect.ClosedDB();
					return null;
				}
				
			}
			return null;
		}

		public DataTable SearchClave(string Word)
		{
			Q_Conect q_Conect = new Q_Conect();
			SqlCommand comand;
			DataTable dt = new DataTable();

			//ROV 29/08/18 Se realiza conexion a la base de datos 
			if (q_Conect.OpenDB())
			{
				//ROV 29/08/18 Se realiza la cnosulta de la base de datos 
				comand = q_Conect.Build_Command("Select sNameProduct as 'Nombre del producto', sUPC as 'Código de barras', dSalesPrice as 'Precio de venta' from Product where sNameProduct like '%" + Word +"%'");

				comand.Parameters.Add(new SqlParameter("@clave", SqlDbType.VarChar));
				comand.Parameters["@clave"].Value = Word;

				SqlDataAdapter da = new SqlDataAdapter(comand);

				//ROV 29/08/18 Se realiza el llenado del datatable con la lista 
				da.Fill(dt);
				return dt;
			}
			q_Conect.ClosedDB();
			return null;
		}

		//ROV 6/08/18: Como extrar una tabla para poder visualizarla en los controles
		public DataRow WievTable(SqlDataAdapter adapter)
		{
			DataSet ds;
			DataRow fila;

			ds = new DataSet();

			try
			{
				adapter.Fill(ds, "Product");
				//Se crea un objeto DataTable para uso de su coleccíon 
				//DataRow para extraer el registro a actualizar
				DataTable miTabla = ds.Tables["Product"];

				//Se almacena el registro en un objeto DataRow
				fila = miTabla.Rows[0];
				return (fila);
			}

			catch //(Exception oEx)
			{

				return (null);
			}
		}
        
        //ROV 29/08/18 funcion para validar la existencia de un producto a la base de datos 
        public bool ValidateRegistre(E_Product e_Product) 
        {
            Q_Conect q_Conect = new Q_Conect();
            SqlDataReader dr;
            SqlCommand comand = new SqlCommand();
            
            //ROV 29/08/18 Se realiza la conexion a la base de datos 
            if (q_Conect.OpenDB())
            {
                //ROV 29/08/18 se ejecuta el comando donde hace el conteo de las veces que se inserto un producto 
                comand = q_Conect.Build_Command("select count (*) from Product where sUPC = @sUPC and sUPCstore = @sUPCstore");

                //ROV 29/08/18 Se valida los parametros 
                comand.Parameters.Add(new SqlParameter("@sUPC", SqlDbType.VarChar));
                comand.Parameters["@sUPC"].Value = e_Product.sUPC;

                comand.Parameters.Add(new SqlParameter("@sUPCstore", SqlDbType.VarChar));
                comand.Parameters["@sUPCstore"].Value = e_Product.sUPCStore;

                //ROV 29/08/18 Se ejecuta la lectura del comando 
                dr = comand.ExecuteReader();

                //ROV 29/08/18 Se realiza la lectura
                dr.Read();

                //ROV 29/08/18 Se valida que hay registro 
                if (Convert.ToInt32(dr[0]) == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;

        }
    }
}
