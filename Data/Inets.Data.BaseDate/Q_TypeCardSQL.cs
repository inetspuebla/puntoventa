﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;
using Inets.Models.Entity_Entities;

namespace Inets.Data.BaseDate
{
	public class Q_TypeCardSQL
	{
		
		public DataTable Visualize()
		{
			Q_Conect q_Conect = new Q_Conect();
			SqlCommand comand;
			DataTable dt = new DataTable();

			//ROV 29/08/18 Se realiza conexion a la base de datos 
			if (q_Conect.OpenDB())
			{
				//ROV 29/08/18 Se realiza la cnosulta de la base de datos 
				comand = q_Conect.Build_Command("Select * from TypeCard");
				SqlDataAdapter da = new SqlDataAdapter(comand);

				//ROV 29/08/18 Se realiza el llenado del datatable con la lista 
				da.Fill(dt);
				return dt;
			}
			q_Conect.ClosedDB();
			return null;
		}

		public E_TypeCard Search(E_TypeCard e_TypeCard)
		{
			Q_Conect q_Conect = new Q_Conect();
			DataRow fila;
			SqlDataAdapter da = new SqlDataAdapter();

			//ROV 29/08/18 Se relaiza conexion a la base de datos 
			if (q_Conect.OpenDB())
			{
				//ROV 29/08/18 Se realiza la consulta de buscar la forma correcta 
				da = q_Conect.Build_Adapter("Select * from TypeCard where sNameTCard = @sNameTCard ;");

				//ROV 29/08/18 Se realiza la validacion de parametros
				da.SelectCommand.Parameters.Add(new SqlParameter("@sNameTCard", SqlDbType.VarChar));
				da.SelectCommand.Parameters["@sNameTCard"].Value = e_TypeCard.sNameTCard;

				fila = WievTable(da);

				//ROV 29/08/18 Se valida que hay datos en la tabla 
				if (fila != null)
				{
					//ROV 29/08/18 Se inserta los datos en cada entidad generada 
					e_TypeCard.idTCard = Convert.ToInt32(fila["idTCard"]);
					e_TypeCard.sNameTCard = fila["sNameTCard"].ToString();

					q_Conect.ClosedDB();
					return e_TypeCard;
				}
				else

					q_Conect.ClosedDB();
				return null;
			}
			return null;
		}

		public DataRow WievTable(SqlDataAdapter adapter)
		{
			DataSet ds;
			DataRow fila;

			ds = new DataSet();

			try
			{
				adapter.Fill(ds, "TypeCard");
				//Se crea un objeto DataTable para uso de su coleccíon 
				//DataRow para extraer el registro a actualizar
				DataTable miTabla = ds.Tables["TypeCard"];

				//Se almacena el registro en un objeto DataRow
				fila = miTabla.Rows[0];
				return (fila);
			}

			catch //(Exception oEx)
			{

				return (null);
			}
		}

	}
}
