﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;

using Inets.Models.Entity_Entities;

namespace Inets.Data.BaseDate
{
    public class Q_DepartmentSQL
    {
        //29/08/18 Funcion para mostrar Depto.
        public DataTable SelectDpto()
        {
            Q_Conect q_Conect = new Q_Conect();

            SqlCommand comand;

            DataTable dt = new DataTable();

            if (q_Conect.OpenDB())
            {
                comand = q_Conect.Build_Command("Select sNameDepartment as 'Nombre de Departamento', iNumberDepartment as 'Numero de Departamento' from Department order by iNumberDepartment asc;");
                SqlDataAdapter da = new SqlDataAdapter(comand);
                da.Fill(dt);
                return dt;
            }
            q_Conect.ClosedDB();
            return null;

        }

        //29/08/18 Funcion para mostrar la tabala en un combobox o en el datagridview
        public E_Departament SearchDepartament(E_Departament e_Departament)
        {
            Q_Conect q_Conect = new Q_Conect();
            DataRow fila;
            SqlDataAdapter da = new SqlDataAdapter();

            if (q_Conect.OpenDB())
            {
                da = q_Conect.Build_Adapter("Select * from Department where sNameDepartment = @sNameDepartment or iIdDepartment=@iIdDepartment ");

                da.SelectCommand.Parameters.Add(new SqlParameter("@sNameDepartment", SqlDbType.VarChar));
                da.SelectCommand.Parameters["@sNameDepartment"].Value = e_Departament.sNameDepartment;

                da.SelectCommand.Parameters.Add(new SqlParameter("@iIdDepartment", SqlDbType.Int));
                da.SelectCommand.Parameters["@iIdDepartment"].Value = e_Departament.iIdDepartment;

                fila = WievTable(da);

                    q_Conect.ClosedDB();
                if (fila != null)
                {
                    e_Departament.iIdDepartment = Convert.ToInt16(fila["iIdDepartment"]);
                    e_Departament.sNameDepartment = fila["sNameDepartment"].ToString();
                    e_Departament.iNumberDepartament = Convert.ToInt32(fila["iNumberDepartment"]);

                    return e_Departament;
                }
                else
                {

                    return null;


                }

            }
            return null;

        }

        //ROV 29/08/18: Como extrar una tabla para poder visualizarla en los controles
        public DataRow WievTable(SqlDataAdapter adapter)
        {
            DataSet ds;
            DataRow fila;

            ds = new DataSet();

            try
            {
                adapter.Fill(ds, "Departament");
                //Se crea un objeto DataTable para uso de su coleccíon 
                //DataRow para extraer el registro a actualizar
                DataTable miTabla = ds.Tables["Departament"];

                //Se almacena el registro en un objeto DataRow
                fila = miTabla.Rows[0];
                return (fila);
            }

            catch //(Exception oEx)
            {

                return (null);
            }
        }

        //ROV 30/08/18 Funcion para validar que no exista departamentos iguales
        public bool ValidateRegistre(E_Departament e_Departament)
        {
            Q_Conect q_Conect = new Q_Conect();
            SqlDataReader dr;
            SqlCommand comand = new SqlCommand();

            //ROV 29/08/18 Se realiza la conexion a la base de datos 
            if (q_Conect.OpenDB())
            {
                //ROV 29/08/18 se ejecuta el comando donde hace el conteo de las veces que se inserto un producto 
                comand = q_Conect.Build_Command("select count (*) from Department where sNameDepartment = @sNameDepartment and iNumberDepartment = @iNumberDepartment");

                //ROV 29/08/18 Se valida los parametros 
                comand.Parameters.Add(new SqlParameter("@sNameDepartment", SqlDbType.VarChar));
                comand.Parameters["@sNameDepartment"].Value = e_Departament.sNameDepartment;

                comand.Parameters.Add(new SqlParameter("@iNumberDepartment", SqlDbType.VarChar));
                comand.Parameters["@iNumberDepartment"].Value = e_Departament.iNumberDepartament;

                //ROV 29/08/18 Se ejecuta la lectura del comando 
                dr = comand.ExecuteReader();

                //ROV 29/08/18 Se realiza la lectura
                dr.Read();

                //ROV 29/08/18 Se valida que hay registro 
                if (Convert.ToInt32(dr[0]) == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;

        }
    }
}
