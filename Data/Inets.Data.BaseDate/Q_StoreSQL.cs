﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;

using Inets.Models.Entity_Entities;

namespace Inets.Data.BaseDate
{
    public class Q_StoreSQL 
    {
        E_Store e_Store = new E_Store();
		
        public E_Store Search(E_Store e_Store)
        {
            Q_Conect q_Conect = new Q_Conect();
            DataRow fila;
            SqlDataAdapter da = new SqlDataAdapter();

            //ROV 29/08/18 Se relaiza conexion a la base de datos 
            if (q_Conect.OpenDB())
            {
                //ROV 29/08/18 Se realiza la consulta de buscar la forma correcta 
                da = q_Conect.Build_Adapter("Select * from Store where iIdStore = @iIdStore;");

                //ROV 29/08/18 Se realiza la validacion de parametros
                da.SelectCommand.Parameters.Add(new SqlParameter("@iIdStore", SqlDbType.VarChar));
                da.SelectCommand.Parameters["@iIdStore"].Value = e_Store.iIdStore;
                
                fila = WievTable(da);

                //ROV 29/08/18 Se valida que hay datos en la tabla 
                if (fila != null)
                {
                    //ROV 29/08/18 Se inserta los datos en cada entidad generada 
                    
                    e_Store.iIdStore = Convert.ToInt32(fila["iIdStore"]);
                    e_Store.sNameStore = fila["sNameStore"].ToString();
                    e_Store.sTelephon = fila["Telephone"].ToString();
                    e_Store.sAddress = fila["sAddress"].ToString();
                    e_Store.sEmail = fila["Email"].ToString();
                    e_Store.sRFC = fila["sRFC"].ToString();

                    q_Conect.ClosedDB();
                    return e_Store;
                }
                else
                {
                    q_Conect.ClosedDB();
                    return null;
                }

            }
            return null;

        }

		public E_Store Search_Name(E_Store e_Store)
		{
			Q_Conect q_Conect = new Q_Conect();
			DataRow fila;
			SqlDataAdapter da = new SqlDataAdapter();

			//ROV 29/08/18 Se relaiza conexion a la base de datos 
			if (q_Conect.OpenDB())
			{
				//ROV 29/08/18 Se realiza la consulta de buscar la forma correcta 
				da = q_Conect.Build_Adapter("Select * from Store where sNameStore = @sNameStore;");

				//ROV 29/08/18 Se realiza la validacion de parametros
				da.SelectCommand.Parameters.Add(new SqlParameter("@sNameStore", SqlDbType.VarChar));
				da.SelectCommand.Parameters["@sNameStore"].Value = e_Store.sNameStore;

				fila = WievTable(da);

				//ROV 29/08/18 Se valida que hay datos en la tabla 
				if (fila != null)
				{
					//ROV 29/08/18 Se inserta los datos en cada entidad generada 

					e_Store.iIdStore = Convert.ToInt32(fila["iIdStore"]);
					e_Store.sNameStore = fila["sNameStore"].ToString();
					e_Store.sTelephon = fila["Telephone"].ToString();
					e_Store.sAddress = fila["sAddress"].ToString();
					e_Store.sEmail = fila["Email"].ToString();
					e_Store.sRFC = fila["sRFC"].ToString();

					q_Conect.ClosedDB();
					return e_Store;
				}
				else
				{
					q_Conect.ClosedDB();
					return null;
				}

			}
			return null;

		}
		
		public DataRow WievTable(SqlDataAdapter adapter)
        {
            DataSet ds;
            DataRow fila;

            ds = new DataSet();

            try
            {
                adapter.Fill(ds, "DetailTicket");
                //Se crea un objeto DataTable para uso de su coleccíon 
                //DataRow para extraer el registro a actualizar
                DataTable miTabla = ds.Tables["DetailTicket"];

                //Se almacena el registro en un objeto DataRow
                fila = miTabla.Rows[0];
                return (fila);
            }

            catch //(Exception oEx)
            {

                return (null);
            }
        }

    }
}
