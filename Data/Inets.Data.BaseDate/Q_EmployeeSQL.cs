﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;

using Inets.Models.Entity_Entities;

namespace Inets.Data.BaseDate
{
    public class Q_EmployeeSQL
    {
        public E_Employee SearchEmployee(E_Employee e_Employee)
        {
            Q_Conect q_Conect = new Q_Conect();
            DataRow fila;
            SqlDataAdapter da = new SqlDataAdapter();

            //ROV 29/08/18 Se relaiza conexion a la base de datos 
            if (q_Conect.OpenDB())
            {
                //ROV 29/08/18 Se realiza la consulta de buscar la forma correcta 
                da = q_Conect.Build_Adapter("Select * from Employees where iIUsers = @iIUsers ");

                //ROV 29/08/18 Se realiza la validacion de parametros
                da.SelectCommand.Parameters.Add(new SqlParameter("@iIUsers", SqlDbType.Int));
                da.SelectCommand.Parameters["@iIUsers"].Value = e_Employee.iUsers;
                
                fila = WievTable(da);

                //ROV 29/08/18 Se valida que hay datos en la tabla 
                if (fila != null)
                {
                    //ROV 29/08/18 Se inserta los datos en cada entidad generada 
                    e_Employee.iUsers = Convert.ToInt32(fila["iIUsers"]);
                    e_Employee.sNameEmployee = fila["sNameEmployee"].ToString();
                    e_Employee.sLastName = fila["sLastName"].ToString();
                    e_Employee.sGener = fila["sGener"].ToString();
                    e_Employee.sBirthday = fila["dBirthday"].ToString();
                    e_Employee.sAddress = fila["sAddress"].ToString();
                    e_Employee.iZipcode = Convert.ToInt32(fila["iZipcode"]);
                    e_Employee.sEmail = fila["sEmail"].ToString();
                    e_Employee.Telephone = fila["sTelephone"].ToString();
                    e_Employee.sRFC = fila["sRFC"].ToString();

                    q_Conect.ClosedDB();
                    return e_Employee;
                }
                else
                {
                    q_Conect.ClosedDB();
                    return null;
                }

            }
            return null;
        }

		public E_Employee Search2(E_Employee e_Employee)
		{
			Q_Conect q_Conect = new Q_Conect();
			DataRow fila;
			SqlDataAdapter da = new SqlDataAdapter();

			//ROV 29/08/18 Se relaiza conexion a la base de datos 
			if (q_Conect.OpenDB())
			{
				//ROV 29/08/18 Se realiza la consulta de buscar la forma correcta 
				da = q_Conect.Build_Adapter("Select * from Employees where sNameEmployee = @sNameEmployee ");

				//ROV 29/08/18 Se realiza la validacion de parametros
				da.SelectCommand.Parameters.Add(new SqlParameter("@sNameEmployee", SqlDbType.Int));
				da.SelectCommand.Parameters["@sNameEmployee"].Value = e_Employee.sNameEmployee;

				fila = WievTable(da);

				//ROV 29/08/18 Se valida que hay datos en la tabla 
				if (fila != null)
				{
					//ROV 29/08/18 Se inserta los datos en cada entidad generada 
					e_Employee.iUsers = Convert.ToInt32(fila["iIUsers"]);
					e_Employee.sNameEmployee = fila["sNameEmployee"].ToString();
					e_Employee.sLastName = fila["sLastName"].ToString();
					e_Employee.sGener = fila["sGener"].ToString();
					e_Employee.sBirthday = fila["dBirthday"].ToString();
					e_Employee.sAddress = fila["sAddress"].ToString();
					e_Employee.iZipcode = Convert.ToInt32(fila["iZipcode"]);
					e_Employee.sEmail = fila["sEmail"].ToString();
					e_Employee.Telephone = fila["sTelephone"].ToString();
					e_Employee.sRFC = fila["sRFC"].ToString();

					q_Conect.ClosedDB();
					return e_Employee;
				}
				else
				{
					q_Conect.ClosedDB();
					return null;
				}

			}
			return null;
		}


		public DataRow WievTable(SqlDataAdapter adapter)
        {
            DataSet ds;
            DataRow fila;

            ds = new DataSet();

            try
            {
                adapter.Fill(ds, "Employee");
                //Se crea un objeto DataTable para uso de su coleccíon 
                //DataRow para extraer el registro a actualizar
                DataTable miTabla = ds.Tables["Employee"];

                //Se almacena el registro en un objeto DataRow
                fila = miTabla.Rows[0];
                return (fila);
            }

            catch //(Exception oEx)
            {

                return (null);
            }
        }
    }
}
