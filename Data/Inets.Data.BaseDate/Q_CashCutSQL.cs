﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;
using System.Data;
using Inets.Models.Entity_Entities;

namespace Inets.Data.BaseDate
{
    public class Q_CashCutSQL
    {
        public DataTable VisualizeTable(E_CashCut e_CashCut)
        {
            Q_Conect q_Conect = new Q_Conect();
            SqlCommand comand;
            DataTable dt = new DataTable();

            //ROV 29/08/18 Se realiza conexion a la base de datos 
            if (q_Conect.OpenDB())
            {
                //ROV 29/08/18 Se realiza la cnosulta de la base de datos 
                comand = q_Conect.Build_Command("Select iIdCashCut  as'Numero de Corte', iIdCashSales as 'Numero de caja', sDatecut as 'Fecha de Corte', dBalanceInitial as 'Saldo Inicial', dBalanceFinal as 'Saldo Final' from CashCut "+
                    "where iIdCashSales = @iIdCashSales ; ");

                comand.Parameters.Add(new SqlParameter("@iIdCashSales ", SqlDbType.Int));
                comand.Parameters["@iIdCashSales "].Value = e_CashCut.iIdCashSales;

                SqlDataAdapter da = new SqlDataAdapter(comand);

                //ROV 29/08/18 Se realiza el llenado del datatable con la lista 
                da.Fill(dt);
                return dt;
            }
            q_Conect.ClosedDB();
            return null;

        }

        public DataRow WievTable(SqlDataAdapter adapter)
        {
            DataSet ds;
            DataRow fila;

            ds = new DataSet();

            try
            {
                adapter.Fill(ds, "CashCut");
                //Se crea un objeto DataTable para uso de su coleccíon 
                //DataRow para extraer el registro a actualizar
                DataTable miTabla = ds.Tables["CashCut"];

                //Se almacena el registro en un objeto DataRow
                fila = miTabla.Rows[0];
                return (fila);
            }

            catch //(Exception oEx)
            {

                return (null);
            }
        }

    }
}
