﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;
using Inets.Models.Entity_Entities;

namespace Inets.Data.BaseDate
{
    public class Q_InventorySQL
    {
        
        public E_Inventory SearchInventory(E_Inventory e_Inventory)
        {
            Q_Conect q_Conect = new Q_Conect();
            DataRow fila;
            SqlDataAdapter da = new SqlDataAdapter();

            if(q_Conect.OpenDB())
            {
                da = q_Conect.Build_Adapter(" ");

                da.SelectCommand.Parameters.Add(new SqlParameter());
                da.SelectCommand.Parameters[" "].Value = e_Inventory;

                fila = WievTable(da);
                
                if(fila != null)
                {
                    e_Inventory.iIdInventary = Convert.ToInt32(fila["iIdInventory"]);
                    e_Inventory.iIdProduct = Convert.ToInt32(fila["iIdProduct"]);
                    e_Inventory.iIdUnityMeasure = Convert.ToInt32( fila["iIdUnityMeasure"]);
                    e_Inventory.iUnity = Convert.ToInt32(fila["iUnity"]);

                    q_Conect.ClosedDB();
                    return e_Inventory;
                }
                else
                {
                    q_Conect.ClosedDB();
                    return null;
                }

            }
            return null;
        }

        public DataRow WievTable(SqlDataAdapter adapter)
        {
            DataSet ds;
            DataRow fila;

            ds = new DataSet();

            try
            {
                adapter.Fill(ds, "Inventory");
                //Se crea un objeto DataTable para uso de su coleccíon 
                //DataRow para extraer el registro a actualizar
                DataTable miTabla = ds.Tables["Inventory"];

                //Se almacena el registro en un objeto DataRow
                fila = miTabla.Rows[0];
                return (fila);
            }

            catch //(Exception oEx)
            {

                return (null);
            }
        }

    }
}
