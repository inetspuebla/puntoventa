﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;
using System.Data;
using Inets.Models.Entity_Entities;
using Inets.Data.BaseDate;

namespace Inets.Data.BaseDate
{
    public class Q_LoginSQL
    {
        public bool StartSesion(E_Users e_Users)//iniciar sesion
        {
            Q_Conect q_Conect = new Q_Conect();
            SqlCommand command;
            SqlDataReader dr;

            if (q_Conect.OpenDB())
            {
                command = q_Conect.Build_Command("Select count(*) from Users where sNickname = @sNickname and sPassword = @sPassword;");

                command.Parameters.Add(new SqlParameter("@sNickname", SqlDbType.VarChar ));
                command.Parameters["@sNickname"].Value = e_Users.sNickname;

                command.Parameters.Add(new SqlParameter("@sPassword", SqlDbType.VarChar));
                command.Parameters["@sPassword"].Value = e_Users.sPassword;

                dr = command.ExecuteReader();

                dr.Read();

                if (Convert.ToInt32(dr[0]) == 1)
                {
                    return true;

                }
                else
                {
                    return false;
                }
            }
            return false;
        }

		public bool Validation (E_Users e_Users)//iniciar sesion
		{
			Q_Conect q_Conect = new Q_Conect();
			SqlCommand command;
			SqlDataReader dr;

			if (q_Conect.OpenDB())
			{
				command = q_Conect.Build_Command("Select  count(*) from Users, Profiles where Users.iIdProfile= Profiles.iIdProfile" +
					" and Profiles.sProfileName = 'Administrador' and sNickname = @sNickname and sPassword = @sPassword;");

				command.Parameters.Add(new SqlParameter("@sNickname", SqlDbType.VarChar));
				command.Parameters["@sNickname"].Value = e_Users.sNickname;

				command.Parameters.Add(new SqlParameter("@sPassword", SqlDbType.VarChar));
				command.Parameters["@sPassword"].Value = e_Users.sPassword;

				dr = command.ExecuteReader();

				dr.Read();

				if (Convert.ToInt32(dr[0]) == 1)
				{
					return true;

				}
				else
				{
					return false;
				}
			}
			return false;
		}
		
		public E_Users SearchUserlogin(E_Users e_Users)
		{
			Q_Conect q_Conect = new Q_Conect();
			DataRow fila;
			SqlDataAdapter da = new SqlDataAdapter();

			//ROV 29/08/18 Se relaiza conexion a la base de datos 
			if (q_Conect.OpenDB())
			{
				//ROV 29/08/18 Se realiza la consulta de buscar la forma correcta 
				da = q_Conect.Build_Adapter("Select * from Users where sNickname = @sNickname and sPassword = @sPassword ");

				//ROV 29/08/18 Se realiza la validacion de parametros
				da.SelectCommand.Parameters.Add(new SqlParameter("@sNickname", SqlDbType.VarChar));
				da.SelectCommand.Parameters["@sNickname"].Value = e_Users.sNickname;

				da.SelectCommand.Parameters.Add(new SqlParameter("@sPassword", SqlDbType.VarChar));
				da.SelectCommand.Parameters["@sPassword"].Value = e_Users.sPassword;

				fila = WievTable(da);

				//ROV 29/08/18 Se valida que hay datos en la tabla 
				if (fila != null)
				{
					//ROV 29/08/18 Se inserta los datos en cada entidad generada 
					e_Users.iIdUsers = Convert.ToInt32(fila["iIdUsers"]);
					e_Users.sNickname = fila["sNickname"].ToString(); ;
					e_Users.sPassword = fila["sPassword"].ToString(); ;
					e_Users.iIdProfile = Convert.ToInt32(fila["iIdProfile"]);

					q_Conect.ClosedDB();
					return e_Users;
				}
				else
				{
					q_Conect.ClosedDB();
					return null;
				}

			}
			return null;
		}

		public E_Users SearchUser(E_Users e_Users)
		{
			Q_Conect q_Conect = new Q_Conect();
			DataRow fila;
			SqlDataAdapter da = new SqlDataAdapter();

			//ROV 29/08/18 Se relaiza conexion a la base de datos 
			if (q_Conect.OpenDB())
			{
				//ROV 29/08/18 Se realiza la consulta de buscar la forma correcta 
				da = q_Conect.Build_Adapter("Select * from Users where sNickname = @sNickname ");

				//ROV 29/08/18 Se realiza la validacion de parametros
				da.SelectCommand.Parameters.Add(new SqlParameter("@sNickname", SqlDbType.VarChar));
				da.SelectCommand.Parameters["@sNickname"].Value = e_Users.sNickname;

				fila = WievTable(da);

				//ROV 29/08/18 Se valida que hay datos en la tabla 
				if (fila != null)
				{
					//ROV 29/08/18 Se inserta los datos en cada entidad generada 
					e_Users.iIdUsers = Convert.ToInt32(fila["iIdUsers"]);
					e_Users.sNickname = fila["sNickname"].ToString(); ;
					e_Users.sPassword = fila["sPassword"].ToString(); ;
					e_Users.iIdProfile = Convert.ToInt32(fila["iIdProfile"]);

					q_Conect.ClosedDB();
					return e_Users;
				}
				else
				{
					q_Conect.ClosedDB();
					return null;
				}

			}
			return null;
		}

		public DataRow WievTable(SqlDataAdapter adapter)
		{
			DataSet ds;
			DataRow fila;

			ds = new DataSet();

			try
			{
				adapter.Fill(ds, "Users");
				//Se crea un objeto DataTable para uso de su coleccíon 
				//DataRow para extraer el registro a actualizar
				DataTable miTabla = ds.Tables["Users"];

				//Se almacena el registro en un objeto DataRow
				fila = miTabla.Rows[0];
				return (fila);
			}

			catch //(Exception oEx)
			{

				return (null);
			}
		}

	}

}
