﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;
using Inets.Models.Entity_Entities;

namespace Inets.Data.BaseDate
{
	public class Q_TicketSQL
	{
		
		public E_Ticket Search_ID(E_Ticket e_Ticket)
		{
			Q_Conect q_Conect = new Q_Conect();
			DataRow fila;
			SqlDataAdapter da = new SqlDataAdapter();

			//ROV 29/08/18 Se relaiza conexion a la base de datos 
			if (q_Conect.OpenDB())
			{
				//ROV 29/08/18 Se realiza la consulta de buscar la forma correcta 
				da = q_Conect.Build_Adapter("Select * from Ticket where iIdTicket = @iIdTicket;");

				//ROV 29/08/18 Se realiza la validacion de parametros
				da.SelectCommand.Parameters.Add(new SqlParameter("@iIdTicket", SqlDbType.VarChar));
				da.SelectCommand.Parameters["@iIdTicket"].Value = e_Ticket.iIdTicket;

				fila = WievTable(da);

				//ROV 29/08/18 Se valida que hay datos en la tabla 
				if (fila != null)
				{
					//ROV 29/08/18 Se inserta los datos en cada entidad generada 

					e_Ticket.iIdTicket = Convert.ToInt32(fila["iIdTicket"]);
					e_Ticket.iIdCashSales = Convert.ToInt32(fila["iIdCashSales"]);
					e_Ticket.NumTicket = fila["sNumerticket"].ToString();
					e_Ticket.sDateTicket = fila["sDateticket"].ToString();
					q_Conect.ClosedDB();
					return e_Ticket;
				}
				else
				{
					q_Conect.ClosedDB();
					return null;
				}
			}
			return null;
		}
		
		public E_Ticket Search_NT(E_Ticket e_Ticket)
		{
			Q_Conect q_Conect = new Q_Conect();
			DataRow fila;
			SqlDataAdapter da = new SqlDataAdapter();

			//ROV 29/08/18 Se relaiza conexion a la base de datos 
			if (q_Conect.OpenDB())
			{
				//ROV 29/08/18 Se realiza la consulta de buscar la forma correcta 
				da = q_Conect.Build_Adapter("Select top 1 * from Ticket where sNumerticket = @sNumerticket order by iIdTicket desc;");

				//ROV 29/08/18 Se realiza la validacion de parametros
				da.SelectCommand.Parameters.Add(new SqlParameter("@sNumerticket", SqlDbType.VarChar));
				da.SelectCommand.Parameters["@sNumerticket"].Value = e_Ticket.NumTicket;

				fila = WievTable(da);

				//ROV 29/08/18 Se valida que hay datos en la tabla 
				if (fila != null)
				{
					//ROV 29/08/18 Se inserta los datos en cada entidad generada 

					e_Ticket.iIdTicket = Convert.ToInt32(fila["iIdTicket"]);
					e_Ticket.iIdCashSales = Convert.ToInt32(fila["iIdCashSales"]);
					e_Ticket.NumTicket = fila["sNumerticket"].ToString();
					e_Ticket.sDateTicket = fila["sDateticket"].ToString();
					q_Conect.ClosedDB();
					return e_Ticket;
				}
				else
				{
					q_Conect.ClosedDB();
					return null;
				}
			}
			return null;
		}

		public E_Ticket Seach_DT(E_Ticket e_Ticket)
		{
			Q_Conect q_Conect = new Q_Conect();
			DataRow fila;
			SqlDataAdapter da = new SqlDataAdapter();

			//ROV 29/08/18 Se relaiza conexion a la base de datos 
			if (q_Conect.OpenDB())
			{
				//ROV 29/08/18 Se realiza la consulta de buscar la forma correcta 
				da = q_Conect.Build_Adapter("Select top 1 * from Ticket where sDateticket = @sDateticket order by iIdTicket desc;");

				//ROV 29/08/18 Se realiza la validacion de parametros
				da.SelectCommand.Parameters.Add(new SqlParameter("@sDateticket", SqlDbType.VarChar));
				da.SelectCommand.Parameters["@sDateticket"].Value = e_Ticket.sDateTicket;

				fila = WievTable(da);

				//ROV 29/08/18 Se valida que hay datos en la tabla 
				if (fila != null)
				{
					//ROV 29/08/18 Se inserta los datos en cada entidad generada 

					e_Ticket.iIdTicket = Convert.ToInt32(fila["iIdTicket"]);
					e_Ticket.iIdCashSales = Convert.ToInt32(fila["iIdCashSales"]);
					e_Ticket.NumTicket = fila["sNumerticket"].ToString();
					e_Ticket.sDateTicket = fila["sDateticket"].ToString();
					q_Conect.ClosedDB();
					return e_Ticket;
				}
				else
				{
					q_Conect.ClosedDB();
					return null;
				}
			}
			return null;
		}

		public E_Ticket Search_UT()
		{
			E_Ticket e_Ticket = new E_Ticket();
			Q_Conect q_Conect = new Q_Conect();
			DataRow fila;
			SqlDataAdapter da = new SqlDataAdapter();

			//ROV 29/08/18 Se relaiza conexion a la base de datos 
			if (q_Conect.OpenDB())
			{
				//ROV 29/08/18 Se realiza la consulta de buscar la forma correcta 
				da = q_Conect.Build_Adapter("Select top 1 * from ticket where DATEPART(YEAR,sDateticket)= @year and DATEPART(MONTH,sDateticket)= @month and DATEPART(DAY,sDateticket)= @day order by sDateticket desc;");

				//ROV 29/08/18 Se realiza la validacion de parametros
				da.SelectCommand.Parameters.Add(new SqlParameter("@year", SqlDbType.VarChar));
				da.SelectCommand.Parameters["@year"].Value = DateTime.Now.Year;

				da.SelectCommand.Parameters.Add(new SqlParameter("@month", SqlDbType.VarChar));
				da.SelectCommand.Parameters["@month"].Value = DateTime.Now.Month;

				da.SelectCommand.Parameters.Add(new SqlParameter("@day", SqlDbType.VarChar));
				da.SelectCommand.Parameters["@day"].Value = DateTime.Now.Day;


				fila = WievTable(da);

				//ROV 29/08/18 Se valida que hay datos en la tabla 
				if (fila != null)
				{
					//ROV 29/08/18 Se inserta los datos en cada entidad generada 

					e_Ticket.iIdTicket = Convert.ToInt32(fila["iIdTicket"]);
					e_Ticket.iIdCashSales = Convert.ToInt32(fila["iIdCashSales"]);
					e_Ticket.NumTicket = fila["sNumerticket"].ToString();
					e_Ticket.sDateTicket = fila["sDateticket"].ToString();
					q_Conect.ClosedDB();
					return e_Ticket;
				}
				else
				{
					q_Conect.ClosedDB();
					return null;
				}
			}
			return null;
		}

		public DataRow WievTable(SqlDataAdapter adapter)
		{
			DataSet ds;
			DataRow fila;

			ds = new DataSet();

			try
			{
				adapter.Fill(ds, "Ticket");
				//Se crea un objeto DataTable para uso de su coleccíon 
				//DataRow para extraer el registro a actualizar
				DataTable miTabla = ds.Tables["Ticket"];

				//Se almacena el registro en un objeto DataRow
				fila = miTabla.Rows[0];
				return (fila);
			}

			catch //(Exception oEx)
			{

				return (null);
			}
		}
		
    }
}
